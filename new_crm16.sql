-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 16, 2022 at 01:35 PM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 7.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `new_crm`
--

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `first_name` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `address1` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address2` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address3` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `landmark` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `area_city` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pincode` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `landline` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_id` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `skype_id` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pancard` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gstin` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`id`, `first_name`, `last_name`, `date_of_birth`, `address1`, `address2`, `address3`, `landmark`, `area_city`, `pincode`, `state`, `country`, `mobile`, `landline`, `email_id`, `skype_id`, `pancard`, `gstin`, `status`, `created_at`, `updated_at`) VALUES
(1, 'vishal lad', 'lad', '1994-11-20', '111', 'harapoer', 'navsari', 'eru. char rasta', 'nasvari', '396445', 'Gujarat', 'India', '987654321', '026375555', 'vishal.lad@neshallweb.com', 'vishal@skype', '21323454763456', '542346545', 'pending', '2022-02-16 00:37:52', '2022-02-16 02:54:22'),
(2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2022-02-16 02:53:03', '2022-02-16 02:53:03'),
(3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2022-02-16 06:22:23', '2022-02-16 06:22:23'),
(4, 'vishal', 'lad', '1994-11-20', '102', 'mogravadi', 'power house', 'tower', 'valsad', '396001', 'Gujarat', 'India', '8239939277', '2626', 'vishal.lad@neshallweb.com', 'skype@id', '1234567890', '0987654321', 'pending', '2022-02-16 06:24:27', '2022-02-16 06:27:00');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `main_services`
--

CREATE TABLE `main_services` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `main_services`
--

INSERT INTO `main_services` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Graphics Design', NULL, NULL, '2022-02-11 06:35:50'),
(2, 'Video Editing', NULL, '2022-02-11 06:29:12', '2022-02-11 06:29:12'),
(4, 'Web Developer', NULL, '2022-02-11 07:01:28', '2022-02-11 07:01:28');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2022_02_04_041505_create_nw_main_categories_table', 1),
(6, '2022_02_04_041557_create_nw_features_table', 1),
(7, '2022_02_04_041619_create_nw_features_list_table', 1),
(8, '2022_02_10_105325_create_clients_table', 2),
(9, '2022_02_11_093119_create_main_services_table', 3),
(10, '2022_02_11_121410_create_service_table', 4),
(11, '2022_02_12_072709_create_question_models_table', 5),
(12, '2022_02_12_085737_create_question_answer_table', 6);

-- --------------------------------------------------------

--
-- Table structure for table `nw_features`
--

CREATE TABLE `nw_features` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `feature_title` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `service_id` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `nw_features`
--

INSERT INTO `nw_features` (`id`, `feature_title`, `service_id`, `created_at`, `updated_at`) VALUES
(2, 'Web Speed & Performance', '1', '2022-02-04 01:22:46', '2022-02-04 03:05:19'),
(3, 'Web Appearance & Graphics', '1', '2022-02-04 03:06:17', '2022-02-04 03:06:17'),
(4, 'Features & Elements', '1', '2022-02-04 03:06:34', '2022-02-04 03:06:34'),
(5, 'Requirements & Support', '1', '2022-02-04 03:06:43', '2022-02-04 03:06:43');

-- --------------------------------------------------------

--
-- Table structure for table `nw_features_list`
--

CREATE TABLE `nw_features_list` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `feature_list` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `feature_id` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `nw_features_list`
--

INSERT INTO `nw_features_list` (`id`, `feature_list`, `feature_id`, `created_at`, `updated_at`) VALUES
(1, 'Light weight theme', '2', '2022-02-04 02:59:15', '2022-02-04 03:07:23'),
(2, 'Optimized plugins', '2', '2022-02-04 03:07:33', '2022-02-04 03:07:33'),
(3, 'Optimized color schemes', '3', '2022-02-04 03:07:42', '2022-02-04 03:07:42'),
(4, 'Stock images', '3', '2022-02-04 03:07:51', '2022-02-04 03:07:51'),
(5, 'Regular icons', '3', '2022-02-04 03:08:01', '2022-02-04 03:08:01'),
(6, 'Attractive website theme', '3', '2022-02-04 03:08:10', '2022-02-04 03:08:10'),
(7, 'Mobile responsive', '4', '2022-02-04 03:08:21', '2022-02-04 03:08:21'),
(8, 'Limited to Five webpages', '4', '2022-02-04 03:08:31', '2022-02-04 03:08:31'),
(9, 'User login, logout, forgot password capabilities', '4', '2022-02-04 03:08:42', '2022-02-04 03:08:42'),
(10, 'Transparent header', '4', '2022-02-04 03:08:53', '2022-02-04 03:08:53'),
(11, 'Media library', '4', '2022-02-04 03:09:01', '2022-02-04 03:09:01'),
(12, 'Video/YouTube links', '4', '2022-02-04 03:09:10', '2022-02-04 03:09:10'),
(13, 'Linked location on google map', '4', '2022-02-04 03:09:18', '2022-02-04 03:09:18'),
(14, 'Product/Service presentation page', '4', '2022-02-04 03:09:27', '2022-02-04 03:09:27'),
(15, '“Contact us” quick link', '4', '2022-02-04 03:09:35', '2022-02-04 03:09:35'),
(16, 'Feedback/Contact form', '4', '2022-02-04 03:09:45', '2022-02-04 03:09:45'),
(17, 'Five company email accounts (email@example.com)', '4', '2022-02-04 03:09:52', '2022-02-04 03:09:52'),
(18, 'SEO optimized website', '4', '2022-02-04 03:10:01', '2022-02-04 03:10:01'),
(19, 'SEO friendly links and URLs', '4', '2022-02-04 03:10:10', '2022-02-04 03:10:10'),
(20, 'Generating robot.txt for website', '4', '2022-02-04 03:10:18', '2022-02-04 03:10:18'),
(21, 'Generating XML sitemap of website', '4', '2022-02-04 03:10:29', '2022-02-04 03:10:29'),
(22, 'Generating HTML sitemap of website', '4', '2022-02-04 03:10:37', '2022-02-04 03:10:37'),
(23, 'Google search console setup', '4', '2022-02-04 03:10:50', '2022-02-04 03:10:50'),
(24, 'Google analytics setup', '4', '2022-02-04 03:11:00', '2022-02-04 03:11:00'),
(25, 'User interface will be designed to represent individual or business entity.', '5', '2022-02-04 03:11:08', '2022-02-04 03:16:40'),
(26, 'User experience will be targeted towards the feasibility of using website according to the user interest of knowing more about individual or business.', '5', '2022-02-04 03:11:19', '2022-02-04 03:16:51'),
(27, 'Free exclusive and proven business growth technique only for our clients.', '5', '2022-02-04 03:11:29', '2022-02-04 03:16:54'),
(28, 'The end goal will be to make user feel luxury of being visiting the website.', '5', '2022-02-04 03:11:45', '2022-02-04 03:11:45'),
(29, 'Training provided for the basic operation of website.', '5', '2022-02-04 03:11:53', '2022-02-04 03:11:53'),
(30, 'Call/text/email support during office hours', '5', '2022-02-04 03:12:02', '2022-02-04 03:12:02'),
(31, 'Shared hosting included for one year (redeemable)', '5', '2022-02-04 03:12:10', '2022-02-04 03:12:10'),
(32, 'Available common domain included for one year [For example .com, .in, .org, .io, etc.] (redeemable)', '5', '2022-02-04 03:12:17', '2022-02-04 03:12:17');

-- --------------------------------------------------------

--
-- Table structure for table `nw_main_categories`
--

CREATE TABLE `nw_main_categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `nw_main_categories`
--

INSERT INTO `nw_main_categories` (`id`, `title`, `created_at`, `updated_at`) VALUES
(2, 'Services/Specifications', '2022-02-04 00:14:23', '2022-02-04 00:14:23');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `question_answer`
--

CREATE TABLE `question_answer` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `questionid` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `answer` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `images` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `clientid` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mainserviceid` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `servicesid` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `question_answer`
--

INSERT INTO `question_answer` (`id`, `questionid`, `answer`, `images`, `clientid`, `mainserviceid`, `servicesid`, `created_at`, `updated_at`) VALUES
(1, NULL, NULL, '', '1', '4', '4', '2022-02-16 00:37:52', '2022-02-16 01:18:50'),
(2, NULL, NULL, '', '2', '4', '4', '2022-02-16 02:53:04', '2022-02-16 02:54:48'),
(3, NULL, NULL, NULL, '4', '4', '4', '2022-02-16 06:24:28', '2022-02-16 06:27:39'),
(4, '1', 'vishallllll', NULL, '4', NULL, '3', '2022-02-16 06:55:40', '2022-02-16 06:55:40'),
(5, '2', 'vishallllll123', NULL, '4', NULL, '3', '2022-02-16 06:56:35', '2022-02-16 06:56:35');

-- --------------------------------------------------------

--
-- Table structure for table `question_models`
--

CREATE TABLE `question_models` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `serviceid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `question` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `images` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `question_models`
--

INSERT INTO `question_models` (`id`, `serviceid`, `question`, `type`, `images`, `created_at`, `updated_at`) VALUES
(1, '4', 'Name of the start-up/company/product/service', '3', '[\"uploads\\/service_1644681476.png\",\"uploads\\/service_1644681476.png\",\"uploads\\/service_1644681476.jpg\"]', '2022-02-12 02:52:46', '2022-02-12 10:27:56'),
(2, '4', 'Name of the start-up/company/product/service111', '1', '[\"uploads\\/service_1644681476.png\",\"uploads\\/service_1644681476.png\",\"uploads\\/service_1644681476.jpg\"]', '2022-02-12 02:52:46', '2022-02-12 10:27:56');

-- --------------------------------------------------------

--
-- Table structure for table `service`
--

CREATE TABLE `service` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `mainserviceid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `img` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `service`
--

INSERT INTO `service` (`id`, `mainserviceid`, `name`, `img`, `created_at`, `updated_at`) VALUES
(1, '2', 'logo', 'uploads/service_1644658559.png', '2022-02-11 23:21:26', '2022-02-12 04:05:59'),
(4, '4', 'admin', NULL, '2022-02-12 00:05:58', '2022-02-12 03:07:58'),
(5, '2', 'admin', NULL, '2022-02-12 00:05:58', '2022-02-12 03:07:58');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `main_services`
--
ALTER TABLE `main_services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nw_features`
--
ALTER TABLE `nw_features`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nw_features_feature_title_unique` (`feature_title`);

--
-- Indexes for table `nw_features_list`
--
ALTER TABLE `nw_features_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nw_main_categories`
--
ALTER TABLE `nw_main_categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nw_main_categories_title_unique` (`title`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `question_answer`
--
ALTER TABLE `question_answer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `question_models`
--
ALTER TABLE `question_models`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service`
--
ALTER TABLE `service`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `main_services`
--
ALTER TABLE `main_services`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `nw_features`
--
ALTER TABLE `nw_features`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `nw_features_list`
--
ALTER TABLE `nw_features_list`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `nw_main_categories`
--
ALTER TABLE `nw_main_categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `question_answer`
--
ALTER TABLE `question_answer`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `question_models`
--
ALTER TABLE `question_models`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `service`
--
ALTER TABLE `service`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGstPercentagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
{
    Schema::create('gst_percentages', function (Blueprint $table) {
        $table->id();
        $table->string('country');
        $table->decimal('percentage', 5, 2);
        $table->timestamps();
    });
}

public function down()
{
    Schema::dropIfExists('gst_percentages');
}
}

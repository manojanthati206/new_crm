<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNwAdditionalFeaturesTable extends Migration
{
    public function up()
    {
        Schema::create('nw_additional_features', function (Blueprint $table) {
            $table->id();
            $table->text('text');
            $table->string('service_id', 10);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('nw_additional_features');
    }
}

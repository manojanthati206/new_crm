<?php



use Illuminate\Database\Migrations\Migration;

use Illuminate\Database\Schema\Blueprint;

use Illuminate\Support\Facades\Schema;



class CreateNwEstimationTable extends Migration

{

    /**

     * Run the migrations.

     *

     * @return void

     */

    public function up()

    {

        Schema::create('nw_estimation', function (Blueprint $table) {

            $table->id();

            $table->date('estimate_date');

            $table->string('estimate_no', 15);

            $table->date('estimate_expire');

            $table->string('service_id', 10);

            $table->text('html');
            $table->string('client_name');
            $table->string('client_address');
            $table->string('mobile_no',10);
            $table->string('client_email');
            $table->timestamps();
            $table->json('checked_features');

        });

    }



    /**

     * Reverse the migrations.

     *

     * @return void

     */

    public function down()

    {

        Schema::dropIfExists('nw_estimation');

    }

}


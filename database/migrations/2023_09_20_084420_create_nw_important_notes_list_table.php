<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNwImportantNotesListTable extends Migration
{
    public function up()
    {
        Schema::create('nw_important_notes_list', function (Blueprint $table) {
            $table->id();
            $table->string('note_name');
            $table->unsignedBigInteger('note_id');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('nw_important_notes_list');
    }
}

<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNwAdditionalFeaturesListTable extends Migration
{
    public function up()
    {
        Schema::create('nw_additional_features_list', function (Blueprint $table) {
            $table->id();
            $table->string('feature_name');
            $table->unsignedBigInteger('add_id');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('nw_additional_features_list');
    }
}

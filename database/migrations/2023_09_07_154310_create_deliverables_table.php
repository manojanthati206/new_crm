<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeliverablesTable extends Migration
{
    public function up()
    {
        Schema::create('deliverables', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('service_id');
            $table->string('name');
            // Add other columns for deliverables as needed

            // Define the foreign key constraint
            $table->foreign('service_id')
                  ->references('id')
                  ->on('nw_main_services')
                  ->onDelete('cascade'); // You can set the desired onDelete behavior

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('deliverables');
    }
}

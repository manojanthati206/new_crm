<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToNwEstimationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('nw_estimation', function (Blueprint $table) {
            // Add checked_additional_features column
            $table->json('checked_additional_features')->nullable();

            // Add checked_important_note column
            $table->json('checked_important_note')->nullable();

            // Add checked_deliverables column
            $table->json('checked_deliverables')->nullable();

            // Add gst_type column
            $table->string('gst_type', 255)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('nw_estimation', function (Blueprint $table) {
            $table->dropColumn('checked_additional_features');
            $table->dropColumn('checked_important_note');
            $table->dropColumn('checked_deliverables');
            $table->dropColumn('gst_type');
        });
    }
}

<?php



use Illuminate\Database\Migrations\Migration;

use Illuminate\Database\Schema\Blueprint;

use Illuminate\Support\Facades\Schema;



class CreateClientsTable extends Migration

{

    /**

     * Run the migrations.

     *

     * @return void

     */

    public function up()

    {

        Schema::create('nw_clients', function (Blueprint $table) {

            $table->id();

            $table->string('first_name', 250);

            $table->string('last_name', 250);

            $table->date('date_of_birth');

            $table->string('address1', 250);

            $table->string('address2', 250);

            $table->string('address3', 250)->nullable();

            $table->string('landmark', 250)->nullable();

            $table->string('area_city', 250);

            $table->string('business_name', 250)->nullable();
            
            $table->string('business_same_as_resedential', 250)->nullable();

            $table->string('business_address1', 250)->nullable();

            $table->string('business_address2', 250)->nullable();

            $table->string('business_address3', 250)->nullable();

            $table->string('company_number', 250)->nullable();

            $table->string('driving_licence', 250)->nullable();

            $table->string('prefered_communication_platform', 250)->nullable();

            $table->string('sign2_name', 250)->nullable();

            $table->string('sign2_email', 250)->nullable();

            $table->string('sign2_designation', 250)->nullable();
            $table->string('sign1_designation', 250)->nullable();

            $table->string('sign3_name', 250)->nullable();

            $table->string('sign3_email', 250)->nullable();

            $table->string('sign3_designation', 250)->nullable();

            $table->string('pincode', 250);

            $table->string('state', 250);

            $table->string('country', 250);

            $table->string('mobile', 250);

            $table->string('landline', 250)->nullable();

            $table->string('email_id', 250);

            $table->string('skype_id', 250)->nullable();

            $table->string('pancard', 250)->nullable();

            $table->string('gstin', 250)->nullable();

            $table->timestamps();

        });

    }



    /**

     * Reverse the migrations.

     *

     * @return void

     */

    public function down()

    {

        Schema::dropIfExists('nw_clients');

    }

}


<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionAnswerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nw_question_answer', function (Blueprint $table) {
            $table->id();
            $table->string('questionid')->nullable();
            $table->string('answer')->nullable();
            $table->string('clientid')->nullable();
            $table->string('mainserviceid')->nullable();
            $table->string('servicesid')->nullable();
            $table->string('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nw_question_answer');
    }
}

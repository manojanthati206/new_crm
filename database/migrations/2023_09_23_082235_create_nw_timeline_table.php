<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNwTimelineTable extends Migration
{
    public function up()
    {
        Schema::create('nw_timeline', function (Blueprint $table) {
            $table->id();
            $table->string('content', 300); // Up to 300 characters for content
            $table->unsignedBigInteger('service_id');
            
            // Define foreign key constraint for service_id
            $table->integer('service_id');
               

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('nw_timeline');
    }
}

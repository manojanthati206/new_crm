<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RecreateNwAdditionalFeaturesTable extends Migration
{
    public function up()
    {
        // Drop the existing table
        Schema::dropIfExists('nw_additional_features');

        // Create the table with the new structure, including the add_id column
        Schema::create('nw_additional_features', function (Blueprint $table) {
            $table->id();
            $table->text('text');
            $table->string('service_id', 10);
            $table->integer('add_id');
            $table->timestamps();
        });
    }

    public function down()
    {
        // Drop the table if needed, but this is usually not recommended in production
        Schema::dropIfExists('nw_additional_features');
    }
}

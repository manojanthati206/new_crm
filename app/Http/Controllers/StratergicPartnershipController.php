<?php

namespace App\Http\Controllers;

use App\Models\StratergicPartnership;
use Illuminate\Http\Request;

class StratergicPartnershipController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $StratergicPartnerships = StratergicPartnership::all();
        return view('partners.index',compact('StratergicPartnerships'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('partners.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'company_name'=> 'required',
            'representive_name'=> 'required',
            'mobile'=> 'required',
            'email'=> 'required',
            'business_category'=> 'required',
            'general_category_of_business'=> 'required',
            'expertice'=> 'required',
            'portfolio'=> 'required',
        ]);

        $files = [];
        if($request->hasfile('any_document'))
        {
            foreach($request->file('any_document') as $file)
            {
                if ( $file ) {
                    $name = uniqid(rand(), true).'.'.$file->extension();
                    $file->move(public_path('uploads'), $name);
                    $files[] = $name;
                }
            }
        }
        $question_n_answers = StratergicPartnership::create(array_merge($request->all(), ['any_document' => implode(", ",$files)]));
        return \Redirect::route('stratergic-partnership-add')->with('success', 'Created Successfully !');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\UpworkQuestionnAnswer  $upworkQuestionnAnswer
     * @return \Illuminate\Http\Response
     */
    public function show(UpworkQuestionnAnswer $upworkQuestionnAnswer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\UpworkQuestionnAnswer  $upworkQuestionnAnswer
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $question_n_answer = UpworkQuestionnAnswer::find($id);
        $projects = UpworkProjects::all();
        $questions = UpworkQuestions::all();
        return view('projects.edit', compact('question_n_answer','projects', 'questions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\UpworkQuestionnAnswer  $upworkQuestionnAnswer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UpworkQuestionnAnswer $upworkQuestionnAnswer)
    {
        //
        $request->validate([
            'upwork_project_id'     => 'required',
            'upwork_question_id'    => 'required',
            'answer'                => 'required',
        ]);
        $question_n_answers = UpworkQuestionnAnswer::find($request->id);
        $question_n_answers->upwork_project_id = $request->upwork_project_id;
        $question_n_answers->upwork_question_id = $request->upwork_question_id;
        $question_n_answers->answer = $request->answer;
        $question_n_answers->save();
        return \Redirect::route('upwork-question-n-answer-index')->with('success', 'Update Successfully !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\UpworkQuestionnAnswer  $upworkQuestionnAnswer
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $question_n_answers = UpworkQuestionnAnswer::find($id);
        $question_n_answers->delete();
        return \Redirect::route('upwork-question-n-answer-index')->with('success', 'Deleted successfully !');
    }
}

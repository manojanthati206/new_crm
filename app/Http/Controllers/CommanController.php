<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ClientsModels;
use App\Models\MainServicesModel;
use App\Models\ServiceModel;
use App\Models\QuestionAnswerModel;
use Illuminate\Support\Facades\Storage;
use Session;
class CommanController extends Controller
{
    // Add clientview side page
    // Client edit
    public function edit($id,$client_type){
        Session::put('clientid', $id);
        $client = ClientsModels::find($id);
        if($client->status != 'Publish')
            return view('clientview.clientedit',compact('client','client_type'));
        else
            return view('blank-dashboard')->with('fail','You have already filled out this form. So, You do not have access to fill it again.');
    }
    // Client edit
    public function first_name(Request $request, ClientsModels  $ClientsModels,$id){  
        $request->validate([
            'first_name'         => 'required',
        ]);
        $client = ClientsModels::find($id);
        $client->first_name = $request->post('first_name');
        $client->save();
        return response()->json(['success'=>' '.$request->post('first_name').' is stored in the database successfully.']);
    }
    // Last Name
    public function last_name(Request $request, ClientsModels  $ClientsModels,$id){ 
        $request->validate([
            'last_name'         => 'required',
        ]); 
        $client = ClientsModels::find($id);
        $client->last_name = $request->post('last_name');
        $client->save();
        return response()->json(['success'=>' '.$request->post('last_name').' is stored in the database successfully.']);
    }
    // Date of birth 
    public function dob(Request $request, ClientsModels  $ClientsModels,$id){ 
        $request->validate([
            'dob' => 'required',
        ]); 
        $client = ClientsModels::find($id);
        $client->date_of_birth = date('Y-m-d',strtotime($request->post('dob')) );
        $client->save();
        return response()->json(['success'=>'Birth date is stored in the database successfully.']);
    }
     // address1 
     public function address1(Request $request, ClientsModels  $ClientsModels,$id){  
        $request->validate([
            'address1'         => 'required',
        ]);
        $client = ClientsModels::find($id);
        $client->address1 = $request->post('address1');
        $client->save();
        return response()->json(['success'=>' '.$request->post('address1').' is stored in the database successfully.']);
    }
    // address2 
    public function address2(Request $request, ClientsModels  $ClientsModels,$id){  
        $request->validate([
            'address2'         => 'required',
        ]);
        $client = ClientsModels::find($id);
        $client->address2 = $request->post('address2');
        $client->save();
        return response()->json(['success'=>' '.$request->post('address2').' is stored in the database successfully.<br/><h4>Form Completed.</h4>']);
    }
    // address3 
    public function address3(Request $request, ClientsModels  $ClientsModels,$id){  
        $client = ClientsModels::find($id);
        $client->address3 = $request->post('address3');
        $client->save();
        return response()->json(['success'=>' '.$request->post('address3').' is stored in the database successfully.']);
    }
    // landmark
    public function landmark(Request $request, ClientsModels  $ClientsModels,$id){  
        $client = ClientsModels::find($id);
        $client->landmark = $request->post('landmark');
        $client->save();
        return response()->json(['success'=>' '.$request->post('landmark').' is stored in the database successfully.']);
    }
    // city
    public function city(Request $request, ClientsModels  $ClientsModels,$id){  
        $request->validate([
            'city'         => 'required'
        ]);
        $client = ClientsModels::find($id);
        $client->area_city = $request->post('city');
        $client->save();
        return response()->json(['success'=>' '.$request->post('area_city').' is stored in the database successfully.']);
    }
    // pincode
    public function pincode(Request $request, ClientsModels  $ClientsModels,$id){ 
        $request->validate([
              'pincode'         => 'required',
        ]); 
        $client = ClientsModels::find($id);
        $client->pincode = $request->post('pincode');
        $client->save();
        return response()->json(['success'=>' '.$request->post('pincode').' is stored in the database successfully.']);
    }
    // state 
    public function state(Request $request, ClientsModels  $ClientsModels,$id){
        $request->validate([
            'state'         => 'required',
        ]);  
        $client = ClientsModels::find($id);
        $client->state = $request->post('state');
        $client->save();
        return response()->json(['success'=>' '.$request->post('state').' is stored in the database successfully.']);
    }
    // country
    public function country(Request $request, ClientsModels  $ClientsModels,$id){
        $request->validate([
            'country'         => 'required',
        ]);  
        $client = ClientsModels::find($id);
        $client->country = $request->post('country');
        $client->save();
        return response()->json(['success'=>' '.$request->post('country').' is stored in the database successfully.']);
    }
    // mobile
    public function mobile(Request $request, ClientsModels  $ClientsModels,$id){ 
        $request->validate([
            'mobile'         => 'required',
        ]); 
        $client = ClientsModels::find($id);
        $client->mobile = $request->post('mobile');
        $client->save();
        return response()->json(['success'=>' '.$request->post('mobile').' is stored in the database successfully.']);
    }
    // landline
    public function landline(Request $request, ClientsModels  $ClientsModels,$id){  
        $client = ClientsModels::find($id);
        $client->landline = $request->post('landline');
        $client->save();
        return response()->json(['success'=>' '.$request->post('landline').' is stored in the database successfully.']);
    }
    // emailid
    public function emailid(Request $request, ClientsModels  $ClientsModels,$id){ 
        $request->validate([
            'emailid'         => 'required|email:rfc,dns',
        ]); 
        $client = ClientsModels::find($id);
        $client->email_id = $request->post('emailid');
        $client->save();
        return response()->json(['success'=>' '.$request->post('emailid').' is stored in the database successfully.']);
    }
    // landline
    public function skypeid(Request $request, ClientsModels  $ClientsModels,$id){  
        $client = ClientsModels::find($id);
        $client->skype_id = $request->post('skypeid');
        $client->save();
        return response()->json(['success'=>' '.$request->post('skypeid').' is stored in the database successfully.']);
    }
    // pancard
    public function pancard(Request $request, ClientsModels  $ClientsModels,$id){  
        $client = ClientsModels::find($id);
        $client->pancard = $request->post('pancard');
        $client->save();
        return response()->json(['success'=>' '.$request->post('pancard').' is stored in the database successfully.']);
    }
    // gstin
    public function gstin(Request $request, ClientsModels  $ClientsModels,$id){  
        $client = ClientsModels::find($id);
        $client->gstin = $request->post('gstin');
        $client->save();
        return response()->json(['success'=>' '.$request->post('gstin').' is stored in the database successfully.']);
    }
    public function business_name(Request $request, ClientsModels  $ClientsModels,$id){  
        $client = ClientsModels::find($id);
        $client->gstin = $request->post('business_name');
        $client->save();
        return response()->json(['success'=>' '.$request->post('business_name').' is stored in the database successfully.']);
    }
    public function business_same_as_resedential(Request $request, ClientsModels  $ClientsModels,$id){  
        $client = ClientsModels::find($id);
        $client->gstin = $request->post('business_same_as_resedential');
        $client->save();
        return response()->json(['success'=>' '.$request->post('business_same_as_resedential').' is stored in the database successfully.']);
    }
    public function business_address1(Request $request, ClientsModels  $ClientsModels,$id){  
        $client = ClientsModels::find($id);
        $client->gstin = $request->post('business_address1');
        $client->save();
        return response()->json(['success'=>' '.$request->post('business_address1').' is stored in the database successfully.']);
    }
    public function business_address2(Request $request, ClientsModels  $ClientsModels,$id){  
        $client = ClientsModels::find($id);
        $client->gstin = $request->post('business_address2');
        $client->save();
        return response()->json(['success'=>' '.$request->post('business_address2').' is stored in the database successfully.']);
    }
    public function business_address3(Request $request, ClientsModels  $ClientsModels,$id){  
        $client = ClientsModels::find($id);
        $client->gstin = $request->post('business_address3');
        $client->save();
        return response()->json(['success'=>' '.$request->post('business_address3').' is stored in the database successfully.']);
    }
    public function company_number(Request $request, ClientsModels  $ClientsModels,$id){  
        $client = ClientsModels::find($id);
        $client->gstin = $request->post('company_number');
        $client->save();
        return response()->json(['success'=>' '.$request->post('company_number').' is stored in the database successfully.']);
    }
    public function driving_licence(Request $request, ClientsModels  $ClientsModels,$id){  
        $client = ClientsModels::find($id);
        $client->gstin = $request->post('driving_licence');
        $client->save();
        return response()->json(['success'=>' '.$request->post('driving_licence').' is stored in the database successfully.']);
    }
    public function prefered_communication_platform(Request $request, ClientsModels  $ClientsModels,$id){  
        $client = ClientsModels::find($id);
        $client->gstin = $request->post('prefered_communication_platform');
        $client->save();
        return response()->json(['success'=>' '.$request->post('prefered_communication_platform').' is stored in the database successfully.']);
    }
    public function sign2_name(Request $request, ClientsModels  $ClientsModels,$id){  
        $client = ClientsModels::find($id);
        $client->gstin = $request->post('sign2_name');
        $client->save();
        return response()->json(['success'=>' '.$request->post('sign2_name').' is stored in the database successfully.']);
    }
    public function sign2_email(Request $request, ClientsModels  $ClientsModels,$id){  
        $client = ClientsModels::find($id);
        $client->gstin = $request->post('sign2_email');
        $client->save();
        return response()->json(['success'=>' '.$request->post('sign2_email').' is stored in the database successfully.']);
    }
    public function sign2_designation(Request $request, ClientsModels  $ClientsModels,$id){  
        $client = ClientsModels::find($id);
        $client->gstin = $request->post('sign2_designation');
        $client->save();
        return response()->json(['success'=>' '.$request->post('sign2_designation').' is stored in the database successfully.']);
    }
    public function sign1_designation(Request $request, ClientsModels  $ClientsModels,$id){  
        $client = ClientsModels::find($id);
        $client->gstin = $request->post('sign1_designation');
        $client->save();
        return response()->json(['success'=>' '.$request->post('sign1_designation').' is stored in the database successfully.']);
    }
    public function sign3_name(Request $request, ClientsModels  $ClientsModels,$id){  
        $client = ClientsModels::find($id);
        $client->gstin = $request->post('sign3_name');
        $client->save();
        return response()->json(['success'=>' '.$request->post('sign3_name').' is stored in the database successfully.']);
    }
    public function sign3_email(Request $request, ClientsModels  $ClientsModels,$id){  
        $client = ClientsModels::find($id);
        $client->gstin = $request->post('sign3_email');
        $client->save();
        return response()->json(['success'=>' '.$request->post('sign3_email').' is stored in the database successfully.']);
    }
    public function sign3_designation(Request $request, ClientsModels  $ClientsModels,$id){  
        $client = ClientsModels::find($id);
        $client->gstin = $request->post('sign3_designation');
        $client->save();
        return response()->json(['success'=>' '.$request->post('sign3_designation').' is stored in the database successfully.']);
    }
}
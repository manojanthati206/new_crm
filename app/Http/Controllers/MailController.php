<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\SendEmailLink;
use Mail;

class MailController extends Controller
{   
     /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
    */
    public function sendMail()
    {
        $email = 'admin25@yopmail.com';
   
        $maildata = [
            'title' => 'Laravel Mail Markdown Link',
        ];
  
        Mail::to($email)->send(new SendEmailLink($maildata));
   
        dd("Mail has been sent successfully");
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
    */
    public function sendMailLink()
    {
        dd('Nicesnippets.com');       
    }
}

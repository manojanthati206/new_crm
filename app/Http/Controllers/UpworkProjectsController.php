<?php

namespace App\Http\Controllers;

use App\Models\UpworkProjects;
use App\Models\UpworkQuestionnAnswer;
use App\Models\UpworkQuestions;
use Illuminate\Http\Request;

class UpworkProjectsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $projects = UpworkProjects::all();
        return view('upwork.projects.index',compact('projects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('upwork.projects.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'project_name' => 'required',
            'client_name' => 'required',
            'profile_name' => 'required',
        ]);
        $files = [];
        if($request->hasfile('project_details_screenshot'))
        {
            foreach($request->file('project_details_screenshot') as $file)
            {
                if ( $file ) {
                    $name = uniqid(rand(), true).'.'.$file->extension();
                    $file->move(public_path('uploads'), $name);
                    $files[] = $name;
                }
            }
        }
        $project = UpworkProjects::create(array_merge($request->all(), ['project_details_screenshot' => implode(", ",$files)]));
        return \Redirect::route('upwork-projects-index')->with('success', 'Created Successfully !');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\UpworkProjects  $upworkProjects
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $project = UpworkProjects::find($id);
        $question_n_answers = UpworkQuestionnAnswer::where('upwork_project_id',$id)->get();
        $questions = UpworkQuestions::all();
        return view('upwork.projects.show', compact('project', 'question_n_answers', 'questions' ));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\UpworkProjects  $upworkProjects
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $project = UpworkProjects::find($id);
        return view('upwork.projects.edit', compact('project'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\UpworkProjects  $upworkProjects
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UpworkProjects $upworkProjects)
    {
        //
        $request->validate([
            'project_name' => 'required',
            'client_name' => 'required',
            'profile_name' => 'required',
        ]);
        $projects = UpworkProjects::find($request->id);
        $projects->project_name = $request->project_name;
        $projects->profile_name = $request->profile_name;
        $projects->client_name = $request->client_name;
        $projects->project_details = $request->project_details;
        $projects->project_link = $request->project_link;

        $fileName = '';
        $fileNames = array();
        $files = [];
        if($request->hasfile('project_details_screenshot'))
        {
            foreach($request->file('project_details_screenshot') as $file)
            {
                if ( $file ) {
                    $name = uniqid(rand(), true).'.'.$file->extension();
                    $file->move(public_path('uploads'), $name);
                    $fileNames[] = $name;
                }
            }
        }
        if ( ! empty( $fileNames ) ) {
            $projects->project_details_screenshot = implode(", ",$fileNames);
        }
        $projects->save();
        return \Redirect::route('upwork-projects-index')->with('success', 'Update Successfully !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\UpworkProjects  $upworkProjects
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $projects = UpworkProjects::find($id);
        $projects->delete();
        return \Redirect::route('upwork-projects-index')->with('success', 'Deleted successfully !');
    }
}

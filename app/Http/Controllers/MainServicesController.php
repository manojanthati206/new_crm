<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\MainServicesModel;
class MainServicesController extends Controller
{
    // fetch data from Services table and show index page.
    public function index(){
        $services = MainServicesModel::all();
        return view('mainservices.index',compact('services'));
    }

    // Add services page
    public function create(){
        return view('mainservices.create');
    }

    // Store services data method
    public function store(Request $request){
        
        $request->validate([
            'name'         => 'required',
            ]);
       
        $servicess = MainServicesModel::create($request->all());
        return \Redirect::route('mainservices-index')->with('success', 'Created Successfully !');
    }

    // Edit data fetach
    public function edit($id){
        $services = MainServicesModel::find($id);
        return view('mainservices.edit', compact('services'));
    }
    // update data to store
    public function update(Request $request, MainServicesModel  $MainServicesModel){
        $request->validate([
            'name'         => 'required'
        ]);
       
        $services = MainServicesModel::find($request->id);
        $services->name = $request->name;
        $services->save();
        return \Redirect::route('mainservices-index')->with('success','Created Successfully !');

    } 
    // Delete services
    public function destroy($id)
    {
        $feature = MainServicesModel::find($id);
        $feature->delete();
        return \Redirect::route('mainservices-index')->with('success', 'Deleted successfully !');
    }
}

<?php
namespace App\Http\Controllers;
use App\Models\QuotationFeatures;
use App\Models\QuotationMainCategory;
use Illuminate\Http\Request;
class QuotationFeaturesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $features = QuotationFeatures::all();
        $main_cats = QuotationMainCategory::all();
        return view('quotation.features.index',compact('features','main_cats'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $main_cats = QuotationMainCategory::all();
        return view('quotation.features.create',compact('main_cats'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'feature_title'         => 'required',
            'service_id'         => 'required',
            'main_category_id'         => 'required',
        ]);
        $feature = QuotationFeatures::create($request->all());
        return \Redirect::route('features-index')->with('success', 'Created Successfully !');
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\QuotationFeatures  $quotationFeatures
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $features_lists = QuotationFeaturesList::where('feature_id',$id);
        return view('quotation.features.show',compact('features_lists'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\QuotationFeatures  $quotationFeatures
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $feature = QuotationFeatures::find($id);
        $main_cats = QuotationMainCategory::all();
        return view('quotation.features.edit', compact('feature','main_cats'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\QuotationFeatures  $quotationFeatures
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, QuotationFeatures $quotationFeatures)
    {
        //
        $request->validate([
            'feature_title'         => 'required',
            'service_id'         => 'required',
            'main_category_id'         => 'required',
        ]);
        $quotation = QuotationFeatures::find($request->id);
        $quotation->feature_title = $request->feature_title;
        $quotation->service_id = $request->service_id;
        $quotation->main_category_id = $request->main_category_id;
        $quotation->save();
        return \Redirect::route('features-index')->with('success', 'Update Successfully !');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\QuotationFeatures  $quotationFeatures
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $feature = QuotationFeatures::find($id);
        $feature->delete();
        return \Redirect::route('features-index')->with('success', 'Deleted successfully !');
    }
}
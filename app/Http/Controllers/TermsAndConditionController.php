<?php

namespace App\Http\Controllers;

use App\Models\TermsAndCondition;
use Illuminate\Http\Request;

class TermsAndConditionController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $all_terms = TermsAndCondition::all();
        return view('quotation.terms_and_condition.index',compact('all_terms'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('quotation.terms_and_condition.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'content'         => 'required',
            'service_id'         => 'required',
        ]);
        $all_terms = TermsAndCondition::create($request->all());
        return \Redirect::route('terms-and-condition-index')->with('success', 'Created Successfully !');
    }

    /**
     * Display the specified resource.
     * @param  \App\Models\TermsAndCondition  $termsAndCondition
     * @return \Illuminate\Http\Response
     */
    public function show(TermsAndCondition $termsAndCondition)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\TermsAndCondition  $termsAndCondition
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $all_term = TermsAndCondition::find($id);
        return view('quotation.terms_and_condition.edit', compact('all_term'));
    }

    /**
     * Update the specified resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\TermsAndCondition  $termsAndCondition
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TermsAndCondition $termsAndCondition)
    {
        //
        $request->validate([
            'term_content'         => 'required',
            'service_id'         => 'required',
        ]);
        $terms = TermsAndCondition::find($request->id);
        $terms->content = $request->term_content;
        $terms->service_id = $request->service_id;
        $terms->save();
        return \Redirect::route('terms-and-condition-index')->with('success', 'Update Successfully !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TermsAndCondition  $termsAndCondition
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $terms = TermsAndCondition::find($id);
        $terms->delete();
        return \Redirect::route('terms-and-condition-index')->with('success', 'Deleted successfully !');
    }
}

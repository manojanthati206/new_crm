<?php

namespace App\Http\Controllers;

use App\Models\QuotationFeatures;
use App\Models\QuotationFeaturesList;
use Illuminate\Http\Request;

class QuotationFeaturesListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $features_lists = QuotationFeaturesList::all();
        $features = QuotationFeatures::all();
        return view('quotation.features-list.index',compact('features_lists', 'features'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $features = QuotationFeatures::all();
        return view('quotation.features-list.create',compact('features'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'feature_list'         => 'required',
            'feature_id'         => 'required',
        ]);
        $featurelist = QuotationFeaturesList::create($request->all());
        return \Redirect::route('features-list-add')->with('success', 'Created Successfully !');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\QuotationFeaturesList  $quotationFeaturesList
     * @return \Illuminate\Http\Response
     */
    public function show(QuotationFeaturesList $quotationFeaturesList)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\QuotationFeaturesList  $quotationFeaturesList
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $featurelist = QuotationFeaturesList::find($id);
        $features = QuotationFeatures::all();
        return view('quotation.features-list.edit', compact('featurelist', 'features'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\QuotationFeaturesList  $quotationFeaturesList
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, QuotationFeaturesList $quotationFeaturesList)
    {
        //
        $request->validate([
            'feature_list'         => 'required',
            'feature_id'         => 'required',
        ]);
        $quotation = QuotationFeaturesList::find($request->id);
        $quotation->feature_list = $request->feature_list;
        $quotation->feature_id = $request->feature_id;
        $quotation->save();
        return \Redirect::route('features-list-index')->with('success', 'Update Successfully !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\QuotationFeaturesList  $quotationFeaturesList
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $feature = QuotationFeaturesList::find($id);
        $feature->delete();
        return \Redirect::route('features-list-index')->with('success', 'Deleted successfully !');
    }
}

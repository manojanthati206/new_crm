<?php

namespace App\Http\Controllers;

use App\Models\Reminder;
use Illuminate\Http\Request;
use DB;
use URL;

class ReminderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $reminders = Reminder::all();
        foreach ($reminders as $reminder) {
            $date_now = date("Y-m-d");
            $reminder_date = date('Y-m-d', strtotime($reminder->last_response_date. ' + 2 days'));
            if( (strtotime($reminder_date)) <= (strtotime($date_now)) && $reminder->reminder_status == "active" && $reminder->followups == "yes" ) {

                $this->sendMail($reminder->reminder_email,'neshallweb@yopmail.com', $reminder,$reminder->reminder_count,$reminder);
                
                $reminders_update = Reminder::find($reminder->id);
                $reminders_update->reminder_status = 'active';
                $reminders_update->last_response_date = date("Y-m-d");
                $reminders_update->followups = 'yes';
                $reminders_update->reminder_count = (int)($reminders_update->reminder_count)+1;
                $reminders_update->save();
            } else if( strtotime($reminder_date) <= strtotime($date_now) && $reminder->reminder_status == 'inactive' && $reminder->followups == 'no' && $reminder->client_status == 'active' ) {
                $this->sendMail($reminder->reminder_email,'neshallweb@yopmail.com', $reminder,$reminder->reminder_count,$reminder);
                $reminders_update = Reminder::find($reminder->id);
                $reminders_update->reminder_status = 'active';
                $reminders_update->last_response_date = date("Y-m-d");
                $reminders_update->followups = 'yes';
                $reminders_update->reminder_count = (int)($reminders_update->reminder_count)+1;
                $reminders_update->save();
            }

        }
        mail('testmail12@yopmail.com', 'test', 'Test MessageViewBody');
        return view('reminder.index',compact('reminders'));
    }
    public function reminder_off($id) {
        $reminders = Reminder::find($id);
        $reminders->reminder_status = 'inactive';
        $reminders->last_response_date = date("Y-m-d");
        $reminders->followups = 'no';
        $reminders->save();
        return \Redirect::route('dashboard')->with('success', 'Reminder off Successfully !');
    }
    public function reminder_off_permanent($id) {
        $reminders = Reminder::find($id);
        $reminders->client_status = 'Close';
        $reminders->reminder_status = 'inactive';
        $reminders->followups = 'no';
        $reminders->save();
        return \Redirect::route('dashboard')->with('success', 'Reminder off permanently !');
    }
    public function sendMail($to, $from, $reminder,$reminder_cnt,$mailreminder){
        $subject = 'Follo-Up Request';

        // To send HTML mail, the Content-type header must be set
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html;' . "\r\n";

        // Create email headers
        $headers .= 'From: '.$from."\r\n".
            'Reply-To: '.$from."\r\n" .
            'X-Mailer: PHP/' . phpversion();

        // Compose a simple HTML email message
        //$message = $this->mail_html($mailreminder->client_name, $mailreminder->approach_channel, $mailreminder->communication_preference, $mailreminder->contact_info, $mailreminder->id,$reminder_cnt);
        $message = '<html><body>';
        //$message .= '<style> img {border: none;-ms-interpolation-mode: bicubic;max-width: 100%;}body {background-color: #f6f6f6;font-family: sans-serif;-webkit-font-smoothing: antialiased;font-size: 14px;line-height: 1.4;margin: 0;padding: 0;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;}table {border-collapse: separate;mso-table-lspace: 0pt;mso-table-rspace: 0pt;width: 100%;}table td {font-family: sans-serif;font-size: 14px;vertical-align: top;}.body {background-color: #f6f6f6;width: 100%;}.container {display: block;margin: 0 auto !important;max-width: 580px;padding: 10px;width: 580px;}.content {box-sizing: border-box;display: block;margin: 0 auto;max-width: 580px;padding: 10px;}.main {background: #ffffff;border-radius: 3px;width: 100%;}.wrapper {box-sizing: border-box;padding: 20px;}.content-block {padding-bottom: 10px;padding-top: 10px;}.footer {clear: both;margin-top: 10px;text-align: center;width: 100%;}.footer td, .footer p, .footer span, .footer a {color: #999999;font-size: 12px;text-align: center;}h1, h2, h3, h4 {color: #000000;font-family: sans-serif;font-weight: 400;line-height: 1.4;margin: 0;margin-bottom: 30px;}h1 {font-size: 35px;font-weight: 300;text-align: center;text-transform: capitalize;}p, ul, ol {font-family: sans-serif;font-size: 14px;font-weight: normal;margin: 0;margin-bottom: 15px;}p li, ul li, ol li {list-style-position: inside;margin-left: 5px;}a {color: #3498db;text-decoration: underline;}.btn {box-sizing: border-box;width: 100%;}.btn > tbody > tr > td {padding-bottom: 15px;}.btn table {width: auto;}.btn table td {background-color: #ffffff;border-radius: 5px;text-align: center;}.btn a {background-color: #ffffff;border: solid 1px #3498db;border-radius: 5px;box-sizing: border-box;color: #3498db;cursor: pointer;display: inline-block;font-size: 14px;font-weight: bold;margin: 0;padding: 12px 25px;text-decoration: none;text-transform: capitalize;}.btn-primary table td {background-color: #3498db;}.btn-primary a {background-color: #3498db;border-color: #3498db;color: #ffffff;}.last {margin-bottom: 0;}.first {margin-top: 0;}.align-center {text-align: center;}.align-right {text-align: right;}.align-left {text-align: left;}.clear {clear: both;}.mt0 {margin-top: 0;}.mb0 {margin-bottom: 0;}.preheader {color: transparent;display: none;height: 0;max-height: 0;max-width: 0;opacity: 0;overflow: hidden;mso-hide: all;visibility: hidden;width: 0;}.powered-by a {text-decoration: none;}hr {border: 0;border-bottom: 1px solid #f6f6f6;margin: 20px 0;}@media only screen and (max-width: 620px) {table.body h1 {font-size: 28px !important;margin-bottom: 10px !important;}table.body p, table.body ul, table.body ol, table.body td, table.body span, table.body a {font-size: 16px !important;}table.body .wrapper, table.body .article {padding: 10px !important;}table.body .content {padding: 0 !important;}table.body .container {padding: 0 !important;width: 100% !important;}table.body .main {border-left-width: 0 !important;border-radius: 0 !important;border-right-width: 0 !important;}table.body .btn table {width: 100% !important;}table.body .btn a {width: 100% !important;}table.body .img-responsive {height: auto !important;max-width: 100% !important;width: auto !important;}}@media all {.ExternalClass {width: 100%;}.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;}.apple-link a {color: inherit !important;font-family: inherit !important;font-size: inherit !important;font-weight: inherit !important;line-height: inherit !important;text-decoration: none !important;}#MessageViewBody a {color: inherit;text-decoration: none;font-size: inherit;font-family: inherit;font-weight: inherit;line-height: inherit;}.btn-primary table td:hover {background-color: #34495e !important;}.btn-primary a:hover {background-color: #34495e !important;border-color: #34495e !important;}}</style>';
        $message .= '<table role="presentation" border="0" cellpadding="0" cellspacing="0" class="body"><tr><td>&nbsp;</td><td class="container"><div class="content">';
        $message .= '<table role="presentation" class="main"><tr><td class="wrapper"><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td>';
        $message .= '<p>Hi '. substr($to,0,strpos($to,"@")) .',</p>';
        $message .= '<p>Hope you&#39;re doing well.</p>';
        $message .= '<p>This is regarding the follow-up to the client '. $mailreminder->client_name .' from '. $mailreminder->approach_channel .'. Let&#39;s get started '. $mailreminder->communication_preference .', so you can call/message me.</p>';
        $message .= '<p>Let me know if you have any questions about the follow-up.</p>';
        $message .= '<p>PS: Ignore this message if you&#39;ve already followed up. Check out this <a href="'. URL::to('/') .'/reminder/off/'.$mailreminder->id.'" target="_blank">Link</a>. </p>';
        if( $reminder_cnt > 0 ) {
            $message .= '<p>If you want to close this reminder permanently then click on this <a href="'. URL::to('/') .'/reminder/off-permanent/'.$mailreminder->id.'">Link</a>.</p>';
        }
        $message .= '<p>Thanks,</p>';
        $message .= '<p>neshallWeb</p>';
        $message .= '</td></tr></tbody></table>';
        $message .= '</tr></tbody></table></td></tr></tbody></table>';
        $message .= '</td></tr></table></td></tr></table>';
        $message .= '</div></td><td>&nbsp;</td></tr></table></body></html>';
        //$message .= 'Please follow this client : '. $reminder->client_name . ' If you already take follow up. Kindly click on this link to avoid reminder. <a href="'. URL::to('/') .'/reminder/off/'.$reminder->id.'"> Click Here </a>';
        // Sending email
        mail($to, $subject, $message, $headers);
        /*if(mail($to, $subject, $message, $headers)){
            echo 'send mail';
        } else {
            echo 'not send';
        }*/
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('reminder.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        //
        $request->validate([
            'client_name'  => 'required',
            'approach_channel'  => 'required',
            'communication_preference'  => 'required',
            'reminder_email'  => 'required',
            'contact_info'  => 'required',
            'last_response_date'  => 'required',
            'client_status'  => 'required',
        ]);

        $last_response_date = date('Y-m-d', strtotime($request->last_response_date));
        $reminders = new Reminder;
        $reminders->client_name =  $request->client_name;
        $reminders->approach_channel = $request->approach_channel;
        $reminders->communication_preference =  $request->communication_preference;
        $reminders->reminder_email =  $request->reminder_email;
        $reminders->contact_info = $request->contact_info;
        $reminders->last_response_date = $last_response_date;
        $reminders->client_status = $request->client_status;
        $reminders->reminder_count = 0;
        $reminders->reminder_status = 'active';
        $reminders->followups = 'yes';
        $reminders->save();
        return \Redirect::route('reminder-index')->with('success', 'Created Successfully !');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Reminder  $reminder
     * @return \Illuminate\Http\Response
     */
    public function show(Reminder $reminder)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Reminder  $reminder
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $reminder = Reminder::find($id);
        return view('reminder.edit', compact('reminder'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Reminder  $reminder
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Reminder $reminder)
    {
        //
        $request->validate([
            'title'         => 'required',
            'client_name'  => 'required',
            'approach_channel'  => 'required',
            'communication_preference'  => 'required',
            'reminder_email'  => 'required',
            'contact_info'  => 'required',
            'last_response_date'  => 'required',
            'client_status'  => 'required',
        ]);
        $reminders = Reminder::find($request->id);
        $last_response_date = date('Y-m-d', strtotime($request->last_response_date));
        $reminders->client_name = $request->client_name;
        $reminders->approach_channel = $request->approach_channel;
        $reminders->communication_preference = $request->communication_preference;
        $reminders->reminder_email = $request->reminder_email;
        $reminders->contact_info = $request->contact_info;
        $reminders->last_response_date = $request->last_response_date;
        $reminders->client_status = $request->client_status;
        $reminders->reminder_count = (int)($reminders->reminder_count)+1;
        if ( $reminders->last_response_date != $last_response_date ) {
            $reminders->reminder_status = 'inactive';
            $reminders->followups = 'no';
        }
        $reminders->save();
        return \Redirect::route('reminder-index')->with('success', 'Update Successfully !');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Reminder  $reminder
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $reminders = Reminder::find($id);
        $reminders->delete();
        return \Redirect::route('reminder-index')->with('success', 'Deleted successfully !');
    }
    public function mail_html($client_name, $approach_channel, $communication_preference, $contact_info, $id, $reminder_count){
        $html = '<html>';
        $html .= '<style> img {border: none;-ms-interpolation-mode: bicubic;max-width: 100%;}body {background-color: #f6f6f6;font-family: sans-serif;-webkit-font-smoothing: antialiased;font-size: 14px;line-height: 1.4;margin: 0;padding: 0;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;}table {border-collapse: separate;mso-table-lspace: 0pt;mso-table-rspace: 0pt;width: 100%;}table td {font-family: sans-serif;font-size: 14px;vertical-align: top;}.body {background-color: #f6f6f6;width: 100%;}.container {display: block;margin: 0 auto !important;max-width: 580px;padding: 10px;width: 580px;}.content {box-sizing: border-box;display: block;margin: 0 auto;max-width: 580px;padding: 10px;}.main {background: #ffffff;border-radius: 3px;width: 100%;}.wrapper {box-sizing: border-box;padding: 20px;}.content-block {padding-bottom: 10px;padding-top: 10px;}.footer {clear: both;margin-top: 10px;text-align: center;width: 100%;}.footer td, .footer p, .footer span, .footer a {color: #999999;font-size: 12px;text-align: center;}h1, h2, h3, h4 {color: #000000;font-family: sans-serif;font-weight: 400;line-height: 1.4;margin: 0;margin-bottom: 30px;}h1 {font-size: 35px;font-weight: 300;text-align: center;text-transform: capitalize;}p, ul, ol {font-family: sans-serif;font-size: 14px;font-weight: normal;margin: 0;margin-bottom: 15px;}p li, ul li, ol li {list-style-position: inside;margin-left: 5px;}a {color: #3498db;text-decoration: underline;}.btn {box-sizing: border-box;width: 100%;}.btn > tbody > tr > td {padding-bottom: 15px;}.btn table {width: auto;}.btn table td {background-color: #ffffff;border-radius: 5px;text-align: center;}.btn a {background-color: #ffffff;border: solid 1px #3498db;border-radius: 5px;box-sizing: border-box;color: #3498db;cursor: pointer;display: inline-block;font-size: 14px;font-weight: bold;margin: 0;padding: 12px 25px;text-decoration: none;text-transform: capitalize;}.btn-primary table td {background-color: #3498db;}.btn-primary a {background-color: #3498db;border-color: #3498db;color: #ffffff;}.last {margin-bottom: 0;}.first {margin-top: 0;}.align-center {text-align: center;}.align-right {text-align: right;}.align-left {text-align: left;}.clear {clear: both;}.mt0 {margin-top: 0;}.mb0 {margin-bottom: 0;}.preheader {color: transparent;display: none;height: 0;max-height: 0;max-width: 0;opacity: 0;overflow: hidden;mso-hide: all;visibility: hidden;width: 0;}.powered-by a {text-decoration: none;}hr {border: 0;border-bottom: 1px solid #f6f6f6;margin: 20px 0;}@media only screen and (max-width: 620px) {table.body h1 {font-size: 28px !important;margin-bottom: 10px !important;}table.body p, table.body ul, table.body ol, table.body td, table.body span, table.body a {font-size: 16px !important;}table.body .wrapper, table.body .article {padding: 10px !important;}table.body .content {padding: 0 !important;}table.body .container {padding: 0 !important;width: 100% !important;}table.body .main {border-left-width: 0 !important;border-radius: 0 !important;border-right-width: 0 !important;}table.body .btn table {width: 100% !important;}table.body .btn a {width: 100% !important;}table.body .img-responsive {height: auto !important;max-width: 100% !important;width: auto !important;}}@media all {.ExternalClass {width: 100%;}.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;}.apple-link a {color: inherit !important;font-family: inherit !important;font-size: inherit !important;font-weight: inherit !important;line-height: inherit !important;text-decoration: none !important;}#MessageViewBody a {color: inherit;text-decoration: none;font-size: inherit;font-family: inherit;font-weight: inherit;line-height: inherit;}.btn-primary table td:hover {background-color: #34495e !important;}.btn-primary a:hover {background-color: #34495e !important;border-color: #34495e !important;}}</style>';
        $html .= '<table role="presentation" border="0" cellpadding="0" cellspacing="0" class="body"><tr><td>&nbsp;</td><td class="container"><div class="content">';
        $html .= '<table role="presentation" class="main"><tr><td class="wrapper"><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td>';
        $html .= '<p>Hi Pranav,</p>';
        $html .= '<p>Hope you’re doing well.</p>';
        $html .= '<p>This is regarding the follow-up to the client '. $client_name .' from '. $approach_channel .'. Let&#39;s get started '. $communication_preference .', so you can call/message me.</p>';
        $html .= '<p>Let me know if you have any questions about the follow-up.</p>';
        $html .= '<p>PS: Ignore this message if you&#39;ve already followed up. Check out this <a href="'. URL::to('/') .'/reminder/off/'.$id.'" target="_blank">Link</a>. </p>';
        if( $reminder_count > 0 ) {
            $html .= '<p>If you want to close this reminder permanently then click on this <a href="'. URL::to('/') .'/reminder/off-permanent/'.$id.'">Link</a>.</p>';
        }
        $html .= '<p>Thanks,</p>';
        $html .= '<p>neshallWeb</p>';
        //$html .= '<table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary"><tbody><tr><td align="left"><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tbody><tr><td>';
        //$html .= '<a href="'.$link.'" target="_blank">Call To Action</a> </td>';
        //$html .= '</tr></tbody></table>';
        $html .= '</td></tr></tbody></table>';
        $html .= '</tr></tbody></table></td></tr></tbody></table>';
        $html .= '</td></tr></table></td></tr></table>';
        $html .= '</div></td><td>&nbsp;</td></tr></table></body></html>';
        return $html;
    }
}

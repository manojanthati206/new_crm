<?php

namespace App\Http\Controllers;
use PDF;
use App\Models\Deliverables;
use Illuminate\Http\Request;
use App\Models\GstPercentage;
use App\Models\QuotationFeatures;
use App\Models\TermsAndCondition;
use Illuminate\Support\Facades\DB;
use App\Models\QuotationEstimation;
use App\Models\QuotationNwTimeline;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Mail;
use App\Models\QuotationFeaturesList;
use App\Models\QuotationMainCategory;
use App\Models\QuotationMainServices;
use App\Models\QuotationImportantNote;
use App\Models\ServiceAgreementContent;
use Illuminate\Support\Facades\Redirect;
use App\Models\QuotationAdditionalFeatures;
use App\Models\QuotationImportantNotesList;
use App\Models\QuotationAdditionalFeaturesList;

class QuotationEstimationController extends Controller
{

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $all_estimates = QuotationEstimation::all();
        return view('quotation.estimation.index',compact('all_estimates'));
    }

    public function service_agreement_signature()
    {
        //
        return view('quotation.service-agreement.service-agreement');
    }
    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\Http\Response
     */
    public function create()
{
    $features_lists = QuotationFeaturesList::all();
    $features = QuotationFeatures::all();
    $main_cats = QuotationMainCategory::all();
    $main_servs = QuotationMainServices::all();
    $gsts = GstPercentage::all();
    $current_date = date('d F, Y');
    $expire_date = date('d F, Y', strtotime("+30 days"));
    $estimate_no =  DB::table('nw_estimation')->latest('id')->first()->estimate_no;
    $next_estimate_no = (int) str_replace('nW-ET', '', $estimate_no) + 1;
    $next_estimate_no = 'nW-ET' . $next_estimate_no;
    $deliverables = Deliverables::all();

    // Additional variables for important notes and note lists
    $importantNotes = QuotationImportantNote::all();
    $importantNoteLists = QuotationImportantNotesList::all();

    // Additional variables for additional features and feature lists
    $additionalFeatures = QuotationAdditionalFeatures::all();
    $additionalFeatureLists = QuotationAdditionalFeaturesList::all();

    return view('quotation.estimation.practice', compact(
        'features_lists',
        'features',
        'main_cats',
        'current_date',
        'expire_date',
        'next_estimate_no',
        'main_servs',
        'deliverables',
        'gsts',
        'importantNotes',
        'importantNoteLists',
        'additionalFeatures',
        'additionalFeatureLists'
    ));
}


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        //
        $request->validate([
           'estimate_date' => 'required',
        'estimate_no' => 'required',
        'estimate_expire' => 'required',
        'price' => 'required',
        'price_type' => 'required',
        'client_name' => 'required',
        'client_address' => 'required',
        'mobile_no' => 'required',
        'client_email' => 'required',
        'main_categories' => 'nullable',
    ]);
        $estimate_date = date('Y-m-d', strtotime($request->estimate_date));
        $estimate_expire = date('Y-m-d', strtotime($request->estimate_expire));
        $client_name = $request->client_name;
        $client_address = $request->client_address;
        $mobile_no = $request->mobile_no;
        $client_email = $request->client_email;
        $ifExists = QuotationEstimation::where('estimate_date', $estimate_date)
            ->where('estimate_no', $request->estimate_no)
            ->where('estimate_expire', $estimate_expire)
            ->where('service_id', $request->service_id)->first();
        if( isset( $ifExists->id ) && ! empty( $ifExists->id ) ) {
            $estimate = QuotationEstimation::find($ifExists->id);
            $estimate->estimate_date = $estimate_date;
            $estimate->estimate_no = $request->estimate_no;
            $estimate->estimate_expire = $estimate_expire;
            $estimate->service_id = $request->service_id;
            $estimate->checked_features=$request->featurelists;
            $estimate->price = $request->price;
            $estimate->price_type = $request->price_type;
            $estimate->client_name = $request->client_name;
            $estimate->client_address = $request->client_address;
            $estimate->mobile_no = $request->mobile_no;
            $estimate->client_email = $request->client_email;
            $estimate->save();
        } else {
            $estimate = new QuotationEstimation;
            $estimate->estimate_date = $estimate_date;
            $estimate->estimate_no = $request->estimate_no;
            $estimate->estimate_expire = $estimate_expire;
            $estimate->service_id = $request->service_id;
            $estimate->checked_features=$request->featurelists;
            $estimate->price = $request->price;
            $estimate->price_type = $request->price_type;
            $estimate->gst_type = $request->gst_type;
            $estimate->client_name = $request->client_name;
            $estimate->client_address = $request->client_address;
            $estimate->mobile_no = $request->mobile_no;
            $estimate->client_email = $request->client_email;
            $estimate->checked_additional_features = $request->selectedAddfeatures;
            $estimate->checked_important_note = $request->selectedImportantNote;
            $estimate->checked_deliverables = $request->selectedDeliverables;
            $estimate->save();
        }
        //
        //$main_cat_ids = $request->estimate_main_cat;
        //$features_ids = $request->estimate_features;
       

        $estimate_date = $request->estimate_date;
        $estimate_no = $request->estimate_no;
        $estimate_expire = $request->estimate_expire;
        $service_id = $request->service_id;
        $price = $request->price;
        $price_type = $request->price_type;
        // Get the client's country from the request
        $country = $request->client_country;

        // Fetch GST percentage based on the client's country
        $gstPercentage = GstPercentage::where('country', $country)->first();

        if ($gstPercentage) {
            // If a record is found, calculate $x by dividing the percentage value by 100
            $x = $gstPercentage->percentage / 100;
        } else {
            // If no matching record is found, set a default value for $x
            $x = 0.18;
        }

                // Assuming $x and $price are already calculated based on your requirements
        if ($request->gst_type == "included") {
            $price = $price/(1+$x);
            
            // Round the price to the nearest whole number without decimals
            $price = ceil($price);
        }

        
       

            $main_cat_ids_json = $request->input('main_categories');
            $feature_ids_json = $request->input('features');
            $features_lists_ids_json = $request->input('featurelists');


            $timelines =QuotationNwTimeline::all();
            $selectedDeliverables = array_map('intval',json_decode($request->input('selectedDeliverables',true)));
            $selectedAddfeatures = json_decode($request->input('selectedAddfeatures',true));
            $selectedImportantNote = json_decode($request->input('selectedImportantNote',true));
            $main_cat_ids = array_map('intval', json_decode($main_cat_ids_json, true));
            $feature_ids = array_map('intval', json_decode($feature_ids_json, true));
            $features_lists_ids = array_map('intval', json_decode($features_lists_ids_json, true));
            $additionalfeature_lists = QuotationAdditionalFeaturesList::all();
            $improtantnote_lists = QuotationImportantNotesList::all();
            
        
            $deliverables = DB::table('deliverables')
            ->whereIn('id', $selectedDeliverables)
            ->get();
        $features_lists = QuotationFeaturesList::whereIn('id', $features_lists_ids)->get();
        $features = QuotationFeatures::whereIn('id', $feature_ids)->get();
        $main_servs = QuotationMainServices::whereIn('id', $main_cat_ids)->get();
        
        
        $term_n_service = TermsAndCondition::all();
        $html = $this->createHTMLForTable($main_servs, $features, $features_lists, $price_type, $price, $deliverables, $selectedAddfeatures, $selectedImportantNote,$improtantnote_lists,$additionalfeature_lists,$timelines,$x);
        // dd($html);

        $estimate = QuotationEstimation::find($estimate->id);
        $estimate->html = $html;
        $estimate->save();

        //$pdf = PDF::loadView('quotation/htmlToPDF/estimation',compact('features_lists' , 'features', 'main_cats','estimate_date', 'estimate_no', 'estimate_expire', 'service'));
       
        $pdf1 = PDF::loadView('quotation.htmlToPDF.estimation', compact('features_lists' , 'features', 'main_servs','estimate_date', 'estimate_no', 'estimate_expire',  'service_id', 'price', 'price_type', 'html', 'client_name','term_n_service', 'client_address', 'mobile_no', 'client_email','x'))
                ->setPaper('a4')
                ->download('estimation.pdf');


        

       return $pdf1;
    }

    public function service_agreement(Request $request){
        if( isset( $request->estimate_ids ) && ! empty($request->estimate_ids) ) {
            $ids = array_values(array_filter(explode(',',$request->estimate_ids)));
            $estimate_date = '';
            $service_id = '';
            $price = 0;
            $price_type = '';
            $html_array = [];
            $heading_array = [];
            for ($i=0; $i < count($ids); $i++) {
                $estimate = QuotationEstimation::find($ids[$i]);
                $estimate_date = $estimate->estimate_date;
                $service_id = $estimate->service_id;
                $price += (int)$estimate->price;
                $price_type = $estimate->price_type;

                if ( $service_id == 1 ){
                    $service = 'Web Development Service';
                } elseif ( $service_id == 2 ) {
                    $service = 'Graphics Designing Service';
                    $feature_price = $request->feature_value;
                } elseif ( $service_id == 5 ) {
                    $service = 'App Development Service';
                } elseif ( $service_id == 3 ) {
                    $service = 'Video Services';
                } elseif ( $service_id == 4 ) {
                    $service = 'Social Media';
                } elseif ( $service_id == 6 ) {
                    $service = 'Digital Marketing';
                } elseif ( $service_id == '7' ) {
                    $service = 'SEO and Marketing Services';
                }
                $heading_array[$i+1] = '<b>' . $service . '</b>';
                if( isset( $estimate->html ) && ! empty( $estimate->html )  ){
                    $html_array[] = '<table style="width: 650px;padding: 10px 15px;text-align: left;"><tr><th><u>' . $service . '</u></tr></th></table>'. $estimate->html;
                }
            }
            $price_40per = ( $price * 40 )/100;
            $price_60per = ( $price * 60 )/100;
            $service = '';
            $pdf2 = PDF::loadView('quotation/htmlToPDF/service-agreement',compact( 'estimate_date', 'service', 'price', 'price_type', 'price_40per', 'price_60per', 'html_array' ))->setPaper('a4')->download('service_agreement.pdf');
            return $pdf2;
        }
    }

    public function checkExistEstimateNo($estimation_no){
        $estimate_no = QuotationEstimation::where('estimate_no',$estimation_no)->first();
        if( $estimate_no ) {
            echo 0;
        } else {
            echo 1;
        }
    }
    public function generatePDF()
    {
        $pdf = PDF::loadView('quotation/htmlToPDF/test');
        return view('quotation/htmlToPDF/test');
        //return $pdf->download('test.pdf');
    }
    public function createHTMLForTable($main_servs, $features, $features_lists, $price_type, $price, $selectedDeliverables, $selectedAddfeatures, $selectedImportantNote, $improtantnote_lists, $additionalfeature_lists, $timelines,$x) {
        $return_variable = '';
        $colspan = 5;
        $linesPerPage = 10; // Number of lines per page
        $lineCount = 0; // Initialize line count
    
        if( $price_type != '$' ) {
            $price_type = '<img width="15px" height="15px" src="' . URL::to('/assets/images/rupees-50x50.png') . '">';
        }

        $return_variable .= '
        <table style="width: 650px;padding: 0 15px; margin-top:10px;">';
    
        $main_serv_count = count($main_servs);
        foreach ($main_servs as $index => $main_serv) {
            $return_variable .= '
                    
                    <tr>
                            <td colspan="12" style="text-align: center;padding: 5px 0; text-align: center;font-weight: 600;border-top: 2px solid #003b5d; border-bottom: 2px solid #003b5d; color: #003b5d; font-size: 16px;"><b> ' . $main_serv->name . '</b></td>
                        </tr>

                    
                    ';
    
            $lineCount += 2; // Account for section title and separator
    
            // Section 1: Features
            foreach ($features as $feature) {
                if ($feature->service_id == $main_serv->id) {
                    $return_variable .= '
                    <tr>
                        <td colspan="12"  style="font-weight: 600; font-size: 16px; text-align: center; width: 100%">' . $feature->feature_title . ' </td>
                        <td colspan="12" style="width: 100%;"></td>
                        <td colspan="3">
                            <table style="width: 450px;">';
    
                    foreach ($features_lists as $features_list) {
                        if ($features_list->feature_id == $feature->id) {
                            $lineCount++; // Account for feature item
                            if ($lineCount > $linesPerPage) {
                                // If adding this content exceeds the line limit, start a new page
                                $return_variable.='<div style="page-break-before: auto;"></div>';
                                $return_variable.='<div style="page-break-before: auto;"></div>';
                                $return_variable.='<div style="page-break-before: auto;"></div>';
                               
                                $lineCount = 1; // Reset line count
                            }
                            $return_variable .= '
                                <tr>
                                    <td colspan="12" style="padding: 10px 0px 3px 20px; border-bottom: 1px solid #bae5ff;">'. $features_list->feature_list .' </td>
                                </tr>';
                        }
                    }
    
                    $return_variable .= '
                            </table>
                        </td>';
    
                    if (isset($feature_price[$feature->id]) && !empty($feature_price[$feature->id])) {
                        $return_variable .= '
                        <td colspan="12" style="font-weight: 600; font-size: 16px; text-align: right; width: 140px">' . $price_type.' ' . number_format($feature_price[$feature->id], 2) . ' </td>';
                    }
    
                    $return_variable .= '</tr>
                    <tr><td colspan="12" style="border-bottom: 1px solid #a5a5a5;width:100%"></td></tr>';
                    $lineCount++; // Account for separator
                }
            }
    
            $return_variable .= '<tr>
            <td colspan="12" style="border-bottom: 50px transparent;width:100%"></td>
        </tr>';
    
            // Section 2: Additional Features
            if (isset($selectedAddfeatures->{$main_serv->id})) {
                $return_variable .= '
                        <tr>
                        <td colspan="12"
                        style="width: 100%; text-align: center; padding: 5px 0; text-align: center; font-weight: 600; border-top: 2px solid #003b5d; border-bottom: 2px solid #003b5d; color: #003b5d; font-size: 16px;">
                        <b>Additional Features</b>
                    </td>
                        </tr>';
    
                $additionalFeatureListIds = $selectedAddfeatures->{$main_serv->id};
    
                foreach ($additionalfeature_lists as $additionalFeatureList) {
                    if (in_array($additionalFeatureList->id, $additionalFeatureListIds)) {
                        $lineCount++; // Account for feature item
                        if ($lineCount > $linesPerPage) {
                            // If adding this content exceeds the line limit, start a new page
                            $return_variable.='<div style="page-break-before: auto;"></div>';
                            $return_variable.='<div style="page-break-before: auto;"></div>';
                            $return_variable.='<div style="page-break-before: auto;"></div>';
                           
                            $lineCount = 1; // Reset line count
                        }
                        $return_variable .= '
                            <tr>
                                <td colspan="12" style="padding: 10px 0px 3px 20px;">'. $additionalFeatureList->feature_name .' </td>
                            </tr>
                            <tr><td colspan="12" style="border-bottom: 1px solid #a5a5a5;"></td></tr>';
                    }
                }
            }
    
            $return_variable .= '<tr>
            <td colspan="12" style="border-bottom: 50px transparent;"></td>
        </tr>';
    
            // Section 3: Deliverables
            $return_variable .= '
            <tr>
            <td colspan="12"
            style="width: 100%; text-align: center; padding: 5px 0; text-align: center; font-weight: 600; border-top: 2px solid #003b5d; border-bottom: 2px solid #003b5d; color: #003b5d; font-size: 16px;">
            <b>Deliverables</b>
        </td>
            </tr>';
    
            foreach ($selectedDeliverables as $deliverable) {
                if ($deliverable->service_id == $main_serv->id) {
                    $lineCount++; // Account for deliverable item
                    if ($lineCount > $linesPerPage) {
                        // If adding this content exceeds the line limit, start a new page
                        $return_variable.='<div style="page-break-before: auto;"></div>';
                        $return_variable.='<div style="page-break-before: auto;"></div>';
                        $return_variable.='<div style="page-break-before: auto;"></div>';
                      
                        $lineCount = 1; // Reset line count
                    }
                    $return_variable .= '
                    <tr>
                        <td colspan="12" style="padding: 10px 0px 3px 20px;">'. $deliverable->name .' </td>
                    </tr>
                    <tr><td colspan="12" style="border-bottom: 1px solid #a5a5a5;"></td></tr>';
                }
            }
    
            $return_variable .= '<tr>
            <td colspan="12" style="border-bottom: 50px transparent;"></td>
        </tr>';
    
            // Section 4: Selected Important Notes
            if (isset($selectedImportantNote->{$main_serv->id})) {
                $return_variable .= '
                <tr>
                <td colspan="12"
                style="width: 100%; text-align: center; padding: 5px 0; text-align: center; font-weight: 600; border-top: 2px solid #003b5d; border-bottom: 2px solid #003b5d; color: #003b5d; font-size: 16px;">
                <b>Important Notes</b>
            </td>
                </tr>';
    
                $importantNoteIds = $selectedImportantNote->{$main_serv->id};
    
                foreach ($improtantnote_lists as $importantNoteList) {
                    if (in_array($importantNoteList->id, $importantNoteIds)) {
                        $lineCount++; // Account for important note item
                        if ($lineCount > $linesPerPage) {
                            // If adding this content exceeds the line limit, start a new page
                            $return_variable.='<div style="page-break-before: auto;"></div>';
                            $return_variable.='<div style="page-break-before: auto;"></div>';
                            $return_variable.='<div style="page-break-before: auto;"></div>';
                            
                            $lineCount = 1; // Reset line count
                        }
                        $return_variable .= '
                            <tr>
                                <td colspan="12" style="padding: 10px 0px 3px 20px;">'. $importantNoteList->note_name .' </td>
                            </tr>
                            <tr><td colspan="12" style="border-bottom: 1px solid #a5a5a5;"></td></tr>';
                    }
                }
            }
    
            $return_variable .= '<tr>
            <td colspan="12" style="border-bottom: 50px transparent;"></td>
        </tr>';
    
            // Section 5: Estimated Overall Service Time
            $return_variable .= '
            <tr>
                <td colspan="12"
                style="width: 100%; text-align: center; padding: 5px 0; text-align: center; font-weight: 600; border-top: 2px solid #003b5d; border-bottom: 2px solid #003b5d; color: #003b5d; font-size: 16px;">
                <b>Estimated Timeline</b>
                </td>
            </tr>';
    
            foreach ($timelines as $timeline) {
                if ($timeline->service_id == $main_serv->id) {
                    $lineCount++; // Account for timeline item
                    if ($lineCount > $linesPerPage) {
                        // If adding this content exceeds the line limit, start a new page
                        $return_variable.='<div style="page-break-before: auto;"></div>';
                        $return_variable.='<div style="page-break-before: auto;"></div>';
                        $return_variable.='<div style="page-break-before: auto;"></div>';
                       
                        $lineCount = 1; // Reset line count
                    }
                    $return_variable .= '
                    <tr>
                        <td colspan="12" style="padding: 10px 0px 3px 20px; border-bottom: 1px solid #bae5ff;">' .
                            e($timeline->content) .
                        '</td>
                    </tr>';
                }
            }
    
            $return_variable .= '<tr>
            <td colspan="12" style="border-bottom: 50px solid white;"></td>
        </tr>';
    
            // Section 6: Price, GST Price, and Total Price
            $subtotal = 0; // Initialize subtotal
            $gstPercentage = $x; // GST percentage
            $gstAmount = ($price * $gstPercentage) ; // Calculate GST amount
            $totalPrice = $price + floor($gstAmount); // Calculate total price
    
            $return_variable .= '
            <tr><td colspan="12" style="border-bottom: 1px solid #a5a5a5;"></td></tr>
            <tr>
                <td style="font-weight: 600; font-size: 16px; width: 140px; text-align: left;">Price</td>
                <td style="width: 20px;"></td>
                <td colspan="' . ($colspan + 1) . '" style="text-align:right">' . $price_type . ' ' . number_format($price, 2) . '</td>
            </tr>
            <tr><td colspan="12" style="border-bottom: 1px solid #a5a5a5;"></td></tr>
            <tr>
                <td style="font-weight: 600; font-size: 16px; width: 140px; text-align: left;">GST (' . $gstPercentage . '%)</td>
                <td style="width: 20px;"></td>
                <td colspan="' . ($colspan + 1) . '" style="text-align:right">' . $price_type . ' ' . number_format($gstAmount, 2) . '</td>
            </tr>
            <tr><td colspan="12" style="border-bottom: 1px solid #a5a5a5;"></td></tr>
            <tr>
                <td style="font-weight: 600; font-size: 16px; width: 140px; text-align: left;">Total</td>
                <td style="width: 20px;"></td>
                <td colspan="' . ($colspan + 1) . '" style="text-align:right">' . $price_type . ' ' . number_format($totalPrice, 2) . '</td>
            </tr>
            <tr><td colspan="12" style="border-bottom: 1px solid #a5a5a5;"></td></tr>';
            
    
            if ($index < $main_serv_count - 1) {
                $lineCount++; // Account for page break
                if ($lineCount > $linesPerPage) {
                    // If adding this content exceeds the line limit, start a new page
                    $return_variable.='<div style="page-break-before: auto;"></div>';
                    $return_variable.='<div style="page-break-before: auto;"></div>';
                    $return_variable.='<div style="page-break-before: auto;"></div>';
                    $lineCount = 1; // Reset line count
                }
            }
        }
    
        // Section 6: Total Price
        $subtotal = 0; // Initialize subtotal
        foreach ($main_servs as $main_serv) {
            $subtotal += floor($price * (1+$x));
        }
    
        $return_variable .= '<tr>
        <td colspan="12" style="border-bottom: 50px solid white;"></td>
    </tr>';
        $return_variable .= '
        <tr>
        <td colspan="12"
        style="width: 100%; text-align: center; padding: 5px 0; text-align: center; font-weight: 600; border-top: 2px solid #003b5d; border-bottom: 2px solid #003b5d; color: #003b5d; font-size: 16px;">
        <b>Total Price</b>
    </td>
        </tr>';
        $return_variable .= '
    <tr>
        <td style="font-weight: 600; font-size: 16px; width: 140px; text-align: left;">Total (Including GST) for selected services</td>
        <td style="width: 20px;"></td>
        <td colspan="' . ($colspan + 1) . '" style="text-align:right">'  . $price_type . ' ' . number_format($subtotal, 2) .  '</td>
    </tr>
    <tr><td colspan="12" style="border-bottom: 1px solid #a5a5a5;"></td></tr>';
    ;
    
        $return_variable .= '</tbody>
        </table>';
    
        return $return_variable;
    }
    
    
    
    
    
    

    public function downloadEstimation($id,Request $request){
        $estimate = QuotationEstimation::find($id);
        $features_lists_ids_json = $estimate->checked_features;
        $estimate_date = $estimate->estimate_date;
        $estimate_expire = $estimate->estimate_expire;
        $estimate_no = $estimate->estimate_no;
        $service_id = $estimate->service_id;
        $client_name = $estimate->client_name;
        $client_address = $estimate->client_address;
        $mobile_no = $estimate->mobile_no;
        $client_email = $estimate->client_email;

        $service = '';
        if ( $service_id == 1 ){
            $service = 'Web Development Service';
        } elseif ( $service_id == 2 ) {
            $service = 'Graphics Designing Service';
            // $feature_price = $request->feature_value;
        } elseif ( $service_id == 5 ) {
            $service = 'App Development Service';
        } elseif ( $service_id == 3 ) {
            $service = 'Video Services';
        } elseif ( $service_id == 4 ) {
            $service = 'Social Media';
        } elseif ( $service_id == 6 ) {
            $service = 'Digital Marketing';
        } elseif ( $service_id == '7' ) {
            $service = 'SEO and Marketing Services';
        }
        $price = $estimate->price;
        $price_type = $estimate->price_type;
        // $html = $estimate->html;
        $html = $estimate->html;
        $term_n_service = TermsAndCondition::all();

        $features_lists = QuotationFeaturesList::all();
        $features = QuotationFeatures::all();
        $main_cats = QuotationMainCategory::all();
        $features_lists_ids = array_map('intval', json_decode($features_lists_ids_json, true));
        $data = [];

        

        $pdf1 = PDF::loadView('quotation/htmlToPDF/estimation',compact('estimate_date', 'estimate_no', 'estimate_expire', 'service', 'service_id', 'price', 'price_type','term_n_service', 'html', 'client_name', 'client_address', 'mobile_no', 'client_email'),[
            'cache_path' => storage_path('app/pdf-cache'),
        ])->setPaper('a4')->download('estimation.pdf');
        // dd($pdf1);

        return $pdf1;
    }
    

    public function downloadEstimationInvoice($id,Request $request){
        $estimate = QuotationEstimation::find($id);
        $estimate_date = $estimate->estimate_date;
        $estimate_expire = $estimate->estimate_expire;
        $estimate_no = $estimate->estimate_no;
        $service_id = $estimate->service_id;
        $client_name = $estimate->client_name;
        $client_address = $estimate->client_address;
        $mobile_no = $estimate->mobile_no;
        $client_email = $estimate->client_email;
        $price = $estimate->price;
        $price_type = $estimate->price_type;
        $term_n_service = TermsAndCondition::all();

        $main_servs_ids = array_map('intval', explode(',', $service_id));
        $selectedServices = QuotationMainServices::whereIn('id', $main_servs_ids)->get();
        // dd($selectedServices);


        // dd($features);

        $selectedDeliverables = array_map('intval', json_decode($estimate->checked_deliverables, true));
        $deliverables = Deliverables::whereIn('id', $selectedDeliverables)->get();
        
        // dd($deliverables) ;


        $selectedImportantNote = json_decode($estimate->checked_important_note,true);
        $improtantnote_lists = QuotationImportantNotesList::all();
        // dd($improtantnote_lists);


        // Default GST percentage
        $gstPercentage = 18; // Set a default GST percentage

        // Determine GST percentage based on country symbol
        if ($price_type == '$') {
            // GST percentage for the United States
            $gstPercentage = 22;
        }

        if ($estimate->gst_type == "included") {
            $price = $price/(1+($gstPercentage/100));
            
            // Round the price to the nearest whole number without decimals
            $price = ceil($price);
        }

       
        $data = [];
        foreach ($selectedServices as $selectedService) {
            $serviceData = [
                'service_name' => $selectedService->name,
                'important_notes' => [],
                'deliverables' => [],
                'qty' => 1,
                'rate' => $price, 
                'taxRate' => $gstPercentage
            ];
    
            // Retrieve important notes for the selected service
            $importantNoteIds = $selectedImportantNote[$selectedService->id] ?? [];
            // dd($selectedImportantNotes[$selectedService->id]);
            foreach ($improtantnote_lists as $importantNoteList) {
                if (in_array($importantNoteList->id, $importantNoteIds)) {
                    $serviceData['important_notes'][] = [
                        'description' => $importantNoteList->note_name,
                    ];
                }
            }
    
            
    
            // Retrieve deliverables for the selected service (Please adjust the model name accordingly)
           
            foreach ($deliverables as $deliverable) {
                $serviceData['deliverables'][] = [
                    'name' => $deliverable->name,
                ];
            }
    
            // Add $serviceData to the $data array
            $data[] = $serviceData;
        }



        
        

        $pdf = PDF::loadView('quotation/htmlToPDF/invoice',compact('estimate_date', 'estimate_no', 'estimate_expire',  'service_id', 'price', 'price_type','term_n_service', 'client_name', 'client_address', 'mobile_no', 'client_email','data'))->setPaper('a4')->download('invoice-'.$id.'.pdf');
        $data['email'] = $client_email;
        $data['title'] = "Invoice of your estimation from neshallweb";
        $data['body'] = "Hi,".$client_name." we have attached the invoice pdf for your quotation,please go through it, 
        Regards neshallWeb";

    
        
    
        Mail::send('mails.mail', $data, function($msg) use ($data, $pdf) {
            $msg->to($data['email'])->subject($data['title']);
            $msg->attachData($pdf,'invoice.pdf');
        });

        return $pdf;
    }

    public function downloadServiceAgreement($id){
        $estimate = QuotationEstimation::find($id);
        $serviceagreementcontents = ServiceAgreementContent::all();
        $estimate_date = $estimate->estimate_date;
        $estimate_expire = $estimate->estimate_expire;
        $estimate_no = $estimate->estimate_no;
        $client_name = $estimate->client_name;
        $service = $estimate->service;
        $price = $estimate->price;
        $price_40per = ( $price*40 )/100;
        $price_60per = ( $price*60 )/100;
        $price_type = $estimate->price_type;
        $html = $estimate->html;
        $term_n_service = TermsAndCondition::all();
        $client_address = $estimate->client_address;
        $mobile_no = $estimate->mobile_no;
        $client_email = $estimate->client_email;

        $pdf2 = PDF::loadView('quotation/htmlToPDF/service-agreement',compact('estimate_date', 'estimate_no', 'estimate_expire', 'service', 'price', 'price_type', 'price_40per', 'price_60per','term_n_service', 'html','serviceagreementcontents','client_name', 'client_address','mobile_no','client_email' ))->setPaper('a4')->download('service_agreement.pdf');

        return $pdf2;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\QuotationEstimation  $quotationEstimation
     * @return \Illuminate\Http\Response
     */
    public function show(QuotationEstimation $quotationEstimation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\QuotationEstimation  $quotationEstimation
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $estimate = QuotationEstimation::find($id);
        $estimate_date = $estimate->estimate_date;
        $estimate_expire = $estimate->estimate_expire;
        $estimate_no = $estimate->estimate_no;
        $service_id = $estimate->service_id;
        $client_name = $estimate->client_name;
        $client_address = $estimate->client_address;
        $mobile_no = $estimate->mobile_no;
        $client_email = $estimate->client_email;
        $price = $estimate->price;
        $price_type = $estimate->price_type;
        $gst_type=$estimate->gst_type;
        $term_n_service = TermsAndCondition::all();
        $client_country = 'India'; // Set a default GST percentage
        $gsts = GstPercentage::all();

        // Determine GST percentage based on country symbol
        if ($price_type == '$') {
            // GST percentage for the United States
            $client_country= 'United States';
        }

        $features_lists = QuotationFeaturesList::all();
        $features = QuotationFeatures::all();
        $main_cats = QuotationMainCategory::all();
        $main_servs = QuotationMainServices::all();
        $deliverables = Deliverables::all();
        $importantNotes = QuotationImportantNote::all();
        $importantNoteLists = QuotationImportantNotesList::all();
        $additionalFeatures = QuotationAdditionalFeatures::all();
        $additionalFeatureLists = QuotationAdditionalFeaturesList::all();

        
        $main_servs_ids = array_map('intval', explode(',', $service_id));
        $selected_Services = QuotationMainServices::whereIn('id', $main_servs_ids)->get();
        // dd(json_encode($selected_Services));
        $features_json=array_map('intval', json_decode($estimate->checked_features, true));
        $selected_Features = QuotationFeatures::whereIn('id', $features_json)->get();
        $selectedDeliverables = array_map('intval', json_decode($estimate->checked_deliverables, true));
        $selected_Deliverables = Deliverables::whereIn('id', $selectedDeliverables)->get();
        $selected_Addfeatures = json_decode($estimate->checked_additional_features,true);
        $selected_ImportantNote = json_decode($estimate->checked_important_note,true);


        return view('quotation.estimation.edit', compact(
            'estimate_date',
            'estimate_expire',
            'estimate_no',
            'service_id',
            'client_name',
            'client_address',
            'mobile_no',
            'client_email',
            'client_country',
            'price',
            'price_type',
            'gst_type',
            'term_n_service',
            'features',
            'deliverables',
            'additionalFeatures',
            'additionalFeatureLists',
            'importantNotes',
            'selected_Services',
            'selected_Features',
            'selected_Deliverables',
            'selected_Addfeatures',
            'selected_ImportantNote',
            'features_lists',
            'main_cats',
            'main_servs',
            'importantNoteLists',
            'gsts'
        ));
    }


    public function editstore(Request $request)
    {
        //
        $request->validate([
            'estimate_date' => 'required',
            'estimate_no' => 'required',
            'estimate_expire' => 'required',
            'service_id' => 'required',
            'price' => 'required',
            'price_type' => 'required',
        ]);
        $estimate_date = date('Y-m-d', strtotime($request->estimate_date));
        $estimate_expire = date('Y-m-d', strtotime($request->estimate_expire));
        $client_name = $request->client_name;
        $client_address = $request->client_address;
        $mobile_no = $request->mobile_no;
        $client_email = $request->client_email;
        $ifExists = QuotationEstimation::find($request->estimate_id);
        if( isset( $ifExists->id ) && ! empty( $ifExists->id ) ) {
            $estimate = QuotationEstimation::find($ifExists->id);
            $estimate->estimate_date = $estimate_date;
            $estimate->estimate_no = $request->estimate_no;
            $estimate->estimate_expire = $estimate_expire;
            $estimate->service_id = $request->service_id;
            $estimate->price = $request->price;
            $estimate->price_type = $request->price_type;
            $estimate->client_name = $request->client_name;
            $estimate->client_address = $request->client_address;
            $estimate->mobile_no = $request->mobile_no;
            $estimate->client_email = $request->client_email;
            $estimate->save();
        } else {
            $estimate = new QuotationEstimation;
            $estimate->estimate_date = $estimate_date;
            $estimate->estimate_no = $request->estimate_no;
            $estimate->estimate_expire = $estimate_expire;
            $estimate->service_id = $request->service_id;
            $estimate->price = $request->price;
            $estimate->price_type = $request->price_type;
            $estimate->client_name = $request->client_name;
            $estimate->client_address = $request->client_address;
            $estimate->mobile_no = $request->mobile_no;
            $estimate->client_email = $request->client_email;
            $estimate->save();
        }

        $estimate_date = $request->estimate_date;
        $estimate_no = $request->estimate_no;
        $estimate_expire = $request->estimate_expire;
        $service_id = $request->service_id;
        $price = $request->price;
        $price_40per = ( $price*40 )/100;
        $price_60per = ( $price*60 )/100;
        $price_type = $request->price_type;
        $service = '';
        $feature_price = [];
        if ( $service_id == 1 ){
            $service = 'Web Development Service';
        } elseif ( $service_id == 2 ) {
            $service = 'Graphics Designing Service';
            $feature_price = $request->feature_value;
        } elseif ( $service_id == 5 ) {
            $service = 'App Development Service';
            $feature_price = $request->feature_value;
        } elseif ( $service_id == 3 ) {
            $service = 'Video Services';
        } elseif ( $service_id == 4 ) {
            $service = 'Social Media';
            $feature_price = $request->feature_value;
        } elseif ( $service_id == 6 ) {
            $service = 'Digital Marketing';
            $feature_price = $request->feature_value;
        } elseif ( $service_id == '7' ) {
            $service = 'SEO and Marketing Services';
        }

        $feature_orders_main = $request->feature_orders_main;
        $featurelist_remove = $request->featurelist_remove;
        $feature_orders_main_arr = explode(' ',$feature_orders_main);
        $featurelist_remove_arr = explode(' ',$featurelist_remove);

        $main_cat_ids = [];
        $feature_ids = [];
        $featurelist_remove_ids = [];


        for ( $i = 0; $i < count($feature_orders_main_arr); $i++ ) {
            if( strpos($feature_orders_main_arr[$i],"cat") ) {
                $main_cat_ids[$i] = str_replace('[[cat_','',$feature_orders_main_arr[$i]);
                $main_cat_ids[$i] = str_replace(']]','',$main_cat_ids[$i]);
            }
            if( strpos($feature_orders_main_arr[$i],"feature") ) {
                $feature_ids[$i] = str_replace('[[feature_', '', $feature_orders_main_arr[$i]);
                $feature_ids[$i] = str_replace(']]', '', $feature_ids[$i]);
            }
        }

        for ($i = 0; $i < count($featurelist_remove_arr); $i++) {
            if( strpos($featurelist_remove_arr[$i],"featurelist") ) {
                $featurelist_remove_ids[$i] = str_replace('[[featurelist_','',$featurelist_remove_arr[$i]);
                $featurelist_remove_ids[$i] = str_replace(']]','',$featurelist_remove_ids[$i]);
            }
        }
        $featurelist_remove_ids = array_values($featurelist_remove_ids);
        $feature_ids = array_values($feature_ids);

        $features_lists = QuotationFeaturesList::all();
        $features = QuotationFeatures::whereIn('id', $feature_ids)->get();
        $main_cats = QuotationMainCategory::whereIn('id', $main_cat_ids)->get();
        $i = 0;

        $feature_order = $features;
        unset($feature_order);
        for ($i=0; $i < count($feature_ids); $i++) { 
            foreach ( $features as $feature ) {
                if( isset( $feature->id ) && isset( $feature_ids[$i] ) ) {
                    if ($feature->id == $feature_ids[$i]) {
                        $feature_order[] = $feature;
                        continue 2;
                     }
                }
            }
        }

        $features = $feature_order;
        $term_n_service = TermsAndCondition::all();

        $html = $this->createHTMLForTable($main_cats, $features, $features_lists, $featurelist_remove_ids, $price_type, $price, $feature_price );
        $html_json = array(
            'feature_ids' => $feature_orders_main,
            'featurelist_remove_ids' => $featurelist_remove
        );
        $estimate = QuotationEstimation::find($estimate->id);
        $estimate->html = $html;
        $estimate->save();

        //$pdf = PDF::loadView('quotation/htmlToPDF/estimation',compact('features_lists' , 'features', 'main_cats','estimate_date', 'estimate_no', 'estimate_expire', 'service'));
        // return View('quotation/htmlToPDF/estimation',compact('features_lists' , 'features', 'main_cats','estimate_date', 'estimate_no', 'estimate_expire', 'service', 'service_id','main_cat_ids', 'feature_ids', 'featurelist_remove_ids', 'price', 'price_type','term_n_service', 'html'));
        //return view('quotation/htmlToPDF/estimation',compact('features_lists' , 'features', 'main_cats','estimate_date', 'estimate_no', 'estimate_expire', 'service', 'service_id','main_cat_ids', 'feature_ids', 'featurelist_remove_ids', 'price', 'price_type','term_n_service', 'html', 'client_name', 'client_address', 'mobile_no', 'client_email'));
        $pdf1 = PDF::loadView('quotation/htmlToPDF/estimation',compact('features_lists' , 'features', 'main_cats','estimate_date', 'estimate_no', 'estimate_expire', 'service', 'service_id','main_cat_ids', 'feature_ids', 'featurelist_remove_ids', 'price', 'price_type','term_n_service', 'html', 'client_name', 'client_address', 'mobile_no', 'client_email'))->setPaper('a4')->download('Estimation-'.$estimate_no.'.pdf');
        // $pdf2 = PDF::loadView('quotation/htmlToPDF/service-agreement',compact('features_lists' , 'features', 'main_cats','estimate_date', 'estimate_no', 'estimate_expire', 'service','main_cat_ids', 'feature_ids', 'featurelist_remove_ids', 'price', 'price_type', 'price_40per', 'price_60per','term_n_service', 'html', 'client_name', 'client_address', 'mobile_no', 'client_email' ))->setPaper('a4')->download('Service-Agreement-'.$estimate_no.'.pdf');
        return $pdf1;
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\QuotationEstimation  $quotationEstimation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, QuotationEstimation $quotationEstimation)
    {
        //
    }

     
    public function destroy($id)
    {
        $estimate = QuotationEstimation::find($id);
        $estimate->delete();
        return Redirect::route('estimation-index')->with('success', 'Deleted successfully !');
    }
    
}

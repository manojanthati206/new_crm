<?php

namespace App\Http\Controllers;

use App\Models\QuotationMainCategory;
use Illuminate\Http\Request;

class QuotationMainCatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $main_cats = QuotationMainCategory::all();
        return view('quotation.main-category.index',compact('main_cats'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('quotation.main-category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'title'         => 'required',
        ]);
        $main_cat = QuotationMainCategory::create($request->all());
        return \Redirect::route('main-category-index')->with('success', 'Created Successfully !');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\QuotationMainCategory  $quotationMainCategory
     * @return \Illuminate\Http\Response
     */
    public function show(QuotationMainCategory $quotationMainCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\QuotationMainCategory  $quotationMainCategory
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $main_cat = QuotationMainCategory::find($id);
        return view('quotation.main-category.edit', compact('main_cat'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\QuotationMainCategory  $quotationMainCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, QuotationMainCategory $quotationMainCategory)
    {
        //
        $request->validate([
            'title'         => 'required',
        ]);
        $quotation = QuotationMainCategory::find($request->id);
        $quotation->title = $request->title;
        $quotation->save();
        return \Redirect::route('main-category-index')->with('success', 'Update Successfully !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\QuotationMainCategory  $quotationMainCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $quotation = QuotationMainCategory::find($id);
        $quotation->delete();
        return \Redirect::route('main-category-index')->with('success', 'Deleted successfully !');
    }
}

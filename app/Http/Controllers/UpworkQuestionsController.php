<?php

namespace App\Http\Controllers;

use App\Models\UpworkQuestions;
use Illuminate\Http\Request;

class UpworkQuestionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $questions = UpworkQuestions::all();
        return view('upwork.questions.index',compact('questions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('upwork.questions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'question'         => 'required',
        ]);
        $question = UpworkQuestions::create($request->all());
        return \Redirect::route('upwork-questions-index')->with('success', 'Created Successfully !');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\UpworkQuestions  $upworkQuestions
     * @return \Illuminate\Http\Response
     */
    public function show(UpworkQuestions $upworkQuestions)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\UpworkQuestions  $upworkQuestions
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $question = UpworkQuestions::find($id);
        return view('upwork.questions.edit', compact('question'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\UpworkQuestions  $upworkQuestions
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UpworkQuestions $upworkQuestions)
    {
        //
        $request->validate([
            'question'         => 'required',
        ]);
        $question = UpworkQuestions::find($request->id);
        $question->question = $request->question;
        $question->save();
        return \Redirect::route('upwork-questions-index')->with('success', 'Update Successfully !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\UpworkQuestions  $upworkQuestions
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $question = UpworkQuestions::find($id);
        $question->delete();
        return \Redirect::route('upwork-questions-index')->with('success', 'Deleted successfully !');
    }
}

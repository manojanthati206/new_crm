<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\QuotationFeatures;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\QuotationFeaturesList;
use App\Models\QuotationMainCategory;

class QuotationEstimationTemplateController extends Controller
{
    public function index()
    {
        // Your code for the index method
        return view('quotation.estimation_templates.index');
    }
    public function digitmarket()
    {
        
        
        $ids=[];
        $features = QuotationFeatures::whereIn('id', $ids)->get();
        $features_lists = QuotationFeaturesList::whereIn('feature_id', $ids)->get();
        $main_cats = QuotationMainCategory::all();
        $current_date = date('d F, Y');
        $expire_date = date('d F, Y', strtotime("+30 days"));
        $estimate_no =  DB::table('nw_estimation')->latest('id')->first()->estimate_no;
        $next_estimate_no = (int) str_replace('nW-ET', '', $estimate_no)+1;
        $next_estimate_no = (string) 'nW-ET'.$next_estimate_no;
        return view('quotation.estimation_templates.Digital_marketing.create',compact('features_lists' , 'features', 'main_cats', 'current_date', 'expire_date', 'next_estimate_no'));
    }
    public function graphdesign()
    {
        $ids=[6,7,8,9,14,22,23,24,25,26,27,28,29];
        $features = QuotationFeatures::whereIn('id', $ids)->get();
        $features_lists = QuotationFeaturesList::whereIn('feature_id', $ids)->get();
        $main_cats = QuotationMainCategory::all();
        $current_date = date('d F, Y');
        $expire_date = date('d F, Y', strtotime("+30 days"));
        $estimate_no =  DB::table('nw_estimation')->latest('id')->first()->estimate_no;
        $next_estimate_no = (int) str_replace('nW-ET', '', $estimate_no)+1;
        $next_estimate_no = (string) 'nW-ET'.$next_estimate_no;
        return view('quotation.estimation_templates.Graphics_design.create',compact('features_lists' , 'features', 'main_cats', 'current_date', 'expire_date', 'next_estimate_no'));
    }
    public function mobapp()
    {
        $ids=[10,11,12,13,15];
        $features = QuotationFeatures::whereIn('id', $ids)->get();
        $features_lists = QuotationFeaturesList::whereIn('feature_id', $ids)->get();
        $main_cats = QuotationMainCategory::all();
        $current_date = date('d F, Y');
        $expire_date = date('d F, Y', strtotime("+30 days"));
        $estimate_no =  DB::table('nw_estimation')->latest('id')->first()->estimate_no;
        $next_estimate_no = (int) str_replace('nW-ET', '', $estimate_no)+1;
        $next_estimate_no = (string) 'nW-ET'.$next_estimate_no;
        return view('quotation.estimation_templates.Mobile_application.create',compact('features_lists' , 'features', 'main_cats', 'current_date', 'expire_date', 'next_estimate_no'));
    }
    public function seo()
    {
        $ids=[];
        $features = QuotationFeatures::whereIn('id', $ids)->get();
        $features_lists = QuotationFeaturesList::whereIn('feature_id', $ids)->get();
        $main_cats = QuotationMainCategory::all();
        $current_date = date('d F, Y');
        $expire_date = date('d F, Y', strtotime("+30 days"));
        $estimate_no =  DB::table('nw_estimation')->latest('id')->first()->estimate_no;
        $next_estimate_no = (int) str_replace('nW-ET', '', $estimate_no)+1;
        $next_estimate_no = (string) 'nW-ET'.$next_estimate_no;
        return view('quotation.estimation_templates.Seo_and_marketingservices.create',compact('features_lists' , 'features', 'main_cats', 'current_date', 'expire_date', 'next_estimate_no'));
    }
    public function social()
    {
        $ids=[18,19];
        $features = QuotationFeatures::whereIn('id', $ids)->get();
        $features_lists = QuotationFeaturesList::whereIn('feature_id', $ids)->get();
        $main_cats = QuotationMainCategory::all();
        $current_date = date('d F, Y');
        $expire_date = date('d F, Y', strtotime("+30 days"));
        $estimate_no =  DB::table('nw_estimation')->latest('id')->first()->estimate_no;
        $next_estimate_no = (int) str_replace('nW-ET', '', $estimate_no)+1;
        $next_estimate_no = (string) 'nW-ET'.$next_estimate_no;
        return view('quotation.estimation_templates.Social_media.create',compact('features_lists' , 'features', 'main_cats', 'current_date', 'expire_date', 'next_estimate_no'));
    }
    public function video()
    {
        $ids=[30,31,32,33,34];
        $features = QuotationFeatures::whereIn('id', $ids)->get();
        $features_lists = QuotationFeaturesList::whereIn('feature_id', $ids)->get();
        $main_cats = QuotationMainCategory::all();  
        $current_date = date('d F, Y');
        $expire_date = date('d F, Y', strtotime("+30 days"));
        $estimate_no =  DB::table('nw_estimation')->latest('id')->first()->estimate_no;
        $next_estimate_no = (int) str_replace('nW-ET', '', $estimate_no)+1;
        $next_estimate_no = (string) 'nW-ET'.$next_estimate_no;
        return view('quotation.estimation_templates.Video_services.create',compact('features_lists' , 'features', 'main_cats', 'current_date', 'expire_date', 'next_estimate_no'));
    }
    public function webdev()
    {
        $ids=[2,3,4,5,20,16,17];
        $features = QuotationFeatures::whereIn('id', $ids)->get();
        $features_lists = QuotationFeaturesList::all();
        $main_cats = QuotationMainCategory::all();
        $current_date = date('d F, Y');
        $expire_date = date('d F, Y', strtotime("+30 days"));
        $estimate_no =  DB::table('nw_estimation')->latest('id')->first()->estimate_no;
        $next_estimate_no = (int) str_replace('nW-ET', '', $estimate_no)+1;
        $next_estimate_no = (string) 'nW-ET'.$next_estimate_no;
        return view('quotation.estimation_templates.web_development.create',compact('features_lists' , 'features', 'main_cats', 'current_date', 'expire_date', 'next_estimate_no'));
    }
}

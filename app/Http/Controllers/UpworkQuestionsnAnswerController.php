<?php

namespace App\Http\Controllers;

use App\Models\UpworkProjects;
use App\Models\UpworkQuestionnAnswer;
use App\Models\UpworkQuestions;
use Illuminate\Http\Request;

class UpworkQuestionsnAnswerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $question_n_answers = UpworkQuestionnAnswer::all();
        $projects = UpworkProjects::all();
        $questions = UpworkQuestions::all();
        return view('upwork.question-n-answer.index',compact('question_n_answers','projects', 'questions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $projects = UpworkProjects::all();
        $questions = UpworkQuestions::all();
        return view('upwork.question-n-answer.create',compact('projects', 'questions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'upwork_project_id'         => 'required',
            'upwork_question_id'         => 'required',
            'answer'         => 'required',
        ]);
        $question_n_answers = UpworkQuestionnAnswer::create($request->all());
        return \Redirect::route('upwork-question-n-answer-index')->with('success', 'Created Successfully !');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\UpworkQuestionnAnswer  $upworkQuestionnAnswer
     * @return \Illuminate\Http\Response
     */
    public function show(UpworkQuestionnAnswer $upworkQuestionnAnswer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\UpworkQuestionnAnswer  $upworkQuestionnAnswer
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $question_n_answer = UpworkQuestionnAnswer::find($id);
        $projects = UpworkProjects::all();
        $questions = UpworkQuestions::all();
        return view('upwork.question-n-answer.edit', compact('question_n_answer','projects', 'questions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\UpworkQuestionnAnswer  $upworkQuestionnAnswer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UpworkQuestionnAnswer $upworkQuestionnAnswer)
    {
        //
        $request->validate([
            'upwork_project_id'     => 'required',
            'upwork_question_id'    => 'required',
            'answer'                => 'required',
        ]);
        $question_n_answers = UpworkQuestionnAnswer::find($request->id);
        $question_n_answers->upwork_project_id = $request->upwork_project_id;
        $question_n_answers->upwork_question_id = $request->upwork_question_id;
        $question_n_answers->answer = $request->answer;
        $question_n_answers->save();
        return \Redirect::route('upwork-question-n-answer-index')->with('success', 'Update Successfully !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\UpworkQuestionnAnswer  $upworkQuestionnAnswer
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $question_n_answers = UpworkQuestionnAnswer::find($id);
        $question_n_answers->delete();
        return \Redirect::route('upwork-question-n-answer-index')->with('success', 'Deleted successfully !');
    }
}

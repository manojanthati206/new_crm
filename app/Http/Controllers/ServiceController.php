<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\MainServicesModel;
use App\Models\ServiceModel;

class ServiceController extends Controller
{
    // fetch data from Services table and show index page.

    public function index()
    {
        $services = ServiceModel::join('nw_main_services', 'nw_main_services.id', '=', 'nw_sub_service.mainserviceid')->get(['nw_main_services.name AS service_name', 'nw_sub_service.*']);
        return view('acquisition.services.index', compact('services'));
    }

    // Add services page

    public function create()
    {
        $mainservices = MainServicesModel::all();

        return view('acquisition.services.create', compact('mainservices'));
    }

    // Store services data method

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'mainserviceid' => 'required',
            'img' => 'mimes:jpg,png,gif,jpeg|max:2048',
        ]);

        $img = $request->file('img');

        if ($img) {
            $fileTitle = 'service_' . time() . '.' . $request->file('img')->getClientOriginalExtension();
            $request->file('img')->move(public_path('uploads'), $fileTitle);
            $img = 'uploads/' . $fileTitle;
        }

        $service = new ServiceModel();
        $service->name = $request->input('name');
        $service->mainserviceid = $request->input('mainserviceid');
        $service->img = $img;
        $service->save();
        return \Redirect::route('services-index')->with('success', 'Created Successfully !');
    }

    // Edit data fetach

    public function edit($id)
    {
        $service = ServiceModel::find($id);
        $mainservices = MainServicesModel::all();
        return view('acquisition.services.edit', compact('service'), compact('mainservices'), compact('mainservices'));
    }

    // update data  to store

    public function update(Request $request, ServiceModel $ServiceModel)
    {
        $request->validate([
            'name' => 'required',
            'mainserviceid' => 'required',
            'img' => 'mimes:jpg,png,gif,jpeg|max:2048',
        ]);

        if ($request->hasfile('img')) {
            $fileTitle = 'service_' . time() . '.' . $request->file('img')->getClientOriginalExtension();
            $request->file('img')->move(public_path('uploads'), $fileTitle);
            $img = 'uploads/' . $fileTitle;
        } else {
            $img = $request->input('oldimg');
        }

        $service = ServiceModel::find($request->id);
        $service->name = $request->input('name');
        $service->mainserviceid = $request->input('mainserviceid');
        $service->img = $img;
        $service->save();
        return \Redirect::route('services-index')->with('success', 'Created Successfully !');
    }

    // Delete services

    public function destroy($id)
    {
        $feature = ServiceModel::find($id);
        $feature->delete();
        return \Redirect::route('services-index')->with('success', 'Deleted successfully !');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ServiceAgreement;
use App\Models\ServiceAgreementContent;
use App\Models\QuotationEstimation;
use Illuminate\Support\Facades\DB;
use App\Mail\ServiceAgreementPDFMail;
use App\Mail\MyDemoMail;
use App\Mail\ClientServiceAgreementMail;
use PDF;
use Storage;
use Mail;

class ServiceAgreementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $serviceagreements = ServiceAgreement::all();
        return view('quotation.service-agreement.index',compact('serviceagreements'));
    }
    public function myDemoMail()
    {
        $myEmail = 'patelrah1991@gmail.com';
        Mail::to($myEmail)->send(new MyDemoMail());
        //$client_email = 'abdulazizok4@gmail.com';
        // $service_data = ['estimate_ids'=>37,'client_email'=>$client_email,'no_of_client'=>1];
        // $mailsend=Mail::to($client_email)->send(new ClientServiceAgreementMail($service_data));
        dd("Mail Send Successfully");
    }
    public function sendServiceAgreement(Request $request){
        $estimate_ids = $request->estimate_ids;
        $client_email = $request->client_email;
        $no_of_client = $request->no_of_client;
        $message = "
        <html>
        <head>
        <title>HTML email</title>
        </head>
        <body>
        <p>Please go to the below link for the Service Agreement.</p>
        <a href='". route('service-agreement-signature',['estimate_ids'=>$estimate_ids,'client_email'=>$client_email,'no_of_client'=>$no_of_client]) ."'> Click here</a>
        </body>
        </html>
        ";
        $service_data = ['estimate_ids'=>$estimate_ids,'client_email'=>$client_email,'no_of_client'=>$no_of_client];
        $mailsend=Mail::to($client_email)->cc('shyamli.saxena@neshallweb.com')->bcc('nirav.tandel@neshallweb.com')->send(new ClientServiceAgreementMail($service_data));
        // Always set content-type when sending HTML email
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

        // More headers
        $headers .= 'From: <neshallweb@yopmail.com>' . "\r\n";

        //$mailsend =  mail($client_email,"Link",$message,$headers);
        if($mailsend) {
            return \Redirect::route('estimation-index')->with('success', 'Mail has sent successfully !');
        } else {
            return \Redirect::route('estimation-index')->with('fail', 'Something went wrong!');
        }
        //return \Redirect::route('service-agreement-signature',[]);
    }
    /**
     * Show the form for creating a new resource.
     * 
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $estimate_numbers = '';$estimate_ids = '';
        $client_email = $request->client_email;
        $no_of_client = $request->no_of_client;
        if( isset( $request->estimate_ids ) && ! empty($request->estimate_ids) ) {
            $estimate_ids = $request->estimate_ids;
            $ids = array_values(array_filter(explode(',',$request->estimate_ids)));
            for ($i=0; $i < count($ids); $i++) {
                $estimate = QuotationEstimation::find($ids[$i]);
                if (isset($estimate->estimate_no)) {
                    $estimate_numbers .= ' '.$estimate->estimate_no;
                }
            }
        }
        $estimate_numbers = trim($estimate_numbers);

        $serviceagreementcontents = ServiceAgreementContent::all();
        $estimate_date = date("d F, Y");
        $ips = $request->ip();
        $ids = array_values(array_filter(explode(',',$request->estimate_ids)));
        $estimate_date = '';
        $service_id = '';
        $price = 0;
        $price_type = '';
        $html_array = [];
        $heading_array = [];
        $client_name = '';
        $client_address = '';
        $mobile_no = '';
        $client_email = '';
        $price = 0; $price_40per = 0; $price_60per = 0; $price_type = '';
        for ($i=0; $i < count($ids); $i++) {
            $estimate = QuotationEstimation::find($ids[$i]);
            if (isset($estimate->estimate_date))
                $estimate_date = $estimate->estimate_date;
            if (isset($estimate->service_id))
                $service_id = $estimate->service_id;
            if (isset($estimate->price))
                $price += (int)$estimate->price;
            if (isset($estimate->price_type))
                $price_type = $estimate->price_type;

            if ( $service_id == 1 ){
                $service = 'Web Development Service';
            } elseif ( $service_id == 2 ) {
                $service = 'Graphics Designing Service';
                $feature_price = $request->feature_value;
            } elseif ( $service_id == 5 ) {
                $service = 'App Development Service';
                $feature_price = $request->feature_value;
            } elseif ( $service_id == 3 ) {
                $service = 'Video Services';
            } elseif ( $service_id == 4 ) {
                $service = 'Social Media';
                $feature_price = $request->feature_value;
            } elseif ( $service_id == 6 ) {
                $service = 'Digital Marketing';
                $feature_price = $request->feature_value;
            } elseif ( $service_id == '7' ) {
                $service = 'SEO and Marketing Services';
            }

            if (isset($estimate->client_name))
                $client_name = $estimate->client_name;
            if (isset($estimate->client_address))
                $client_address = $estimate->client_address;
            if (isset($estimate->mobile_no))
                $mobile_no = $estimate->mobile_no;
            if (isset($estimate->client_email))
                $client_email = $estimate->client_email;

            $heading_array[$i+1] = '<b>' . $service . '</b>';
            if( isset( $estimate->html ) && ! empty( $estimate->html )  ){
                $html_array[] = '<table style="width: 650px;padding: 10px 15px;text-align: left;"><tr><th><u>' . $service . '</u></tr></th></table>'. $estimate->html.'</table>';
            }
        }
        $price_40per = ( $price*40 )/100;
        $price_60per = ( $price*60 )/100;
        $price_35per = ( $price*35 )/100;
        $price_30per = ( $price*30 )/100;
        return view('quotation.service-agreement.test',compact('estimate_numbers','estimate_ids', 'estimate_date','ips','html_array', 'price', 'price_type', 'price_40per', 'price_60per', 'price_35per', 'price_30per','client_email','no_of_client','client_name','client_address','mobile_no','client_email','serviceagreementcontents'));
    }

    public function downloadServiceAgreement(Request $request){
        $estimate_numbers = '';$estimate_ids = '';
        $client_email = $request->client_email;
        $no_of_client = $request->no_of_client;
        if( isset( $request->estimate_ids ) && ! empty($request->estimate_ids) ) {
            $estimate_ids = $request->estimate_ids;
            $ids = array_values(array_filter(explode(',',$request->estimate_ids)));
            for ($i=0; $i < count($ids); $i++) {
                $estimate = QuotationEstimation::find($ids[$i]);
                if (isset($estimate->estimate_no)) {
                    $estimate_numbers .= ' '.$estimate->estimate_no;
                }
            }
        }
        $estimate_numbers = trim($estimate_numbers);

        $serviceagreementcontents = ServiceAgreementContent::all();
        $estimate_date = date("d F, Y");
        $ips = $request->ip();
        $ids = array_values(array_filter(explode(',',$request->estimate_ids)));
        $estimate_date = '';
        $service_id = '';
        $price = 0;
        $price_type = '';
        $html_array = [];
        $heading_array = [];
        $client_name = '';
        $client_address = '';
        $mobile_no = '';
        $client_email = '';
        $price = 0; $price_40per = 0; $price_60per = 0; $price_type = '';
        for ($i=0; $i < count($ids); $i++) {
            $estimate = QuotationEstimation::find($ids[$i]);
            if (isset($estimate->estimate_date))
                $estimate_date = $estimate->estimate_date;
            if (isset($estimate->service_id))
                $service_id = $estimate->service_id;
            if (isset($estimate->price))
                $price += (int)$estimate->price;
            if (isset($estimate->price_type))
                $price_type = $estimate->price_type;

            if ( $service_id == 1 ){
                $service = 'Web Development Service';
            } elseif ( $service_id == 2 ) {
                $service = 'Graphics Designing Service';
                $feature_price = $request->feature_value;
            } elseif ( $service_id == 5 ) {
                $service = 'App Development Service';
                $feature_price = $request->feature_value;
            } elseif ( $service_id == 3 ) {
                $service = 'Video Services';
            } elseif ( $service_id == 4 ) {
                $service = "Social Media";
                $feature_price = $request->feature_value;
            } elseif ( $service_id == 6 ) {
                $service = 'Digital Marketing';
                $feature_price = $request->feature_value;
            } elseif ( $service_id == '7' ) {
                $service = 'SEO and Marketing Services';
            }
            if (isset($estimate->client_name))
                $client_name = $estimate->client_name;
            if (isset($estimate->client_address))
                $client_address = $estimate->client_address;
            if (isset($estimate->mobile_no))
                $mobile_no = $estimate->mobile_no;
            if (isset($estimate->client_email))
                $client_email = $estimate->client_email;

            $heading_array[$i+1] = '<b>' . $service . '</b>';
            if( isset( $estimate->html ) && ! empty( $estimate->html )  ){
                $html_array[] = '<table style="width: 650px;padding: 10px 15px;text-align: left;"><tr><th><u>' . $service . '</u></tr></th></table>'. $estimate->html.'</table>';
            }
        }
        $price -= 13;
        $price_40per = ( $price*40 )/100;
        $price_60per = ( $price*60 )/100;
        $price_35per = ( $price*35 )/100;
        $price_30per = ( $price*30 )/100;

        /*$estimate = QuotationEstimation::find($id);
        $serviceagreementcontents = ServiceAgreementContent::all();
        $estimate_date = $estimate->estimate_date;
        $estimate_expire = $estimate->estimate_expire;
        $estimate_no = $estimate->estimate_no;
        $client_name = $estimate->client_name;
        $service = $estimate->service;
        $price = $estimate->price;
        $price_40per = ( $price*40 )/100;
        $price_60per = ( $price*60 )/100;
        $price_type = $estimate->price_type;
        $html = $estimate->html;
        $term_n_service = TermsAndCondition::all();
        $client_address = $estimate->client_address;
        $mobile_no = $estimate->mobile_no;
        $client_email = $estimate->client_email;*/

        $pdf2 = PDF::loadView('quotation/htmlToPDF/service-agreement-multi',compact('estimate_numbers','estimate_ids', 'estimate_date','ips','html_array', 'price', 'price_type', 'price_40per', 'price_60per', 'price_35per', 'price_30per','client_email','no_of_client','client_name','client_address','mobile_no','client_email','serviceagreementcontents'))->setPaper('a4')->download('service_agreement.pdf');
        //return view('quotation/htmlToPDF/service-agreement-multi',compact('estimate_numbers','estimate_ids', 'estimate_date','ips','html_array', 'price', 'price_type', 'price_40per', 'price_60per', 'price_35per', 'price_30per','client_email','no_of_client','client_name','client_address','mobile_no','client_email','serviceagreementcontents'));
        return $pdf2;
    }
    public function readonly(Request $request)
    {
        $estimate_numbers = '';$estimate_ids = '';
        $client_email = $request->client_email;
        $no_of_client = $request->no_of_client;
        if( isset( $request->estimate_ids ) && ! empty($request->estimate_ids) ) {
            $estimate_ids = $request->estimate_ids;
            $ids = array_values(array_filter(explode(',',$request->estimate_ids)));
            for ($i=0; $i < count($ids); $i++) {
                $estimate = QuotationEstimation::find($ids[$i]);
                if (isset($estimate->estimate_no)) {
                    $estimate_numbers .= ' '.$estimate->estimate_no;
                }
            }
        }
        $estimate_numbers = trim($estimate_numbers);

        $serviceagreementcontents = ServiceAgreementContent::all();
        $estimate_date = date("d F, Y");
        $ips = $request->ip();
        $ids = array_values(array_filter(explode(',',$request->estimate_ids)));
        $estimate_date = '';
        $service_id = '';
        $price = 0;
        $price_type = '';
        $html_array = [];
        $heading_array = [];
        $client_name = '';
        $client_address = '';
        $mobile_no = '';
        $client_email = '';
        $price = 0; $price_40per = 0; $price_60per = 0; $price_type = '';
        for ($i=0; $i < count($ids); $i++) {
            $estimate = QuotationEstimation::find($ids[$i]);
            if (isset($estimate->estimate_date))
                $estimate_date = $estimate->estimate_date;
            if (isset($estimate->service_id))
                $service_id = $estimate->service_id;
            if (isset($estimate->price))
                $price += (int)$estimate->price;
            if (isset($estimate->price_type))
                $price_type = $estimate->price_type;

            if ( $service_id == 1 ){
                $service = 'Web Development Service';
            } elseif ( $service_id == 2 ) {
                $service = 'Graphics Designing Service';
                $feature_price = $request->feature_value;
            } elseif ( $service_id == 5 ) {
                $service = 'App Development Service';
                $feature_price = $request->feature_value;
            } elseif ( $service_id == 3 ) {
                $service = 'Video Services';
            } elseif ( $service_id == 4 ) {
                $service = 'Social Media';
                $feature_price = $request->feature_value;
            } elseif ( $service_id == 6 ) {
                $service = 'Digital Marketing';
                $feature_price = $request->feature_value;
            } elseif ( $service_id == '7' ) {
                $service = 'SEO and Marketing Services';
            }
            if (isset($estimate->client_name))
                $client_name = $estimate->client_name;
            if (isset($estimate->client_address))
                $client_address = $estimate->client_address;
            if (isset($estimate->mobile_no))
                $mobile_no = $estimate->mobile_no;
            if (isset($estimate->client_email))
                $client_email = $estimate->client_email;

            $heading_array[$i+1] = '<b>' . $service . '</b>';
            if( isset( $estimate->html ) && ! empty( $estimate->html )  ){
                $html_array[] = '<table style="width: 650px;padding: 10px 15px;text-align: left;"><tr><th><u>' . $service . '</u></tr></th></table>'. $estimate->html.'</table>';
            }
        }
        $price_40per = ( $price*40 )/100;
        $price_60per = ( $price*60 )/100;
        $price_35per = ( $price*35 )/100;
        $price_30per = ( $price*30 )/100;
        return view('quotation.service-agreement.test-readonly',compact('estimate_numbers','estimate_ids', 'estimate_date','ips','html_array', 'price', 'price_type', 'price_40per', 'price_60per', 'price_35per', 'price_30per','client_email','no_of_client','client_name','client_address','mobile_no','client_email','serviceagreementcontents'));
    }
    
    public function downloadSA(Request $request)
    {
        $estimate_numbers = '';$estimate_ids = '';
        $client_email = $request->client_email;
        $no_of_client = $request->no_of_client;
        if( isset( $request->estimate_ids ) && ! empty($request->estimate_ids) ) {
            $estimate_ids = $request->estimate_ids;
            $ids = array_values(array_filter(explode(',',$request->estimate_ids)));
            for ($i=0; $i < count($ids); $i++) {
                $estimate = QuotationEstimation::find($ids[$i]);
                if (isset($estimate->estimate_no)) {
                    $estimate_numbers .= ' '.$estimate->estimate_no;
                }
            }
        }
        $estimate_numbers = trim($estimate_numbers);

        $serviceagreementcontents = ServiceAgreementContent::all();
        $estimate_date = date("d F, Y");
        $ips = $request->ip();
        $ids = array_values(array_filter(explode(',',$request->estimate_ids)));
        $estimate_date = '';
        $service_id = '';
        $price = 0;
        $price_type = '';
        $html_array = [];
        $heading_array = [];
        $client_name = '';
        $client_address = '';
        $mobile_no = '';
        $client_email = '';
        $price = 0; $price_40per = 0; $price_60per = 0; $price_type = '';
        for ($i=0; $i < count($ids); $i++) {
            $estimate = QuotationEstimation::find($ids[$i]);
            if (isset($estimate->estimate_date))
                $estimate_date = $estimate->estimate_date;
            if (isset($estimate->service_id))
                $service_id = $estimate->service_id;
            if (isset($estimate->price))
                $price += (int)$estimate->price;
            if (isset($estimate->price_type))
                $price_type = $estimate->price_type;

            if ( $service_id == 1 ){
                $service = 'Web Development Service';
            } elseif ( $service_id == 2 ) {
                $service = 'Graphics Designing Service';
                $feature_price = $request->feature_value;
            } elseif ( $service_id == 5 ) {
                $service = 'App Development Service';
                $feature_price = $request->feature_value;
            } elseif ( $service_id == 3 ) {
                $service = 'Video Services';
            } elseif ( $service_id == 4 ) {
                $service = 'Social Media';
                $feature_price = $request->feature_value;
            } elseif ( $service_id == 6 ) {
                $service = 'Digital Marketing';
                $feature_price = $request->feature_value;
            } elseif ( $service_id == '7' ) {
                $service = 'SEO and Marketing Services';
            }
            if (isset($estimate->client_name))
                $client_name = $estimate->client_name;
            if (isset($estimate->client_address))
                $client_address = $estimate->client_address;
            if (isset($estimate->mobile_no))
                $mobile_no = $estimate->mobile_no;
            if (isset($estimate->client_email))
                $client_email = $estimate->client_email;

            $heading_array[$i+1] = '<b>' . $service . '</b>';
            if( isset( $estimate->html ) && ! empty( $estimate->html )  ){
                $html_array[] = '<table style="width: 650px;padding: 10px 15px;text-align: left;"><tr><th><u>' . $service . '</u></tr></th></table>'. $estimate->html.'</table>';
            }
        }
        $price_40per = ( $price*40 )/100;
        $price_60per = ( $price*60 )/100;
        $price_35per = ( $price*35 )/100;
        $price_30per = ( $price*30 )/100;
        //return view('quotation.service-agreement.test-readonly',compact('estimate_numbers','estimate_ids', 'estimate_date','ips','html_array', 'price', 'price_type', 'price_40per', 'price_60per', 'price_35per', 'price_30per','client_email','no_of_client','client_name','client_address','mobile_no','client_email','serviceagreementcontents'));
        $pdf2 = PDF::loadView('quotation.service-agreement.test-readonly',compact('estimate_numbers','estimate_ids', 'estimate_date','ips','html_array', 'price', 'price_type', 'price_40per', 'price_60per', 'price_35per', 'price_30per','client_email','no_of_client','client_name','client_address','mobile_no','client_email','serviceagreementcontents'))->setPaper('a4')->download('service_agreement.pdf');
        return $pdf2;
    }
    /**
     * Store a newly created resource in storage.
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'estimate_ids'   => 'required',
            //'sign1_type'    => 'required',
            'client_name'           => 'required',
            'client_address'        => 'required',
            //'recipient_initials1'   => 'required',
            //'recipient_initials2'   => 'required',
            //'recipient_initials3'   => 'required',
            //'recipient_initials4'   => 'required',
            'sign1'                 => 'required',
        ]);
        $client_name = $request->client_name;
        $client_address = $request->client_address;
        $recipient_initials1 = $request->recipient_initials1;
        $recipient_initials2 = $request->recipient_initials2;
        $recipient_initials3 = $request->recipient_initials3;
        $recipient_initials4 = $request->recipient_initials4;
        $client_email = $request->client_email;
        $service_agreement = new ServiceAgreement;
        $service_agreement->estimate_no = $request->estimate_no;
        if (isset($request->sign1) && !empty($request->sign1) )
            $service_agreement->sign1 = $request->sign1;
        $service_agreement->sign1_type = $request->sign1_type;
        $service_agreement->sign1_font = $request->sign1_font;
        $service_agreement->sign1_designation = $request->sign1_designation;
        if($request->hasfile('sign1_img'))
        {
            if ( $request->file('sign1_img') ) {
                $file = $request->file('sign1_img');
                $name = uniqid(rand(), true).'-sign1.'.$file->extension();
                $file->move(public_path('uploads'), $name);
                $service_agreement->sign1_img = $name;
            }
        }
        if (isset($request->sign2) && !empty($request->sign2) )
            $service_agreement->sign2 = $request->sign2;
        $service_agreement->sign2_type = $request->sign2_type;
        $service_agreement->sign2_font = $request->sign2_font;
        $service_agreement->sign2_designation = $request->sign2_designation;
        if($request->hasfile('sign2_img'))
        {
            if ( $request->file('sign2_img') ) {
                $file = $request->file('sign2_img');
                $name = uniqid(rand(), true).'-sign2.'.$file->extension();
                $file->move(public_path('uploads'), $name);
                $service_agreement->sign2_img = $name;
            }
        }
        if (isset($request->sign3) && !empty($request->sign3) )
            $service_agreement->sign3 = $request->sign3;
        $service_agreement->sign3_type = $request->sign3_type;
        $service_agreement->sign3_font = $request->sign3_font;
        $service_agreement->sign3_designation = $request->sign3_designation;
        if($request->hasfile('sign3_img'))
        {
            if ( $request->file('sign3_img') ) {
                $file = $request->file('sign3_img');
                $name = uniqid(rand(), true).'-sign3.'.$file->extension();
                $file->move(public_path('uploads'), $name);
                $service_agreement->sign3_img = $name;
            }
        }
        $service_agreement->save(); 
        $estimate_date = ''; $estimate_expire = ''; $estimate_no = ''; $service = ''; $price = 0; $price_40per = 0; $price_60per = 0; $price_type = ''; $html = ''; $sign_image = '';
        $sign_image = $service_agreement->sign1_img;
        $ids = array_values(array_filter(explode(',',$request->estimate_ids)));
        $estimate_date = '';
        $service_id = '';
        $price = 0;
        $price_type = '';
        $html_array = [];
        $heading_array = [];
        for ($i=0; $i < count($ids); $i++) {
            $estimate = QuotationEstimation::find($ids[$i]);
            if (isset($estimate->estimate_date))
                $estimate_date = $estimate->estimate_date;
            if (isset($estimate->service_id))
                $service_id = $estimate->service_id;
            if (isset($estimate->price))
                $price += (int)$estimate->price;
            if (isset($estimate->price_type))
                $price_type = $estimate->price_type;

            if ( $service_id == 1 ){
                $service = 'Web Development Service';
            } elseif ( $service_id == 2 ) {
                $service = 'Graphics Designing Service';
                $feature_price = $request->feature_value;
            } elseif ( $service_id == 5 ) {
                $service = 'App Development Service';
                $feature_price = $request->feature_value;
            } elseif ( $service_id == 3 ) {
                $service = 'Video Services';
            } elseif ( $service_id == 4 ) {
                $service = 'Social Media';
                $feature_price = $request->feature_value;
            } elseif ( $service_id == 6 ) {
                $service = 'Digital Marketing';
                $feature_price = $request->feature_value;
            } elseif ( $service_id == '7' ) {
                $service = 'SEO and Marketing Services';
            }
            $heading_array[$i+1] = '<b>' . $service . '</b>';
            if( isset( $estimate->html ) && ! empty( $estimate->html )  ){
                $html_array[] = '<table style="width: 650px;padding: 10px 15px;text-align: left;"><tr><th><u>' . $service . '</u></tr></th></table>'. $estimate->html.'</table>';
            }
        }
        $price_40per = ( $price*40 )/100;
        $price_60per = ( $price*60 )/100;
        $price_35per = ( $price*35 )/100;
        $price_30per = ( $price*30 )/100;
        //return View('quotation/htmlToPDF/service-agreement2',compact('estimate_date', 'estimate_no', 'estimate_expire', 'service', 'price', 'price_type', 'price_40per', 'price_60per', 'html_array' , 'service_agreement', 'client_name', 'client_address', 'recipient_initials1', 'recipient_initials2', 'recipient_initials3', 'recipient_initials4' ));
        $pdf2 = PDF::loadView('quotation/htmlToPDF/service-agreement2',compact('estimate_date', 'estimate_no', 'estimate_expire', 'service', 'price', 'price_type', 'price_40per', 'price_60per', 'price_35per','price_30per','html_array' , 'service_agreement', 'client_name', 'client_address', 'recipient_initials1', 'recipient_initials2', 'recipient_initials3', 'recipient_initials4' ))->setPaper('a4')->save(public_path().'/pdf/service_agreement-'.$service_agreement->id.'.pdf')->stream('service_agreement-'.$service_agreement->id.'.pdf');
        $myEmail = 'nirav.tandel@yopmail.com';//$client_email;
        //Mail::to($myEmail)->send(new ServiceAgreementPDFMail($service_agreement->id));
        //->download('service_agreement.pdf')

        if(isset( $service_agreement->sign2_img ) && Storage::exists('upload/'.$service_agreement->sign2_img)){
            Storage::delete('upload/'.$service_agreement->sign2_img);
            /*
                Delete Multiple File like this way
                Storage::delete(['upload/test.png', 'upload/test2.png']);
            */
        }
        if(isset( $service_agreement->sign3_img ) && Storage::exists('upload/'.$service_agreement->sign3_img)){
            Storage::delete('upload/'.$service_agreement->sign3_img);
            /*
                Delete Multiple File like this way
                Storage::delete(['upload/test.png', 'upload/test2.png']);
            */
        }
        if(isset( $service_agreement->sign1_img ) && Storage::exists('upload/'.$service_agreement->sign1_img)){
            Storage::delete('upload/'.$service_agreement->sign1_img);
            /*
                Delete Multiple File like this way
                Storage::delete(['upload/test.png', 'upload/test2.png']);
            */
        }
        return $pdf2;
        //return \Redirect::route('features-index')->with('success', 'Created Successfully !');
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ServiceAgreement  $serviceagreement
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $features_lists = QuotationFeaturesList::where('feature_id',$id);
        return view('quotation.service-agreement.show',compact('features_lists'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ServiceAgreement  $serviceagreement
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $feature = ServiceAgreement::find($id);
        $main_cats = QuotationMainCategory::all();
        return view('quotation.service-agreement.edit', compact('feature','main_cats'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ServiceAgreement  $serviceagreement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ServiceAgreement $serviceagreement)
    {
        //
        $request->validate([
            'estimate_no'   => 'required',
            'sign1_type'    => 'required',
        ]);
        $quotation = ServiceAgreement::find($request->id);
        $quotation->feature_title = $request->feature_title;
        $quotation->service_id = $request->service_id;
        $quotation->main_category_id = $request->main_category_id;
        $quotation->save();
        return \Redirect::route('features-index')->with('success', 'Update Successfully !');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $feature = ServiceAgreement::find($id);
        $feature->delete();
        return \Redirect::route('features-index')->with('success', 'Deleted successfully !');
    }
    public function getClientIps()
    {
        $clientIps = array();
        $ip = $this->server->get('REMOTE_ADDR');
        if (!$this->isFromTrustedProxy()) {
            return array($ip);
        }
        if (self::$trustedHeaders[self::HEADER_FORWARDED] && $this->headers->has(self::$trustedHeaders[self::HEADER_FORWARDED])) {
            $forwardedHeader = $this->headers->get(self::$trustedHeaders[self::HEADER_FORWARDED]);
            preg_match_all('{(for)=("?\[?)([a-z0-9\.:_\-/]*)}', $forwardedHeader, $matches);
            $clientIps = $matches[3];
        } elseif (self::$trustedHeaders[self::HEADER_CLIENT_IP] && $this->headers->has(self::$trustedHeaders[self::HEADER_CLIENT_IP])) {
            $clientIps = array_map('trim', explode(',', $this->headers->get(self::$trustedHeaders[self::HEADER_CLIENT_IP])));
        }
        $clientIps[] = $ip; // Complete the IP chain with the IP the request actually came from
        $ip = $clientIps[0]; // Fallback to this when the client IP falls into the range of trusted proxies
        foreach ($clientIps as $key => $clientIp) {
            // Remove port (unfortunately, it does happen)
            if (preg_match('{((?:\d+\.){3}\d+)\:\d+}', $clientIp, $match)) {
                $clientIps[$key] = $clientIp = $match[1];
            }
            if (IpUtils::checkIp($clientIp, self::$trustedProxies)) {
                unset($clientIps[$key]);
            }
        }
        // Now the IP chain contains only untrusted proxies and the client IP
        return $clientIps ? array_reverse($clientIps) : array($ip);
    }
    public function content()
    {
        //
        $serviceagreementcontents = ServiceAgreementContent::all();
        return view('quotation.service-agreement-content.index',compact('serviceagreementcontents'));
    }
    public function contentcreate()
    {
        //
        return view('quotation.service-agreement-content.create');
    }
    public function contentstore(Request $request)
    {
        //
        $request->validate([
            'content'         => 'required',
        ]);
        $all_sac = ServiceAgreementContent::create($request->all());
        return \Redirect::route('service-agreement-content-index')->with('success', 'Created Successfully !');
    }
    public function contentedit($id)
    {
        //
        $all_sac = ServiceAgreementContent::find($id);
        return view('quotation.service-agreement-content.edit', compact('all_sac'));
    }
    public function contentupdate(Request $request, ServiceAgreementContent $serviceagreementcontent)
    {
        //
        $request->validate([
            'content'         => 'required',
        ]);
        $all_sac = ServiceAgreementContent::find($request->id);
        $all_sac->content = $request->content;
        $all_sac->save();
        return \Redirect::route('service-agreement-content-index')->with('success', 'Update Successfully !');
    }
    public function contentdestroy($id)
    {
        //
        $terms = ServiceAgreementContent::find($id);
        $terms->delete();
        return \Redirect::route('terms-and-condition-index')->with('success', 'Deleted successfully !');
    }
}

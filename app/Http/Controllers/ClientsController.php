<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ClientsModels;
use App\Models\QuestionAnswerModel;
use App\Models\ServiceModel;
use App\Models\MainServicesModel;
use App\Models\QuestionModel;
use App\Mail\ClientMail;
use Illuminate\Support\Facades\DB;

use Mail;
use Session;

class ClientsController extends Controller
{
    // fetch data from client table and show index page.
    public function index(){
        $clients = ClientsModels::all();        
        return view('clients.index',compact('clients'));
    }

    // Add client page
    public function create(){
        return view('clients.create');
    }
    public function view($id){

        // return view('clients.view'); join('main_services', 'main_services.id', '=', 'service.mainserviceid')
        $acquistion = QuestionAnswerModel::join('nw_question_models','nw_question_models.id', '=','nw_question_answer.questionid')->where('clientid',$id)->get(['nw_question_models.images AS imgs','nw_question_models.question','nw_question_models.type','nw_question_answer.*']);        
        $client = ClientsModels::find($id);
        return view('clients.view', compact('client','acquistion'));

    }
    // Store client data method
    public function store(Request $request){
        
        $request->validate([
            'first_name'         => 'required',
            'last_name'         => 'required',
            'date_of_birth'         => 'required',
            'address1'         => 'required',
            'address2'         => 'required',
            'area_city'         => 'required',
            'pincode'         => 'required',
            'state'         => 'required',
            'country'         => 'required',
            'mobile'         => 'required',
            'email_id'         => 'required|email:rfc,dns',
        ]);
       
        $client =new ClientsModels();
        $client->first_name = $request->first_name;
        $client->last_name = $request->last_name;
        $client->date_of_birth = date('Y-m-d',strtotime($request->date_of_birth));
        $client->address1 = $request->address1;
        $client->address2 = $request->address2;
        $client->address3 = $request->address3;
        $client->landmark = $request->landmark;
        $client->area_city = $request->area_city;
        $client->state = $request->state;
        $client->country = $request->country;
        $client->mobile = $request->mobile;
        $client->landline = $request->landline;
        $client->email_id = $request->email_id;
        $client->skype_id = $request->skype_id;
        $client->pancard = $request->pancard;
        $client->gstin = $request->gstin;
        $client->save();
        return \Redirect::route('clients-index')->with('success', 'Created Successfully !');
    }

    // Edit data fetach
    public function edit($id, $client_type='indian'){
        $client = ClientsModels::find($id);
        $client_type = $client_type;
        return view('clients.edit', compact('client','client_type'));
    }
    public function submit_form($id){
        $client = ClientsModels::find($id);
        $client->status = 'Publish';
        $client->save();
        return \Redirect::route('blank-dashboard')->with('success','Your form has been submitted successfully!');    
    }
    public function submit_question(){
        $submit_ac_form = '<script>window.close();</script>';
        //echo "<script>window.close();</script>";
        return view('blank-dashboard',compact('submit_ac_form'))->with('success','Your form has been submitted successfully!');
    }

    // update data store
    public function update(Request $request, ClientsModels  $ClientsModels){
        $request->validate([
            'first_name'         => 'required',
            'last_name'         => 'required',
            'date_of_birth'         => 'required',
            'address1'         => 'required',
            'address2'         => 'required',
            'area_city'         => 'required',
            'pincode'         => 'required',
            'state'         => 'required',
            'country'         => 'required',
            'mobile'         => 'required|numeric',
            'email_id'         => 'required|email:rfc,dns',
        ]);
       
        $client = ClientsModels::find($request->id);
        $client->first_name = $request->first_name;
        $client->last_name = $request->last_name;
        $client->date_of_birth = date('Y-m-d',strtotime($request->date_of_birth));
        $client->address1 = $request->address1;
        $client->address2 = $request->address2;
        $client->address3 = $request->address3;
        $client->landmark = $request->landmark;
        $client->area_city = $request->area_city;
        $client->state = $request->state;
        $client->country = $request->country;
        $client->mobile = $request->mobile;
        $client->landline = $request->landline;
        $client->email_id = $request->email_id;
        $client->skype_id = $request->skype_id;
        $client->pancard = $request->pancard;
        $client->gstin = $request->gstin;
        $client->business_name = $request->business_name;
        $client->business_same_as_resedential = $request->business_same_as_resedential;
        $client->business_address1 = $request->business_address1;
        $client->business_address2 = $request->business_address2;
        $client->business_address3 = $request->business_address3;
        $client->company_number = $request->company_number;
        $client->driving_licence = $request->driving_licence;
        $client->prefered_communication_platform = $request->prefered_communication_platform;
        $client->sign2_name = $request->sign2_name;
        $client->sign2_email = $request->sign2_email;
        $client->sign2_designation = $request->sign2_designation;
        $client->sign1_designation = $request->sign1_designation;
        $client->sign3_name = $request->sign3_name;
        $client->sign3_email = $request->sign3_email;
        $client->sign3_designation = $request->sign3_designation;
        $client_type = $request->client_type;
        $client->save();
        if ( isset( $request->client_form ) && ! empty( $request->client_form ) ) {
            //return \Redirect::route('blank-dashbaord')->with('success','Your details are stored successfully!');
            return view('clientview.client-detail', compact('client','client_type'));
        } else {
            return \Redirect::route('clients-index')->with('success','Created Successfully !');    
        }
    } 
    // Delete client
    public function destroy($id)
    {
        $feature = ClientsModels::find($id);
        $feature->delete();
        return \Redirect::route('clients-index')->with('success', 'Deleted successfully !');
    }
    // URL generate 
    public function url(Request $request){
        $email = $request->email;
        $clientid =new ClientsModels;
        $clientid->status = 'pending';
        $clientid ->save();
        $cid = $clientid->id; // fetch last id
        $client_type = $request->client_type;
        $urlid = url('client/edit/'.$cid.'/'.$client_type); //ganarete url.
        $clients = ClientsModels::all();
        //$mail = $this->sendMail($email, 'neshallweb@yopmail.com', $urlid);
        $mail = Mail::to($email)->send(new ClientMail($urlid));
        if($mail) {
            return \Redirect::route('clients-index',['clients'=> $clients])->with('success', 'Mail has sent successfully !');
        } else {
            return \Redirect::route('clients-index',['clients'=> $clients])->with('fail', 'Something went wrong!');
        }
    }

     // Main Services edit
     public function servcieedit($id){
        $checkqution = ClientsModels::where('id',$id)->get(['first_name','last_name','email_id']);
        Session::put('clientid',$id); 
        Session::put('clientname',$checkqution[0]['first_name'].' '.$checkqution[0]['last_name']); 
        Session::put('email',$checkqution[0]['email_id']);
        $mainservices = MainServicesModel::all();
        $services = ServiceModel::all();
        return view('clients.serviceedit',compact('mainservices'),compact('services'));
    }
   
    //  Services edit
    public function servcies(Request $request, QuestionAnswerModel $QuestionAnswerModel){
        
        $mainservicesid = $request->mainservices; 
        Session::put('mainservice', $mainservicesid );
        $services = ServiceModel::where('mainserviceid', $mainservicesid )->get();
        $service1='<option value="" >Select Service</option>';
        foreach($services as $service){
            $service1 .= '<option value="'.$service['id'].'">'. $service['name'] .'</option>';
        }
        echo $service1;
        return response()->json(['success'=>'Selected successfully!!!!']);
    }   
    

    //  Services store
    public function servicesstore(Request $request){
        Session::put('currentservicesid', $request->post('services'));   
        echo '<a href="'.url('mail').'" class="btn btn-danger"> Send Mail </a>';
    }

    // Send Mail
    public function mail(){
        $details = [
            'clientid' => session('clientid'),
            'servicesid' => session('currentservicesid'),
            'email' => session('email'),
            'mainservicesid' => session('mainservice')
        ];
        $mail = \Mail::to(session('email'))->send(new \App\Mail\Q_Mail($details));
        if($mail){
            return \Redirect::route('clients-index');
        }else{
            return \Redirect::route('clients-index');
        }
    }

    // Question  
    public function questions($cid, $sid,$msid){
        Session::put('clientid',$cid);
        Session::put('currentservicesid',$sid);
        Session::put('mainservice', $msid );
        echo session('cleintid');
        DB::enableQueryLog();
        $questions = DB::table("nw_question_models")->leftJoin('nw_question_answer', function ($join) {
            $join->on('nw_question_models.id', '=', 'nw_question_answer.questionid')
                 ->where('nw_question_answer.clientid', session('cleintid'));
        })->where('nw_question_models.serviceid', session('currentservicesid'))
        ->get(['nw_question_answer.*','nw_question_answer.id','nw_question_models.question','nw_question_models.id as qid','nw_question_models.type','nw_question_models.options','nw_question_models.images AS expimages']);
        $answers = DB::table("nw_question_answer")->where('clientid', $cid)->where('mainserviceid', $msid)->where('servicesid', $sid)->get();
        $query = DB::getQueryLog();
        //dd($answers);
        //dd(end($query));
        //dd($questions);
        if(!empty($questions->count())){
            return view('clientview.question',compact('questions','cid','answers'),['sid'=>$sid,'msid'=>$msid]); // sid(service id), msid (main service id)
        }else{
            $questions = QuestionModel::where('serviceid', $sid)->get();
            return view('clientview.question',compact('questions','cid'),['sid'=>$sid,'msid'=>$msid]);
        }
    }
    /*
    This methods are for API
    */
    public function checkLogin( Request $request ) {
        if ($request->has('email_id') && $request->has('password') ) {
            //

            $client = ClientsModels::where([
                                            ['email_id',$request->input('email_id')],
                                            ['password',md5($request->input('password'))]
                                        ])->first();
            if( isset($client) && ! empty( $client ) ) {
                return json_encode(array('status'=>true));
            } else {
                return json_encode(array('status'=>false));
            }
        } else {
            return json_encode(array('error_message'=>'There are missing field.'));
        }
       
    }
    public function registerUser( Request $request ) {
        if (!$request->has('first_name') ) {
            return json_encode(array('error_message'=>'first_name is missing in field.'));
        }
        if (!$request->has('last_name') ) {
            return json_encode(array('error_message'=>'last_name is missing in field.'));
        }
        if (!$request->has('address1') ) {
            return json_encode(array('error_message'=>'address1 is missing in field.'));
        }
        if (!$request->has('mobile') ) {
            return json_encode(array('error_message'=>'mobile is missing in field.'));
        }
        if (!$request->has('pincode') ) {
            return json_encode(array('error_message'=>'pincode is missing in field.'));
        }
        if (!$request->has('email_id') ) {
            return json_encode(array('error_message'=>'email_id is missing in field.'));
        }
        if (!$request->has('password') ) {
            return json_encode(array('error_message'=>'password is missing in field.'));
        }
        $client = new ClientsModels;
        $client->first_name = $request->input('first_name'); //retrieving user inputs
        $client->last_name = $request->input('last_name');  //retrieving user inputs
        $client->date_of_birth = $request->input('date_of_birth');  //retrieving user inputs
        $client->address1 = $request->input('address1');  //retrieving user inputs
        $client->mobile = $request->input('mobile');  //retrieving user inputs
        $client->pincode = $request->input('pincode');  //retrieving user inputs
        $client->email_id = $request->input('email_id');  //retrieving user inputs
        $client->password = md5($request->input('password'));  //retrieving user inputs
        $client->save(); //storing values as an object
        return json_encode(array('status'=>$client)); //returns the stored value if the operation was successful.
    }
    public function getUserData() {
        return json_encode(['userData' => ClientsModels::findorFail(1)]); //searches for the object in the database using its id and returns it.
    }

    public function sendMail( $to, $from, $link ){

        $message = "
        <html>
        <head>
        <title>HTML email</title>
        </head>
        <body>
        <p>Please go to the below link to submit your details for us.</p>
        <a href='". $link ."'> Click here</a>
        </body>
        </html>
        ";

        // Always set content-type when sending HTML email
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

        // More headers
        $headers .= 'From: <'.$from.'>' . "\r\n";

        $mailsend =  mail($to,"Client Detail",$message,$headers);
        return $mailsend;
    }
}
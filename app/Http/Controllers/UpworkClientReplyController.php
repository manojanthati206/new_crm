<?php

namespace App\Http\Controllers;

use App\Models\UpworkClientReply;
use Illuminate\Http\Request;

class UpworkClientReplyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $replies = UpworkClientReply::all();
        return view('upwork.client-reply.index',compact('replies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('upwork.client-reply.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'client_question'         => 'required',
            'answer'         => 'required',
        ]);
        $replies = UpworkClientReply::create($request->all());
        return \Redirect::route('upwork-client-replies-index')->with('success', 'Created Successfully !');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\UpworkClientReply  $upworkClientReply
     * @return \Illuminate\Http\Response
     */
    public function show(UpworkClientReply $upworkClientReply)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\UpworkClientReply  $upworkClientReply
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $replies = UpworkClientReply::find($id);
        return view('upwork.client-reply.edit', compact('replies'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\UpworkClientReply  $upworkClientReply
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UpworkClientReply $upworkClientReply)
    {
        //
        $request->validate([
            'client_question'         => 'required',
            'answer'         => 'required',
        ]);
        $replies = UpworkClientReply::find($request->id);
        $replies->client_question = $request->client_question;
        $replies->answer = $request->answer;
        $replies->save();
        return \Redirect::route('upwork-client-replies-index')->with('success', 'Update Successfully !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\UpworkClientReply  $upworkClientReply
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $replies = UpworkClientReply::find($id);
        $replies->delete();
        return \Redirect::route('upwork-client-replies-index')->with('success', 'Deleted successfully !');
    }
}

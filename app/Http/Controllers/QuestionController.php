<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\QuestionModel;
use App\Models\QuestionAnswerModel;
use App\Models\ServiceModel;

class QuestionController extends Controller
{
    // fetch data from Question table and show index page.
    public function index(){
        $questions = QuestionModel::join('nw_sub_service', 'nw_sub_service.id', '=', 'nw_question_models.serviceid')->get(['nw_sub_service.name AS service_name', 'nw_question_models.*']);        
        return view('acquisition.question.index',compact('questions'));
    }

    // Add Question page
    public function create(){
        $mainservices = ServiceModel::all();
        return view('acquisition.question.create',compact('mainservices'));
    }

    // Store Question data method
    public function store(Request $request){
        
        $request->validate([
            'question'         => 'required',
            'serviceid' => 'required',
            'images.*' => 'mimes:jpg,png,gif,jpeg'
            ]);

            $imgs =[]; 
            if ($request->hasfile('images')) {
               
                foreach($request->file('images') as $file)
                {
                    $filename = $file->getClientOriginalName();
                    $fileTitle = 's_'.$filename.time().'.'.$file->getClientOriginalExtension();
                    $file->move(public_path('uploads'), $fileTitle);
                    $imgs[] = 'uploads/'.$fileTitle;
                }
            }
            $imgs= json_encode($imgs);
        
        $service =new QuestionModel;
        $service->question = $request->input('question');
        $service->type = $request->input('type');
        $service->options    = $request->input('options');
        $service->serviceid = $request->input('serviceid');
        $service->images =  $imgs;
        $service->save();

        return \Redirect::route('questions-add')->with('success', 'Created Successfully !');
    }

    // Edit data fetach
    public function edit($id){
        $question = QuestionModel::find($id);
        $mainservices = ServiceModel::all();
        return view('acquisition.question.edit', compact('question'),compact('mainservices'));
    }
    public function finish($id){
        $answers = QuestionAnswerModel::where('clientid',$id)->get();
        $questions = QuestionModel::all();
        return view('clientview.question-detail', compact('questions','answers'));
        //return \Redirect::route('client-detail')->with('success', 'Your question are submited successfully!');
    }
    // update data  to store
    public function update(Request $request, ServiceModel  $ServiceModel){
        $request->validate([
            'question'         => 'required',
            'serviceid' => 'required',
            'images.*' => 'mimes:jpg,png,gif,jpeg'
        ]);
        
        $imgs =[]; 
        if ($request->hasfile('images')) {
            foreach($request->file('images') as $file)
            {
                $filename = $file->getClientOriginalName();
                $fileTitle = 's_'.$filename.time().'.'.$file->getClientOriginalExtension();
                $file->move(public_path('uploads'), $fileTitle);
                $imgs[] = 'uploads/'.$fileTitle;
            }
            $imgs= json_encode($imgs);
        }else{
            $imgs = $request->input('oldimage');
        }
       
        $service = QuestionModel::find($request->id);
        $service->question = $request->input('question');
        $service->type = $request->input('type');
        $service->serviceid = $request->input('serviceid');
        $service->options    = $request->input('options');
        $service->images =  $imgs;
        $service->save();
        return \Redirect::route('questions-index')->with('success','Created Successfully !');

    } 
    // Delete Question
    public function destroy($id)
    {
        $feature = QuestionModel::find($id);
        $feature->delete();
        return \Redirect::route('questions-index')->with('success', 'Deleted successfully !');
    }
}

<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ClientMail extends Mailable
{
    use Queueable, SerializesModels;
    public $urlid;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($urlid)
    {
        $this->urlid=$urlid;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $new_urlid = $this->urlid;
        return $this->view('mails.client',compact('new_urlid'))
                    ->from($address = 'info@neshallweb.com', $name = 'neshallWeb Team');
    }
}

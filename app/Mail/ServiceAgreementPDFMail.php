<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ServiceAgreementPDFMail extends Mailable
{
    use Queueable, SerializesModels;
    public $id;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($id)
    {
        $this->id=$id;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mails.service-agreement-pdf')
                    ->from($address = 'info@neshallweb.com', $name = 'neshallWeb Team')
                    ->attach(public_path('pdf/service_agreement-'.$this->id.'.pdf'), [
                         'as' => 'service_agreement.pdf',
                         'mime' => 'application/pdf',
                    ]);
    }
}

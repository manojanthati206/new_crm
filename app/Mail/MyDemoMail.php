<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MyDemoMail extends Mailable
{
    use Queueable, SerializesModels;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $client_email = 'abdulazizok4@gmail.com';
        $service_data = ['estimate_ids'=>37,'client_email'=>$client_email,'no_of_client'=>1];
        return $this->view('mails.myDemoMail',compact('service_data'))
                    ->subject('neshallWeb Mail')
                    ->from($address = 'info@neshallweb.com', $name = 'neshallWeb Team')
                    //->cc(['asa@asatechbh.com'])
                    /*->attach(public_path('mail-template.html'), [
                         'as' => 'mail-template.html',
                         'mime' => 'text/html',
                    ])*/;
    }
}

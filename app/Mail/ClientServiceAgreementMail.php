<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ClientServiceAgreementMail extends Mailable
{
    use Queueable, SerializesModels;
    public $service_data;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($service_data)
    {
        $this->service_data=$service_data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $service_data = $this->service_data;
        return $this->view('mails.service-agreement',compact('service_data'))
                    ->from($address = 'info@neshallweb.com', $name = 'neshallWeb Team');
    }
}

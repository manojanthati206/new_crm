<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ServiceAgreement extends Model
{
    use HasFactory;

    protected $fillable = [
        'estimate_no',
        'sign1',
        'sign1_type',
        'sign1_font',
        'sign1_designation',
        'sign2',
        'sign2_type',
        'sign2_font',
        'sign2_designation',
        'sign3',
        'sign3_type',
        'sign3_font',
        'sign3_designation',
        'service_agreement_date',
    ];
    protected $table = 'nw_service_agreement';
}

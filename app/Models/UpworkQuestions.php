<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UpworkQuestions extends Model
{
    use HasFactory;
    protected $fillable = [
        'question',
    ];
    protected $table = 'nw_upwork_questions';
}

<?php



namespace App\Models;



use Illuminate\Database\Eloquent\Factories\HasFactory;

use Illuminate\Database\Eloquent\Model;



class ClientsModels extends Model

{

    use HasFactory;

    protected $fillable = ['first_name','last_name','date_of_birth','address1','address2','address3','landmark','area_city','pincode','state','country','mobile','landline','email_id','skype_id','pancard','gstin', 'business_name', 'business_same_as_resedential', 'business_address1', 'business_address2', 'business_address3', 'company_number', 'driving_licence', 'prefered_communication_platform', 'sign2_name', 'sign2_email', 'sign2_designation', 'sign1_designation', 'sign3_name', 'sign3_email', 'sign3_designation'];

    protected $table = 'nw_clients';

}


<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QuotationImportantNotesList extends Model
{
    use HasFactory;
    protected $table = 'nw_important_notes_list';
}

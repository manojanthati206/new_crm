<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QuotationMainCategory extends Model
{
    use HasFactory;
    protected $fillable = [
        'title',
    ];
    protected $table = 'nw_main_categories';
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QuotationImportantNote extends Model
{
    use HasFactory;
    protected $table = 'nw_important_note';
}

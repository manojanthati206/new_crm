<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TermsAndCondition extends Model
{
    use HasFactory;
    protected $fillable = [
        'content',
        'service_id',
    ];
    protected $table = 'nw_terms_and_condition';
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QuestionModel extends Model
{
    use HasFactory;
    protected $fillable = ['serviceid','question','images'];
    protected $table = 'nw_question_models';
    
    public function setFilenamesAttribute($value)
    {
        $this->attributes['images'] = json_encode($value);
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StratergicPartnership extends Model
{
    use HasFactory;
    protected $fillable = [
        'company_name',
        'representive_name',
        'mobile',
        'email',
        'business_category',
        'general_category_of_business',
        'expertice',
        'portfolio',
        'any_document',
    ];
    protected $table = 'nw_stratergic_partnership';
}

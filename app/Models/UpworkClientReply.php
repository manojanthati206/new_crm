<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UpworkClientReply extends Model
{
    use HasFactory;
    protected $fillable = [
        'client_question',
        'answer',
    ];
    protected $table = 'nw_upwork_client_reply';
}


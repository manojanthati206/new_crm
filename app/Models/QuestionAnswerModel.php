<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QuestionAnswerModel extends Model
{
    use HasFactory;
    protected $fillable = ['questionid','answer','clientid','mainserviceid','servicesid','status'];
    protected $table = 'nw_question_answer';  
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QuotationFeaturesList extends Model
{
    use HasFactory;
    protected $fillable = [
        'feature_list',
        'feature_id',
    ];
    protected $table = 'nw_features_list';
}

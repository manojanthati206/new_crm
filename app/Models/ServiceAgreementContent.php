<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ServiceAgreementContent extends Model
{
    use HasFactory;

    protected $fillable = [
        'content',
    ];
    protected $table = 'nw_service_agreement_content';
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QuotationAdditionalFeaturesList extends Model
{
    use HasFactory;
    protected $table = 'nw_additional_features_list';
}

<?php
// app/Models/Deliverable.php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Deliverables extends Model
{
    use HasFactory;

    protected $table = 'deliverables'; // Set the table name

    protected $fillable = ['service_id', 'name']; // Define the fillable columns

    // Define the relationship with the Service model
    public function service()
    {
        return $this->belongsTo(Service::class);
    }
}


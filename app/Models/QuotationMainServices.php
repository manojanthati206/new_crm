<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QuotationMainServices extends Model
{
    use HasFactory;
    protected $table = 'nw_main_services';
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UpworkProjects extends Model
{
    use HasFactory;
    protected $fillable = [
        'project_name',
        'profile_name',
        'client_name',
        'project_details',
        'project_details_screenshot',
        'project_link',
    ];
    protected $table = 'nw_upwork_projects';
}

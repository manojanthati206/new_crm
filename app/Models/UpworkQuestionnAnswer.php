<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UpworkQuestionnAnswer extends Model
{
    use HasFactory;
    protected $fillable = [
        'upwork_project_id',
        'upwork_question_id',
        'answer',
    ];
    protected $table = 'nw_upwork_question_n_answer';
}

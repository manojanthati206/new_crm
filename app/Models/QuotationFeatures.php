<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QuotationFeatures extends Model
{
    use HasFactory;
    protected $fillable = [
        'feature_title',
        'service_id',
        'main_category_id',
    ];
    protected $table = 'nw_features';
}

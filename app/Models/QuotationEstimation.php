<?php



namespace App\Models;



use Illuminate\Database\Eloquent\Factories\HasFactory;

use Illuminate\Database\Eloquent\Model;



class QuotationEstimation extends Model

{

    use HasFactory;

    protected $fillable = [

        'estimate_date',

        'estimate_no',

        'estimate_expire',

        'service_id',

        'html',
        'checked_features'

    ];

    protected $table = 'nw_estimation';

}


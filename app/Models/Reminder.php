<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Reminder extends Model
{
    use HasFactory;
    protected $fillable = [
        'client_name',
        'approach_channel',
        'communication_preference',
        'contact_info',
        'last_response_date',
        'client_status',
        'reminder_status',
        'followups',
        'reminder_count',
    ];
    protected $table = 'nw_reminder';
}

jQuery(document).ready(function(event){
    jQuery(".first_name").change(function(event){
        event.preventDefault();
        var first_name = jQuery(".first_name").val();  
        var cid = jQuery("#cid").val();      
        jQuery.ajax({
            url: "{{ url('client/first_name/') }}/"+cid,
            data: {
                "_token": "{{ csrf_token() }}",
                "first_name": first_name
            },
            type:"post",
            success:function(result){
                alert(result);
            }
        });
    });

    jQuery(".last_name").change(function(event){
        event.preventDefault();
        var last_name = jQuery(".last_name").val();        
        jQuery.ajax({
            url: "{{ url('client/last_name/'.$client->id) }}",
            data: {
                "_token": "{{ csrf_token() }}",
                "last_name": last_name
            },
            type:"post",
            success:function(result){
                alert(result);
            }
        });
    });
   
   jQuery(".dob").change(function(event){
       event.preventDefault();
       var dob = jQuery(".dob").val();        
       jQuery.ajax({
           url: "{{ url('client/dob/'.$client->id) }}",
           data: {
               "_token": "{{ csrf_token() }}",
               "dob": dob
           },
           type:"post",
           success:function(result){
               alert(result);
           }
       });
   });
});
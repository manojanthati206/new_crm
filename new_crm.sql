-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Feb 04, 2022 at 12:38 PM
-- Server version: 5.7.31
-- PHP Version: 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `new_crm`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uuid` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2022_02_04_041505_create_nw_main_categories_table', 1),
(6, '2022_02_04_041557_create_nw_features_table', 1),
(7, '2022_02_04_041619_create_nw_features_list_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `nw_features`
--

DROP TABLE IF EXISTS `nw_features`;
CREATE TABLE IF NOT EXISTS `nw_features` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `feature_title` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `service_id` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nw_features_feature_title_unique` (`feature_title`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `nw_features`
--

INSERT INTO `nw_features` (`id`, `feature_title`, `service_id`, `created_at`, `updated_at`) VALUES
(2, 'Web Speed & Performance', '1', '2022-02-04 01:22:46', '2022-02-04 03:05:19'),
(3, 'Web Appearance & Graphics', '1', '2022-02-04 03:06:17', '2022-02-04 03:06:17'),
(4, 'Features & Elements', '1', '2022-02-04 03:06:34', '2022-02-04 03:06:34'),
(5, 'Requirements & Support', '1', '2022-02-04 03:06:43', '2022-02-04 03:06:43');

-- --------------------------------------------------------

--
-- Table structure for table `nw_features_list`
--

DROP TABLE IF EXISTS `nw_features_list`;
CREATE TABLE IF NOT EXISTS `nw_features_list` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `feature_list` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `feature_id` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=33 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `nw_features_list`
--

INSERT INTO `nw_features_list` (`id`, `feature_list`, `feature_id`, `created_at`, `updated_at`) VALUES
(1, 'Light weight theme', '2', '2022-02-04 02:59:15', '2022-02-04 03:07:23'),
(2, 'Optimized plugins', '2', '2022-02-04 03:07:33', '2022-02-04 03:07:33'),
(3, 'Optimized color schemes', '3', '2022-02-04 03:07:42', '2022-02-04 03:07:42'),
(4, 'Stock images', '3', '2022-02-04 03:07:51', '2022-02-04 03:07:51'),
(5, 'Regular icons', '3', '2022-02-04 03:08:01', '2022-02-04 03:08:01'),
(6, 'Attractive website theme', '3', '2022-02-04 03:08:10', '2022-02-04 03:08:10'),
(7, 'Mobile responsive', '4', '2022-02-04 03:08:21', '2022-02-04 03:08:21'),
(8, 'Limited to Five webpages', '4', '2022-02-04 03:08:31', '2022-02-04 03:08:31'),
(9, 'User login, logout, forgot password capabilities', '4', '2022-02-04 03:08:42', '2022-02-04 03:08:42'),
(10, 'Transparent header', '4', '2022-02-04 03:08:53', '2022-02-04 03:08:53'),
(11, 'Media library', '4', '2022-02-04 03:09:01', '2022-02-04 03:09:01'),
(12, 'Video/YouTube links', '4', '2022-02-04 03:09:10', '2022-02-04 03:09:10'),
(13, 'Linked location on google map', '4', '2022-02-04 03:09:18', '2022-02-04 03:09:18'),
(14, 'Product/Service presentation page', '4', '2022-02-04 03:09:27', '2022-02-04 03:09:27'),
(15, '“Contact us” quick link', '4', '2022-02-04 03:09:35', '2022-02-04 03:09:35'),
(16, 'Feedback/Contact form', '4', '2022-02-04 03:09:45', '2022-02-04 03:09:45'),
(17, 'Five company email accounts (email@example.com)', '4', '2022-02-04 03:09:52', '2022-02-04 03:09:52'),
(18, 'SEO optimized website', '4', '2022-02-04 03:10:01', '2022-02-04 03:10:01'),
(19, 'SEO friendly links and URLs', '4', '2022-02-04 03:10:10', '2022-02-04 03:10:10'),
(20, 'Generating robot.txt for website', '4', '2022-02-04 03:10:18', '2022-02-04 03:10:18'),
(21, 'Generating XML sitemap of website', '4', '2022-02-04 03:10:29', '2022-02-04 03:10:29'),
(22, 'Generating HTML sitemap of website', '4', '2022-02-04 03:10:37', '2022-02-04 03:10:37'),
(23, 'Google search console setup', '4', '2022-02-04 03:10:50', '2022-02-04 03:10:50'),
(24, 'Google analytics setup', '4', '2022-02-04 03:11:00', '2022-02-04 03:11:00'),
(25, 'User interface will be designed to represent individual or business entity.', '5', '2022-02-04 03:11:08', '2022-02-04 03:16:40'),
(26, 'User experience will be targeted towards the feasibility of using website according to the user interest of knowing more about individual or business.', '5', '2022-02-04 03:11:19', '2022-02-04 03:16:51'),
(27, 'Free exclusive and proven business growth technique only for our clients.', '5', '2022-02-04 03:11:29', '2022-02-04 03:16:54'),
(28, 'The end goal will be to make user feel luxury of being visiting the website.', '5', '2022-02-04 03:11:45', '2022-02-04 03:11:45'),
(29, 'Training provided for the basic operation of website.', '5', '2022-02-04 03:11:53', '2022-02-04 03:11:53'),
(30, 'Call/text/email support during office hours', '5', '2022-02-04 03:12:02', '2022-02-04 03:12:02'),
(31, 'Shared hosting included for one year (redeemable)', '5', '2022-02-04 03:12:10', '2022-02-04 03:12:10'),
(32, 'Available common domain included for one year [For example .com, .in, .org, .io, etc.] (redeemable)', '5', '2022-02-04 03:12:17', '2022-02-04 03:12:17');

-- --------------------------------------------------------

--
-- Table structure for table `nw_main_categories`
--

DROP TABLE IF EXISTS `nw_main_categories`;
CREATE TABLE IF NOT EXISTS `nw_main_categories` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nw_main_categories_title_unique` (`title`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `nw_main_categories`
--

INSERT INTO `nw_main_categories` (`id`, `title`, `created_at`, `updated_at`) VALUES
(2, 'Services/Specifications', '2022-02-04 00:14:23', '2022-02-04 00:14:23'),
(3, 'Additional Features (Optional)', '2022-02-04 00:14:32', '2022-02-04 00:14:32');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

DROP TABLE IF EXISTS `personal_access_tokens`;
CREATE TABLE IF NOT EXISTS `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

/*checking*/

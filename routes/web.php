<?php

use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Artisan;
use App\Http\Controllers\MailController;
use App\Http\Controllers\CommanController;
use App\Http\Controllers\ClientsController;
use App\Http\Controllers\ServiceController;
use App\Http\Controllers\QuestionController;
use App\Http\Controllers\ReminderController;
use App\Http\Controllers\ClientanswerController;
use App\Http\Controllers\MainServicesController;
use App\Http\Controllers\UpworkProjectsController;
use App\Http\Controllers\UpworkQuestionsController;
use App\Http\Controllers\QuotationMainCatController;
use App\Http\Controllers\ServiceAgreementController;
use App\Http\Controllers\QuotationFeaturesController;
use App\Http\Controllers\TermsAndConditionController;
use App\Http\Controllers\UpworkClientReplyController;
use App\Http\Controllers\QuotationEstimationController;
use App\Http\Controllers\QuotationFeaturesListController;
use App\Http\Controllers\StratergicPartnershipController;
use App\Http\Controllers\UpworkQuestionsnAnswerController;
use App\Http\Controllers\QuotationEstimationTemplateController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    return '<h1>Cache facade value cleared</h1>';
});
Route::get('/', function () {
    // dd("hello");
    return view('dashboard');
})->name('dashboard');
Route::get('new-send-mail', [MailController::class, 'sendMail']);
Route::get('send-mail-link', [MailController::class, 'sendMailLink'])->name('send-mail-link');
Route::get('dashbaord', function () {
    return view('blank-dashboard');
})->name('blank-dashboard');
Route::get('my-demo-mail',[ServiceAgreementController::class,'myDemoMail']);

Route::get('test', function () {
    return view('test');
})->name('test');
// Main Category
Route::get('quotation/main-category/add', [QuotationMainCatController::class,'create'])->name('main-category-add');
Route::post('quotation/main-category/store', [QuotationMainCatController::class,'store'])->name('main-category-store');
Route::get('quotation/main-category/index', [QuotationMainCatController::class,'index'])->name('main-category-index');
Route::get('quotation/main-category/edit/{id}', [QuotationMainCatController::class, 'edit'])->name('main-category-edit');
Route::get('quotation/main-category/delete/{id}', [QuotationMainCatController::class, 'destroy'])->name('main-category-delete');
Route::PUT('quotation/main-category/update', [QuotationMainCatController::class, 'update'])->name('main-category-update');
// Features
Route::get('quotation/features/add', [QuotationFeaturesController::class,'create'])->name('features-add');
Route::post('quotation/features/store', [QuotationFeaturesController::class,'store'])->name('features-store');
Route::get('quotation/features/index', [QuotationFeaturesController::class,'index'])->name('features-index');
Route::get('quotation/features/edit/{id}', [QuotationFeaturesController::class, 'edit'])->name('features-edit');
Route::get('quotation/features/delete/{id}', [QuotationFeaturesController::class, 'destroy'])->name('features-delete');
Route::PUT('quotation/features/update', [QuotationFeaturesController::class, 'update'])->name('features-update');
// Features List
Route::get('quotation/features-list/add', [QuotationFeaturesListController::class,'create'])->name('features-list-add');
Route::post('quotation/features-list/store', [QuotationFeaturesListController::class,'store'])->name('features-list-store');
Route::get('quotation/features-list/index', [QuotationFeaturesListController::class,'index'])->name('features-list-index');
Route::get('quotation/features-list/edit/{id}', [QuotationFeaturesListController::class, 'edit'])->name('features-list-edit');
Route::get('quotation/features-list/delete/{id}', [QuotationFeaturesListController::class, 'destroy'])->name('features-list-delete');
Route::PUT('quotation/features-list/update', [QuotationFeaturesListController::class, 'update'])->name('features-list-update');

// Estimation
Route::get('quotation/estimation/add', [QuotationEstimationController::class,'create'])->name('estimation-add');
Route::get('quotation/manoj', function(){
    echo "hello world";
});
Route::post('quotation/estimation/store', [QuotationEstimationController::class,'store'])->name('estimation-store');
Route::get('quotation/estimation/index', [QuotationEstimationController::class,'index'])->name('estimation-index');
Route::get('quotation/estimation/edit/{id}', [QuotationEstimationController::class, 'edit'])->name('estimation-edit');
Route::get('quotation/estimation/delete/{id}', [QuotationEstimationController::class, 'destroy'])->name('estimation-delete');
Route::PUT('quotation/estimation/update', [QuotationEstimationController::class, 'update'])->name('estimation-update');
Route::get('quotation/estimation/checkExistEstimateNo/{estimation_no}', [QuotationEstimationController::class, 'checkExistEstimateNo'])->name('check-existestimation-no');


//downloading pdfs of estimations
Route::get('quotation/estimation/downloadEstimation/{id}', [QuotationEstimationController::class, 'downloadEstimation'])->name('download-estimation-pdf');
Route::get('quotation/estimation/downloadEstimationInvoice/{id}', [QuotationEstimationController::class, 'downloadEstimationInvoice'])->name('download-estimation-invoice');

Route::get('quotation/estimation/downloadServiceAgreement/{id}', [QuotationEstimationController::class, 'downloadServiceAgreement'])->name('download-service-pdf');
Route::get('quotation/estimation/generatePDF', [QuotationEstimationController::class, 'generatePDF'])->name('generatePDF');
Route::get('quotation/estimation/department', function () {
    return view('quotation/estimation/department');
})->name('estimation-department');
Route::post('quotation/estimation/service-agreement', [QuotationEstimationController::class,'service_agreement'])->name('estimation-service-agreement');
Route::get('quotation/estimation/service-agreement-signature', [ServiceAgreementController::class,'create'])->name('service-agreement-signature');
Route::get('quotation/estimation/service-agreement-signature-readonly', [ServiceAgreementController::class,'readonly'])->name('service-agreement-signature-readonly');
Route::get('quotation/estimation/service-agreement-signature-download', [ServiceAgreementController::class,'downloadServiceAgreement'])->name('service-agreement-signature-download');

Route::post('quotation/service-agreement/store', [ServiceAgreementController::class,'store'])->name('service-agreement-store');
Route::post('quotation/service-agreement/contentupdtae', [ServiceAgreementController::class,'contentupdate'])->name('service-agreement-content-update');
Route::get('quotation/service-agreement/send-service-agreement', [ServiceAgreementController::class,'sendServiceAgreement'])->name('send-service-agreement');


//Estimation Templates
Route::get('quotation/estimation_template/index', [QuotationEstimationTemplateController::class,'index'])->name('estimation-template-index');
Route::get('quotation/estimation_template/webdevelopment', [QuotationEstimationTemplateController::class,'webdev'])->name('estimation-template-webdev');
Route::get('quotation/estimation_template/videoservices', [QuotationEstimationTemplateController::class,'video'])->name('estimation-template-video');
Route::get('quotation/estimation_template/socialmedia', [QuotationEstimationTemplateController::class,'social'])->name('estimation-template-social');
Route::get('quotation/estimation_template/seoandmarketing', [QuotationEstimationTemplateController::class,'seo'])->name('estimation-template-seo');
Route::get('quotation/estimation_template/mobileapplication', [QuotationEstimationTemplateController::class,'mobapp'])->name('estimation-template-mobapp');
Route::get('quotation/estimation_template/graphicsdesign', [QuotationEstimationTemplateController::class,'graphdesign'])->name('estimation-template-graphdesign');
Route::get('quotation/estimation_template/digitalmarketing', [QuotationEstimationTemplateController::class,'digitmarket'])->name('estimation-template-digitmarket');

// Terms and Condition
Route::get('quotation/terms_and_condition/add', [TermsAndConditionController::class,'create'])->name('terms-and-condition-add');
Route::post('quotation/terms_and_condition/store', [TermsAndConditionController::class,'store'])->name('terms-and-condition-store');
Route::get('quotation/terms_and_condition/index', [TermsAndConditionController::class,'index'])->name('terms-and-condition-index');
Route::get('quotation/terms_and_condition/edit/{id}', [TermsAndConditionController::class, 'edit'])->name('terms-and-condition-edit');
Route::get('quotation/terms_and_condition/delete/{id}', [TermsAndConditionController::class, 'destroy'])->name('terms-and-condition-delete');
Route::PUT('quotation/terms_and_condition/update', [TermsAndConditionController::class, 'update'])->name('terms-and-condition-update');

// service-agreement-content
Route::get('quotation/service-agreement-content/add', [ServiceAgreementController::class,'contentcreate'])->name('service-agreement-content-add');
Route::post('quotation/service-agreement-content/store', [ServiceAgreementController::class,'contentstore'])->name('service-agreement-content-store');
Route::get('quotation/service-agreement-content/index', [ServiceAgreementController::class,'content'])->name('service-agreement-content-index');
Route::get('quotation/service-agreement-content/edit/{id}', [ServiceAgreementController::class, 'contentedit'])->name('service-agreement-content-edit');
Route::get('quotation/service-agreement-content/delete/{id}', [ServiceAgreementController::class, 'contentdestroy'])->name('service-agreement-content-delete');
Route::PUT('quotation/service-agreement-content/update', [ServiceAgreementController::class, 'contentupdate'])->name('service-agreement-content-update');

// Reminder
Route::get('reminder/add', [ReminderController::class,'create'])->name('reminder-add');
Route::post('reminder/store', [ReminderController::class,'store'])->name('reminder-store');
Route::get('reminder/index', [ReminderController::class,'index'])->name('reminder-index');
Route::get('reminder/edit/{id}', [ReminderController::class, 'edit'])->name('reminder-edit');
Route::get('reminder/delete/{id}', [ReminderController::class, 'destroy'])->name('reminder-delete');
Route::PUT('reminder/update', [ReminderController::class, 'update'])->name('reminder-update');
Route::get('reminder/off/{id}', [ReminderController::class, 'reminder_off'])->name('reminder-off');
Route::get('reminder/off-permanent/{id}', [ReminderController::class, 'reminder_off_permanent'])->name('reminder-off-permanent');

// Upwork Questions
Route::get('upwork/question/add', [UpworkQuestionsController::class,'create'])->name('upwork-questions-add');
Route::post('upwork/question/store', [UpworkQuestionsController::class,'store'])->name('upwork-questions-store');
Route::get('upwork/question/index', [UpworkQuestionsController::class,'index'])->name('upwork-questions-index');
Route::get('upwork/question/edit/{id}', [UpworkQuestionsController::class, 'edit'])->name('upwork-questions-edit');
Route::get('upwork/question/delete/{id}', [UpworkQuestionsController::class, 'destroy'])->name('upwork-questions-delete');
Route::PUT('upwork/question/update', [UpworkQuestionsController::class, 'update'])->name('upwork-questions-update');

// Upwork Projects
Route::get('upwork/projects/add', [UpworkProjectsController::class,'create'])->name('upwork-projects-add');
Route::post('upwork/projects/store', [UpworkProjectsController::class,'store'])->name('upwork-projects-store');
Route::get('upwork/projects/index', [UpworkProjectsController::class,'index'])->name('upwork-projects-index');
Route::get('upwork/projects/edit/{id}', [UpworkProjectsController::class, 'edit'])->name('upwork-projects-edit');
Route::get('upwork/projects/delete/{id}', [UpworkProjectsController::class, 'destroy'])->name('upwork-projects-delete');
Route::PUT('upwork/projects/update', [UpworkProjectsController::class, 'update'])->name('upwork-projects-update');
Route::get('upwork/projects/show/{id}', [UpworkProjectsController::class, 'show'])->name('upwork-projects-show');

// Upwork Question N Answer
Route::get('upwork/question-n-answer/add', [UpworkQuestionsnAnswerController::class,'create'])->name('upwork-question-n-answer-add');
Route::post('upwork/question-n-answer/store', [UpworkQuestionsnAnswerController::class,'store'])->name('upwork-question-n-answer-store');
Route::get('upwork/question-n-answer/index', [UpworkQuestionsnAnswerController::class,'index'])->name('upwork-question-n-answer-index');
Route::get('upwork/question-n-answer/edit/{id}', [UpworkQuestionsnAnswerController::class, 'edit'])->name('upwork-question-n-answer-edit');
Route::get('upwork/question-n-answer/delete/{id}', [UpworkQuestionsnAnswerController::class, 'destroy'])->name('upwork-question-n-answer-delete');
Route::PUT('upwork/question-n-answer/update', [UpworkQuestionsnAnswerController::class, 'update'])->name('upwork-question-n-answer-update');

// Upwork Client Reply
Route::get('upwork/client-replies/add', [UpworkClientReplyController::class,'create'])->name('upwork-client-replies-add');
Route::post('upwork/client-replies/store', [UpworkClientReplyController::class,'store'])->name('upwork-client-replies-store');
Route::get('upwork/client-replies/index', [UpworkClientReplyController::class,'index'])->name('upwork-client-replies-index');
Route::get('upwork/client-replies/edit/{id}', [UpworkClientReplyController::class, 'edit'])->name('upwork-client-replies-edit');
Route::get('upwork/client-replies/delete/{id}', [UpworkClientReplyController::class, 'destroy'])->name('upwork-client-replies-delete');
Route::PUT('upwork/client-replies/update', [UpworkClientReplyController::class, 'update'])->name('upwork-client-replies-update');

// Partners
Route::get('stratergic-partnership/add', [StratergicPartnershipController::class,'create'])->name('stratergic-partnership-add');
Route::post('stratergic-partnership/store', [StratergicPartnershipController::class,'store'])->name('stratergic-partnership-store');
Route::get('stratergic-partnership/index', [StratergicPartnershipController::class,'index'])->name('stratergic-partnership-index');

// Route::get('/banner/add', [BannerController::class, 'create']);
// Route::get('/banner/index', [BannerController::class, 'index']);
// Route::get('/banner/', [BannerController::class, 'index']);
// Route::post('/banner/store', [BannerController::class, 'store']);
// Route::get('/banner/edit/{id}', [BannerController::class, 'edit']);
// Route::get('/banner/delete/{id}', [BannerController::class, 'delete']);
// Route::PUT('/banner/update', [BannerController::class, 'update']);
// Clints side Route
    Route::get('client/url', [CommanController::class,'url'])->name('clientview-url');
    Route::get('client/edit/{id}/{client_type}', [CommanController::class,'edit'])->name('clientview-edit');
    Route::get('client/service/{id}', [ClientsController::class,'servcieedit']);
    Route::post('client/servcies', [ClientsController::class,'servcies']);
    Route::post('client/servicesstore/', [ClientsController::class,'servicesstore']);
    Route::get('send-mail', function () {
        $details = [
            'clientid' => session('clientid'),
            'servicesid' => session('currentservicesid')
        ];
        Mail::to('lad.vishal7@gmail.com')->send(new \App\Mail\Q_Mail($details));
        
    });
    Route::get('client/questions/{cid}/{sid}/{msid}', [ClientsController::class,'questions'])->name('questiona.Q_Mail');
    Route::get('mail', [ClientsController::class,'mail']);
    
    // Route::get('client/questions/', [ClientanswerController::class,'questions']);
    Route::post('client/questionsans/', [ClientanswerController::class,'questionsans']);
    Route::post('client/pagesection_1/', [ClientanswerController::class,'pagesection_1']);
    Route::post('client/pagesection_2/', [ClientanswerController::class,'pagesection_2']);
    Route::post('client/pagesection_3/', [ClientanswerController::class,'pagesection_3']);
    Route::post('client/pagesection_4/', [ClientanswerController::class,'pagesection_4']);
    Route::post('client/pagesection_5/', [ClientanswerController::class,'pagesection_5']);
    Route::post('client/pagesection_6/', [ClientanswerController::class,'pagesection_6']);
    Route::post('client/pagesection_7/', [ClientanswerController::class,'pagesection_7']);
    Route::post('client/pagesection_8/', [ClientanswerController::class,'pagesection_8']);
    Route::post('client/pagesection_9/', [ClientanswerController::class,'pagesection_9']);
    Route::post('client/pagesection_10/', [ClientanswerController::class,'pagesection_10']);
    Route::post('client/pagesectioncontent_1/', [ClientanswerController::class,'pagesectioncontent_1']);
    Route::post('client/pagesectioncontent_2/', [ClientanswerController::class,'pagesectioncontent_2']);
    Route::post('client/pagesectioncontent_3/', [ClientanswerController::class,'pagesectioncontent_3']);
    Route::post('client/pagesectioncontent_4/', [ClientanswerController::class,'pagesectioncontent_4']);
    Route::post('client/pagesectioncontent_5/', [ClientanswerController::class,'pagesectioncontent_5']);
    Route::post('client/pagesectioncontent_6/', [ClientanswerController::class,'pagesectioncontent_6']);
    Route::post('client/pagesectioncontent_7/', [ClientanswerController::class,'pagesectioncontent_7']);
    Route::post('client/pagesectioncontent_8/', [ClientanswerController::class,'pagesectioncontent_8']);
    Route::post('client/pagesectioncontent_9/', [ClientanswerController::class,'pagesectioncontent_9']);
    Route::post('client/pagesection_content_10/', [ClientanswerController::class,'pagesection_content_10']);
    Route::post('client/referencelink_1/', [ClientanswerController::class,'referencelink_1']);
    Route::post('client/referencelink_2/', [ClientanswerController::class,'referencelink_2']);
    Route::post('client/referencelink_3/', [ClientanswerController::class,'referencelink_3']);
    Route::post('client/referencelink_4/', [ClientanswerController::class,'referencelink_4']);
    Route::post('client/referencelink_5/', [ClientanswerController::class,'referencelink_5']);


    Route::post('client/new_questionsans/', [ClientanswerController::class,'new_questionsans']);
    // Client form route
    Route::post('client/first_name/{id}', [CommanController::class,'first_name']);
    Route::post('client/last_name/{id}', [CommanController::class,'last_name']);
    Route::post('client/dob/{id}', [CommanController::class,'dob']);
    Route::post('client/address1/{id}', [CommanController::class,'address1']);
    Route::post('client/address2/{id}', [CommanController::class,'address2']);
    Route::post('client/address3/{id}', [CommanController::class,'address3']);
    Route::post('client/landmark/{id}', [CommanController::class,'landmark']);
    Route::post('client/city/{id}', [CommanController::class,'city']);
    Route::post('client/pincode/{id}', [CommanController::class,'pincode']);
    Route::post('client/state/{id}', [CommanController::class,'state']);
    Route::post('client/country/{id}', [CommanController::class,'country']);
    Route::post('client/mobile/{id}', [CommanController::class,'mobile']);
    Route::post('client/landline/{id}', [CommanController::class,'landline']);
    Route::post('client/emailid/{id}', [CommanController::class,'emailid']);
    Route::post('client/skypeid/{id}', [CommanController::class,'skypeid']);
    Route::post('client/pancard/{id}', [CommanController::class,'pancard']);
    Route::post('client/gstin/{id}', [CommanController::class,'gstin']);
    Route::post('client/prefered_communication_platform/{id}', [CommanController::class,'prefered_communication_platform']);
    Route::post('client/business_name/{id}', [CommanController::class,'business_name']);
    Route::post('client/business_same_as_resedential/{id}', [CommanController::class,'business_same_as_resedential']);
    Route::post('client/business_address1/{id}', [CommanController::class,'business_address1']);
    Route::post('client/business_address2/{id}', [CommanController::class,'business_address2']);
    Route::post('client/business_address3/{id}', [CommanController::class,'business_address3']);
    Route::post('client/company_number/{id}', [CommanController::class,'company_number']);
    Route::post('client/driving_licence/{id}', [CommanController::class,'driving_licence']);
    Route::post('client/sign2_name/{id}', [CommanController::class,'sign2_name']);
    Route::post('client/sign2_email/{id}', [CommanController::class,'sign2_email']);
    Route::post('client/sign2_designation/{id}', [CommanController::class,'sign2_designation']);
    Route::post('client/sign1_designation/{id}', [CommanController::class,'sign1_designation']);
    Route::post('client/sign3_name/{id}', [CommanController::class,'sign3_name']);
    Route::post('client/sign3_email/{id}', [CommanController::class,'sign3_email']);
    Route::post('client/sign3_designation/{id}', [CommanController::class,'sign3_designation']);
// Clients List
    Route::get('clients/url/', [ClientsController::class,'url'])->name('clients-url');
    Route::get('clients/add', [ClientsController::class,'create'])->name('clients-add');
    Route::post('clients/store', [ClientsController::class,'store'])->name('clients-store');
    Route::get('clients/index', [ClientsController::class,'index'])->name('clients-index');
    Route::get('clients/edit/{id}', [ClientsController::class, 'edit'])->name('clients-edit');
    Route::get('clients/delete/{id}', [ClientsController::class, 'destroy'])->name('clients-delete');
    Route::PUT('clients/update', [ClientsController::class, 'update'])->name('clients-update');
    Route::get('clients/view/{id}', [ClientsController::class, 'view'])->name('clients-view');
    // MainServices List
    Route::get('mainservices/add', [MainServicesController::class,'create'])->name('mainservices-add');
    Route::post('mainservices/store', [MainServicesController::class,'store'])->name('mainservices-store');
    Route::get('mainservices/index', [MainServicesController::class,'index'])->name('mainservices-index');
    Route::get('mainservices/edit/{id}', [MainServicesController::class, 'edit'])->name('mainservices-edit');
    Route::get('mainservices/delete/{id}', [MainServicesController::class, 'destroy'])->name('mainservices-delete');
    Route::PUT('mainservices/update', [MainServicesController::class, 'update'])->name('mainservices-update');
// Services List
    Route::get('services/add', [ServiceController::class,'create'])->name('services-add');
    Route::post('services/store', [ServiceController::class,'store'])->name('services-store');
    Route::get('services/index', [ServiceController::class,'index'])->name('services-index');
    Route::get('services/edit/{id}', [ServiceController::class, 'edit'])->name('services-edit');
    Route::get('services/delete/{id}', [ServiceController::class, 'destroy'])->name('services-delete');
    Route::PUT('services/update', [ServiceController::class, 'update'])->name('services-update');
// Questions List
    Route::get('questions/add', [QuestionController::class,'create'])->name('questions-add');
    Route::post('questions/store', [QuestionController::class,'store'])->name('questions-store');
    Route::get('questions/index', [QuestionController::class,'index'])->name('questions-index');
    Route::get('questions/edit/{id}', [QuestionController::class, 'edit'])->name('questions-edit');
    Route::get('questions/delete/{id}', [QuestionController::class, 'destroy'])->name('questions-delete');
    Route::PUT('questions/update', [QuestionController::class, 'update'])->name('questions-update');
    Route::get('question-finish/{id}', [QuestionController::class, 'finish'])->name('question-finish');


Route::get('submit-form/{id}', [ClientsController::class,'submit_form'])->name('submit-form');
Route::get('submit-question', [ClientsController::class,'submit_question'])->name('submit-question');

Route::get('submit-form', function () {
    $successMessage = 'Your form has been submitted successfully!';
    return view('blank-dashboard', compact('successMessage'));
})->name('submit-form');


    Route::get('/add', function () {
        return view('quotation/create');
    });
Route::get('html-pdf', [QuotationEstimationController::class, 'htmlPdf'])->name('htmlPdf');
Route::get('/add', function () {
    return view('quotation/create');
});
Route::get('/html', function () {
    return view('htmlView');
});



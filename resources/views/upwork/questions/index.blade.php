@extends('layouts.index')

@section('title')
    Upwork Questions
@endsection()

@section('content')
    <?php $i = 0; ?>
    <!-- Content Header (Page header) -->


    <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
        <div class="breadcrumb-title pe-3">All Upwork Questions</div>
        <div class="ps-3">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb mb-0 p-0">
                    <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">Upwork Questions</li>
                </ol>
            </nav>
        </div>
        <div class="ms-auto">
            <div class="btn-group">
                <a href="{{ route('upwork-questions-add') }}">
                    <button type="button" class="btn btn-light">Add New</button>
                </a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-9 mx-auto">
            <div class="card">
                <div class="card-body">
                    <table class="table mb-0">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col" colspan="2">Action</th>
                                <th scope="col">Questions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($questions as $question)
                                <tr>
                                    <td scope="row">{{ ++$i }}</td>
                                    <td>
                                        <a href="{{route('upwork-questions-edit', $question->id)}}">
                                            Edit
                                        </a>
                                    </td>
                                    <td>
                                        <a onclick="return confirm('Are you sure?')" href="{{route('upwork-questions-delete', $question->id)}}">
                                            Delete
                                        </a>
                                    </td>
                                    <td>
                                        {{ $question->question }}
                                    </td>
                                </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
      <!-- /.content-header -->
    <?php $i = 0;?>

    <!--!! $projects->links() !!-->

@endsection()

@section('scripts')

@endsection()

@extends('layouts.index')

@section('title')
            Question and Answer
@endsection()

@section('content')
    <?php $i = 0; ?>
    <!-- Content Header (Page header) -->


    <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
        <div class="breadcrumb-title pe-3">All Question and Answer</div>
        <div class="ps-3">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb mb-0 p-0">
                    <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">Question and Answer</li>
                </ol>
            </nav>
        </div>
        <div class="ms-auto">
            <div class="btn-group">
                <a href="{{ route('upwork-question-n-answer-add') }}">
                    <button type="button" class="btn btn-light">Add New</button>
                </a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-9 mx-auto">
            <div class="card">
                <div class="card-body">
                    <table class="table mb-0">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col" colspan="2">Action</th>
                                <th scope="col">Project Name</th>
                                <th scope="col">Question</th>
                                <th scope="col">Answer</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($question_n_answers as $question_n_answer)
                                <tr>
                                    <td scope="row">{{ ++$i }}</td>
                                    <td>
                                        <a href="{{route('upwork-question-n-answer-edit', $question_n_answer->id)}}">
                                            Edit
                                        </a>
                                    </td>
                                    <td>
                                        <a onclick="return confirm('Are you sure?')" href="{{route('upwork-question-n-answer-delete', $question_n_answer->id)}}">
                                            Delete
                                        </a>
                                    </td>
                                    <td>
                                        @foreach ($projects as $project)
                                            @if ( isset($project->id) )
                                                @if ($project->id == $question_n_answer->upwork_project_id )
                                                    {{ $project->project_name }}
                                                @endif
                                            @endif
                                        @endforeach
                                    </td>
                                    <td>
                                        @foreach ($questions as $question)
                                            @if ( isset($question->id) )
                                                @if ($question->id == $question_n_answer->upwork_question_id )
                                                    {{ $question->question }}
                                                @endif
                                            @endif
                                        @endforeach
                                    </td>
                                    <td>
                                        {{ $question_n_answer->answer }}
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
      <!-- /.content-header -->
    <?php $i = 0;?>

    <!--!! $projects->links() !!-->

@endsection()

@section('scripts')

@endsection()

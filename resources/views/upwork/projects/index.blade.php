@extends('layouts.index')

@section('title')
            Upwork Project
@endsection()

@section('content')
    <?php $i = 0; ?>
    <!-- Content Header (Page header) -->
    <style type="text/css">
        .imageThumb {
            max-height: 75px;
            border: 2px solid;
            padding: 1px;
            cursor: pointer;
        }
        .pip {
            display: inline-block;
            margin: 10px 10px 0 0;
        }
    </style>

    <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
        <div class="breadcrumb-title pe-3">All Upwork Categories</div>
        <div class="ps-3">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb mb-0 p-0">
                    <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">Upwork Project</li>
                </ol>
            </nav>
        </div>
        <div class="ms-auto">
            <div class="btn-group">
                <a href="{{ route('upwork-projects-add') }}">
                    <button type="button" class="btn btn-light">Add New</button>
                </a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-9 mx-auto">
            <div class="card">
                <div class="card-body">
                    <table class="table mb-0">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col" colspan="2">Action</th>
                                <th scope="col">Project Name</th>
                                <th scope="col">Profile Name</th>
                                <th scope="col">Client Name</th>
                                <th scope="col">Project Details</th>
                                <th scope="col">Project Details Screenshot</th>
                                <th scope="col">Project Link</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($projects as $project)
                                <tr>
                                    <td scope="row">{{ ++$i }}</td>
                                    <td>
                                        <a href="{{route('upwork-projects-edit', $project->id)}}">
                                            Edit
                                        </a>
                                    </td>
                                    <td>
                                        <a onclick="return confirm('Are you sure?')" href="{{route('upwork-projects-delete', $project->id)}}">
                                            Delete
                                        </a>
                                    </td>
                                    <td>
                                        {{ $project->project_name }}
                                    </td>
                                    <td>
                                        {{ $project->profile_name }}
                                    </td>
                                    <td>
                                        {{ $project->client_name }}
                                    </td>
                                    <td>
                                        {{ $project->project_details }}
                                    </td>
                                    <td>
                                        <?php
                                        $imgs = explode(", ",$project->project_details_screenshot);
                                        for ($i=0; $i < count($imgs); $i++) { 
                                            // code...
                                            if (isset($imgs[$i]) && ! empty($imgs[$i])) {
                                                ?>
                                                <span class="pip">
                                                    <img class="imageThumb" class="img-fluid" src="{{ url('public/uploads/'.$imgs[$i]) }}">
                                                </span>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </td>
                                    <td>
                                        @if ( isset( $project->project_link ) )
                                            <a href="{{ $project->project_link }}" target="_blank"> Open Project</a>
                                        @endif
                                        <a href="{{route('upwork-projects-show', $project->id)}}">Show project</a>
                                    </td>
                                </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
      <!-- /.content-header -->
    <?php $i = 0;?>

    <!--!! $projects->links() !!-->

@endsection()

@section('scripts')

  function previewFiles(input){
      var file = $("input[type=file]").get(0).files[0];

      if(file){
          var reader = new FileReader();

          reader.onload = function(){
              $("#previewImg").attr("style", "height: 180px; display:block;");
              $("#previewImg").attr("src", reader.result);
          }

          reader.readAsDataURL(file);
      }
  }
@endsection()

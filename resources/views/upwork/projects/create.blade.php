
@extends('layouts.index')

@section('title')
            Upwork Project
@endsection()

@section('content')
    <style type="text/css">
        input[type="file"] {
            display: block;
        }
        .imageThumb {
            max-height: 75px;
            border: 2px solid;
            padding: 1px;
            cursor: pointer;
        }
        .pip {
            display: inline-block;
            margin: 10px 10px 0 0;
        }
        .remove {
            display: block;
            background: #444;
            border: 1px solid black;
            color: white;
            text-align: center;
            cursor: pointer;
        }
        .remove:hover {
            background: white;
            color: black;
        }
        .side-menu{
            position: fixed !important;
        }

    </style>
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
        <div class="breadcrumb-title pe-3">Add Upwork Project</div>
        <div class="ps-3">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb mb-0 p-0">
                    <li class="breadcrumb-item"><a href="javascript:void(0);"><i class="bx bx-home-alt"></i></a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">Upwork Project</li>
                </ol>
            </nav>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-9 mx-auto">
            <hr/>
            <div class="card">
                <div class="card-body">
                    <div class="p-4 border rounded">
                        <form method="post" action="{{ route('upwork-projects-store') }}" class="row g-3 needs-validation" enctype="multipart/form-data"  novalidate>
                            @csrf
                            <div class="col-md-12">
                                <label for="validationCustom01" class="form-label">Project Name</label>
                                <input type="text" class="form-control" id="validationCustom01" value="" name="project_name" placeholder="Project Name" required>
                                <div class="valid-feedback">Looks good!</div>
                            </div>
                            <div class="col-md-12">
                                <label for="validationCustom06" class="form-label">Profile Name</label>
                                <input type="text" class="form-control" id="validationCustom06" value="" name="profile_name" placeholder="Profile Name" required>
                                <div class="valid-feedback">Looks good!</div>
                            </div>
                            <div class="col-md-12">
                                <label for="validationCustom02" class="form-label">Client Name</label>
                                <input type="text" class="form-control" id="validationCustom02" value="" name="client_name" placeholder="Client Name" required>
                                <div class="valid-feedback">Looks good!</div>
                            </div>
                            <div class="col-md-12">
                                <label for="validationCustom03" class="form-label">Project Details</label>
                                <input type="text" class="form-control" id="validationCustom03" value="" name="project_details" placeholder="Project Details" >
                                <div class="valid-feedback">Looks good!</div>
                            </div>
                            <div class="col-md-12">
                                <label for="validationCustom04" class="form-label">Project Details Screenshot</label>
                                <input type="file" name="project_details_screenshot[]" class="custom-file-input" id="files" multiple>
                                <div class="valid-feedback">Looks good!</div>
                            </div>
                            <div class="col-md-12">
                                <label for="validationCustom05" class="form-label">Project Link</label>
                                <input type="text" class="form-control" id="validationCustom05" value="" name="project_link" placeholder="Project Link">
                                <div class="valid-feedback">Looks good!</div>
                            </div>
                            <div class="col-12">
                                <button class="btn btn-light" type="submit">Submit form</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection()
@section('loadstyles')
    <!-- <link href="{{URL::to('/')}}/assets/plugins/fancy-file-uploader/fancy_fileupload.css" rel="stylesheet" />
    <link href="{{URL::to('/')}}/assets/plugins/Drag-And-Drop/dist/imageuploadify.min.css" rel="stylesheet" />
    <link href="{{URL::to('/')}}/assets/plugins/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet" /> -->
@endsection()
@section('loadscript')
    <script src="{{URL::to('/')}}/assets/plugins/simplebar/js/simplebar.min.js"></script>
    <script src="{{URL::to('/')}}/assets/plugins/metismenu/js/metisMenu.min.js"></script>
    <script src="{{URL::to('/')}}/assets/plugins/perfect-scrollbar/js/perfect-scrollbar.js"></script>
    <!-- <script src="{{URL::to('/')}}/assets/plugins/fancy-file-uploader/jquery.ui.widget.js"></script>
    <script src="{{URL::to('/')}}/assets/plugins/fancy-file-uploader/jquery.fileupload.js"></script>
    <script src="{{URL::to('/')}}/assets/plugins/fancy-file-uploader/jquery.iframe-transport.js"></script>
    <script src="{{URL::to('/')}}/assets/plugins/fancy-file-uploader/jquery.fancy-fileupload.js"></script>
    <script src="{{URL::to('/')}}/assets/plugins/Drag-And-Drop/dist/imageuploadify.min.js"></script> -->
    <script type="text/javascript">
        
        function validURL(str) {
          var pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
            '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
            '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
            '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
            '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
            '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
          return !!pattern.test(str);
        }
    </script>
@endsection()
@section('scripts')
$('#validationCustom05').change(function(){
    if( $('#validationCustom05').val() ) {
        if( ! validURL($('#validationCustom05').val()) ) {
            alert('Please enter proper URL.');
        }
    }
});
if (window.File && window.FileList && window.FileReader) {
    $("#files").on("change", function(e) {
      var files = e.target.files,
        filesLength = files.length;
      for (var i = 0; i < filesLength; i++) {
        var f = files[i]
        var fileReader = new FileReader();
        fileReader.onload = (function(e) {
          var file = e.target;
          $("<span class=\"pip\">" +
            "<img class=\"imageThumb\" src=\"" + e.target.result + "\" title=\"" + file.name + "\"/>" +
            "<br/><span class=\"remove\">Remove image</span>" +
            "</span>").insertAfter("#files");
          $(".remove").click(function(){
            $(this).parent(".pip").remove();
          });
          
          // Old code here
          /*$("<img></img>", {
            class: "imageThumb",
            src: e.target.result,
            title: file.name + " | Click to remove"
          }).insertAfter("#files").click(function(){$(this).remove();});*/
          
        });
        fileReader.readAsDataURL(f);
      }
      console.log(files);
    });
  } 
var forms = document.querySelectorAll('.needs-validation')

// Loop over them and prevent submission
Array.prototype.slice.call(forms)
.forEach(function (form) {
  form.addEventListener('submit', function (event) {
    if (!form.checkValidity()) {
      event.preventDefault()
      event.stopPropagation()
    }

    form.classList.add('was-validated')
  }, false)
})

@endsection()

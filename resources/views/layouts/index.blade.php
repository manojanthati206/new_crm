<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--favicon-->
    <link rel="icon" href="{{URL::to('/')}}/assets/images/favicon-32x32.png" type="image/png" />
    <!--plugins-->
    <link href="{{URL::to('/')}}/assets/plugins/simplebar/css/simplebar.css" rel="stylesheet" />
    <link href="{{URL::to('/')}}/assets/plugins/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet" />
    <link href="{{URL::to('/')}}/assets/plugins/metismenu/css/metisMenu.min.css" rel="stylesheet" />
    <link href="{{URL::to('/')}}/assets/plugins/datatable/css/dataTables.bootstrap5.min.css" rel="stylesheet" />
    @yield('loadstyles')
    <!-- loader-->
    <link href="{{URL::to('/')}}/assets/css/pace.min.css" rel="stylesheet" />
    <script src="{{URL::to('/')}}/assets/js/pace.min.js"></script>
    <!-- Bootstrap CSS -->
    <link href="{{URL::to('/')}}/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{URL::to('/')}}/assets/css/app.css" rel="stylesheet">
    <link href="{{URL::to('/')}}/assets/css/icons.css" rel="stylesheet">
    <!-- Theme Style CSS -->
    <!-- <link rel="stylesheet" href="{{URL::to('/')}}/assets/css/dark-theme.css" />
    <link rel="stylesheet" href="{{URL::to('/')}}/assets/css/semi-dark.css" />
    <link rel="stylesheet" href="{{URL::to('/')}}/assets/css/header-colors.css" /> -->
    <title>@yield('title')</title>
</head>
<body class="bg-theme bg-theme2">
    <!--wrapper-->
    <div class="wrapper">
        <!--sidebar wrapper -->
        <div class="sidebar-wrapper" data-simplebar="true">
            <div class="sidebar-header">
                <div>
                    <img src="{{URL::to('/')}}/assets/images/logo-icon.png" class="logo-icon" alt="logo icon">
                </div>
                <div>
                    <h4 class="logo-text">neshallWeb</h4>
                </div>
                <div class="toggle-icon ms-auto">
                    <i class='bx bx-arrow-to-left'></i>
                </div>
            </div>
            <!--navigation-->
            <ul class="metismenu" id="menu">
                <li>
                    <a href="{{url('/')}}" class="prevent-default">
                        <div class="parent-icon"><i class='bx bx-home-circle'></i>
                        </div>
                        <div class="menu-title">Dashboard</div>
                    </a>
                </li>
                <li>
                    <a href="{{route('main-category-index')}}" class="prevent-default">
                        <div class="parent-icon"><i class='bx bx-home-circle'></i>
                        </div>
                        <div class="menu-title">Main Category</div>
                    </a>
                </li>
                <li>
                    <a href="{{route('features-index')}}" class="prevent-default">
                        <div class="parent-icon"><i class='bx bx-home-circle'></i>
                        </div>
                        <div class="menu-title">Features</div>
                    </a>
                </li>
                <li>
                    <a href="{{route('features-list-index')}}" class="prevent-default">
                        <div class="parent-icon"><i class='bx bx-home-circle'></i>
                        </div>
                        <div class="menu-title">Features List</div>
                    </a>
                </li>
                <li>
                    <a href="{{route('estimation-index')}}" class="prevent-default">
                        <div class="parent-icon"><i class='bx bx-home-circle'></i>
                        </div>
                        <div class="menu-title">Estimation</div>
                    </a>
                </li>
                <li>
                    <a href="{{route('estimation-template-index')}}" class="prevent-default">
                        <div class="parent-icon"><i class='bx bx-home-circle'></i>
                        </div>
                        <div class="menu-title">Estimation templates</div>
                    </a>
                </li>
                <li>
                    <a href="{{route('clients-index')}}" class="prevent-default">
                        <div class="parent-icon"><i class='bx bx-user'></i>
                        </div>
                        <div class="menu-title">Client</div>
                    </a>
                </li>
                <li>
                    <a href="{{route('mainservices-index')}}" class="prevent-default">
                        <div class="parent-icon"><i class='bx bx-user'></i>
                        </div>
                        <div class="menu-title">Main Services</div>
                    </a>
                </li>
                <li>
                    <a href="{{route('services-index')}}" class="prevent-default">
                        <div class="parent-icon"><i class='bx bx-user'></i>
                        </div>
                        <div class="menu-title"> Services</div>
                    </a>
                </li>
                <li>
                    <a href="{{route('questions-index')}}" class="prevent-default">
                        <div class="parent-icon"><i class='bx bx-user'></i>
                        </div>
                        <div class="menu-title">Questions</div>
                    </a>
                </li>
                <li>
                    <a href="{{route('reminder-index')}}" class="prevent-default">
                        <div class="parent-icon"><i class='bx bx-home-circle'></i>
                        </div>
                        <div class="menu-title">Reminder</div>
                    </a>
                </li>
                <li>
                    <a href="{{route('upwork-questions-index')}}" class="prevent-default">
                        <div class="parent-icon"><i class='bx bx-home-circle'></i>
                        </div>
                        <div class="menu-title">Upwork Question</div>
                    </a>
                </li>
                <li>
                    <a href="{{route('upwork-projects-index')}}" class="prevent-default">
                        <div class="parent-icon"><i class='bx bx-home-circle'></i>
                        </div>
                        <div class="menu-title">Upwork Project</div>
                    </a>
                </li>
                <li>
                    <a href="{{route('upwork-question-n-answer-index')}}" class="prevent-default">
                        <div class="parent-icon"><i class='bx bx-home-circle'></i>
                        </div>
                        <div class="menu-title">Upwork Question and Answer</div>
                    </a>
                </li>
                <li>
                    <a href="{{route('upwork-client-replies-index')}}" class="prevent-default">
                        <div class="parent-icon"><i class='bx bx-home-circle'></i>
                        </div>
                        <div class="menu-title">Upwork Client Reply</div>
                    </a>
                </li>
                <li>
                    <a href="{{route('terms-and-condition-index')}}" class="prevent-default">
                        <div class="parent-icon"><i class='bx bx-home-circle'></i>
                        </div>
                        <div class="menu-title">Terms and Condition</div>
                    </a>
                </li>
                <li>
                    <a href="{{route('service-agreement-content-index')}}" class="prevent-default">
                        <div class="parent-icon"><i class='bx bx-home-circle'></i>
                        </div>
                        <div class="menu-title">Service Agreement Content</div>
                    </a>
                </li>
            </ul>
            <!--end navigation-->
        </div>
        <!--end sidebar wrapper -->
        <!--start header -->
        <header>
            <div class="topbar d-flex align-items-center">
                <nav class="navbar navbar-expand">
                    <div class="mobile-toggle-menu"><i class='bx bx-menu'></i>
                    </div>
                </nav>
            </div>
        </header>
        <!--end header -->
        <!--start page wrapper -->
        <div class="page-wrapper">
            <div class="page-content">
                @yield('content')
            </div>
        </div>
        <!--end page wrapper -->
        <!--start overlay-->
        <div class="overlay toggle-icon"></div>
        <!--end overlay-->
        <!--Start Back To Top Button--> <a href="javaScript:;" class="back-to-top"><i class='bx bxs-up-arrow-alt'></i></a>
        <!--End Back To Top Button-->
        <footer class="page-footer" >
            <p class="mb-0">neshallWeb  Copyright © 2021. All right reserved.</p>
        </footer>
    </div>
    <!--end wrapper-->
    <!--start switcher-->
    <!--end switcher-->
    <!-- Bootstrap JS -->
    <script src="{{URL::to('/')}}/assets/js/bootstrap.bundle.min.js"></script>
    <!--plugins-->
    <script src="{{URL::to('/')}}/assets/js/jquery.min.js"></script>
    <script src="{{URL::to('/')}}/assets/js/jquery.validate.min.js"></script>
    @yield('loadscript')
    <script defer>
        @yield('outsidescripts')
        
            @yield('scripts')
            jQuery('#validationCustom02').focusout(function(e){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                });
                var $estimation_no = jQuery('#validationCustom02').val();
                jQuery.ajax({
                    url: "{{ url('quotation/estimation/checkExistEstimateNo') }}/"+$estimation_no,
                    method: 'get',
                    data: {
                        estimate_no: jQuery('#validationCustom02').val(),
                    },
                    success: function(result){
                        if( result == '0' ) {
                            alert( 'Estimate ID is already exists.' );
                            $('#validationCustom02').css('border','1px solid red');
                            $('.btn-quotation').attr("disabled", true);
                            $('.btn-service-agreement').attr("disabled", true);
                            $('#validationCustom02').parent().find('.valid-feedback').css('display','none');
                        } else {
                            $('#validationCustom02').parent().find('.valid-feedback').css('display','block');
                            $('#validationCustom02').css('border','1px solid #17a00e');
                            $('.btn-quotation').attr("disabled", false);
                            $('.btn-service-agreement').attr("disabled", false);
                        }
                    }
                });
            
          } );
    </script>
    <!--app JS-->
    <script src="{{URL::to('/')}}/assets/js/app.js"></script>
    <script>
    </script>
</body>
</html>
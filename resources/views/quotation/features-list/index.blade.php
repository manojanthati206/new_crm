@extends('layouts.index')

@section('title')
    Features List
@endsection()

@section('content')
    <?php $i = 0; ?>
    <!-- Content Header (Page header) -->


    <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
        <div class="breadcrumb-title pe-3">All Features List</div>
        <div class="ps-3">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb mb-0 p-0">
                    <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">Features List</li>
                </ol>
            </nav>
        </div>
        <div class="ms-auto">
            <div class="btn-group">
                <a href="{{ route('features-list-add') }}">
                    <button type="button" class="btn btn-light">Add New</button>
                </a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-9 mx-auto">
            <div class="card">
                <div class="card-body">
                    <table class="table mb-0">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col" colspan="2">Action</th>
                                <th scope="col">Title</th>
                                <th scope="col">Service</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($features_lists as $features_list)
                                <tr>
                                    <td scope="row">{{ ++$i }}</td>
                                    <td>
                                        <a href="{{route('features-list-edit', $features_list->id)}}">
                                            Edit
                                        </a>
                                    </td>
                                    <td>
                                        <a onclick="return confirm('Are you sure?')" href="{{route('features-list-delete', $features_list->id)}}">
                                            Delete
                                        </a>
                                    </td>
                                    <td>
                                        {{ $features_list->feature_list }}
                                    </td>
                                    <td>
                                        @foreach ($features as $feature)
                                            @if ($features_list->feature_id == $feature->id )
                                                {{ $feature->feature_title }}
                                            @endif
                                        @endforeach
                                    </td>
                                </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
      <!-- /.content-header -->
    <?php $i = 0;?>

    <!--!! $projects->links() !!-->

@endsection()

@section('scripts')

$("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');

@endsection()

@extends('layouts.index')

@section('title')
    Templates
@endsection()

@section('content')
    <?php $i = 0; ?>
    <!-- Content Header (Page header) -->


    <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
        <div class="breadcrumb-title pe-3">All templates</div>
        <div class="ps-3">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb mb-0 p-0">
                    <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">Templates</li>
                </ol>
            </nav>
        </div>
        <div class="ms-auto">
            <div class="btn-group">
                <a href="#">
                    <button type="button" class="btn btn-light">Add New</button>
                </a>
            </div>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-12 col-lg-4">
            <div class="card shadow-none border radius-15">
                <a href="{{ route('estimation-template-webdev') }}">
                    <div class="card-body">
                        <div class="d-flex align-items-center">
                            <div class="fm-icon-box radius-15"><i class='lni lni-google-drive'></i>
                            </div>
                            <div class="ms-auto font-24"><i class='bx bx-dots-horizontal-rounded'></i>
                            </div>
                        </div>
                        <h5 class="mt-3 mb-0">Web Developement</h5>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-12 col-lg-4">
            <div class="card shadow-none border radius-15">
                <a href="{{ route('estimation-template-graphdesign') }}">
                    <div class="card-body">
                        <div class="d-flex align-items-center">
                            <div class="fm-icon-box radius-15"><i class='lni lni-dropbox-original'></i>
                            </div>
                            <div class="ms-auto font-24"><i class='bx bx-dots-horizontal-rounded'></i>
                            </div>
                        </div>
                        <h5 class="mt-3 mb-0">Graphics Design</h5>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-12 col-lg-4">
            <div class="card shadow-none border radius-15">
                <a href="{{ route('estimation-template-video') }}">
                    <div class="card-body">
                        <div class="d-flex align-items-center">
                            <div class="fm-icon-box radius-15"><i class='bx bxs-door-open'></i>
                            </div>
                            <div class="ms-auto font-24"><i class='bx bx-dots-horizontal-rounded'></i>
                            </div>
                        </div>
                        <h5 class="mt-3 mb-0">Video Services</h5>
                    </div>
                </a>
            </div>
        </div>

        <div class="col-12 col-lg-4">
            <div class="card shadow-none border radius-15">
                <a href="{{ route('estimation-template-social') }}">
                    <div class="card-body">
                        <div class="d-flex align-items-center">
                            <div class="fm-icon-box radius-15"><i class='lni lni-google-drive'></i>
                            </div>
                            <div class="ms-auto font-24"><i class='bx bx-dots-horizontal-rounded'></i>
                            </div>
                        </div>
                        <h5 class="mt-3 mb-0">Social Media</h5>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-12 col-lg-4">
            <div class="card shadow-none border radius-15">
                <a href="{{ route('estimation-template-mobapp') }}">
                    <div class="card-body">
                        <div class="d-flex align-items-center">
                            <div class="fm-icon-box radius-15"><i class='lni lni-dropbox-original'></i>
                            </div>
                            <div class="ms-auto font-24"><i class='bx bx-dots-horizontal-rounded'></i>
                            </div>
                        </div>
                        <h5 class="mt-3 mb-0">Mobile Application</h5>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-12 col-lg-4">
            <div class="card shadow-none border radius-15">
                <a href="{{ route('estimation-template-digitmarket') }}">
                    <div class="card-body">
                        <div class="d-flex align-items-center">
                            <div class="fm-icon-box radius-15"><i class='bx bxs-door-open'></i>
                            </div>
                            <div class="ms-auto font-24"><i class='bx bx-dots-horizontal-rounded'></i>
                            </div>
                        </div>
                        <h5 class="mt-3 mb-0">Digital Marketing</h5>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-12 col-lg-4">
            <div class="card shadow-none border radius-15">
                <a href="{{ route('estimation-template-seo') }}">
                    <div class="card-body">
                        <div class="d-flex align-items-center">
                            <div class="fm-icon-box radius-15"><i class='bx bxs-door-open'></i>
                            </div>
                            <div class="ms-auto font-24"><i class='bx bx-dots-horizontal-rounded'></i>
                            </div>
                        </div>
                        <h5 class="mt-3 mb-0">SEO and Marketing Services</h5>
                    </div>
                </a>
            </div>
        </div>
    </div>
    
      <!-- /.content-header -->
    <?php $i = 0;?>

    <!--!! $projects->links() !!-->

@endsection()

@section('scripts')

$("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');

@endsection()

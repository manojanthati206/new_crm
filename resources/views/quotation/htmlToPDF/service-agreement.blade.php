<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>ESTIMATION</title>
    <style>
        /**
                Set the margins of the page to 0, so the footer and the header
                can be of the full height and width !
             **/
        @page {
            margin: 0cm 0cm;
        }

        /** Define now the real margins of every page in the PDF **/
        body {
            margin-top: 2cm;
            margin-left: 2cm;
            margin-right: 2cm;
            margin-bottom: 2cm;
        }

        /** Define the header rules **/
        header {
            position: fixed;
            top: 0cm;
            left: 0cm;
            right: 0cm;
            height: 2cm;

            /** Extra personal styles **/
            background-color: #f5f5f5;
            color: #afafaf;
            text-align: center;
        }

        /** Define the footer rules **/
        footer {
            position: fixed;
            bottom: 0cm;
            left: 0cm;
            right: 0cm;
            height: 2cm;

            /** Extra personal styles **/
            background-color: #f5f5f5;
            color: white;
            text-align: center;
        }
    </style>
</head>
<body>
    <?php
        if ( $price_type != "$" ) {
            $price_type = '<img width="12px" height="12px" src="' . URL::to('/assets/images/rupees-50x50.png') . '">';
        }
    ?>
    <!-- Define header and footer blocks before your content -->
    <header>
        <div class="container" style="max-width: 980px; margin: 0 auto; width: 100%;">
            <div class="row" style="padding: 0 15px;display: flex; align-items: center; justify-content: space-between;">
                <div class="logo" style="float: left; padding-top: 15px;"><img src="{{URL::to('/')}}/assets/images/logo.png" style="opacity: 0.5; width: 200px;"></div>
                <div class="right-content" style="float: right;color: #afafaf;line-height: normal;padding-top: 20px;font-size: 14px">
                    <span>CIN: U72900GJ2021PTC119524</span><br>
                </div>
            </div>
        </div>
    </header>

    <footer style="height: 100px;">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <p style="font-size: 12px; color: #626262; line-height: 20px;">C/O PATEL BIPINBHAI DHIRUBHAI, ATAKPARDI, KUBER SIDHHI, Valsad, Gujrat, India, 396007 </p>
                </div>
                <div class="col-md-6">
                    <div class="footer-address" style="font-size:12px;color: #626262;">
                        <img style="width: 12px;padding-right: 5px;" src="{{URL::to('/')}}/assets/images/call-icon.png">+91 6354425747
                        <img style="width: 12px; margin-left: 10px; padding-right: 5px;" src="{{URL::to('/')}}/assets/images/mail-icon.png">info@neshallweb.com
                        <img style="width: 12px;  margin-left: 10px;padding-right: 5px;" src="{{URL::to('/')}}/assets/images/url-icon.png">www.neshallweb.com
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <!-- Wrap the content of your PDF inside a main tag -->
    <main style="background-image: url({{URL::to('/')}}/assets/images//Neshallweb-logo.png); background-repeat: no-repeat; background-position: center center; opacity: 0.08;">
        <table class="table" style="width: 650px;padding: 0 15px">
            <thead>
            <tr>
                <td colspan="2"></td>
                <td colspan="2" style="text-align: right; font-size: 24px;padding-top: 30px;padding-bottom: 10px;color: #7bb4d8;">SERVICES AGREEMENT</td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td colspan="5" style="line-height: 20px;">This Contract for Services is made effective as of {{date('d F Y', strtotime($estimate_date))}}, by and between {{$client_name}} of {{$client_address}} (the "Recipient"), and neshallWeb Private Limited of Valsad, GJ, India (the "Provider").</td>
            </tr>
            <tr>
                <td colspan="5" style="line-height: 20px;"><b>1. DESCRIPTION OF SERVICES.</b> Beginning on {{date('d F Y', strtotime($estimate_date))}}, the Provider will serve to the Recipient the following services (collectively, the "Services")</td>
            </tr>
            </tbody>
        </table>
        @if( isset( $html_array ) && ! empty( $html_array ) )
            @for($i=0;$i < count($html_array);$i++)
                @if( isset( $heading_array[$i] ) && ! empty( $heading_array[$i] ) )
                    {!! $heading_array[$i] !!}
                @endif
                @if( isset( $html_array[$i] ) && ! empty( $html_array[$i] ) )
                    {!! $html_array[$i] !!}
                @endif
            @endfor
        @else
            {!! $html !!}
        @endif
        <?php $gst = 0;?>
        <table style="width: 650px;padding: 0 15px">
            <tbody>
                <tr><td colspan="5" style="border-bottom: 1px solid #a5a5a5;"></td></tr>
                <tr><td colspan="5" style="height: 20px"></td></tr>
                <tr>
                    <td colspan="5" style="line-height: 22px;"><b>2. PAYMENT.</b> Recipient will pay total of <b>{!!$price_type!!}{{$price}} </b> to the Provider for the agreed services. Payment shall be made to Provider according to the following schedule:</td>
                </tr>
                <tr><td colspan="5" style="height: 5px"></td></tr>
                <tr>
                    <td colspan="5" style="line-height: 20px;"><b>Events and Payments Schedule:</b></td>
                </tr>
                <tr>
                    <td colspan="3" style="line-height: 20px; width: 500px;">
                        <table style="width: 100%;">
                            <tbody>
                            <tr>
                                <td colspan="4">• Signing Service Agreement (40%)</td>
                            </tr>
                            <tr>
                                <td width="20px"></td>
                                <td colspan="4">Amount payable: {!!$price_type!!}<?php echo (($price+$gst)*40/100);?></td>
                            </tr>
                            <tr>
                                <td colspan="4">• Final Project Delivery (60%)</td>
                            </tr>
                            <tr>
                                <td width="20px"></td>
                                <td colspan="4">Amount payable: {!!$price_type!!}<?php echo (($price+$gst)*60/100);?> </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr><td colspan="5" style="height: 5px"></td></tr>
                <tr>
                    <td colspan="5" style="line-height: 22px;">If any invoice is not paid when due, interest will be added to and payable on all overdue amounts at 20 percent per year, or the maximum percentage allowed under applicable State laws, whichever is less.</td>
                </tr>
                <tr><td colspan="2" style="height: 5px"></td></tr>
                <tr>
                    <td colspan="5" style="line-height: 22px;">In addition to any other right or remedy provided by law, if Recipient fail to pay for the Services when due, Provider has the option to treat such failure to pay as a material breach of this Contract and may cancel this Contract and/or seek legal remedies.</td>
                </tr>
                @foreach ($serviceagreementcontents as $serviceagreementcontent)
                    <tr>
                        <td colspan="3" style="height: 5px"></td>
                    </tr>
                    <tr>
                        <td colspan="5" style="line-height: 22px;">
                            <?php echo $serviceagreementcontent->content;?>
                        </td>
                    </tr>
                @endforeach
                <tr><td colspan="2" style="height: 5px"></td></tr>
                <tr>
                    <td style="line-height: 22px; text-decoration: underline;"><b>Recipient:</b></td>
                </tr>
                <tr>
                    <td colspan="2" style="height: 3px"></td>
                </tr>
                <tr>
                    <td colspan="5"><span class="client-name">{{$client_name}}</span></td>
                </tr>
                <tr>
                    <td colspan="2" style="height: 2px"></td>
                </tr>
                <tr>
                    <td height="50px"></td>
                </tr>
                <tr>
                    <td>
                        <table>
                            <tbody>
                                <tr>
                                    <td>
                                    </td>
                                </tr>
                            <tr>
                                <td width="200px">__________________________</td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <?php
                    if( isset( $service_agreement->sign1 ) && ! empty( $service_agreement->sign1 ) ){
                        ?>
                        <tr>
                            <td colspan="5">
                                <table>
                                    <tbody>
                                    <tr>
                                        <td width="200px">By: {{ $service_agreement->sign1 }}</td>
                                        <td width="200px">Date: <u>{{date('d F Y')}}</u> </td>
                                    </tr>
                                    <?php
                                    if (isset($service_agreement->sign1_designation)) {
                                        // code...
                                        ?>
                                        <tr>
                                            <td colspan="2"> ({{ $service_agreement->sign1_designation }}) </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height="50px"></td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <tbody>
                                        <?php 
                                        if( isset($service_agreement->sign1_type ) && $service_agreement->sign1_type == 'Image' ) {
                                            if( isset($service_agreement->sign1_img)){
                                                ?>
                                                <tr>
                                                    <td>
                                                        <img src="{{ url('public/uploads/'.$service_agreement->sign1_img) }}">
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                        } elseif( isset($service_agreement->sign1_type ) && $service_agreement->sign1_type == 'Font' ) {
                                            if( isset($service_agreement->sign1_font)){
                                                ?>
                                                <tr>
                                                    <td>
                                                        <h2 style="font-family: '{{$service_agreement->sign1_font}}'">{{$service_agreement->sign1}}</h2>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                        }
                                        ?>
                                    <tr>
                                        <td width="200px">__________________________</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <?php
                    }
                ?>
                <?php
                    if( isset( $service_agreement->sign2 ) && ! empty( $service_agreement->sign2 ) ){
                        ?>
                        <tr>
                            <td colspan="5">
                                <table>
                                    <tbody>
                                    <tr>
                                        <td width="200px">By: {{ $service_agreement->sign2 }}</td>
                                        <td width="200px">Date: <u>{{date('d F Y')}}</u> </td>
                                    </tr>
                                    <?php
                                    if (isset($service_agreement->sign2_designation)) {
                                        // code...
                                        ?>
                                        <tr>
                                            <td colspan="2"> ({{ $service_agreement->sign2_designation }}) </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height="50px"></td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <tbody>
                                        <?php 
                                        if( isset($service_agreement->sign2_type ) && $service_agreement->sign2_type == 'Image' ) {
                                            if( isset($service_agreement->sign2_img)){
                                                ?>
                                                <tr>
                                                    <td>
                                                        <img src="{{ url('public/uploads/'.$service_agreement->sign2_img) }}">
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                        } elseif( isset($service_agreement->sign2_type ) && $service_agreement->sign2_type == 'Font' ) {
                                            if( isset($service_agreement->sign2_font)){
                                                ?>
                                                <tr>
                                                    <td>
                                                        <h2 style="font-family: '{{$service_agreement->sign2_font}}'">{{$service_agreement->sign2}}</h2>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                        }
                                        ?>
                                    <tr>
                                        <td width="200px">__________________________</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <?php
                    }
                ?>
                <?php
                    if( isset( $service_agreement->sign3 ) && ! empty( $service_agreement->sign3 ) ){
                        ?>
                        <tr>
                            <td colspan="5">
                                <table>
                                    <tbody>
                                    <tr>
                                        <td width="200px">By: {{ $service_agreement->sign3 }}</td>
                                        <td width="200px">Date: <u>{{date('d F Y')}}</u> </td>
                                    </tr>
                                    <?php
                                    if (isset($service_agreement->sign3_designation)) {
                                        // code...
                                        ?>
                                        <tr>
                                            <td colspan="2"> ({{ $service_agreement->sign3_designation }}) </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height="50px"></td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <tbody>
                                        <?php 
                                        if( isset($service_agreement->sign3_type ) && $service_agreement->sign3_type == 'Image' ) {
                                            if( isset($service_agreement->sign3_img)){
                                                ?>
                                                <tr>
                                                    <td>
                                                        <img src="{{ url('public/uploads/'.$service_agreement->sign3_img) }}">
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                        } elseif( isset($service_agreement->sign3_type ) && $service_agreement->sign3_type == 'Font' ) {
                                            if( isset($service_agreement->sign3_font)){
                                                ?>
                                                <tr>
                                                    <td>
                                                        <h2 style="font-family: '{{$service_agreement->sign3_font}}'">{{$service_agreement->sign3}}</h2>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                        }
                                        ?>
                                    <tr>
                                        <td width="200px">__________________________</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <?php
                    }
                ?>
                <tr>
                    <td height="10px"></td>
                </tr>
                <tr><td colspan="2" style="height: 15px"></td></tr>
                <tr><td colspan="2" style="height: 15px"></td></tr>
                <tr>
                    <td style="line-height: 22px; text-decoration: underline;"><b>Provider:</b></td>
                </tr>
                <tr><td colspan="2" style="height: 3px"></td></tr>
                <tr>
                    <td colspan="5">neshallWeb Private Limited</td>
                </tr>
                <tr><td colspan="2" style="height: 2px"></td></tr>
                <tr>
                    <td colspan="5">
                        <table>
                            <tbody>
                            <tr>
                                <td width="180px">By: Mr. Anjalkumar Patel</td>
                                <td width="200px">Date: <u>{{date('d F Y', strtotime($estimate_date))}}</u></td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                    <!-- <td width="90px">By: Mr. Anjalkumar Patel</td>
                    <td width="90px">Date: {{$estimate_date}}</td> -->
                </tr>
                <tr>
                    <td height="50px"></td>
                </tr>
                <tr>
                    <td>
                        <table style="text-align: center; width: 100%;">
                            <tbody>
                            <tr><td><img style="width: 50px" src="{{URL::to('/')}}/assets/images/signture.png"></td></tr>
                            <tr><td>--------------------------</td></tr>
                            <tr><td><img style="width: 180px" src="{{URL::to('/')}}/assets/images/stamp.png"></td></tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr><td colspan="2" style="height: 15px"></td></tr>
                <tr><td colspan="2" style="height: 15px"></td></tr>
                <tr><td colspan="2" style="height: 15px"></td></tr>
                <tr><td colspan="2" style="height: 15px"></td></tr>
                <tr><td colspan="2" style="height: 15px"></td></tr>
                <tr><td colspan="2" style="height: 15px"></td></tr>
                <tr><td colspan="2" style="height: 15px"></td></tr>
                <tr><td colspan="2" style="height: 15px"></td></tr>
                <tr><td colspan="2" style="height: 15px"></td></tr>
                <tr><td colspan="2" style="height: 15px"></td></tr>
                <tr><td colspan="2" style="height: 15px"></td></tr>
                <tr><td colspan="2" style="height: 15px"></td></tr>
                <tr><td colspan="2" style="height: 15px"></td></tr>
                <tr><td colspan="2" style="height: 15px"></td></tr>
                <tr><td colspan="2" style="height: 15px"></td></tr>
                <tr><td colspan="2" style="height: 15px"></td></tr>
            </tbody>
        </table>
    </main>
</body>
</html>

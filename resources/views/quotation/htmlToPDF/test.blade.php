<html>
<head>
    <style>
        /**
            Set the margins of the page to 0, so the footer and the header
            can be of the full height and width !
         **/
        @page {
            margin: 0cm 0cm;
        }

        /** Define now the real margins of every page in the PDF **/
        body {
            margin-top: 2cm;
            margin-left: 2cm;
            margin-right: 2cm;
            margin-bottom: 2cm;
        }

        /** Define the header rules **/
        header {
            position: fixed;
            top: 0cm;
            left: 0cm;
            right: 0cm;
            height: 2cm;

            /** Extra personal styles **/
            background-color: #f5f5f5;
            display: inline-block;
            margin: 0 auto;
            line-height: 1.5cm;
        }
        main{
            top: 2cm;
        }
        /** Define the footer rules **/
        footer {
            position: fixed;
            bottom: 0cm;
            left: 0cm;
            right: 0cm;
            height: 2cm;

            /** Extra personal styles **/
            background-color: #f2f2f2;
            color: #626262;
            font-size: 10px;
        }
    </style>
</head>
<body>
<!-- Define header and footer blocks before your content -->
<header>
    <div style="max-width: 980px; margin: 0 auto; width: 450px;">
        <div style="padding: 0 15px;display: flex; align-items: center; justify-content: space-between;">
            <div style="float: left;">
                <img style="opacity: 0.5;" src="{{URL::to('/')}}/assets/images/logo.png">
            </div>
            <div style="float: right;color: #afafaf;line-height: normal;">
                <span>CIN: U72900GJ2021PTC119524</span><br>
                <span>PAN: AAHCN3844K</span>
            </div>
        </div>
    </div>
</header>

<footer style="margin: 0px">
    C/O PATEL BIPINBHAI DHIRUBHAI, ATAKPARDI, KUBER SIDHHI, Valsad, Gujrat, India, 396007 <br>
        <img style="width: 12px; margin-right: 5px;" src="{{URL::to('/')}}/assets/images/call-icon.png">+91 6354425747 <br>
        <img style="width: 12px; margin-right: 5px;" src="{{URL::to('/')}}/assets/images/mail-icon.png">info@neshallweb.com <br>
        <img style="width: 12px; margin-right: 5px;" src="{{URL::to('/')}}/assets/images/url-icon.png">www.neshallweb.com<br>

   {{-- <table class="table" style="background: #f5f5f5;">
        <thead>
        <tr>
            <th colspan="5" style="text-align: left; font-size: 10px;color: #626262;">C/O PATEL BIPINBHAI DHIRUBHAI,                    ATAKPARDI, KUBER SIDHHI, Valsad, Gujrat, India, 396007</th>
        </tr>
        <tr>
            <th colspan="5" style="text-align: left; font-size: 12px;color: #626262;"><img style="width:10px;margin-right: 5px;" src="{{URL::to('/')}}/assets/images/call-icon.png">+91 6354425747</th>
        </tr>
        <tr>
            <th colspan="5" style="text-align: left; font-size: 12px;color: #626262;"><img style="width:10px;margin-right: 5px" src="{{URL::to('/')}}/assets/images/mail-icon.png">info@neshallweb.com</th>
        </tr>
        <tr>
            <th colspan="5" style="text-align: left; font-size: 12px;color: #626262;"><img style="width:10px;margin-right: 5px" src="{{URL::to('/')}}/assets/images/url-icon.png">www.neshallweb.com</th>
        </tr>
        </thead>
    </table>--}}
</footer>

<!-- Wrap the content of your PDF inside a main tag -->
<main>
    <table style="width: 650px;padding: 0 15px">
        <thead>
        <tr>
            <td colspan="5" style="text-align: center;padding: 5px 0; text-align: center;font-weight: 600;border-top: 2px solid #003b5d; border-bottom: 2px solid #003b5d; color: #003b5d; font-size: 16px;"><b> Services/Specifications</b></td>
        </tr>
        </thead>
        <tbody><tr style="padding: 10px 0 0 0;">
            <td colspan="1" style="font-weight: 600; font-size: 16px; text-align: center; width: 140px">Web Speed & Performance </td>
            <td style="width: 20px;"></td>
            <td colspan="2">
                <table style="width: 450px;">
                    <tbody>
                    <tr>
                        <td colspan="5" style="padding: 10px 0px 3px 20px; border-bottom: 1px solid #bae5ff;">Light weight theme</td>
                    </tr>
                    <tr>
                        <td colspan="5" style="padding: 10px 0px 3px 20px; border-bottom: 1px solid #bae5ff;">Optimized plugins</td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr><td colspan="5" style="border-bottom: 1px solid #a5a5a5;"></td></tr><tr style="padding: 10px 0 0 0;">
            <td colspan="1" style="font-weight: 600; font-size: 16px; text-align: center; width: 140px">Web Appearance & Graphics </td>
            <td style="width: 20px;"></td>
            <td colspan="2">
                <table style="width: 450px;">
                    <tbody>
                    <tr>
                        <td colspan="5" style="padding: 10px 0px 3px 20px; border-bottom: 1px solid #bae5ff;">Optimized color schemes</td>
                    </tr>
                    <tr>
                        <td colspan="5" style="padding: 10px 0px 3px 20px; border-bottom: 1px solid #bae5ff;">Stock images</td>
                    </tr>
                    <tr>
                        <td colspan="5" style="padding: 10px 0px 3px 20px; border-bottom: 1px solid #bae5ff;">Regular icons</td>
                    </tr>
                    <tr>
                        <td colspan="5" style="padding: 10px 0px 3px 20px; border-bottom: 1px solid #bae5ff;">Attractive website theme</td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr><td colspan="5" style="border-bottom: 1px solid #a5a5a5;"></td></tr><tr style="padding: 10px 0 0 0;">
            <td colspan="1" style="font-weight: 600; font-size: 16px; text-align: center; width: 140px">Features & Elements </td>
            <td style="width: 20px;"></td>
            <td colspan="2">
                <table style="width: 450px;">
                    <tbody>
                    <tr>
                        <td colspan="5" style="padding: 10px 0px 3px 20px; border-bottom: 1px solid #bae5ff;">Mobile responsive</td>
                    </tr>
                    <tr>
                        <td colspan="5" style="padding: 10px 0px 3px 20px; border-bottom: 1px solid #bae5ff;">Limited to Five webpages</td>
                    </tr>
                    <tr>
                        <td colspan="5" style="padding: 10px 0px 3px 20px; border-bottom: 1px solid #bae5ff;">User login, logout, forgot password capabilities</td>
                    </tr>
                    <tr>
                        <td colspan="5" style="padding: 10px 0px 3px 20px; border-bottom: 1px solid #bae5ff;">Transparent header</td>
                    </tr>
                    <tr>
                        <td colspan="5" style="padding: 10px 0px 3px 20px; border-bottom: 1px solid #bae5ff;">Media library</td>
                    </tr>
                    <tr>
                        <td colspan="5" style="padding: 10px 0px 3px 20px; border-bottom: 1px solid #bae5ff;">Video/YouTube links</td>
                    </tr>
                    <tr>
                        <td colspan="5" style="padding: 10px 0px 3px 20px; border-bottom: 1px solid #bae5ff;">Linked location on google map</td>
                    </tr>
                    <tr>
                        <td colspan="5" style="padding: 10px 0px 3px 20px; border-bottom: 1px solid #bae5ff;">Product/Service presentation page</td>
                    </tr>
                    <tr>
                        <td colspan="5" style="padding: 10px 0px 3px 20px; border-bottom: 1px solid #bae5ff;">“Contact us” quick link</td>
                    </tr>
                    <tr>
                        <td colspan="5" style="padding: 10px 0px 3px 20px; border-bottom: 1px solid #bae5ff;">Feedback/Contact form</td>
                    </tr>
                    <tr>
                        <td colspan="5" style="padding: 10px 0px 3px 20px; border-bottom: 1px solid #bae5ff;">Five company email accounts (email@example.com)</td>
                    </tr>
                    <tr>
                        <td colspan="5" style="padding: 10px 0px 3px 20px; border-bottom: 1px solid #bae5ff;">SEO optimized website</td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr><td colspan="5" style="border-bottom: 1px solid #a5a5a5;"></td></tr><tr style="padding: 10px 0 0 0;">
            <td colspan="1" style="font-weight: 600; font-size: 16px; text-align: center; width: 140px">Features & Elements </td>
            <td style="width: 20px;"></td>
            <td colspan="2">
                <table style="width: 100%;">
                    <tbody>
                    <tr>
                        <td colspan="5" style="padding: 10px 0px 3px 20px; border-bottom: 1px solid #bae5ff;">SEO friendly links and URLs</td>
                    </tr>
                    <tr>
                        <td colspan="5" style="padding: 10px 0px 3px 20px; border-bottom: 1px solid #bae5ff;">Generating robot.txt for website</td>
                    </tr>
                    <tr>
                        <td colspan="5" style="padding: 10px 0px 3px 20px; border-bottom: 1px solid #bae5ff;">Generating XML sitemap of website</td>
                    </tr>
                    <tr>
                        <td colspan="5" style="padding: 10px 0px 3px 20px; border-bottom: 1px solid #bae5ff;">Generating HTML sitemap of website</td>
                    </tr>
                    <tr>
                        <td colspan="5" style="padding: 10px 0px 3px 20px; border-bottom: 1px solid #bae5ff;">Google search console setup</td>
                    </tr>
                    <tr>
                        <td colspan="5" style="padding: 10px 0px 3px 20px; border-bottom: 1px solid #bae5ff;">Google analytics setup</td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr><td colspan="5" style="border-bottom: 1px solid #a5a5a5;"></td></tr><tr style="padding: 10px 0 0 0;">
            <td colspan="1" style="font-weight: 600; font-size: 16px; text-align: center; width: 140px">Requirements & Support </td>
            <td style="width: 20px;"></td>
            <td colspan="2">
                <table style="width: 450px;">
                    <tbody>
                    <tr>
                        <td colspan="5" style="padding: 10px 0px 3px 20px; border-bottom: 1px solid #bae5ff;">User interface will be designed to represent individual or business entity.</td>
                    </tr>
                    <tr>
                        <td colspan="5" style="padding: 10px 0px 3px 20px; border-bottom: 1px solid #bae5ff;">User experience will be targeted towards the feasibility of using website according to the user interest of knowing more about individual or business.</td>
                    </tr>
                    <tr>
                        <td colspan="5" style="padding: 10px 0px 3px 20px; border-bottom: 1px solid #bae5ff;">Free exclusive and proven business growth technique only for our clients.</td>
                    </tr>
                    <tr>
                        <td colspan="5" style="padding: 10px 0px 3px 20px; border-bottom: 1px solid #bae5ff;">The end goal will be to make user feel luxury of being visiting the website.</td>
                    </tr>
                    <tr>
                        <td colspan="5" style="padding: 10px 0px 3px 20px; border-bottom: 1px solid #bae5ff;">Training provided for the basic operation of website.</td>
                    </tr>
                    <tr>
                        <td colspan="5" style="padding: 10px 0px 3px 20px; border-bottom: 1px solid #bae5ff;">Call/text/email support during office hours</td>
                    </tr>
                    <tr>
                        <td colspan="5" style="padding: 10px 0px 3px 20px; border-bottom: 1px solid #bae5ff;">Shared hosting included for one year (redeemable)</td>
                    </tr>
                    <tr>
                        <td colspan="5" style="padding: 10px 0px 3px 20px; border-bottom: 1px solid #bae5ff;">Available common domain included for one year [For example .com, .in, .org, .io, etc.] (redeemable)</td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr><td colspan="5" style="border-bottom: 1px solid #a5a5a5;"></td></tr><tr style="padding: 10px 0 0 0;">
            <td colspan="1" style="font-weight: 600; font-size: 16px; text-align: center; width: 140px">Total</td>
            <td style="width: 20px;"></td>
            <td colspan="2">
                <table style="width: 100%;">
                    <tbody>
                    <tr><td colspan="5" style="padding: 10px 0px 3px 20px; text-align: center;"><b>$ 5000</b></td></tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr><td colspan="5" style="border-bottom: 1px solid #a5a5a5;"></td></tr>
        </tbody>
    </table>
</main>
</body>
</html>

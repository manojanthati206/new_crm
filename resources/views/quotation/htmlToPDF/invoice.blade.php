<!DOCTYPE html>

<html lang="en">

<head>

    <meta charset="UTF-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        /* Style for the label */
        .service-label {
            font-weight: bold;
        }

        /* Style for the highlighted service name */
        .highlighted-service {
            font-size: 18px;
            color: #007bff;
            /* You can adjust the color as needed */
            margin-bottom: 5px;
        }
    </style>

    <title>Document</title>

</head>



<body style="font-family: arial;">

    <table style="width: 720px;  font-size: 14px;"cellpadding="10px">

        <tr>

            <td style="width: 30%;"></td>

            <td style="width: 40%; text-align: center;">TAX INVOICE</td>

            <td style="width: 30%; text-align: center; padding: 8px 5px; border:2px solid #cc4646; color: #cc4646;">
                ORIGINAL FOR RECIPIENT</td>

        </tr>

    </table>

    <table style="width: 720px;  font-size: 14px; height:10px">

        <tr style="height:10px;"></tr>

    </table>

    <table style="width: 720px;  font-size: 14px;"cellpadding="10px" cellspacing="0">

        <tr>

            <td style="width: 50%; border: 1px solid gray;" rowspan="2">

                <img src="{{ URL::to('/') }}/assets/images/logo.png" alt="" width="100px"
                    style="border: 0; margin: 0 0 20px 0;">

                <span style="display: block; color: gray; margin: 0 0 20px 0;">From</span>

                <span style="display: block; margin: 0 0 10px 0;">NESHALLWEB PRIVATED LIMITED</span>

                <span style="display: block; margin: 0 0 15px 0;">#125, Kuber Siddhi Building, Atak Pardi,

                    Valsad, Gujarat

                    396007 +919773441655, info@neshallweb.com</span>

                <div style="display: block; margin: 0 0 10px 0;">

                    <span style="width: 20%; display: inline-block;">GSTIN</span>

                    <span>24AAHCN3844K1Z9</span>

                </div>

                <div style="display: block; margin: 0 0 10px 0;">

                    <span style="width: 20%; display: inline-block;">PAN</span>

                    <span>AAHCN3844K</span>

                </div>

                <div style="display: block; margin: 0 0 10px 0;">

                    <span style="width: 20%; display: inline-block;">CIN</span>

                    <span>U72900GJ2021PTC1195</span>

                </div>



            </td>

            <td style="border: 1px solid gray;" valign="top">

                <div style="display: block; margin: 0 0 10px 0;">

                    <span style="width: 35%; display: inline-block;">Invoice No.</span>

                    <span>: nW-VAL/INV29</span>

                </div>

                <div style="display: block; margin: 0 0 10px 0;">

                    <span style="width: 35%; display: inline-block;">Invoice Date</span>

                    <span>: 08/05/2023</span>

                </div>

                <div style="display: block; margin: 0 0 10px 0;">

                    <span style="width: 35%; display: inline-block;">Reference No</span>

                    <span>: -</span>

                </div>

                <div style="display: block; margin: 0 0 10px 0;">

                    <span style="width: 35%; display: inline-block;">Place of supply </span>

                    <span>: India</span>

                </div>

                <div style="display: block; margin: 0 0 10px 0;">

                    <span style="width: 35%; display: inline-block;">Due Date</span>

                    <span>: 07/06/2023</span>

                </div>

            </td>

        </tr>

        <tr>

            <td style="border: 1px solid gray;" valign="top">

                <div style="display: block; margin: 0 0 10px 0;">

                    <span style="width: 35%; display: inline-block;">LUT</span>

                    <span>: AD240821000076N</span>

                </div>

            </td>

        </tr>



        <tr>

            <td style="border: 1px solid gray;">

                <span style="display: block; color: gray; margin: 0 0 20px 0;">Billing Address</span>

                <span style="display: block; margin: 0 0 10px 0;">{{ $client_name }}</span>

                <span style="display: block; margin: 0 0 10px 0;">{{ $client_address }}</span>

                <span style="display: block; margin: 0 0 15px 0;">{{ $mobile_no }}</span>

                <span style="display: block; margin: 0 0 15px 0;">{{ $client_email }}</span>

                <div style="display: block; margin: 0 0 10px 0;">

                </div>

            </td>

            <td style="border: 1px solid gray;">

                <span style="display: block; color: gray; margin: 0 0 20px 0;">Shipping Address</span>

                <span style="display: block; margin: 0 0 10px 0;">{{ $client_name }}</span>

                <span style="display: block; margin: 0 0 10px 0;">{{ $client_address }}</span>

                <span style="display: block; margin: 0 0 15px 0;">{{ $mobile_no }}</span>

                <span style="display: block; margin: 0 0 15px 0;">{{ $client_email }}</span>

                <div style="display: block; margin: 0 0 10px 0;">

                </div>

            </td>

        </tr>

    </table>


    @php
        $y = 'INR';
        $z = '(18%)';
        if ($price_type == '$') {
            $y = 'USD';
            $z = '(22%)';
        }
        
        $totalRate = 0;
        $totalTaxAmount = 0;
    @endphp


    <table style="width: 720px; font-size: 14px" border="1" cellpadding="10px" cellspacing="0">
        <tr style="background-color: rgb(235, 233, 233); border: 0;">
            <th>#</th>
            <th>Description</th>
            <th>HSN /SAC</th>
            <th>Qty</th>
            <th>Rate /</th>
            <th>Taxable</th>
            <th>CGST/IGST</th>
            <th>TotalAmount</th>
        </tr>
        @foreach ($data as $index => $item)
            @php
                // Accumulate total rate
                $totalRate += $item['rate'];
                
                // Calculate and accumulate total tax amount
                $taxAmount = floor($item['rate'] * ($item['taxRate'] / 100));
                $totalTaxAmount += $taxAmount;
            @endphp
            <tr>
                <td></td>
                <td colspan="1">
                    <div>
                        <div class="highlighted-service" style="margin-bottom: 10px;">
                            <span
                                style="font-weight: bold; color: black; font-size: 14px;">{{ $item['service_name'] }}</span>
                        </div>
                    </div>
                    <div style="padding-left: 20px; font-size: 12px;">
                        <strong>Deliverables:</strong><br>
                        @foreach ($item['deliverables'] as $index => $deliverable)
                            <span style="font-weight: bold;padding-left: 5px; color: black;">{{ $index + 1 }}.
                            </span>{{ $deliverable['name'] }}<br>
                        @endforeach
                    </div>
                </td>
                <td style="text-align: center">-</td>
                <td style="text-align: center">{{ $item['qty'] }}</td>
                <td style="text-align: center">{{ $item['rate'] }}</td>
                <td style="text-align: center">{{ $item['rate'] }}</td>
                <td style="text-align: center">
                    {{ floor($item['rate'] * ($item['taxRate'] / 100)) }}{{ $z }}</td>
                <td style="text-align: center">{{ floor($item['rate'] + $item['rate'] * ($item['taxRate'] / 100)) }}
                </td>
            </tr>
        @endforeach
        <tr style="background-color: rgb(235, 233, 233); border: 0;">
            <th colspan="5">Total ({{ $y }})</th>
            <th>{{ $totalRate }}</th>
            <th>{{ floor($totalTaxAmount) }}</th>
            <th>{{ floor($totalRate) + floor($totalTaxAmount) }}</th>
        </tr>
    </table>
    @php
        function numberToWordsINR($number)
        {
            $words = ['', 'One', 'Two', 'Three', 'Four', 'Five', 'Six', 'Seven', 'Eight', 'Nine', 'Ten', 'Eleven', 'Twelve', 'Thirteen', 'Fourteen', 'Fifteen', 'Sixteen', 'Seventeen', 'Eighteen', 'Nineteen'];
            $tens = ['', '', 'Twenty', 'Thirty', 'Forty', 'Fifty', 'Sixty', 'Seventy', 'Eighty', 'Ninety'];
        
            // Convert the number to words
            if ($number < 20) {
                return $words[$number];
            } elseif ($number < 100) {
                return $tens[$number / 10] . ' ' . $words[$number % 10];
            } elseif ($number < 1000) {
                return $words[$number / 100] . ' Hundred ' . numberToWordsINR($number % 100);
            } elseif ($number < 100000) {
                return numberToWordsINR($number / 1000) . ' Thousand ' . numberToWordsINR($number % 1000);
            } elseif ($number < 10000000) {
                return numberToWordsINR($number / 100000) . ' Lakh ' . numberToWordsINR($number % 100000);
            } elseif ($number < 1000000000) {
                return numberToWordsINR($number / 10000000) . ' Crore ' . numberToWordsINR($number % 10000000);
            }
        }
        function numberToWordsUSD($number)
        {
            $words = ['', 'One', 'Two', 'Three', 'Four', 'Five', 'Six', 'Seven', 'Eight', 'Nine', 'Ten', 'Eleven', 'Twelve', 'Thirteen', 'Fourteen', 'Fifteen', 'Sixteen', 'Seventeen', 'Eighteen', 'Nineteen'];
            $tens = ['', '', 'Twenty', 'Thirty', 'Forty', 'Fifty', 'Sixty', 'Seventy', 'Eighty', 'Ninety'];
        
            // Convert the number to words
            if ($number < 20) {
                return $words[$number];
            } elseif ($number < 100) {
                return $tens[$number / 10] . ' ' . $words[$number % 10];
            } elseif ($number < 1000) {
                return $words[$number / 100] . ' Hundred ' . numberToWordsUSD($number % 100);
            } elseif ($number < 1000000) {
                return numberToWordsUSD($number / 1000) . ' Thousand ' . numberToWordsUSD($number % 1000);
            } elseif ($number < 1000000000) {
                return numberToWordsUSD($number / 1000000) . ' Million ' . numberToWordsUSD($number % 1000000);
            }
        }
        
        // Calculate the total amount in words
        if ($y == 'INR') {
            $totalAmountWords = numberToWordsINR(floor($totalRate) + floor($totalTaxAmount));
        } else {
            $totalAmountWords = numberToWordsUSD(floor($totalRate) + floor($totalTaxAmount));
        }
        
    @endphp

    <table style=" width: 720px;  font-size: 14px; border-top: 0;" border="1" cellpadding="10px" cellspacing="0">



        <tr style="">

            <td colspan="4">Bank Details:</td>

            <td>TaxableAmount</td>

            <td>{{ $y }} {{ $totalRate }}</td>

        </tr>

        <tr style="">

            <td style="border-right: 0;">Account Number</td>

            <td style="border-left: 0;">: 50200056316051</td>

            <td style="border-right: 0;">IFSC</td>

            <td style="border-left: 0;">: HDFC0004864</td>

            <td>Total Tax</td>

            <td>{{ $y }} {{ floor($totalTaxAmount) }}</td>

        </tr>

        <tr style="">

            <td style="border-right: 0;">Bank Name</td>

            <td style="border-left: 0;">: HDFC bank</td>

            <td style="border-right: 0;">Branch Name</td>

            <td style="border-left: 0;">:Abrama</td>

            <td></td>

            <td></td>

        </tr>

        <tr>
            <td colspan="4">Total amount (in words) <b>{{ ucfirst($totalAmountWords) }} {{ $y }}
                    only</b></td>
            <td>Total Amount</td>
            <td>{{ $y }} {{ floor($totalRate) + floor($totalTaxAmount) }}</td>
        </tr>


        <tr style="">

            <td colspan="4" style="width: 65%;">

                <span style="display: block; margin: 0 0 10px 0;">Terms & Conditions</span>

                <span style="display: block; margin: 0 0 10px 0;">*Prices including GST as applicable.</span>

                <span style="display: block; margin: 0 0 10px 0;">*Refer to more terms & conditions at <a
                        href="https://neshallweb.com/terms-conditions/"> https://neshallweb.com/terms-conditions/
                    </a></span>

                <span style="display: block; margin: 0 0 10px 0;">*Refer to more privacy policy at <a
                        href="https://neshallweb.com/privacy-policy/"> https://neshallweb.com/privacy-policy/
                    </a></span>

                <span style="display: block; margin: 0 0 10px 0;"> *All disputes subject to Valsad Jurisdiction
                    only.</span>

            </td>

            <td colspan="2">

                <span style="display: block;  margin: 0 0 20px 0; text-align: right;"><b> NESHALLWEB PRIVATE LIMITED
                    </b></span>

                <img src="{{ URL::to('/') }}/assets/images/signture.png" alt="" style="text-align: right;"
                    width="80px" height="80px">

                <span style="display: block;  margin: 0 0 20px 0; text-align: right;">Authorised Signatory</span>

            </td>

        </tr>

        <tr style="">

            <td colspan="6" style="height: 300px;"></td>

        </tr>

    </table>

    <table style="width: 720px;  font-size: 14px; text-align:center"cellpadding="10px">

        <tr>

            <td>NESHALLWEB PRIVATE LIMITED, #125, Kuber Siddhi Building, Beside RTO Office, Atak Pardi, Valsad, Gujarat,

                INDIA, 396007 <a href="Tel:02632296161" style="color: #000; text-decoration: none;">
                    Tel:02632296161</a>, <a href="Mailto:info@neshallweb.com"
                    style="color: #000; text-decoration: none;">Mail:info@neshallweb.com</a></td>

        </tr>

    </table>



</body>

</html>

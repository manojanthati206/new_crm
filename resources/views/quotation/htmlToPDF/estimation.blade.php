<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>ESTIMATION</title>
    {{-- <style>
        /**
                Set the margins of the page to 0, so the footer and the header
                can be of the full height and width !
             **/
        @page {
            margin: 0cm 0cm;
        }

        /** Define now the real margins of every page in the PDF **/
        body {
            margin-top: 2cm;
            margin-left: 2cm;
            margin-right: 2cm;
            margin-bottom: 2cm;
        }

        /** Define the header rules **/
        header {
            position: fixed;
            top: 0cm;
            left: 0cm;
            right: 0cm;
            height: 2cm;

            /** Extra personal styles **/
            background-color: #f5f5f5;
            color: #afafaf;
            text-align: center;
        }

        /** Define the footer rules **/
        footer {
            position: fixed;
            bottom: 0cm;
            left: 0cm;
            right: 0cm;
            height: 2cm;

            /** Extra personal styles **/
            background-color: #f5f5f5;
            color: white;
            text-align: center;
        }
    </style> --}}
    <style>
        /**
                Set the margins of the page to 0, so the footer and the header
                can be of the full height and width !
             **/
        @page {
            margin: 1cm 1cm;
            /* Set margins for all pages */
        }

        body {
            margin-top: 2.5cm;
            /* Adjust top margin for content to accommodate header */
            margin-bottom: 2.5cm;
            /* Adjust bottom margin for content to accommodate footer */
            margin-left: 1cm;
            margin-right: 1cm;
        }

        header {
            position: fixed;
            top: 0;
            left: 0;
            right: 0;
            height: 2cm;
            /* Set header height */
            background-color: #f5f5f5;
            color: #afafaf;
            text-align: center;
        }

        footer {
            position: fixed;
            bottom: 0;
            left: 0;
            right: 0;
            height: 2cm;
            /* Set footer height */
            background-color: #f5f5f5;
            color: white;
            text-align: center;
        }


        main {
            margin-top: 1cm;
            /* Adjust top margin to create space between header and main content */
            margin-bottom: 1cm;
            /* Adjust bottom margin to create space between main content and footer */
        }

        /* Additional styles for your content if needed */
    </style>
</head>

<body>
    <!-- Define header and footer blocks before your content -->
    <header>
        <div class="container" style="max-width: 980px; margin: 0 auto; width: 100%;">
            <div class="row"
                style="padding: 0 15px;display: flex; align-items: center; justify-content: space-between;">
                <div class="logo" style="float: left; padding-top: 15px;"><img
                        src="{{ URL::to('/') }}/assets/images/logo.png" style="opacity: 0.5; width: 200px;">
                </div>
                <div class="right-content"
                    style="float: right;color: #afafaf;line-height: normal;padding-top: 20px;font-size: 14px">
                    <span>CIN: U72900GJ2021PTC119524</span><br>
                </div>
            </div>
        </div>
    </header>

    <footer style="height: 100px;">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <p style="font-size: 12px; color: #626262; line-height: 20px;">C/O PATEL BIPINBHAI DHIRUBHAI,
                        ATAKPARDI, KUBER SIDHHI, Valsad, Gujrat, India, 396007 </p>
                </div>
                <div class="col-md-6">
                    <div class="footer-address" style="font-size:12px;color: #626262;">
                        <img style="width: 12px;padding-right: 5px;"
                            src="{{ URL::to('/') }}/assets/images/call-icon.png">+91 6354425747
                        <img style="width: 12px; margin-left: 10px; padding-right: 5px;"
                            src="{{ URL::to('/') }}/assets/images/mail-icon.png">info@neshallweb.com
                        <img style="width: 12px;  margin-left: 10px;padding-right: 5px;"
                            src="{{ URL::to('/') }}/assets/images/url-icon.png">www.neshallweb.com
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <!-- Wrap the content of your PDF inside a main tag -->
    <main
        style="background-image: url({{ URL::to('/') }}/assets/images/Neshallweb-logo.png); background-repeat: no-repeat; background-position: center center; opacity: 0.08;">

        <table class="table" style="width: 650px;padding: 0 15px">
            <thead>
                <tr>
                    <td colspan="2"></td>
                    <td colspan="2"
                        style="text-align: right; font-size: 24px;padding-top: 30px;padding-bottom: 10px;color: #7bb4d8;">
                        ESTIMATION</td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td colspan="2" style="text-align: left;"><b>To:</b></td>
                    <td colspan="2"></td>
                </tr>
                <tr>
                    <td colspan="2" style="text-align: left;"><b>{{ $client_name }}</b></td>
                    <td colspan="2">Estimation Date: {{ date('d F, Y', strtotime($estimate_date)) }}</td>
                </tr>
                <tr>
                    <td colspan="2" style="text-align: left;">{{ $client_email }}</td>
                    <td colspan="2">Estimation No.: {{ $estimate_no }}</td>
                </tr>
                <tr>
                    <td colspan="2" style="text-align: left;">{{ $mobile_no }}</td>
                    <td colspan="2">Expiry Date: {{ date('d F, Y', strtotime($estimate_expire)) }}</td>
                </tr>
                <tr>
                    <td colspan="2">{{ $client_address }}</td>
                </tr>
            </tbody>
        </table>
        {!! $html !!}
        <br>
        <table class="table" style="width: 650px;padding:10px 15px;">
            <thead>
                <tr style="vertical-align: top;">
                    <td colspan="5">
                        <table>
                            <tr>
                                <th>Terms &amp; Conditions</th>
                            </tr>
                            <tr>
                                <td>
                                    *This is simply the proposed price for selected services and allied services. This
                                    ballpark figure is commonly used when the client is comparing prices.
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    *These prices are only valid for 30 days from the estimation date.
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    *For additional terms and conditions refer <a href="">
                                        https://neshallweb.com/terms-conditions/</a>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <!-- <td colspan="5">
                @foreach ($term_n_service as $tns)
@if ($tns->service_id == $service_id)
{!! $tns->content !!}
@endif
@endforeach
            </td> -->
                    <td style="width: 50px"></td>
                    <td style="width: 180px">
                        <table style="text-align: center; width: 100%;">
                            <tbody>
                                <tr>
                                    <td><img style="width: 50px" src="">
                                    </td>
                                </tr>
                                <tr>
                                    <td>Authorized Signatory</td>
                                </tr>
                                <tr>
                                    <td>(Director)</td>
                                </tr>
                                <tr>
                                    <td>

                                        <img style="width: 150px" src="{{ URL::to('/') }}/assets/images/stamp.png">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </thead>
        </table>
    </main>
</body>

</html>

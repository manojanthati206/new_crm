<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>ESTIMATION</title>
    <style>
        /**
                Set the margins of the page to 0, so the footer and the header
                can be of the full height and width !
             **/
        @page {
            margin: 0cm 0cm;
        }

        /** Define now the real margins of every page in the PDF **/
        body {
            margin-top: 2cm;
            margin-left: 2cm;
            margin-right: 2cm;
            margin-bottom: 2cm;
        }

        /** Define the header rules **/
        header {
            position: fixed;
            top: 0cm;
            left: 0cm;
            right: 0cm;
            height: 2cm;

            /** Extra personal styles **/
            background-color: #f5f5f5;
            color: #afafaf;
            text-align: center;
        }

        /** Define the footer rules **/
        footer {
            position: fixed;
            bottom: 0cm;
            left: 0cm;
            right: 0cm;
            height: 2cm;

            /** Extra personal styles **/
            background-color: #f5f5f5;
            color: white;
            text-align: center;
        }
        .h5 {
            font-family: cursive;
        }
        .h2-text {
            font-family: {{$service_agreement->sign1_font}}
        }
    </style>
</head>
<body>
    <?php
        if ( $price_type != "$" ) {
            $price_type = '<img width="12px" height="12px" src="' . URL::to('/assets/images/rupees-50x50.png') . '">';
        }
    ?>
    <!-- Define header and footer blocks before your content -->
    <header>
        <div class="container" style="max-width: 980px; margin: 0 auto; width: 100%;">
            <div class="row" style="padding: 0 15px;display: flex; align-items: center; justify-content: space-between;">
                <div class="logo" style="float: left; padding-top: 15px;"><img src="{{URL::to('/')}}/assets/images/logo.png" style="opacity: 0.5; width: 200px;"></div>
                <div class="right-content" style="float: right;color: #afafaf;line-height: normal;padding-top: 20px;font-size: 14px">
                    <span>CIN: U72900GJ2021PTC119524</span><br>
                </div>
            </div>
        </div>
    </header>

    <footer style="height: 100px;">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <p style="font-size: 12px; color: #626262; line-height: 20px;">C/O PATEL BIPINBHAI DHIRUBHAI, ATAKPARDI, KUBER SIDHHI, Valsad, Gujrat, India, 396007 </p>
                </div>
                <div class="col-md-6">
                    <div class="footer-address" style="font-size:12px;color: #626262;">
                        <img style="width: 12px;padding-right: 5px;" src="{{URL::to('/')}}/assets/images/call-icon.png">+91 6354425747
                        <img style="width: 12px; margin-left: 10px; padding-right: 5px;" src="{{URL::to('/')}}/assets/images/mail-icon.png">info@neshallweb.com
                        <img style="width: 12px;  margin-left: 10px;padding-right: 5px;" src="{{URL::to('/')}}/assets/images/url-icon.png">www.neshallweb.com
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <!-- Wrap the content of your PDF inside a main tag -->
    <main style="background-image: url({{URL::to('/')}}/assets/images//Neshallweb-logo.png); background-repeat: no-repeat; background-position: center center; opacity: 0.08;">
        <table class="table" style="width: 650px;padding: 0 15px">
            <thead>
            <tr>
                <td colspan="2"></td>
                <td colspan="2" style="text-align: right; font-size: 24px;padding-top: 30px;padding-bottom: 10px;color: #7bb4d8;">SERVICES AGREEMENT</td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td colspan="4" style="line-height: 20px;">This Contract for Services is made effective as of {{date('d F Y', strtotime($estimate_date))}}, by and between {{$client_name}} of <u>{{$client_address}}</u> (the "Recipient"), and neshallWeb Private Limited of Valsad, GJ, India (the "Provider").</td>
            </tr>
            <tr>

                <td colspan="4" style="line-height: 20px;"><b>1. DESCRIPTION OF SERVICES.</b> Beginning on {{date('d F Y', strtotime($estimate_date))}}, the Provider will serve to the Recipient the following services (collectively, the "Services")</td>
            </tr>
            </tbody>
        </table>
        @if( isset( $html_array ) && ! empty( $html_array ) )
            @for($i=0;$i < count($html_array);$i++)
                @if( isset( $heading_array[$i] ) && ! empty( $heading_array[$i] ) )
                    {!! $heading_array[$i] !!}
                @endif
                @if( isset( $html_array[$i] ) && ! empty( $html_array[$i] ) )
                    {!! $html_array[$i] !!}
                @endif
            @endfor
        
        @endif
        <table style="width: 650px;padding: 0 15px">
            <tbody>
                <tr><td colspan="5" style="border-bottom: 1px solid #a5a5a5;"></td></tr>
                <tr><td colspan="5" style="height: 20px"></td></tr>
                <tr>
                    <td colspan="5" style="line-height: 22px;"><b>2. PAYMENT.</b> Recipient will pay total of {!!$price_type!!}{{$price}} to the Provider for the agreed services. Payment shall be made to Provider according to the following schedule:</td>
                </tr>
                <tr><td colspan="5" style="height: 5px"></td></tr>
                <tr>
                    <td colspan="5" style="line-height: 20px;"><b>Events and Payments Schedule:</b></td>
                </tr>
                <tr>
                    <td colspan="3" style="line-height: 20px; width: 500px;">
                        <table style="width: 100%;">
                            <tbody>
                            <tr>
                                <td colspan="4">• Signing Service Agreement (35%)</td>
                            </tr>
                            <tr>
                                <td width="20px"></td>
                                <td colspan="4">Amount payable: {!!$price_type!!}{{$price_35per}}</td>
                            </tr>
                            <tr>
                                <td colspan="4">• First Draft Delivery (35%)</td>
                            </tr>
                            <tr>
                                <td width="20px"></td>
                                <td colspan="4">Amount payable: {!!$price_type!!}{{$price_35per}}</td>
                            </tr>
                            <tr>
                                <td colspan="4">• Final Project Delivery (30%)</td>
                            </tr>
                            <tr>
                                <td width="20px"></td>
                                <td colspan="4">Amount payable: {!!$price_type!!}{{$price_30per}} </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                    <td colspan="1" style="line-height: 20px;">
                        <table>
                            <tbody>
                            <tr>
                                <td>Recipient’s Initials: <h5 style="font-family:cursive;">{{$recipient_initials1}}</h5></td>
                            </tr>
                            <tr>
                                <td height="20px"></td>
                            </tr>
                            <tr>
                                <td>Recipient’s Initials: <h5 style="font-family:cursive;">{{$recipient_initials2}}</h5></td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr><td colspan="5" style="height: 5px"></td></tr>
                <tr>
                    <td colspan="5" style="line-height: 22px;">If any invoice is not paid when due, interest will be added to and payable on all overdue amounts at 20 percent per year, or the maximum percentage allowed under applicable State laws, whichever is less.</td>
                </tr>
                <tr><td colspan="2" style="height: 5px"></td></tr>
                <tr>
                    <td colspan="5" style="line-height: 22px;">In addition to any other right or remedy provided by law, if {{$client_name}} fail to pay for the Services when due, neshallWeb Private Limited has the option to treat such failure to pay as a material breach of this Contract and may cancel this Contract and/or seek legal remedies.</td>
                </tr>
                <tr><td colspan="3" style="height: 5px"></td></tr>
                <tr>
                    <td colspan="5" style="line-height: 22px;"><b>3. TERM.</b> This Contract will terminate automatically upon completion by Provider of the Services required by this Contract.
                    Estimated Timeline for the project completion:</td>
                </tr>
                <tr>
                    <td colspan="5" style="line-height: 22px;">
                    The delivery of MVP application might take 3-4 months, in the ideal estimated condition. The estimated timeline may vary according to the response time, number and amount of changes coming from the client.</td>
                </tr>
                <tr><td colspan="3" style="height: 5px"></td></tr>
                <tr>
                    <td colspan="5" style="line-height: 22px;"><b>4. WORK PRODUCT OWNERSHIP.</b>Any copyrightable works, ideas, discoveries, inventions, patents, products, or other information (collectively the "Work Product") developed in whole or in part by Provider in connection with the Services will be the exclusive property of Recipient. Upon request, Provider will execute, within a reasonable period of time, all documents necessary to confirm or perfect the exclusive ownership of Recipient to the Work Product.</td>
                </tr>
                <tr><td colspan="3" style="height: 5px"></td></tr>
                <tr>
                    <td colspan="5" style="line-height: 22px;"><b>5. DEFAULT</b> The occurrence of any of the following shall constitute a material default under this Contract:</td>
                </tr>
                <tr><td colspan="3" style="height: 5px"></td></tr>
                <tr>
                    <td colspan="5">a. The failure to make a required payment when due. </td>
                </tr>
                <tr><td colspan="2" style="height: 5px"></td></tr>
                <tr>
                    <td colspan="5">b. The insolvency or bankruptcy of either party.</td>
                </tr>
                <tr><td colspan="2" style="height: 5px"></td></tr>
                <tr>
                    <td colspan="5" style="line-height: 22px;">c. The subjection of any of either party's property to any levy, seizure, general assignment for the benefit of creditors, application, or sale for or by any creditor or government agency. </td>
                </tr>
                <tr><td colspan="2" style="height: 5px"></td></tr>
                <tr>
                    <td colspan="5" style="line-height: 22px;">d. The failure to make available or deliver the Services in the time and manner provided for in this Contract.</td>
                </tr>
                <tr><td colspan="2" style="height: 5px"></td></tr>
                <tr>
                    <td colspan="5" style="line-height: 22px;"><b>6. ATTORNEYS' FEES AND COLLECTION COSTS.</b> . If there is dispute relating to any provisions in this Contract, the prevailing party is entitled to, and the non-prevailing party shall pay, the costs and expenses incurred by the prevailing party in the dispute, including but not limited to all out-of-pocket costs of collection, court costs, and reasonable attorney fees and expenses. </td>
                </tr>
                <tr><td colspan="2" style="height: 5px"></td></tr>
                <tr>
                    <td colspan="5" style="line-height: 22px;"><b>7. REMEDIES.</b>In addition to any and all other rights a party may have available according to law, if a party defaults by failing to substantially perform any provision, term or condition of this Contract (including without limitation the failure to make a monetary payment when due), the other party may terminate the Contract by providing written notice to the defaulting party. This notice shall describe with sufficient detail the nature of the default. The party receiving such notice shall have 30 days from the effective date of such notice to cure the default(s). Unless waived in writing by a party providing notice, the failure to cure the default(s) within such time period shall result in the automatic termination of this Contract. </td>
                </tr>
                <tr><td colspan="2" style="height: 5px"></td></tr>
                <tr>
                    <td colspan="5" style="line-height: 22px;"><b>8. FORCE MAJEURE.</b>If performance of this Contract or any obligation under this Contract is prevented, restricted, or interfered with by causes beyond either party's reasonable control ("Force Majeure"), and if the party unable to carry out its obligations gives the other party prompt written notice of such event, then the obligations of the party invoking this provision shall be suspended to the extent necessary by such event. The term Force Majeure shall include, without limitation, acts of God, fire, explosion, vandalism, storm or other similar occurrence, orders or acts of military or civil authority, or by national emergencies, insurrections, riots, or wars, or strikes, lockouts, work stoppages or other labor disputes, or supplier failures. The excused party shall use reasonable efforts under the circumstances to avoid or remove such causes of non-performance and shall proceed to perform with reasonable dispatch whenever such causes are removed or ceased. An act or omission shall be deemed within the reasonable control of a party if committed, omitted, or caused by such party, or its employees, officers, agents, or affiliates. </td>
                </tr>
                <tr><td colspan="2" style="height: 5px"></td></tr>
                <tr>
                    <td colspan="5" style="line-height: 22px;"><b>9. DISPUTE RESOLUTION.</b>The parties will attempt to resolve any dispute arising out of or relating to this Agreement through friendly negotiations amongst the parties. If the matter is not resolved by negotiation within 30 days, the parties will resolve the dispute using the below Alternative Dispute Resolution (ADR) procedure.<br>
                        Any controversies or disputes arising out of or relating to this Agreement will be resolved by binding arbitration under the rules of the Indian Council of Arbitration. The arbitrator's award will be final, and judgment may be entered upon it by any court having proper jurisdiction.</td>
                </tr>
                <tr><td colspan="2" style="height: 5px"></td></tr>
                <tr>
                    <td colspan="5" style="line-height: 22px;"><b>10. ENTIRE AGREEMENT.</b>This Contract contains the entire agreement of the parties, and there are no other promises or conditions in any other agreement whether oral or written concerning the subject matter of this Contract. This Contract supersedes any prior written or oral agreements between the parties. </td>
                </tr>
                <tr><td colspan="2" style="height: 5px"></td></tr>
                <tr>
                    <td colspan="5" style="line-height: 22px;"><b>11. SEVERABILITY.</b>If any provision of this Contract will be held to be invalid or unenforceable for any reason, the remaining provisions will continue to be valid and enforceable. If a court finds that any provision of this Contract is invalid or unenforceable, but that by limiting such provision it would become valid andenforceable, then such provision will be deemed to be written, construed, and enforced as so limited. </td>
                </tr>
                <tr><td colspan="2" style="height: 5px"></td></tr>
                <tr>
                    <td colspan="5" style="line-height: 22px;"><b>12. AMENDMENT.</b>This Contract may be modified or amended in writing by mutual agreement between the parties, if the writing is signed by the party obligated under the amendment.</td>
                </tr>
                <tr><td colspan="2" style="height: 5px"></td></tr>
                <tr>
                    <td colspan="5" style="line-height: 22px;"><b>13. INDEMNIFICATION.</b>Recipient agree to indemnify, defend and hold harmless the Provider, its officers, directors, employees, agents and third parties, for any losses, costs, liabilities and expenses (including reasonable attorney’s fees) relating to or arising out of the use of or inability to use the services, consultation, site or product, recipient’s violation of any terms of this Agreement or Recipient’s violation of any rights of a third party, or Recipient’s violation of any applicable laws, rules or regulations. Provider reserves the right, at its own cost, to assume the exclusive defense and control of any matter otherwise subject to indemnification by you, in which event you will fully cooperate with provider in asserting any available defenses.</td>
                </tr>
                <tr><td colspan="2" style="height: 5px"></td></tr>
                <tr>
                    <td colspan="5" style="line-height: 22px;"><b>14. GOVERNING LAW.</b>This Contract shall be construed in accordance with the laws of the State of Gujarat. </td>
                </tr>
                <tr><td colspan="2" style="height: 5px"></td></tr>
                <tr>
                    <td colspan="5" style="line-height: 22px;"><b>15. NOTICE.</b>Any notice or communication required or permitted under this Contract shall be sufficiently given if delivered in person or by certified mail, return receipt requested, to the address set forth in the opening paragraph or to such other address as one party may have furnished to the other in writing. </td>
                </tr>
                <tr><td colspan="2" style="height: 5px"></td></tr>
                <tr>
                    <td colspan="5" style="line-height: 22px;"><b>16. WAIVER OF CONTRACTUAL RIGHT.</b>The failure of either party to enforce any provision of this Contract shall not be construed as a waiver or limitation of that party's right to subsequently enforce and compel strict compliance with every provision of this Contract. </td>
                </tr>
                <tr><td colspan="2" style="height: 5px"></td></tr>
                <tr>
                    <td colspan="5" style="line-height: 22px;"><b>17. ATTORNEY'S FEES TO PREVAILING PARTY.</b>In any action arising hereunder or any separate action pertaining to the validity of this Agreement, the prevailing party shall be awarded reasonable attorney's fees and costs, both in the trial court and on appeal. </td>
                </tr>
                <tr><td colspan="2" style="height: 5px"></td></tr>
                <tr>
                    <td colspan="5" style="line-height: 22px;"><b>18. CONSTRUCTION AND INTERPRETATION.</b>The rule requiring construction or interpretation against the drafter is waived. The document shall be deemed as if it were drafted by both parties in a mutual effort.</td>
                </tr>
                <tr><td colspan="2" style="height: 5px"></td></tr>
                <tr>
                    <td colspan="5" style="line-height: 22px;"><b>19. ASSIGNMENT.</b>Neither party may assign or transfer this Contract without the prior written consent of the non-assigning party, which approval shall not be unreasonably withheld.<br>
                        IN WITNESS WHEREOF, the parties hereto have caused this Agreement to be executed by their duly authorized representatives as of the date first above written.</td>
                </tr>
                <tr>
                    <td colspan="5" style="text-align: right;">Recipient’s Initials: <h5 style="font-family:cursive;">{{$recipient_initials3}}</h5></td>
                </tr>
                <tr><td colspan="2" style="height: 5px"></td></tr>
                <tr>
                    <td style="line-height: 22px; text-decoration: underline;"><b>Recipient:</b></td>
                </tr>
                <tr><td colspan="2" style="height: 3px"></td></tr>
                <tr>
                    <td colspan="5">{{$client_name}}</td>
                </tr>
                <tr><td colspan="2" style="height: 2px"></td></tr>
                <?php
                    if( isset( $service_agreement->sign1 ) && ! empty( $service_agreement->sign1 ) ){
                        ?>
                        <tr>
                            <td colspan="5">
                                <table>
                                    <tbody>
                                    <tr>
                                        <td width="200px">By: {{ $service_agreement->sign1 }}</td>
                                        <td width="200px">Date: <u>{{date('d F Y')}}</u> </td>
                                    </tr>
                                    <?php
                                    if (isset($service_agreement->sign1_designation)) {
                                        // code...
                                        ?>
                                        <tr>
                                            <td colspan="2"> ({{ $service_agreement->sign1_designation }}) </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height="50px"></td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <tbody>
                                        <?php 
                                        if( isset($service_agreement->sign1_type ) && $service_agreement->sign1_type == 'Image' ) {
                                            if( isset($service_agreement->sign1_img)){
                                                ?>
                                                <tr>
                                                    <td>
                                                        <img src="{{ url('public/uploads/'.$service_agreement->sign1_img) }}">
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                        } elseif( isset($service_agreement->sign1_type ) && $service_agreement->sign1_type == 'Font' ) {
                                            if( isset($service_agreement->sign1_font)){
                                                ?>
                                                <tr>
                                                    <td>
                                                        <h2 class="h2-text" style="font-family: {{$service_agreement->sign1_font}}">{{$service_agreement->sign1}}</h2>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                        }
                                        ?>
                                    <tr>
                                        <td width="200px">__________________________</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <?php
                    }
                ?>
                <?php
                    if( isset( $service_agreement->sign2 ) && ! empty( $service_agreement->sign2 ) ){
                        ?>
                        <tr>
                            <td colspan="5">
                                <table>
                                    <tbody>
                                    <tr>
                                        <td width="200px">By: {{ $service_agreement->sign2 }}</td>
                                        <td width="200px">Date: <u>{{date('d F Y')}}</u> </td>
                                    </tr>
                                    <?php
                                    if (isset($service_agreement->sign2_designation)) {
                                        // code...
                                        ?>
                                        <tr>
                                            <td colspan="2"> ({{ $service_agreement->sign2_designation }}) </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height="50px"></td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <tbody>
                                        <?php 
                                        if( isset($service_agreement->sign2_type ) && $service_agreement->sign2_type == 'Image' ) {
                                            if( isset($service_agreement->sign2_img)){
                                                ?>
                                                <tr>
                                                    <td>
                                                        <img src="{{ url('public/uploads/'.$service_agreement->sign2_img) }}">
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                        } elseif( isset($service_agreement->sign2_type ) && $service_agreement->sign2_type == 'Font' ) {
                                            if( isset($service_agreement->sign2_font)){
                                                ?>
                                                <tr>
                                                    <td>
                                                        <h2 style="font-family: '{{$service_agreement->sign2_font}}'">{{$service_agreement->sign2}}</h2>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                        }
                                        ?>
                                    <tr>
                                        <td width="200px">__________________________</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <?php
                    }
                ?>
                <?php
                    if( isset( $service_agreement->sign3 ) && ! empty( $service_agreement->sign3 ) ){
                        ?>
                        <tr>
                            <td colspan="5">
                                <table>
                                    <tbody>
                                    <tr>
                                        <td width="200px">By: {{ $service_agreement->sign3 }}</td>
                                        <td width="200px">Date: <u>{{date('d F Y')}}</u> </td>
                                    </tr>
                                    <?php
                                    if (isset($service_agreement->sign3_designation)) {
                                        // code...
                                        ?>
                                        <tr>
                                            <td colspan="2"> ({{ $service_agreement->sign3_designation }}) </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height="50px"></td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <tbody>
                                        <?php 
                                        if( isset($service_agreement->sign3_type ) && $service_agreement->sign3_type == 'Image' ) {
                                            if( isset($service_agreement->sign3_img)){
                                                ?>
                                                <tr>
                                                    <td>
                                                        <img src="{{ url('public/uploads/'.$service_agreement->sign3_img) }}">
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                        } elseif( isset($service_agreement->sign3_type ) && $service_agreement->sign3_type == 'Font' ) {
                                            if( isset($service_agreement->sign3_font)){
                                                ?>
                                                <tr>
                                                    <td>
                                                        <h2 style="font-family: '{{$service_agreement->sign3_font}}'">{{$service_agreement->sign3}}</h2>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                        }
                                        ?>
                                    <tr>
                                        <td width="200px">__________________________</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <?php
                    }
                ?>
                <tr>
                    <td height="10px"></td>
                </tr>
                <tr>
                    <td style="line-height: 22px; text-decoration: underline;"><b>Provider:</b></td>
                </tr>
                <tr><td colspan="2" style="height: 3px"></td></tr>
                <tr>
                    <td colspan="5">neshallWeb Private Limited</td>
                </tr>
                <tr><td colspan="2" style="height: 2px"></td></tr>
                <tr>
                    <td colspan="5">
                        <table>
                            <tbody>
                            <tr>
                                <td width="180px">By: Mr. Anjalkumar Patel</td>
                                <td width="200px">Date: <u>{{date('d F Y', strtotime($estimate_date))}}</u></td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                    <!-- <td width="90px">By: Mr. Anjalkumar Patel</td>
                    <td width="90px">Date: {{$estimate_date}}</td> -->
                </tr>
                <tr>
                    <td height="50px"></td>
                </tr>
                <tr>
                    <td>
                        <table style="text-align: center; width: 100%;">
                            <tbody>
                            <tr><td><img style="width: 50px" src="{{URL::to('/')}}/assets/images/signture.png"></td></tr>
                            <tr><td>--------------------------</td></tr>
                            <tr><td><img style="width: 180px" src="{{URL::to('/')}}/assets/images/stamp.png"></td></tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="text-align: right; font-size: 12px;color: #626262;">Recipient’s Initials: <h5 style="font-family:cursive;">{{$recipient_initials4}}</h5></td>
                </tr>
            </tbody>
        </table>
    </main>
</body>
</html>

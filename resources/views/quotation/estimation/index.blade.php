@extends('layouts.index')

@section('title')
    Estimation
@endsection()

@section('content')
    <?php $i = 0; ?>
    <!-- Content Header (Page header) -->


    <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
        <div class="breadcrumb-title pe-3">All Estimation</div>
        <div class="ps-3">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb mb-0 p-0">
                    <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">Estimation</li>
                </ol>
            </nav>
        </div>
        <div class="ms-auto">
            <div class="btn-group">
                <a href="{{ route('estimation-add') }}">
                    <button type="button" class="btn btn-light">Add New Estimation</button>
                </a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-9 mx-auto">
            <div class="card">
                <div class="card-body">
                    <table class="table mb-0">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Action</th>
                                <th scope="col">Estimate date</th>
                                <th scope="col">Estimate expire</th>
                                <th scope="col">Estimate No</th>
                                <th scope="col">Service</th>
                                <th scope="col">Total Price</th>
                                <th scope="col">Download PDF</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($all_estimates as $estimate)
                                <tr>
                                    <td scope="row">{{ ++$i }}</td>
                                    <td>
                                       <a  href="{{route('estimation-edit', $estimate->id)}}">
                                            Edit
                                        </a>
                                        <a onclick="return confirm('Are you sure?')" href="{{route('estimation-delete', $estimate->id)}}">
                                            Delete
                                        </a>
                                    </td>
                                    <td>
                                        {{ $estimate->estimate_date }}
                                    </td>
                                    <td>
                                        {{ $estimate->estimate_no }}
                                    </td>
                                    <td>
                                        {{ $estimate->estimate_expire }}
                                    </td>
                                    <td>
                                        @if ($estimate->service_id == '1')
                                            Web Development
                                        @endif
                                        @if ($estimate->service_id == '2')
                                            Graphics
                                        @endif
                                        @if ($estimate->service_id == '3')
                                            Video Service
                                        @endif
                                    </td>
                                    <td>
                                        {{ $estimate->price_type }}{{ $estimate->price }}
                                    </td>
                                    <td>
                                        <a  href="{{route('download-estimation-pdf', $estimate->id)}}">
                                            Estimation
                                        </a> |
                                        <a  href="{{route('download-estimation-invoice', $estimate->id)}}">
                                            Invoice
                                        </a> |
                                        <a  href="{{route('download-service-pdf', $estimate->id)}}">
                                            Service Estimation
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
      <!-- /.content-header -->
    <?php $i = 0;?>

    <!--!! $projects->links() !!-->

@endsection()

@section('scripts')

$("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');

@endsection()
      
@extends('layouts.index')

@section('title')
    Estimation
@endsection()

@section('content')
    <style>
        /* this start */


        li.neshalldepartment {
            list-style: none;
            width: max-content;
            height: auto;
            background-color: #59b5f3;
            color: #000000;
            border-radius: 0.3rem;
            padding: 10px;
            margin-bottom: 10px;
        }

        .neshallcontainer {
            display: flex;
            padding: 20px;
        }

        .neshalldepartments {
            flex: 1;
            padding-right: 20px;
            /* border-right: 1px solid #ccc; */
            /* background-color: #fff; */
            border-radius: 8px;
            /* box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1); */
        }

        .neshallselected-services {
            flex: 2;
            display: flex;
            gap: 20px;
            overflow-x: auto;

        }

        .neshalldepartment {
            cursor: pointer;
            padding: 10px 0;
            color: #333;
            transition: background-color 0.3s ease;
        }

        .neshalldepartment:hover {
            background-color: #e0e0e0;
        }

        .neshallfeatures {
            display: none;
        }

        .neshallselected-features {
            display: flex;
            flex-wrap: wrap;
            flex: none;
            justify-content: flex-start;
            /* Distributes items evenly along the main axis */
            gap: 10px;
            padding: 20px;
            border: 1px solid #ccc;
            background-color: #fff;
            border-radius: 8px;
            box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
            animation: fadeIn 0.5s ease;
        }


        @keyframes fadeIn {
            from {
                opacity: 0;
            }

            to {
                opacity: 1;
            }
        }

        .neshallservice {
            margin-bottom: 20px;
            padding: 15px;
            background-color: #f0f0f0;
            border-radius: 8px;
            box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
        }

        .neshallservice h3 {
            margin: 0;
            color: #333;
        }

        .neshallservice ul {
            padding-left: 20px;
            list-style: disc;
            color: #666;
        }



        /* this end newmodel */




        .main-services {
            display: flex;
            flex-direction: row;
            border: 1px solid #000000;
        }

        .main-service {
            display: flex;
            flex-direction: column;
            border: 1px solid #000000;
            margin: 10px;
            padding: 10px;
        }

        .main-service-label {
            font-weight: bold;
            color: #0df0ca;
        }

        .services-container {
            display: flex;
            flex-direction: column;
        }

        .service {
            background-color: #f2f2f2;
            padding: 5px;
            margin: 5px;
            border: 1px solid #ccc;
            border-radius: 5px;
        }

        .manojdraggable {
            padding: 0.5rem 0.3rem 0.2rem 0.5rem;
            border-radius: 0.5rem;
            width: fit-content;
            height: fit-content;
            background-color: #5b6873;
            margin-bottom: 10px;
            border: 1px solid black;
            cursor: move;
        }

        .manojbtn {
            padding: 0.5rem;
            border-radius: 0.3rem;
            width: 200px;
            height: fit-content;
            cursor: move;
            background-color: #67bae3;
        }

        .manojdraggable.manojdragging {
            opacity: .5;
        }

        #div1,
        #div2,
        #div3,
        #div4 {
            float: left;
            width: 46%;
            height: auto;
            margin: 10px;
            padding: 20px;
            border: 1px solid black;
            cursor: pointer;
        }

        @media only screen and (max-width: 1600px) {

            #div1,
            #div2,
            #div3,
            #div4 {
                float: left;
                width: 46%;
                height: auto;
                margin: 10px;
                padding: 20px;
                border: 1px solid black;
                cursor: pointer;
            }
        }

        @media only screen and (max-width: 1200px) {

            #div1,
            #div2,
            #div3,
            #div4 {
                float: left;
                width: 47%;
                height: auto;
                margin: 10px;
                padding: 20px;
                border: 1px solid black;
                cursor: pointer;
            }
        }

        @media only screen and (max-width: 762px) {

            #div1,
            #div2,
            #div3,
            #div4 {
                float: left;
                width: 46%;
                height: auto;
                margin: 10px;
                padding: 20px;
                border: 1px solid black;
                cursor: pointer;
            }
        }

        #div1 div,
        #div2 div,
        #div3 div,
        #div4 div {
            margin: 15px;
            padding: 15px;
        }

        label.error {
            color: red;
            padding: 5px;

        }


        .dropdown {
            position: relative;

        }




        .manojdropdown {
            position: relative;
            margin-right: 10px;
            margin-bottom: 10px;
        }

        .arrow {
            border: solid black;
            border-width: 0 3px 3px 0;
            display: inline-block;
            padding: 3px;
        }


        .down {
            transform: rotate(45deg);
            -webkit-transform: rotate(45deg);
        }

        .up {
            transform: rotate(-135deg);
            -webkit-transform: rotate(-135deg);
        }

        .manojdropdown-menu {
            display: grid;
            grid-template-columns: repeat(2, max-content);
            gap: .2rem;
            position: absolute;
            height: fit-content;
            max-height: 200px;
            overflow: auto;
            left: 0;
            top: calc(100% + .3rem);
            padding: 1rem;
            color: black;
            background-color: #73e9bc;
            box-shadow: 0 2px 15px 0 rgba(0, 0, 0, 0.5);
            opacity: 0;
            cursor: move;
            pointer-events: none;
            transform: translateY(-10px);
            transition: opacity 0.8s ease-in-out, transform 0.8s ease-in-out, max-height 0.3s ease;
            ;


        }

        .manojdropdown-menu input[type="checkbox"] {
            margin-right: 10px;
            width: 20px;
            height: 20px;
        }

        .show-toggle {
            background-color: #3498db;
            color: white;
            border: none;
            padding: 2px 4px;
            cursor: pointer;
        }

        .hidden {
            display: none;
        }

        .manojdropdown.active>.manojbtn+.manojdropdown-menu {

            opacity: 1;
            transform: translateY(0);
            pointer-events: auto;
            z-index: 10;
        }

        .container {
            height: fit-content;
            width: fit-content;
        }
    </style>
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
        <div class="breadcrumb-title pe-3">Add New Estimation</div>
        <div class="ps-3">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb mb-0 p-0">
                    <li class="breadcrumb-item"><a href="javascript:void(0);"><i class="bx bx-home-alt"></i></a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">Estimation</li>
                </ol>
            </nav>
        </div>
    </div>

    <div class="row">

        <div class="col-xl-9 mx-auto">
            <hr />
            <div class="card">
                <div class="card-body">
                    <div class="p-4 border rounded">
                        <p>hi manoj im here</p>
                        <form method="post" id="estimation-form" action="{{ URL::to('/') }}/quotation/estimation/store"
                            class="row g-3 needs-validation" enctype="multipart/form-data" novalidate>
                            @csrf
                            <div class="col-md-6">
                                <label for="validationCustom101" class="form-label">Client Name</label>
                                <input type="text" class="form-control" id="validationCustom101" value=""
                                    name="client_name" placeholder="Client Name" required>
                                <div class="valid-feedback">Looks good!</div>
                            </div>
                            <div class="col-md-6">
                                <label for="validationCustom102" class="form-label">Client Address</label>
                                <input type="text" class="form-control" id="validationCustom102" value=""
                                    name="client_address" placeholder="Client Address" required>
                                <div class="valid-feedback">Looks good!</div>
                            </div>
                            <div class="col-md-6">
                                <label for="validationCustom103" class="form-label">Client Country</label>
                                <select class="form-select client_country" id="validationCustom105" name="client_country"
                                    required>
                                    <option value="">Select a Country</option>
                                    <option value="US">United States</option>
                                    <option value="India">India</option>
                                </select>
                                <div class="valid-feedback">Looks good!</div>
                            </div>

                            <div class="col-md-6">
                                <label for="validationCustom104" class="form-label">Client Mobile no</label>
                                <input type="text" class="form-control" id="validationCustom103" value=""
                                    name="mobile_no" placeholder="Mobile no" required>
                                <div class="valid-feedback">Looks good!</div>
                            </div>
                            <div class="col-md-6">
                                <label for="validationCustom105" class="form-label">Client Email</label>
                                <input type="text" class="form-control" id="validationCustom104" value=""
                                    name="client_email" placeholder="Client Email" required>
                                <div class="valid-feedback">Looks good!</div>
                            </div>
                            <div class="col-md-6">
                                <label for="validationCustom01" class="form-label">Estimate date</label>
                                <input type="text" class="form-control datepicker" id="validationCustom01"
                                    value="{{ $current_date }}" name="estimate_date" placeholder="Estimate date" required>
                                <div class="valid-feedback">Looks good!</div>
                            </div>
                            <div class="col-md-6">
                                <label for="validationCustom03" class="form-label">Estimate expire</label>
                                <input type="text" class="form-control datepicker" id="validationCustom03"
                                    value="{{ $expire_date }}" name="estimate_expire" placeholder="Estimate expire"
                                    required>
                                <div class="valid-feedback">Looks good!</div>
                            </div>
                            <div class="col-md-6">
                                <label for="validationCustom02" class="form-label">Estimate no</label>
                                <input type="text" class="form-control" id="validationCustom02"
                                    value="{{ $next_estimate_no }}" name="estimate_no" placeholder="Estimate no" required>
                                <div class="valid-feedback">Looks good!</div>
                            </div>
                            <div class="col-md-6">
                                <label for="validationCustom106" class="form-label">Select Entered Price to have</label>
                                <select class="gst-type form-select" id="gst-type" name="gst_type">
                                    <option value="included">GST Included</option>
                                    <option value="excluded">GST Excluded</option>
                                </select>
                                <div class="valid-feedback">Looks good!</div>
                            </div>
                            <div class="col-md-6">
                                <label for="validationCustom002" class="form-label">Total Price</label>
                                <input type="text" class="form-control" id="validationCustom002" value=""
                                    name="price" placeholder="Total Price" onchange="updatePriceWithGST(this.value)"
                                    required>
                                <div class="valid-feedback">Looks good!</div>
                            </div>


                            <div class="col-md-6">
                                <label for="inputState" class="form-label">Price Type</label>
                                <select id="inputState" name="price_type" class="form-select currency">
                                    <option value="">Choose...</option>
                                    <option selected value="$">$</option>
                                    <option value="₹">₹</option>
                                </select>
                            </div>

                            <input type="hidden" name="main_categories" id="main-servicesInput">
                            <input type="hidden" name="features" id="featuresInput">
                            <input type="hidden" name="featurelists" id="featurelistInput">
                            <input type="hidden" value="" id="form-type" name="form_type">
                            <input type="hidden" id="selectedDeliverables" name="selectedDeliverables">
                            <input type="hidden" id="selectedaddfeat" name="selectedAddfeatures">
                            <input type="hidden" id="selectedimpnote" name="selectedImportantNote">
                            <input type="hidden" id="selectedservices" name="service_id">

                            <label class="form-check-label">These are the main services we provide.</label>
                            <div style="display: flex; flex-direction: column;border: 1px solid #000000">
                                @foreach ($main_cats as $main_cat)
                                    @if ($main_cat->id == 2)
                                        <div class="catparent">
                                            <label class="form-check-label manojname" style="color:white;font-size:2rem;"
                                                data-id="{{ $main_cat->id }}">Services
                                            </label>
                                            <div style="display: flex; flex-direction: column;border:solid black"
                                                class="newdepartments">

                                                <div class="neshallcontainer">
                                                    <div class="neshalldepartments">
                                                        <ul>
                                                            @foreach ($main_servs as $service)
                                                                <li class="neshalldepartment main-service-label"
                                                                    data-id="{{ $service->name }}">
                                                                    {{ $service->name }}</li>
                                                            @endforeach
                                                        </ul>
                                                    </div>

                                                    <div class="neshallselected-services">
                                                        <!-- Selected services will be displayed here -->
                                                    </div>
                                                </div>
                                                @foreach ($main_servs as $service)
                                                    <div class="neshallfeatures mainselect" id="{{ $service->name }}"
                                                        data-id="{{ $service->id }}">
                                                        <h2 style="color: black">{{ $service->name }}</h2>
                                                        <div class="neshallservice">
                                                            <h3>Features</h3>
                                                            <div style="display: flex;flex-direction:column">
                                                                <div
                                                                    style=" display: grid; grid-template-columns: repeat(2, max-content); gap: 7px;">
                                                                    @foreach ($features as $feature)
                                                                        @if ($feature->service_id == $service->id)
                                                                            <div class="manojdraggable neshallservice"
                                                                                draggable="true">
                                                                                <div class="manojdropdown"
                                                                                    data-manojdropdown>
                                                                                    <button class="manojbtn"
                                                                                        data-manojdropdown-btn
                                                                                        data-id="{{ $feature->id }}"
                                                                                        data-mainid="{{ $main_cat->id }}">{{ $feature->feature_title }}
                                                                                        <span class="arrow down"></span>
                                                                                    </button>
                                                                                    <div class="manojdropdown-menu">
                                                                                        @php
                                                                                            $found = false; // A flag to check if any feature list was found
                                                                                        @endphp

                                                                                        @foreach ($features_lists as $features_list)
                                                                                            @if ($features_list->feature_id == $feature->id)
                                                                                                <div class="feature-list">
                                                                                                    <input type="checkbox"
                                                                                                        class="checkbox"
                                                                                                        data-id="{{ $features_list->id }}">
                                                                                                    <label
                                                                                                        for="features">{{ $features_list->feature_list }}</label>
                                                                                                </div>
                                                                                                @php
                                                                                                    $found = true; // Set the flag to true if a feature list is found
                                                                                                @endphp
                                                                                            @endif
                                                                                        @endforeach

                                                                                        @if (!$found)
                                                                                            <!-- Add the feature itself as a feature list -->
                                                                                            <div class="feature-list">
                                                                                                <input type="checkbox"
                                                                                                    class="checkbox"
                                                                                                    data-id="{{ $feature->id }}">
                                                                                                <label for="features"
                                                                                                    style="color: #000000">{{ $feature->feature_title }}</label>
                                                                                            </div>
                                                                                        @endif
                                                                                    </div>

                                                                                </div>
                                                                            </div>
                                                                        @endif
                                                                    @endforeach
                                                                </div>
                                                                <div
                                                                    style="display: flex; justify-content: center; align-items: center;">
                                                                    <div
                                                                        style="color: #000000; text-align: center; padding: 10px; background-color: #1dead2;margin-left:10px;border-radius:10px;height:fit-content;">
                                                                        <h4 style="margin: 0;">Collective Price for the
                                                                            features</h4>
                                                                        <p class="featpr" style="text-align: center">
                                                                            Rs.2500</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="neshallservice ">
                                                            <h3 class="addfeat">Additional Features</h3>
                                                            <div style="display: flex;flex-direction:row">
                                                                @foreach ($additionalFeatures as $additionalFeature)
                                                                    @if ($additionalFeature->service_id == $service->id)
                                                                        <div class="manojdraggable neshallservice"
                                                                            draggable="true">
                                                                            <div class="manojdropdown" data-manojdropdown>
                                                                                <button class="manojbtn"
                                                                                    data-manojdropdown-btn
                                                                                    data-id="{{ $additionalFeature->id }}">
                                                                                    {{ $additionalFeature->text }}
                                                                                    <span class="arrow down"></span>
                                                                                </button>
                                                                                <div class="manojdropdown-menu">
                                                                                    @foreach ($additionalFeatureLists as $additionalFeatureList)
                                                                                        @if ($additionalFeatureList->add_id == $additionalFeature->add_id)
                                                                                            <div class="addfeature-list"
                                                                                                data-id="{{ $service->id }}">
                                                                                                <input type="checkbox"
                                                                                                    class="checkbox addfeat"
                                                                                                    data-id="{{ $additionalFeatureList->id }}">
                                                                                                <label
                                                                                                    for="features">{{ $additionalFeatureList->feature_name }}</label>
                                                                                            </div>
                                                                                        @endif
                                                                                    @endforeach
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    @endif
                                                                @endforeach

                                                                <div
                                                                    style="display: flex; justify-content: center; align-items: center;">
                                                                    <div
                                                                        style="color: #000000; text-align: center; padding: 10px; background-color: #1dead2;margin-left:10px;border-radius:10px;height:fit-content;">
                                                                        <h4 style="margin: 0;">Collective Price for the
                                                                            Additional
                                                                            features</h4>
                                                                        <p class="addpr" style="text-align: center">
                                                                            Rs.2500</p>

                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>

                                                        <div class="neshallservice">
                                                            <h1 class="deliverables"
                                                                style="font-weight: bold;font-size: 18px; color: #333;margin-bottom:10px">
                                                                Deliverables</h1>

                                                            <div
                                                                style="display: grid; grid-template-columns: repeat(2, max-content); gap: 7px;">
                                                                @foreach ($deliverables as $deliverable)
                                                                    @if ($deliverable->service_id == $service->id)
                                                                        <div style="display: flex; align-items: center;">
                                                                            <input type="checkbox" class="manojnit"
                                                                                data-id="{{ $deliverable->id }}"
                                                                                style=" margin-right: 10px;width: 20px;height: 20px;">
                                                                            <label
                                                                                style="color:#000000;margin-left:10px">{{ $deliverable->name }}
                                                                            </label>
                                                                        </div>
                                                                    @endif
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                        <div class="neshallservice">
                                                            <h3 class="impnote">Important Note </h3>
                                                            @foreach ($importantNotes as $importantNote)
                                                                @if ($importantNote->service_id == $service->id)
                                                                    <div class="manojdraggable neshallservice"
                                                                        draggable="true">
                                                                        <div class="manojdropdown" data-manojdropdown>
                                                                            <button class="manojbtn" data-manojdropdown-btn
                                                                                data-id="{{ $importantNote->id }}">
                                                                                {{ $importantNote->text }}
                                                                                <span class="arrow down"></span>
                                                                            </button>
                                                                            <div class="manojdropdown-menu">
                                                                                @foreach ($importantNoteLists as $importantNoteList)
                                                                                    @if ($importantNoteList->note_id == $importantNote->note_id)
                                                                                        <div class="note-list"
                                                                                            data-id="{{ $service->id }}">
                                                                                            <input type="checkbox"
                                                                                                class="checkbox note"
                                                                                                data-id="{{ $importantNoteList->id }}">
                                                                                            <label
                                                                                                for="notes">{{ $importantNoteList->note_name }}</label>
                                                                                        </div>
                                                                                    @endif
                                                                                @endforeach
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                @endif
                                                            @endforeach


                                                        </div>
                                                        <div class="neshallservice">
                                                            <h3 style="margin-bottom: 20px; color: #0074cc">Estimated
                                                                timeline</h3>
                                                            <p style="color: black; margin-left: 10px; font-size: 22px">
                                                                Pre-production phase: <span
                                                                    style="color: #555; font-weight: bold">2 weeks or
                                                                    more</span></p>
                                                            <p style="color: black; margin-left: 10px; font-size: 22px">
                                                                Production phase: <span
                                                                    style="color: #555; font-weight: bold">variable weeks
                                                                    to months</span></p>
                                                            <p style="color: black; margin-left: 10px; font-size: 22px">
                                                                Post-production phase: <span
                                                                    style="color: #555; font-weight: bold">1-2 weeks</span>
                                                            </p>
                                                            <p style="color: black; margin-left: 10px; font-size: 22px">
                                                                Delivery and Documentation: <span
                                                                    style="color: #555; font-weight: bold">1-2 weeks</span>
                                                            </p>



                                                        </div>
                                                        <div class="neshallservice" style="background-color: #1dead2;">
                                                            <h3>Sub Total price of the service</h3>
                                                            <h4 style="color: white" class="subpr">Rs.5000</h4>
                                                            <h4 style="color: white" class="subprgst">GST 18% of Rs.5000
                                                                i.e 900</h4>
                                                            <h4 style="color: white" class="subprtot">Total Price after
                                                                GST is Rs.5900</h4>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                            <div>
                                <h4 id="selectedServicesCount" style="color: #73e9bc"></h4>


                            </div>
                            <div class="col-12">
                                <button class="btn btn-light btn-quotation" type="submit">Download Quotation</button>
                                <button class="btn btn-light btn-service-agreement" type="submit">Download Service
                                    Agreement</button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection()
@section('loadstyles')
    <link href="{{ URL::to('/') }}/assets/plugins/datetimepicker/css/classic.css" rel="stylesheet" />
    <link href="{{ URL::to('/') }}/assets/plugins/datetimepicker/css/classic.time.css" rel="stylesheet" />
    <link href="{{ URL::to('/') }}/assets/plugins/datetimepicker/css/classic.date.css" rel="stylesheet" />
@endsection()
@section('loadscript')
    <script src="{{ URL::to('/') }}/assets/plugins/simplebar/js/simplebar.min.js"></script>
    <script src="{{ URL::to('/') }}/assets/plugins/metismenu/js/metisMenu.min.js"></script>
    <script src="{{ URL::to('/') }}/assets/plugins/perfect-scrollbar/js/perfect-scrollbar.js"></script>
    <script src="{{ URL::to('/') }}/assets/plugins/datetimepicker/js/legacy.js"></script>
    <script src="{{ URL::to('/') }}/assets/plugins/datetimepicker/js/picker.js"></script>
    <script src="{{ URL::to('/') }}/assets/plugins/datetimepicker/js/picker.time.js"></script>
    <script src="{{ URL::to('/') }}/assets/plugins/datetimepicker/js/picker.date.js"></script>
@endsection()

@section('outsidescripts')
    // declaring useful variables
    function retrieveDataIdValues() {
    const checkboxes = document.querySelectorAll('.manojnit:checked');
    const dataIdValues = Array.from(checkboxes).map(checkbox => checkbox.dataset.id);

    console.log(dataIdValues);
    }

    // Call the function every 10 seconds
    {{-- setInterval(retrieveDataIdValues, 10000); // 10000 milliseconds = 10 seconds --}}


    let price = 2500;
    let country = ''; // Initialize country with an empty string
    let gstpr=0.18;
    let selectedGSTType='included';
    var gstPercentages = {};

    //setting the gstobject with countries we have and percentages
    @foreach ($gsts as $gst)
        gstPercentages["{{ $gst->country }}"] = {{ $gst->percentage }};
    @endforeach

    //selecting the country fileds and pricetype fields
    const selectElement = document.querySelector('.client_country');
    const priceTypeSelect = document.querySelector('.currency');
    const gstTypeSelect = document.querySelector('.gst-type');
    console.log(selectElement);

    // Event listener for selecting GST type
    gstTypeSelect.addEventListener('change', function () {
    console.log("hi im in gsttype");
    selectedGSTType = gstTypeSelect.value;
    if (selectedGSTType === 'included') {
    pregstcalc(price);
    } else {
    postgstcalc(price);
    }
    updateSelectedServicesCount();
    });

    // Function to update price with GST based on the selected GST type
    function updatePriceWithGST(val) {
    console.log("hi im in pricevalue");
    selectedGSTType = gstTypeSelect.value;
    updateGstForCountry(country);
    if (selectedGSTType === 'included') {
    pregstcalc(val);
    } else {
    postgstcalc(val);
    }
    }

    //eventlistener to refelect with changes in gst according to country selected
    selectElement.addEventListener('change', function() {
    console.log("hi im in countrytype");
    const selectedCountry = selectElement.options[selectElement.selectedIndex].text;
    console.log(selectedCountry);
    const prevgst=gstpr;
    updateGstForCountry(selectedCountry);
    if (selectedGSTType === 'included') {
    pregstcalc((1 + prevgst) * price);
    } else {
    postgstcalc(price);
    }
    const cntry = selectElement.value;

    // Logic to select an option in the price type select based on the selected country
    if (cntry === 'US') {
    priceTypeSelect.querySelector('option[value="$"]').selected = true;
    }
    else if (cntry === 'India') {
    priceTypeSelect.querySelector('option[value="₹"]').selected = true;
    }

    });



    // Method to update country dynamically
    function updateGstForCountry(country) {
    console.log("hi im in updategstaccording to country");
    if (country) {
    gstpr = gstPercentages[country] / 100;
    console.log(gstpr);
    } else {
    gstpr = 0.18; // default 18% as for India
    }
    }

    // Function to calculate total amount after adding taxes
    function postgstcalc(val) {
    console.log("hi im in postgsttype");
    price = Number(val);
    document.querySelectorAll('.addpr').forEach((x) => {
    x.innerHTML = `Rs.${Math.ceil(0.1 * price)}`;
    });

    document.querySelectorAll('.featpr').forEach((y) => {
    y.innerHTML = `Rs.${Math.ceil(0.9 * price)}`;
    });

    document.querySelectorAll('.subpr').forEach((z) => {
    z.innerHTML = `Rs.${Math.ceil(price)}`;
    });

    document.querySelectorAll('.subprgst').forEach((a) => {
    a.innerHTML = `GST ${gstpr * 100}% of Rs.${Math.ceil(price)} i.e Rs.${Math.ceil(gstpr * price)}`;
    });

    document.querySelectorAll('.subprtot').forEach((b) => {
    b.innerHTML = `Total Price after GST is Rs.${Math.ceil(price+(price*gstpr))}`;
    });
    updateSelectedServicesCount();
    }

    // Function to calculate total amount with GST included
    function pregstcalc(val) {
    console.log("hi im in pregsttype");
    price = Number(val);
    const x = 1 + gstpr;
    const pr=price/x;
    document.querySelectorAll('.addpr').forEach((x) => {
    x.innerHTML = `Rs.${Math.ceil(0.1*pr)}`;
    });

    document.querySelectorAll('.featpr').forEach((y) => {
    y.innerHTML = `Rs.${Math.ceil(0.9*pr)}`;
    });

    document.querySelectorAll('.subpr').forEach((z) => {
    z.innerHTML = `Rs.${Math.ceil(pr)}`;
    });

    document.querySelectorAll('.subprgst').forEach((a) => {
    a.innerHTML = `GST ${gstpr * 100}% of Rs.${Math.ceil(pr)} i.e Rs.${Math.ceil(gstpr * pr)}`;
    });

    document.querySelectorAll('.subprtot').forEach((b) => {
    b.innerHTML = `Total Price after GST is Rs.${Math.ceil(price)}`;
    });
    updateSelectedServicesCount();
    }





    const selectedServices = new Set(); // To keep track of selected services

    function toggleService(service) {
    const selectedServicesContainer = document.querySelector('.neshallselected-services');
    const serviceElement = document.getElementById(service);

    // Check if the service is already in the selected services
    if (selectedServices.has(service)) {
    // If it's already selected, remove it from selectedServices
    document.querySelector('.newdepartments').appendChild(serviceElement);
    selectedServices.delete(service);
    serviceElement.classList.remove('neshallselected-features');
    serviceElement.classList.add('neshallfeatures');
    // Append the serviceElement back to its original location

    } else {
    // If it's not selected, add it to selectedServices
    selectedServices.add(service);
    serviceElement.classList.remove('neshallfeatures');
    serviceElement.classList.add('neshallselected-features');
    }

    updateSelectedServices();
    }

    function updateSelectedServices() {
    selectedServices.forEach(service => {
    const serviceElement = document.getElementById(service);
    document.querySelector('.newdepartments').appendChild(serviceElement);
    });

    const selectedServicesContainer = document.querySelector('.neshallselected-services');
    selectedServicesContainer.innerHTML = ''; // Clear previous content

    selectedServices.forEach(service => {
    const serviceColumn = document.createElement('div');
    serviceColumn.classList.add('neshallservice-column');

    const serviceSection = document.getElementById(service);

    if (serviceSection) {
    serviceSection.classList.remove('neshallfeatures');
    serviceSection.classList.add('neshallselected-features');

    serviceColumn.appendChild(serviceSection);
    selectedServicesContainer.appendChild(serviceColumn);
    }
    });

    updateSelectedServicesCount();
    }




    function updateSelectedServicesCount() {
    const selectedServicesDiv = document.querySelector(".neshallselected-services");
    const selectedServicesCount = selectedServicesDiv.childElementCount;
    const h4Element = document.getElementById("selectedServicesCount");
    const prc = (selectedGSTType === 'included') ? price : price * (1 + gstpr);
    h4Element.textContent = `Total cost of the selected services: Rs.${Number(selectedServicesCount)*Math.ceil(prc)}`;
    }





    document.addEventListener("click", e => {
    // Handle quotation button click
    if (e.target.classList.contains('btn-quotation')) {
    handleQuotationButtonClick(e);
    }

    // Handle main-service-label click
    if (e.target.classList.contains('main-service-label')) {
    console.log(e.target.getAttribute('data-id'));
    toggleService(e.target.getAttribute('data-id'));
    }

    // Prevent default for specific elements
    if (!(e.target.closest('.prevent-default') || e.target.matches('input') || e.target.closest('.btn-quotation'))) {
    e.preventDefault();
    }


    const isDropbtn = e.target.matches("[data-manojdropdown-btn]");
    if (!isDropbtn && e.target.closest("[data-manojdropdown]") != null)
    return;

    let curentDropdown;
    if (isDropbtn) {

    const spanElement = e.target.querySelector('span');
    spanElement.classList.toggle("up");
    curentDropdown = e.target.closest("[data-manojdropdown]");
    curentDropdown.classList.toggle("active");

    }
    document.querySelectorAll("[data-manojdropdown].active").forEach(dropdown=>{
    if (dropdown === curentDropdown)
    return;
    dropdown.classList.toggle("active");
    const spanElement = dropdown.querySelector("span.arrow");
    spanElement.classList.toggle("up");
    }
    )
    }

    );
@endsection()
@section('scripts')
    $('.datepicker').pickadate({
    selectMonths: true,
    selectYears: true,
    });
    var forms = document.querySelectorAll('.needs-validation');

    // Loop over them and prevent submission
    Array.prototype.slice.call(forms)
    .forEach(function (form) {
    form.addEventListener('submit', function (event) {
    if (!form.checkValidity()) {
    event.preventDefault()
    event.stopPropagation()
    }

    form.classList.add('was-validated')
    }, false)
    })
    const dropdownButtons = document.querySelectorAll('.manojbtn');
    function handleQuotationButtonClick(e) {
    e.preventDefault();


    const checkboxes = document.querySelectorAll('.manojnit:checked');
    const dataIdValues = Array.from(checkboxes).map(checkbox => checkbox.dataset.id);


    // Set the hidden input field's value to a comma-separated list of selected deliverable IDs
    document.getElementById('selectedDeliverables').value = JSON.stringify(dataIdValues);

    const mainServicesArray = [];
    const buttonNamesArray = [];
    const checkedCheckboxesArray = [];
    const importantNoteData = {};
    const additionalFeatureData = {};


    dropdownButtons.forEach(button => {


    {{-- // Get the parent dropdown container --}}
    const dropdownContainer = button.nextElementSibling;

    {{-- // Get all the checked checkboxes within this dropdown container --}}
    const checkedCheckboxes = dropdownContainer.querySelectorAll('input[type="checkbox"]:checked');


    {{-- // If there are checked checkboxes, gather information --}}
    if (checkedCheckboxes.length > 0) {
    checkedCheckboxes.forEach(checkbox => {


    if (checkbox.classList.contains('note')) {
    const parentId = checkbox.closest('.note-list').getAttribute('data-id');
    const checkboxId = checkbox.getAttribute('data-id');

    // Check if the parent ID already exists in the importantNoteData object
    if (!importantNoteData[parentId]) {
    importantNoteData[parentId] = [checkboxId]; // Create a new array with the checkbox ID
    } else {
    importantNoteData[parentId].push(checkboxId); // Add the checkbox ID to the existing array
    }
    }

    else if (checkbox.classList.contains('addfeat')) {
    const parentId = checkbox.closest('.addfeature-list').getAttribute('data-id');
    const checkboxId = checkbox.getAttribute('data-id');

    // Check if the parent ID already exists in the additionalFeatureData object
    if (!additionalFeatureData[parentId]) {
    additionalFeatureData[parentId] = [checkboxId]; // Create a new array with the checkbox ID
    } else {
    additionalFeatureData[parentId].push(checkboxId); // Add the checkbox ID to the existing array
    }
    }





    else {
    const buttonDataId = button.getAttribute('data-id');
    const parentDiv = button.closest('.mainselect');

    if (parentDiv) {
    // Get the ID of the parent div
    const parentId = parentDiv.getAttribute('data-id');
    mainServicesArray.push(parentId);

    // Now you have the ID of the parent div with class "mainselect"
    console.log("Parent Div ID:", parentId);
    }

    buttonNamesArray.push(buttonDataId);
    checkedCheckboxesArray.push(checkbox.getAttribute('data-id'));
    }
    });

    }
    // Assuming you have an array called mainServicesArray with possibly duplicate values

    // Convert the array to a Set to automatically remove duplicates
    const uniqueMainServicesSet = new Set(mainServicesArray);

    // Convert the Set back to an array (if needed)
    const uniqueMainServicesArray = [...uniqueMainServicesSet];

    // uniqueMainServicesArray now contains unique values
    console.log(uniqueMainServicesArray);

    document.getElementById('main-servicesInput').value = JSON.stringify(uniqueMainServicesArray);
    document.getElementById('featuresInput').value = JSON.stringify(buttonNamesArray);
    document.getElementById('featurelistInput').value = JSON.stringify(checkedCheckboxesArray);
    document.getElementById('selectedimpnote').value = JSON.stringify(importantNoteData);
    document.getElementById('selectedaddfeat').value = JSON.stringify(additionalFeatureData);
    const selectedServicesInput = document.getElementById('selectedservices'); // Get the hidden input element

    // Join the array elements with the separator
    const selectedServicesValue = uniqueMainServicesArray.join(',');

    // Set the value of the hidden input
    selectedServicesInput.value = selectedServicesValue;


    });
    {{-- // Set form type and submit the form --}}
    console.log("hi manoj");
    console.log(checkedCheckboxesArray);
    $('#form-type').val('quotation-form');
    $('#estimation-form').submit();
    {{-- console.log(e.error); --}}
    };




    $("#estimation-form").validate({
    rules: {
    estimate_date: "required",
    estimate_expire: "required",
    estimate_no: "required",
    price: {
    required: true,
    number: true
    },
    price_type: "required",
    },
    messages: {
    estimate_date: "Please select estimate date",
    estimate_expire: "Please select estimate expire date",
    estimate_no: "Please enter estimate no",
    price: {
    required: "Please enter price",
    number: "Please enter number in price",
    },
    price_type: "Please select price type",
    }
    });
@endsection()

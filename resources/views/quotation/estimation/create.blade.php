@extends('layouts.index')

@section('title')
    Estimation
@endsection()

@section('content')
    <style>
        .manojdraggable {
            padding: 0.5rem 0.3rem 0.2rem 0.5rem;
            border-radius: 0.5rem;
            width: fit-content;
            height: fit-content;
            background-color: #5b6873;
            margin-bottom: 10px;
            border: 1px solid black;
            cursor: move;
        }

        .manojbtn {
            padding: 0.5rem;
            border-radius: 0.3rem;
            width: 200px;
            height: fit-content;
            cursor: move;
            background-color: #67bae3;
        }

        .manojdraggable.manojdragging {
            opacity: .5;
        }

        #div1,
        #div2,
        #div3,
        #div4 {
            float: left;
            width: 46%;
            height: auto;
            margin: 10px;
            padding: 20px;
            border: 1px solid black;
            cursor: pointer;
        }

        @media only screen and (max-width: 1600px) {

            #div1,
            #div2,
            #div3,
            #div4 {
                float: left;
                width: 46%;
                height: auto;
                margin: 10px;
                padding: 20px;
                border: 1px solid black;
                cursor: pointer;
            }
        }

        @media only screen and (max-width: 1200px) {

            #div1,
            #div2,
            #div3,
            #div4 {
                float: left;
                width: 47%;
                height: auto;
                margin: 10px;
                padding: 20px;
                border: 1px solid black;
                cursor: pointer;
            }
        }

        @media only screen and (max-width: 762px) {

            #div1,
            #div2,
            #div3,
            #div4 {
                float: left;
                width: 46%;
                height: auto;
                margin: 10px;
                padding: 20px;
                border: 1px solid black;
                cursor: pointer;
            }
        }

        #div1 div,
        #div2 div,
        #div3 div,
        #div4 div {
            margin: 15px;
            padding: 15px;
        }

        label.error {
            color: red;
            padding: 5px;

        }


        .dropdown {
            position: relative;

        }




        .manojdropdown {
            position: relative;
            margin-right: 10px;
            margin-bottom: 10px;
        }

        .arrow {
            border: solid black;
            border-width: 0 3px 3px 0;
            display: inline-block;
            padding: 3px;
        }


        .down {
            transform: rotate(45deg);
            -webkit-transform: rotate(45deg);
        }

        .up {
            transform: rotate(-135deg);
            -webkit-transform: rotate(-135deg);
        }

        .manojdropdown-menu {
            display: grid;
            grid-template-columns: repeat(2, max-content);
            gap: .2rem;
            position: absolute;
            height: fit-content;
            max-height: 200px;
            overflow: auto;
            left: 0;
            top: calc(100% + .3rem);
            padding: 1rem;
            color: black;
            background-color: #73e9bc;
            box-shadow: 0 2px 15px 0 rgba(0, 0, 0, 0.5);
            opacity: 0;
            cursor: move;
            pointer-events: none;
            transform: translateY(-10px);
            transition: opacity 0.8s ease-in-out, transform 0.8s ease-in-out, max-height 0.3s ease;
            ;


        }

        .manojdropdown-menu input[type="checkbox"] {
            margin-right: 10px;
            width: 20px;
            height: 20px;
        }

        .show-toggle {
            background-color: #3498db;
            color: white;
            border: none;
            padding: 2px 4px;
            cursor: pointer;
        }

        .hidden {
            display: none;
        }

        .manojdropdown.active>.manojbtn+.manojdropdown-menu {

            opacity: 1;
            transform: translateY(0);
            pointer-events: auto;
            z-index: 10;
        }

        .container {
            height: fit-content;
            width: fit-content;
        }
    </style>
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
        <div class="breadcrumb-title pe-3">Add New Estimation</div>
        <div class="ps-3">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb mb-0 p-0">
                    <li class="breadcrumb-item"><a href="javascript:void(0);"><i class="bx bx-home-alt"></i></a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">Estimation</li>
                </ol>
            </nav>
        </div>
    </div>

    <div class="row">
        <div class="col-xl-9 mx-auto">
            <hr />
            <div class="card">
                <div class="card-body">
                    <div class="p-4 border rounded">
                        <form method="post" id="estimation-form" action="{{ URL::to('/') }}/quotation/estimation/store"
                            class="row g-3 needs-validation" enctype="multipart/form-data" novalidate>
                            @csrf
                            <div class="col-md-6">
                                <label for="validationCustom101" class="form-label">Client Name</label>
                                <input type="text" class="form-control" id="validationCustom101" value=""
                                    name="client_name" placeholder="Client Name" required>
                                <div class="valid-feedback">Looks good!</div>
                            </div>
                            <div class="col-md-6">
                                <label for="validationCustom102" class="form-label">Client Address</label>
                                <input type="text" class="form-control" id="validationCustom102" value=""
                                    name="client_address" placeholder="Client Address" required>
                                <div class="valid-feedback">Looks good!</div>
                            </div>
                            <div class="col-md-6">
                                <label for="validationCustom103" class="form-label">Mobile no</label>
                                <input type="text" class="form-control" id="validationCustom103" value=""
                                    name="mobile_no" placeholder="Mobile no" required>
                                <div class="valid-feedback">Looks good!</div>
                            </div>
                            <div class="col-md-6">
                                <label for="validationCustom104" class="form-label">Client Email</label>
                                <input type="text" class="form-control" id="validationCustom104" value=""
                                    name="client_email" placeholder="Client Email" required>
                                <div class="valid-feedback">Looks good!</div>
                            </div>
                            <div class="col-md-6">
                                <label for="validationCustom01" class="form-label">Estimate date</label>
                                <input type="text" class="form-control datepicker" id="validationCustom01"
                                    value="{{ $current_date }}" name="estimate_date" placeholder="Estimate date" required>
                                <div class="valid-feedback">Looks good!</div>
                            </div>
                            <div class="col-md-6">
                                <label for="validationCustom03" class="form-label">Estimate expire</label>
                                <input type="text" class="form-control datepicker" id="validationCustom03"
                                    value="{{ $expire_date }}" name="estimate_expire" placeholder="Estimate expire"
                                    required>
                                <div class="valid-feedback">Looks good!</div>
                            </div>
                            <div class="col-md-6">
                                <label for="validationCustom02" class="form-label">Estimate no</label>
                                <input type="text" class="form-control" id="validationCustom02"
                                    value="{{ $next_estimate_no }}" name="estimate_no" placeholder="Estimate no" required>
                                <div class="valid-feedback">Looks good!</div>
                            </div>
                            <div class="col-md-6">
                                <label for="inputState" class="form-label">Service</label>
                                <select id="inputState" name="service_id" class="form-select">
                                    <option value="">Choose...</option>
                                    <option value="1" selected>Web Development</option>
                                    <option value="2">Graphics</option>
                                    <option value="3">Video Services</option>
                                    <option value="4">Social Media</option>
                                    <option value="5">Mobile Application</option>
                                    <option value="6">Digital Marketing</option>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label for="validationCustom002" class="form-label">Total Price</label>
                                <input type="text" class="form-control" id="validationCustom002" value=""
                                    name="price" placeholder="Total Price" required>
                                <div class="valid-feedback">Looks good!</div>
                            </div>
                            <div class="col-md-6">
                                <label for="inputState" class="form-label">Price Type</label>
                                <select id="inputState" name="price_type" class="form-select">
                                    <option value="">Choose...</option>
                                    <option selected value="$">$</option>
                                    <option value="₹">₹</option>
                                </select>
                            </div>

                            <input type="hidden" name="main_categories" id="main-servicesInput">
                            <input type="hidden" name="features" id="featuresInput">
                            <input type="hidden" name="featurelists" id="featurelistInput">
                            <input type="hidden" value="" id="form-type" name="form_type">

                            <label class="form-check-label">What are the main services you want us to provide?</label>
                            <div style="display: flex; flex-direction: row;border: 1px solid #000000">
                                @foreach ($main_cats as $main_cat)
                                    <div>
                                        <label class="form-check-label manojname" style="font-weight: bold;color:#0dcaf0;"
                                            data-id="{{ $main_cat->id }}">{{ $main_cat->title }}</label>
                                        <div style="display: flex; flex-direction: column;border:solid black"
                                            class="container">

                                            @foreach ($features as $feature)
                                                @if ($feature->main_category_id == $main_cat->id)
                                                    <div class="manojdraggable" draggable="true">
                                                        <div class="manojdropdown" data-manojdropdown>
                                                            <button class="manojbtn" data-manojdropdown-btn
                                                                data-id="{{ $feature->id }}"
                                                                data-mainid="{{ $main_cat->id }}">{{ $feature->feature_title }}
                                                                <span class="arrow down"></span> </button>
                                                            <div class="manojdropdown-menu">
                                                                @foreach ($features_lists as $features_list)
                                                                    @if ($features_list->feature_id == $feature->id)
                                                                        <input type="checkbox" class="checkbox"
                                                                            data-id="{{ $features_list->id }}"> <label
                                                                            for="features">{{ $features_list->feature_list }}</label>
                                                                    @endif
                                                                @endforeach
                                                                {{-- <button class="show-toggle" id="show-toggle">Show More</button> --}}
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif
                                            @endforeach
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <div class="col-12">
                                <button class="btn btn-light btn-quotation" type="submit">Download Quotation</button>
                                <button class="btn btn-light btn-service-agreement" type="submit">Download Service
                                    Agreement</button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection()
@section('loadstyles')
    <!-- <link href="{{ URL::to('/') }}/assets/plugins/fancy-file-uploader/fancy_fileupload.css" rel="stylesheet" />
                                                                                    <link href="{{ URL::to('/') }}/assets/plugins/Drag-And-Drop/dist/imageuploadify.min.css" rel="stylesheet" />
                                                                                    <link href="{{ URL::to('/') }}/assets/plugins/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet" /> -->
    <link href="{{ URL::to('/') }}/assets/plugins/datetimepicker/css/classic.css" rel="stylesheet" />
    {{-- <link href="{{URL::to('/')}}/assets/css/stylesmanoj.css" rel="stylesheet" /> --}}
    <link href="{{ URL::to('/') }}/assets/plugins/datetimepicker/css/classic.time.css" rel="stylesheet" />
    <link href="{{ URL::to('/') }}/assets/plugins/datetimepicker/css/classic.date.css" rel="stylesheet" />
@endsection()
@section('loadscript')
    <script src="{{ URL::to('/') }}/assets/plugins/simplebar/js/simplebar.min.js"></script>
    <script src="{{ URL::to('/') }}/assets/plugins/metismenu/js/metisMenu.min.js"></script>
    <script src="{{ URL::to('/') }}/assets/plugins/perfect-scrollbar/js/perfect-scrollbar.js"></script>
    <script src="{{ URL::to('/') }}/assets/plugins/datetimepicker/js/legacy.js"></script>
    <script src="{{ URL::to('/') }}/assets/plugins/datetimepicker/js/picker.js"></script>
    <script src="{{ URL::to('/') }}/assets/plugins/datetimepicker/js/picker.time.js"></script>
    <script src="{{ URL::to('/') }}/assets/plugins/datetimepicker/js/picker.date.js"></script>
    <!-- <script src="{{ URL::to('/') }}/assets/plugins/fancy-file-uploader/jquery.ui.widget.js"></script>
                                                                                    <script src="{{ URL::to('/') }}/assets/plugins/fancy-file-uploader/jquery.fileupload.js"></script>
                                                                                    <script src="{{ URL::to('/') }}/assets/plugins/fancy-file-uploader/jquery.iframe-transport.js"></script>
                                                                                    <script src="{{ URL::to('/') }}/assets/plugins/fancy-file-uploader/jquery.fancy-fileupload.js"></script>
                                                                                    <script src="{{ URL::to('/') }}/assets/plugins/Drag-And-Drop/dist/imageuploadify.min.js"></script> -->
@endsection()

@section('outsidescripts')
    const draggables = document.querySelectorAll('.manojdraggable');
    const containers = document.querySelectorAll('.container');

    draggables.forEach(draggable => {
    draggable.addEventListener('dragstart', () => {
    draggable.classList.add('manojdragging');
    });

    draggable.addEventListener('dragend', () => {
    draggable.classList.remove('manojdragging');
    });
    });

    containers.forEach(container => {
    container.addEventListener('dragover', e => {
    e.preventDefault();
    const afterElement = getDragAfterElement(container, e.clientY);
    const draggable = container.querySelector('.manojdragging');

    if (afterElement == null) {
    container.appendChild(draggable);
    } else {
    container.insertBefore(draggable, afterElement);
    }
    });
    });

    function getDragAfterElement(container, y) {
    const draggableElements = [...container.querySelectorAll('.manojdraggable:not(.manojdragging)')];

    return draggableElements.reduce((closest, child) => {
    const box = child.getBoundingClientRect();
    const offset = y - box.top - box.height / 2;
    if (offset < 0 && offset> closest.offset) {
        return { offset: offset, element: child };
        } else {
        return closest;
        }
        }, { offset: Number.NEGATIVE_INFINITY }).element;
        }

        document.addEventListener("click", e=>{
        if (e.target.classList.contains('btn-quotation')) {
        handleQuotationButtonClick(e);

        }
        if (!(e.target.closest('.prevent-default') || e.target.matches('input'))) {
        e.preventDefault();
        }

        const isDropbtn = e.target.matches("[data-manojdropdown-btn]");
        if (!isDropbtn && e.target.closest("[data-manojdropdown]") != null)
        return;

        let curentDropdown;
        if (isDropbtn) {

        const spanElement = e.target.querySelector('span');
        spanElement.classList.toggle("up");
        curentDropdown = e.target.closest("[data-manojdropdown]");
        curentDropdown.classList.toggle("active");

        }
        document.querySelectorAll("[data-manojdropdown].active").forEach(dropdown=>{
        if (dropdown === curentDropdown)
        return;
        dropdown.classList.toggle("active");
        const spanElement = dropdown.querySelector("span.arrow");
        spanElement.classList.toggle("up");
        }
        )

        }
        );
    @endsection()
    @section('scripts')
        $('.datepicker').pickadate({
        selectMonths: true,
        selectYears: true,
        });
        var forms = document.querySelectorAll('.needs-validation');

        // Loop over them and prevent submission
        Array.prototype.slice.call(forms)
        .forEach(function (form) {
        form.addEventListener('submit', function (event) {
        if (!form.checkValidity()) {
        event.preventDefault()
        event.stopPropagation()
        }

        form.classList.add('was-validated')
        }, false)
        })

        const dropdownButtons = document.querySelectorAll('.manojbtn');
        function handleQuotationButtonClick(e) {

        e.preventDefault();
        console.log("hi submit is clicked");
        const mainServicesArray = [];
        const buttonNamesArray = [];
        const checkedCheckboxesArray = [];

        dropdownButtons.forEach(button => {


        {{-- // Get the parent dropdown container --}}
        const dropdownContainer = button.nextElementSibling;

        {{-- // Get all the checked checkboxes within this dropdown container --}}
        const checkedCheckboxes = dropdownContainer.querySelectorAll('input[type="checkbox"]:checked');


        {{-- // If there are checked checkboxes, gather information --}}
        if (checkedCheckboxes.length > 0) {
        const buttonDataId = button.getAttribute('data-id');
        const mainServiceValue =button.getAttribute('data-mainid') ;
        mainServicesArray.push(mainServiceValue);
        buttonNamesArray.push(buttonDataId);
        checkedCheckboxes.forEach(checkbox => {
        checkedCheckboxesArray.push(checkbox.getAttribute('data-id'));
        });
        }
        document.getElementById('main-servicesInput').value = JSON.stringify(mainServicesArray);
        document.getElementById('featuresInput').value = JSON.stringify(buttonNamesArray);
        document.getElementById('featurelistInput').value = JSON.stringify(checkedCheckboxesArray);

        });
        {{-- // Set form type and submit the form --}}
        console.log("hi manoj");
        console.log(checkedCheckboxesArray);
        $('#form-type').val('quotation-form');
        $('#estimation-form').submit();
        {{-- console.log(e.error); --}}
        };


        $('.btn-service-agreement').click(function(e){
        e.preventDefault();
        {{-- const f=document.getElementById('estimation-form');
        const actionValue = f.getAttribute('action');
        console.log('Form action:', actionValue); --}}

        const mainServicesArray = [];
        const buttonNamesArray = [];
        const checkedCheckboxesArray = [];

        {{-- // Get all the dropdown buttons --}}
        {{-- const dropdownButtons = document.querySelectorAll('.manojbtn'); --}}

        {{-- // Add event listeners to each dropdown button --}}
        dropdownButtons.forEach(button => {


        {{-- // Get the parent dropdown container --}}
        const dropdownContainer = button.nextElementSibling;

        {{-- // Get all the checked checkboxes within this dropdown container --}}
        const checkedCheckboxes = dropdownContainer.querySelectorAll('input[type="checkbox"]:checked');


        {{-- // If there are checked checkboxes, gather information --}}
        if (checkedCheckboxes.length > 0) {
        const buttonDataId = button.getAttribute('data-id');
        const mainServiceValue =button.getAttribute('data-mainid') ;
        mainServicesArray.push(mainServiceValue);
        buttonNamesArray.push(buttonDataId);
        checkedCheckboxes.forEach(checkbox => {
        checkedCheckboxesArray.push(checkbox.getAttribute('data-id'));
        });
        }
        document.getElementById('main-servicesInput').value = JSON.stringify(mainServicesArray);
        document.getElementById('featuresInput').value = JSON.stringify(buttonNamesArray);
        document.getElementById('featurelistInput').value = JSON.stringify(checkedCheckboxesArray);

        });
        console.log(checkedCheckboxesArray);
        $('#form-type').val('service-agreement-form');
        $("#estimation-form").validate();
        $('#estimation-form').submit();
        });

        $("#estimation-form").validate({
        rules: {
        estimate_date: "required",
        estimate_expire: "required",
        estimate_no: "required",
        service_id: "required",
        price: {
        required: true,
        number: true
        },
        price_type: "required",
        },
        messages: {
        estimate_date: "Please select estimate date",
        estimate_expire: "Please select estimate expire date",
        estimate_no: "Please enter estimate no",
        service_id: "Please select estimate service",
        price: {
        required: "Please enter price",
        number: "Please enter number in price",
        },
        price_type: "Please select price type",
        }
        });
    @endsection()

<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>ESTIMATION</title>
        <!-- <link href="{{URL::to('/')}}/assets/css/bootstrap.min.css" rel="stylesheet"> -->
        <style type="text/css">
            input[type=text], select {
                padding: 12px 20px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }
            .red{
                color: red;
            }
        </style>
    </head>
    <body style="margin: 0;">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <?php
            if ( $price_type != "$" ) {
                $price_type = '<img width="12px" height="12px" src="' . URL::to('/assets/images/rupees-50x50.png') . '">';
            }
        ?>
        <form method="post" action="{{ route('service-agreement-store') }}" class="row g-3 needs-validation" enctype="multipart/form-data" id="signature-form"  novalidate>
            @csrf
            <input type="hidden" name="estimate_ids" value="{{$estimate_ids}}">
            <center>
            <table class="table" style="width: 650px; background: #f5f5f5; padding: 20px 15px;">
                <thead>
                    <tr>
                        <th colspan="2" style="text-align: left;"><img style="width: 200px" src="{{URL::to('/')}}/assets/images/logo.png"></th>
                        <td colspan="2" style="padding-left: 17%; font-size: 14px; color: #afafaf;">CIN: 72900GJ2021PTC119524
                            <br>PAN: AAHCN3844K
                        </td>
                    </tr>
                </thead>
            </table>
            <table class="table" style="width: 650px;padding: 0 15px">
                <thead>
                    <tr>
                        <td colspan="2"></td>
                        <td colspan="2" style="text-align: right; font-size: 24px;padding-top: 30px;padding-bottom: 10px;color: #7bb4d8;">SERVICES AGREEMENT</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td colspan="5" style="line-height: 20px;">This Contract for Services is made effective as of {{date('d F Y', strtotime($estimate_date))}}, by and between <input type="text" value="{{$client_name}}" id="client-name" name="client_name" placeholder="Enter Client Name"> of <input type="text" class="client-address" id="client-address" name="client_address" value="{{$client_address}}" placeholder="Enter Address"> (the "Recipient"), and neshallWeb Private Limited of Valsad, GJ, India (the "Provider").</td>
                    </tr>
                    <tr style="height:5px;"></tr>
                    <tr>
                        <td colspan="5" style="line-height: 20px;"><b>1. DESCRIPTION OF SERVICES.</b> Beginning on {{date('d F Y', strtotime($estimate_date))}}, the Provider will serve to the Recipient the following services (collectively, the "Services")</td>
                    </tr>
                    <tr style="height:10px;"></tr>
                </tbody>
            </table>
            @if( isset( $html_array ) && ! empty( $html_array ) )
                @for($i=0;$i < count($html_array);$i++)
                    @if( isset( $heading_array[$i] ) && ! empty( $heading_array[$i] ) )
                        {!! $heading_array[$i] !!}
                    @endif
                    @if( isset( $html_array[$i] ) && ! empty( $html_array[$i] ) )
                        {!! $html_array[$i] !!}
                    @endif
                @endfor
            
            @endif
            <table style="width: 650px;padding: 0 15px">
                <tbody>
                    <tr>
                        <td colspan="5" style="border-bottom: 1px solid #a5a5a5;"></td>
                    </tr>
                    <tr>
                        <td colspan="5" style="height: 20px"></td>
                    </tr>
                    <tr>
                        <td colspan="5" style="line-height: 22px;"><b>2. PAYMENT.</b> Recipient will pay total of {!!$price_type!!}{{$price}} to the Provider for the agreed services. Payment shall be made to Provider according to the following schedule:</td>
                    </tr>
                    <tr>
                        <td colspan="5" style="height: 5px"></td>
                    </tr>
                    <tr>
                        <td colspan="5" style="line-height: 20px;"><b>Events and Payments Schedule:</b></td>
                    </tr>
                    <tr>
                        <td colspan="3" style="line-height: 20px; width: 500px;">
                            <table style="width: 100%;">
                                <tbody>
                                <tr>
                                    <td colspan="4">• Signing Service Agreement (40%)</td>
                                </tr>
                                <tr>
                                    <td width="20px"></td>
                                    <td colspan="4">Amount payable: {!!$price_type!!}{{$price_40per}}</td>
                                </tr>
                                <!-- <tr>
                                    <td colspan="4">• First Draft Delivery (35%)</td>
                                </tr>
                                <tr>
                                    <td width="20px"></td>
                                    <td colspan="4">Amount payable: {!!$price_type!!}{{$price_35per}}</td>
                                </tr> -->
                                <tr>
                                    <td colspan="4">• Final Project Delivery (60%)</td>
                                </tr>
                                <tr>
                                    <td width="20px"></td>
                                    <td colspan="4">Amount payable: {!!$price_type!!}{{$price_60per}} </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                        <td colspan="1" style="line-height: 20px;">
                            <table>
                                <tbody>
                                    <tr>
                                        <td>Recipient’s Initials: <input type="text" class="recipient-initials" id="recipient-initials1" name="recipient_initials1" placeholder="Enter Recipient’s Initials text"></td>
                                    </tr>
                                    <tr>
                                        <td height="20px"></td>
                                    </tr>
                                    <tr>
                                        <td>Recipient’s Initials: <input type="text" class="recipient-initials" id="recipient-initials2" name="recipient_initials2" placeholder="Enter Recipient’s Initials text"></td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5" style="height: 5px"></td>
                    </tr>
                    <tr>
                        <td colspan="5" style="line-height: 22px;">If any invoice is not paid when due, interest will be added to and payable on all overdue amounts at 20 percent per year, or the maximum percentage allowed under applicable State laws, whichever is less.</td>
                    </tr>
                    <tr>
                        <td colspan="2" style="height: 5px"></td>
                    </tr>
                    <tr>
                        <td colspan="5" style="line-height: 22px;">In addition to any other right or remedy provided by law, if <span class="client-name">{{$client_name}}</span> fail to pay for the Services when due, neshallWeb Private Limited has the option to treat such failure to pay as a material breach of this Contract and may cancel this Contract and/or seek legal remedies.</td>
                    </tr>
                    @foreach ($serviceagreementcontents as $serviceagreementcontent)
                        <tr>
                            <td colspan="3" style="height: 5px"></td>
                        </tr>
                        <tr>
                            <td colspan="5" style="line-height: 22px;">
                                <?php echo $serviceagreementcontent->content;?>
                            </td>
                        </tr>
                    @endforeach
                    <tr>
                        <td colspan="5" style="text-align: right;">Recipient’s Initials: <input type="text" class="recipient-initials" id="recipient-initials3" name="recipient_initials3" placeholder="Enter Recipient’s Initials text"></td>
                    </tr>
                    <tr>
                        <td colspan="2" style="height: 5px"></td>
                    </tr>
                    <tr>
                        <td style="line-height: 22px; text-decoration: underline;"><b>Recipient:</b></td>
                    </tr>
                    <tr>
                        <td colspan="2" style="height: 3px"></td>
                    </tr>
                    <tr>
                        <td colspan="5"><span class="client-name">{{$client_name}}</span></td>
                    </tr>
                    <tr>
                        <td colspan="2" style="height: 2px"></td>
                    </tr>
                    <tr>
                        <td colspan="5">
                            <table>
                                <tbody>
                                    <tr>
                                        <td width="200px">By:
                                            <div style="display: flex;"> 
                                                <input type="text" id="sign1" value="" name="sign1" placeholder="Name" >
                                            </div>
                                        </td>
                                        <td width="200px">Date: <input type="date" name="sign1_date" value="{{date('Y-m-d')}}" min="{{date('Y-m-d')}}"> </td>
                                    </tr>
                                    <?php
                                     if( $no_of_client > 1 ) {
                                        // code...
                                        ?>
                                        <tr>
                                            <td colspan="2"> <input type="text" id="sign1_designation" value="" name="sign1_designation" placeholder="Designation" > </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </td>
                        <!-- <td width="200px">By: Mr. Sangeet Thakkar</td>
                            <td width="200px">Date: __ </td>-->
                    </tr>
                    <tr>
                        <td height="50px"></td>
                    </tr>
                    <tr>
                        <td>
                            <table>
                                <tbody>
                                    <tr>
                                        <div class="col-md-12">
                                            <label for="inputState" class="form-label">Signature Type</label>
                                            <select id="sign1_type" name="sign1_type" class="form-select">
                                                <option value="" selected>Choose...</option>
                                                <option value="Image">Image</option>
                                                <option value="Font">Font</option>
                                            </select>
                                        </div>
                                        <div class="col-md-12 sign1-font">
                                            <label for="inputState" class="form-label">Fonts</label>
                                            <select id="sign1-font" name="sign1_font" class="form-select">
                                                <option value="" selected>Choose...</option>
                                                <option style="font-family:'Lucida Handwriting ,cursive' ;" value="Lucida Handwriting,cursive"> Lucida Handwriting </option>
                                                <option style="font-family:'Verdana';" value="Verdana"> Verdana </option>
                                                <option style="font-family:'Helvetica';" value="Helvetica"> Helvetica </option>
                                                <option style="font-family:'Tahoma';" value="Tahoma"> Tahoma </option>
                                                <option style="font-family:'Trebuchet MS';" value="Trebuchet MS"> Trebuchet MS </option>
                                                <option style="font-family:'Times New Roman';" value="Times New Roman"> Times New Roman </option>
                                                <option style="font-family:'Georgia';" value="Georgia"> Georgia </option>
                                                <option style="font-family:'Garamond';" value="Garamond"> Garamond </option>
                                                <option style="font-family:'Courier New';" value="Courier New"> Courier New </option>
                                                <option style="font-family:'Brush Script MT';" value="Brush Script MT"> Brush Script MT </option>
                                                <option style="font-family:'Cursive' ;" value="Cursive"> Cursive </option>
                                                <option style="font-family:'Serif' ;" value="Serif"> Serif </option>
                                                <option style="font-family:'Sans-serif' ;" value="Sans-serif"> Sans-serif </option>
                                            </select>
                                        </div>
                                        <div class="col-md-12 sign1-img">
                                            <input type="file" name="sign1_img" class="custom-file-input" id="files">
                                        </div>
                                        <div class="col-md-12">
                                            <h2 id="sign1-text" style="display: none;"></h2>
                                        </div>
                                    </tr>
                                    <tr>
                                        <td width="200px">__________________________</td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                        <!-- <td width="200px">__________________________</td> -->
                    </tr>
                    <?php
                    if( $no_of_client >= 2 ) {
                        ?>
                        <tr>
                            <td colspan="5">
                                <table>
                                    <tbody>
                                        <tr>
                                            <td width="200px">By:
                                                <div style="display: flex;"> 
                                                    <input type="text" id="sign2" value="" name="sign2" placeholder="Name" >
                                                </div>
                                            </td>
                                            <td width="200px">Date: <input type="date" name="sign2_date" value="{{date('Y-m-d')}}" min="{{date('Y-m-d')}}"> </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2"> <input type="text" id="sign2_designation" value="" name="sign2_designation" placeholder="Designation" > </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                            <!-- <td width="200px">By: Mr. Sangeet Thakkar</td>
                                <td width="200px">Date: __ </td>-->
                        </tr>
                        <tr>
                            <td height="50px"></td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <tbody>
                                        <tr>
                                            <div class="col-md-12">
                                                <label for="inputState" class="form-label">Signature Type</label>
                                                <select id="sign2_type" name="sign2_type" class="form-select">
                                                    <option value="" selected>Choose...</option>
                                                    <option value="Image">Image</option>
                                                    <option value="Font">Font</option>
                                                </select>
                                            </div>
                                            <div class="col-md-12 sign2-font">
                                                <label for="inputState" class="form-label">Fonts</label>
                                                <select id="sign2-font" name="sign2_font" class="form-select">
                                                    <option value="" selected>Choose...</option>
                                                    <option style="font-family:'Lucida Handwriting ,cursive' ;" value="Lucida Handwriting,cursive"> Lucida Handwriting </option>
                                                    <option style="font-family:'Verdana';" value="Verdana"> Verdana </option>
                                                    <option style="font-family:'Helvetica';" value="Helvetica"> Helvetica </option>
                                                    <option style="font-family:'Tahoma';" value="Tahoma"> Tahoma </option>
                                                    <option style="font-family:'Trebuchet MS';" value="Trebuchet MS"> Trebuchet MS </option>
                                                    <option style="font-family:'Times New Roman';" value="Times New Roman"> Times New Roman </option>
                                                    <option style="font-family:'Georgia';" value="Georgia"> Georgia </option>
                                                    <option style="font-family:'Garamond';" value="Garamond"> Garamond </option>
                                                    <option style="font-family:'Courier New';" value="Courier New"> Courier New </option>
                                                    <option style="font-family:'Brush Script MT';" value="Brush Script MT"> Brush Script MT </option>
                                                    <option style="font-family:'Cursive' ;" value="Cursive"> Cursive </option>
                                                    <option style="font-family:'Serif' ;" value="Serif"> Serif </option>
                                                    <option style="font-family:'Sans-serif' ;" value="Sans-serif"> Sans-serif </option>
                                                </select>
                                            </div>
                                            <div class="col-md-12 sign2-img">
                                                <input type="file" name="sign2_img" class="custom-file-input" id="files">
                                            </div>
                                            <div class="col-md-12">
                                                <h2 id="sign2-text" style="display: none;"></h2>
                                            </div>
                                        </tr>
                                        <tr>
                                            <td width="200px">__________________________</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                            <!-- <td width="200px">__________________________</td> -->
                        </tr>
                        <?php
                    }
                    if ($no_of_client == 3 ) {
                        ?>
                        <tr>
                            <td colspan="5">
                                <table>
                                    <tbody>
                                        <tr>
                                            <td width="200px">By:
                                                <div style="display: flex;"> 
                                                    <input type="text" id="sign3" value="" name="sign3" placeholder="Name" >
                                                </div>
                                            </td>
                                            <td width="200px">Date: <input type="date" name="sign3_date" value="{{date('Y-m-d')}}" min="{{date('Y-m-d')}}"> </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2"> <input type="text" id="sign3_designation" value="" name="sign3_designation" placeholder="Designation" > </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                            <!-- <td width="200px">By: Mr. Sangeet Thakkar</td>
                                <td width="200px">Date: __ </td>-->
                        </tr>
                        <tr>
                            <td height="50px"></td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <tbody>
                                        <tr>
                                            <div class="col-md-12">
                                                <label for="inputState" class="form-label">Signature Type</label>
                                                <select id="sign3_type" name="sign3_type" class="form-select">
                                                    <option value="" selected>Choose...</option>
                                                    <option value="Image">Image</option>
                                                    <option value="Font">Font</option>
                                                </select>
                                            </div>
                                            <div class="col-md-12 sign3-font">
                                                <label for="inputState" class="form-label">Fonts</label>
                                                <select id="sign3-font" name="sign3_font" class="form-select">
                                                    <option value="" selected>Choose...</option>
                                                    <option style="font-family:'Lucida Handwriting ,cursive' ;" value="Lucida Handwriting,cursive"> Lucida Handwriting </option>
                                                    <option style="font-family:'Verdana';" value="Verdana"> Verdana </option>
                                                    <option style="font-family:'Helvetica';" value="Helvetica"> Helvetica </option>
                                                    <option style="font-family:'Tahoma';" value="Tahoma"> Tahoma </option>
                                                    <option style="font-family:'Trebuchet MS';" value="Trebuchet MS"> Trebuchet MS </option>
                                                    <option style="font-family:'Times New Roman';" value="Times New Roman"> Times New Roman </option>
                                                    <option style="font-family:'Georgia';" value="Georgia"> Georgia </option>
                                                    <option style="font-family:'Garamond';" value="Garamond"> Garamond </option>
                                                    <option style="font-family:'Courier New';" value="Courier New"> Courier New </option>
                                                    <option style="font-family:'Brush Script MT';" value="Brush Script MT"> Brush Script MT </option>
                                                    <option style="font-family:'Cursive' ;" value="Cursive"> Cursive </option>
                                                    <option style="font-family:'Serif' ;" value="Serif"> Serif </option>
                                                    <option style="font-family:'Sans-serif' ;" value="Sans-serif"> Sans-serif </option>
                                                </select>
                                            </div>
                                            <div class="col-md-12 sign3-img">
                                                <input type="file" name="sign3_img" class="custom-file-input" id="files">
                                            </div>
                                            <div class="col-md-12">
                                                <h2 id="sign3-text" style="display: none;"></h2>
                                            </div>
                                        </tr>
                                        <tr>
                                            <td width="200px">__________________________</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                            <!-- <td width="200px">__________________________</td> -->
                        </tr>
                        <?php
                    }
                    ?>
                    <tr>
                        <td height="10px"></td>
                    </tr>
                    <tr>
                        <td style="line-height: 22px; text-decoration: underline;"><b>Provider:</b></td>
                    </tr>
                    <tr>
                        <td colspan="2" style="height: 3px"></td>
                    </tr>
                    <tr>
                        <td colspan="5">neshallWeb Private Limited</td>
                    </tr>
                    <tr>
                        <td colspan="2" style="height: 2px"></td>
                    </tr>
                    <tr>
                        <td colspan="5">
                            <table>
                                <tbody>
                                    <tr>
                                        <td width="180px">By: Mr. Anjalkumar Patel</td>
                                        <td width="200px">Date: {{date('d F Y', strtotime($estimate_date))}}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                        <!-- <td width="90px">By: Mr. Anjalkumar Patel</td>
                            <td width="90px">Date: {{date('d F Y', strtotime($estimate_date))}}</td> -->
                    </tr>
                    <tr>
                        <td height="50px"></td>
                    </tr>
                    <tr>
                        <td>
                            <table style="text-align: center; width: 100%;">
                                <tbody>
                                    <tr>
                                        <td><img style="width: 50px" src="{{URL::to('/')}}/assets/images/signture.png"></td>
                                    </tr>
                                    <tr>
                                        <td>--------------------------</td>
                                    </tr>
                                    <tr>
                                        <td><img style="width: 180px" src="{{URL::to('/')}}/assets/images/stamp.png"></td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5" style="text-align: right; font-size: 12px;color: #626262;">Recipient’s Initials: <input type="text" id="recipient-initials4" class="recipient-initials" name="recipient_initials4" placeholder="Enter Recipient’s Initials text"></td>
                    </tr>
                </tbody>
            </table>
            <table class="table" style="width: 650px; background: #f5f5f5; padding: 20px 15px; margin-top: 50px;">
                <thead>
                    <tr>
                        <td colspan="5" style="text-align:center;font-size: 12px; color: #626262; line-height: 20px;">
                            C/O PATEL BIPINBHAI DHIRUBHAI, ATAKPARDI, KUBER SIDHHI, Valsad, Gujrat, India, 396007
                        </td>
                    </tr>
                    <tr>
                        <th colspan="5" style="text-align: center; font-size: 12px;color: #626262;">
                            <img style="width: 12px;padding-right: 5px;" src="{{URL::to('/')}}/assets/images/call-icon.png">+91 6354425747
                            <img style="width: 12px; margin-left: 10px; padding-right: 5px;" src="{{URL::to('/')}}/assets/images/mail-icon.png">info@neshallweb.com
                            <img style="width: 12px;  margin-left: 10px;padding-right: 5px;" src="{{URL::to('/')}}/assets/images/url-icon.png">www.neshallweb.com
                        </th>
                    </tr>
                    <!-- <tr>
                        <th colspan="5" style="text-align: center; font-size: 12px;color: #626262;"><img style="width:12px;margin-right: 5px;" src="{{URL::to('/')}}/assets/images/mail-icon.png">info@neshallweb.com</th>
                    </tr>
                    <tr>
                        <th colspan="5" style="text-align: center; font-size: 12px;color: #626262;"><img style="width:12px;margin-right: 5px;" src="{{URL::to('/')}}/assets/images/url-icon.png">www.neshallweb.com</th>
                    </tr> -->
                </thead>
            </table>

            <!-- <table id="sing1-certificate" style="display:none;width: 650px; margin-top: 50px;">
                <thead>
                    <tr>
                        <td colspan="3" style="font-size: 24px; font-weight: bold;">Signature Certificate</td>
                    </tr>
                    <tr><td height="5"></td></tr>
                    <tr>
                        <td style="font-size: 14px;">
                            <table>
                                <tr>
                                    <td style="color: #818191;">Reference number:</td>
                                    <td>GDT8T-KR8YH-ZDDDW-N8XZJ</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <table style="width: 650px; margin-top: 40px; font-weight: bold;">
                                <tr style=" border-bottom: 2px solid #c1c1c1; display: table; width: 100%;">
                                    <td>Signer</td>
                                    <td>Timestamp</td>
                                    <td>Signature</td>
                                </tr>
                                <tr style="margin-top: 7px;">
                                    <td style="width: 270px;">
                                        <table cellpadding="5" style="width: 100%; background: rgb(255, 255, 255, 0.5);">
                                            <tr>
                                                <td>
                                                    <table style="font-size: 13px;">
                                                        <tr>
                                                            <td id="client-name-td" style="font-size: 20px; font-weight: bold;">Mohamed Ouedraogo</td>
                                                        </tr>
                                                        <tr><td style="font-size: 14px;">Email: {{$client_email}}</td></tr>
                                                        <tr><td height="10"></td></tr>
                                                        <tr><td></td></tr>
                                                        <tr><td>Sent:</td></tr>
                                                        <tr><td>Viewed: </td></tr>
                                                        <tr><td>Signed: </td></tr>
                                                    </table>
                                                </td>
                                                <td style="vertical-align: bottom; width: 200px;">
                                                    <table style="margin-top: 5px; font-size: 13px;">
                                                        <tr><td>20 Feb 2022 18:21:27 UTC</td></tr>
                                                        <tr><td>21 Feb 2022 14:12:32 UTC</td></tr>
                                                        <tr><td>21 Feb 2022 14:14:49 UTC</td></tr>
                                                    </table>
                                                </td>
                                                <td style="width: 280px;">
                                                    <table style="border: 2px solid #a9a9a9; width: 100%; display: table;">
                                                        <tr>
                                                            <td align="center"><img id="sing1-certificate-img" style="width: 70px" src="https://newcrm.neshallweb.com/assets/images/signture.png"></td>
                                                        </tr>
                                                    </table>
                                                    <table style="font-size: 13px;">
                                                        <tr><td>IP address: {{$ips}}</td></tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr><td height="10"></td></tr>
                                <tr>
                                    <td>
                                        <table>
                                            <tr><td style="color: #818191;">Document completed by all parties on:</td></tr>
                                            <tr><td>21 Feb 2022 14:14:49 UTC</td></tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </thead>
            </table> -->
                <!-- <footer style="height: 100px;">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6">
                                <p style="font-size: 12px; color: #626262; line-height: 20px;">C/O PATEL BIPINBHAI DHIRUBHAI, ATAKPARDI, KUBER SIDHHI, Valsad, Gujrat, India, 396007 </p>
                            </div>
                            <div class="col-md-6">
                                <div class="footer-address" style="font-size:12px;color: #626262;">
                                    <img style="width: 12px;padding-right: 5px;" src="{{URL::to('/')}}/assets/images/call-icon.png">+91 6354425747
                                    <img style="width: 12px; margin-left: 10px; padding-right: 5px;" src="{{URL::to('/')}}/assets/images/mail-icon.png">info@neshallweb.com
                                    <img style="width: 12px;  margin-left: 10px;padding-right: 5px;" src="{{URL::to('/')}}/assets/images/url-icon.png">www.neshallweb.com
                                </div>
                            </div>
                        </div>
                    </div>
                </footer> -->
                <input type="hidden" name="client_email" value="{{ $client_email }}">
            <button type="submit" class="btn-form-submit">Submit</button>
            </center>
        </form>
    </body>
    <script src="{{URL::to('/')}}/assets/js/bootstrap.bundle.min.js"></script>
    <!--plugins-->
    <script src="{{URL::to('/')}}/assets/js/jquery.min.js"></script>
    <script src="{{URL::to('/')}}/assets/js/jquery.validate.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.btn-form-submit').click(function(e){
                e.preventDefault();
                if ( $('#client-name').val() == '' ){
                    $('#client-name').focus();
                    alert("Please enter value for Client Name.");
                }
                else if ($('#client-address').val() == ''){
                    $('#client-address').focus();
                    alert("Please enter value for Client Address.");
                }
                /*else if ( $('#recipient-initials1').val() == '' ){
                    $('#recipient-initials1').focus();
                    alert("Please enter value for Your Initisals.");
                }
                else if ( $('#recipient-initials2').val() == '' ){
                    $('#recipient-initials2').focus();
                    alert("Please enter value for Your Initisals.");
                }
                else if ( $('#recipient-initials3').val() == '' ){
                    $('#recipient-initials3').focus();
                    alert("Please enter value for Your Initisals.");
                }*/
                else if ( $('#sign1').val() == '' ){
                    $('#sign1').focus();
                    alert("Please enter your Name.");
                }
                /*else if ( $('#recipient-initials4').val() == '' ){
                    $('#recipient-initials4').focus();
                    alert("Please enter value for Your Initisals.");
                }*/

                if ( $('#client-name').val() != '' &&  $('#client-address').val() != '' && /*$('#recipient-initials1').val() != '' && $('#recipient-initials2').val() != '' && $('#recipient-initials3').val() != '' && $('#recipient-initials4').val() != '' &&*/ $('#sign1').val() != '' ) {
                    $("#signature-form").submit();
                }
            });
            $('#client-name').change(function(){
                $('.client-name').html($(this).val());

            });

            $('#sign1').change(function(){
                $('#sign1-text').html($(this).val());
                $('#client-name-td').html($(this).val());
            });
            $('#sign1-font').change(function(){
                $('#sign1-text').css('display','block');
                $('#sign1-text').css('font-family',$(this).val());
            });

            $('#sign2').change(function(){
                $('#sign2-text').html($(this).val());
                $('#client-name-td').html($(this).val());
            });
            $('#sign2-font').change(function(){
                $('#sign2-text').css('display','block');
                $('#sign2-text').css('font-family',$(this).val());
            });

            $('#sign3').change(function(){
                $('#sign3-text').html($(this).val());
                $('#client-name-td').html($(this).val());
            });
            $('#sign3-font').change(function(){
                $('#sign3-text').css('display','block');
                $('#sign3-text').css('font-family',$(this).val());
            });

            $(".signature-3").hide();
            $(".signature-2").hide();
            $('#flexRadioDefault1').click(function(){
                $(".signature-3").hide();
                $(".signature-2").hide();
            });

            $('#flexRadioDefault2').click(function(){
                $(".signature-3").hide();
                $(".signature-2").show();
            });
            $('#flexRadioDefault3').click(function(){
                $(".signature-3").show();
                $(".signature-2").show();
            });
            $('.sign1-img').hide();
            $('.sign1-font').hide();
            $('#sign1_type').change(function(){
                if( $(this).val() == 'Image' ) {
                    $('.sign1-font').hide();
                    $('.sign1-img').show();
                } else if( $(this).val() == 'Font' ) {
                    $('.sign1-font').show();
                    $('.sign1-img').hide();
                }
            });
            $('.sign2-img').hide();
             $('.sign2-font').hide();
            $('#sign2_type').change(function(){
                if( $(this).val() == 'Image' ) {
                    $('.sign2-font').hide();
                    $('.sign2-img').show();
                } else if( $(this).val() == 'Font' ) {
                    $('.sign2-font').show();
                    $('.sign2-img').hide();
                }
            });
            $('.sign3-img').hide();
            $('.sign3-font').hide();
            $('#sign3_type').change(function(){
                if( $(this).val() == 'Image' ) {
                    $('.sign3-font').hide();
                    $('.sign3-img').show();
                } else if( $(this).val() == 'Font' ) {
                    $('.sign3-font').show();
                    $('.sign3-img').hide();
                }
            });
            if (window.File && window.FileList && window.FileReader) {
                $("#files").on("change", function(e) {
                  var files = e.target.files,
                    filesLength = files.length;
                  for (var i = 0; i < filesLength; i++) {
                    var f = files[i]
                    var fileReader = new FileReader();
                    fileReader.onload = (function(e) {
                      var file = e.target;
                      $("<span class=\"pip\">" +
                        "<img class=\"imageThumb\" src=\"" + e.target.result + "\" title=\"" + file.name + "\"/>" +
                        "<br/><span class=\"remove\">Remove image</span>" +
                        "</span>").insertAfter("#files");
                        document.getElementById("sing1-certificate").style.display = "block";
                        const img = document.getElementById("sing1-certificate-img");
                        img.src = e.target.result;
                      $(".remove").click(function(){
                        $(this).parent(".pip").remove();
                      });
                      
                      // Old code here
                      /*$("<img></img>", {
                        class: "imageThumb",
                        src: e.target.result,
                        title: file.name + " | Click to remove"
                      }).insertAfter("#files").click(function(){$(this).remove();});*/
                      
                    });
                    fileReader.readAsDataURL(f);
                  }
                  console.log(files);
                });
              } 
            $(".recipient-initials").focus();
            $(".recipient-initials").blur(function(){
                var name=$('.recipient-initials').val();
                if(name.length == 0){
                    $('.recipient-initials').parent().find('.red').remove();
                    $('.recipient-initials').after('<div class="red">This field is Required</div>');
                }
                else {
                    $('.recipient-initials').next(".red").remove(); // *** this line have been added ***
                    return true;
                }
            });
            $(".client-address").focus();
            $(".client-address").blur(function(){
                var name=$('.client-address').val();
                if(name.length == 0){
                    $('.client-address').parent().find('.red').remove();
                    $('.client-address').after('<div class="red">This field is Required</div>');
                }
                else {
                    $('.client-address').next(".red").remove(); // *** this line have been added ***
                    return true;
                }
            });
            $(".sign1").focus();
            $(".sign1").blur(function(){
                var name=$('.sign1').val();
                if(name.length == 0){
                    $('.sign1').parent().find('.red').remove();
                    $('.sign1').after('<div class="red">This field is Required</div>');
                }
                else {
                    $('.sign1').next(".red").remove(); // *** this line have been added ***
                    return true;
                }
            });

        });
    </script>
</html>
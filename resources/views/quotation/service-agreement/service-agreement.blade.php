@extends('layouts.index')
@section('title')
            Service Agreement
@endsection()
@section('content')
    <style type="text/css">
        input[type="file"] {
            display: block;
        }
        .imageThumb {
            max-height: 75px;
            border: 2px solid;
            padding: 1px;
            cursor: pointer;
        }
        .pip {
            display: inline-block;
            margin: 10px 10px 0 0;
        }
        .remove {
            display: block;
            background: #444;
            border: 1px solid black;
            color: white;
            text-align: center;
            cursor: pointer;
        }
        .remove:hover {
            background: white;
            color: black;
        }
        .side-menu{
            position: fixed !important;
        }

    </style>
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
        <div class="breadcrumb-title pe-3">Add New Service Agreement</div>
        <div class="ps-3">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb mb-0 p-0">
                    <li class="breadcrumb-item"><a href="javascript:void(0);"><i class="bx bx-home-alt"></i></a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">Service Agreement</li>
                </ol>
            </nav>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-9 mx-auto">
            <hr/>
            <div class="card">
                <div class="card-body">
                    <div class="p-4 border rounded">
                        <form method="post" action="{{ route('service-agreement-store') }}" class="row g-3 needs-validation" enctype="multipart/form-data"  novalidate>
                            @csrf
                            <div class="col-md-12">
                                <label for="inputState" class="form-label">Total Number of Signatures</label>
                                <div class="col">
                                    <input class="form-check-input" value="1" type="radio" name="flexRadioDefault" id="flexRadioDefault1" checked>
                                    <label class="form-check-label" for="flexRadioDefault1">1</label>
                                </div>
                                <div class="col">
                                    <input class="form-check-input" value="2" type="radio" name="flexRadioDefault" id="flexRadioDefault2">
                                    <label class="form-check-label" for="flexRadioDefault2">2</label>
                                </div>
                                <div class="col">
                                    <input class="form-check-input" value="3" type="radio" name="flexRadioDefault" id="flexRadioDefault3">
                                    <label class="form-check-label" for="flexRadioDefault3">3</label>
                                </div>
                            </div>
                            <input type="hidden" name="estimate_ids" value="{{$estimate_ids}}">
                            <div class="col-md-12">
                                <label for="validationCustom01" class="form-label">Estimate No.</label>
                                <input type="text" class="form-control" id="validationCustom01" readonly value="{{$estimate_numbers}}" name="estimate_no" placeholder="Estimate No">
                                <div class="valid-feedback">Looks good!</div>
                            </div>
                            <div class="col-md-3">
                                <label for="inputState" class="form-label">Name Title</label>
                                <select id="inputState" name="sign1_nametitle" class="form-select">
                                    <option value="Mr." selected>Mr.</option>
                                    <option value="Mrs.">Mrs.</option>
                                    <option value="Miss.">Miss.</option>
                                </select>
                            </div>
                            <div class="col-md-9">
                                <label for="validationCustom022" class="form-label">Name</label>
                                <input type="text" class="form-control" id="validationCustom022" value="" name="sign1" placeholder="Name" >
                                <div class="valid-feedback">Looks good!</div>
                            </div>
                            <div class="col-md-12">
                                <label for="inputState" class="form-label">Signature Type</label>
                                <select id="sign1_type" name="sign1_type" class="form-select">
                                    <option value="" selected>Choose...</option>
                                    <option value="Image">Image</option>
                                    <option value="Font">Font</option>
                                </select>
                            </div>
                            <div class="col-md-12 sign1-font">
                                <label for="inputState" class="form-label">Fonts</label>
                                <select id="inputState" name="sign1_font" class="form-select">
                                    <option value="" selected>Choose...</option>
                                    <option style="font-family:'Arial' ;" value="Arial"> Arial </option>
                                    <option style="font-family:'Verdana';" value="Verdana"> Verdana </option>
                                    <option style="font-family:'Helvetica';" value="Helvetica"> Helvetica </option>
                                    <option style="font-family:'Tahoma';" value="Tahoma"> Tahoma </option>
                                    <option style="font-family:'Trebuchet MS';" value="Trebuchet MS"> Trebuchet MS </option>
                                    <option style="font-family:'Times New Roman';" value="Times New Roman"> Times New Roman </option>
                                    <option style="font-family:'Georgia';" value="Georgia"> Georgia </option>
                                    <option style="font-family:'Garamond';" value="Garamond"> Garamond </option>
                                    <option style="font-family:'Courier New';" value="Courier New"> Courier New </option>
                                    <option style="font-family:'Brush Script MT';" value="Brush Script MT"> Brush Script MT </option>
                                    <option style="font-family:'Cursive' ;" value="Cursive"> Cursive </option>
                                    <option style="font-family:'Serif' ;" value="Serif"> Serif </option>
                                    <option style="font-family:'Sans-serif' ;" value="Sans-serif"> Sans-serif </option>
                                </select>
                            </div>
                            <div class="col-md-12">
                                <label for="validationCustom02" class="form-label">Designation</label>
                                <input type="text" class="form-control" id="validationCustom02" value="" name="sign1_designation" placeholder="Designation" >
                                <div class="valid-feedback">Looks good!</div>
                            </div>
                            <div class="col-md-12 sign1-img">
                                <label for="validationCustom04" class="form-label">Signature Image</label>
                                <input type="file" name="sign1_img" class="custom-file-input" id="files">
                                <div class="valid-feedback">Looks good!</div>
                            </div>
                            <div class="signature-2">
                                <hr>
                                <div class="row">
                                    <div class="col-md-3">
                                        <label for="inputState" class="form-label">Name Title</label>
                                        <select id="inputState" name="sign2_nametitle" class="form-select">
                                            <option value="Mr." selected>Mr.</option>
                                            <option value="Mrs.">Mrs.</option>
                                            <option value="Miss.">Miss.</option>
                                        </select>
                                    </div>
                                    <div class="col-md-9">
                                        <label for="validationCustom033" class="form-label">Name</label>
                                        <input type="text" class="form-control" id="validationCustom033" value="" name="sign2" placeholder="Name" >
                                        <div class="valid-feedback">Looks good!</div>
                                    </div>
                                    <div class="col-md-12">
                                        <label for="inputState" class="form-label">Signature Type</label>
                                        <select id="sign2_type" name="sign2_type" class="form-select">
                                            <option value="" selected>Choose...</option>
                                            <option value="Image">Image</option>
                                            <option value="Font">Font</option>
                                        </select>
                                    </div>
                                    <div class="col-md-12 sign2-font">
                                        <label for="inputState" class="form-label">Fonts</label>
                                        <select id="inputState" name="sign2_font" class="form-select">
                                            <option value="" selected>Choose...</option>
                                            <option style="font-family:'Arial' ;" value="Arial"> Arial </option>
                                            <option style="font-family:'Verdana';" value="Verdana"> Verdana </option>
                                            <option style="font-family:'Helvetica';" value="Helvetica"> Helvetica </option>
                                            <option style="font-family:'Tahoma';" value="Tahoma"> Tahoma </option>
                                            <option style="font-family:'Trebuchet MS';" value="Trebuchet MS"> Trebuchet MS </option>
                                            <option style="font-family:'Times New Roman';" value="Times New Roman"> Times New Roman </option>
                                            <option style="font-family:'Georgia';" value="Georgia"> Georgia </option>
                                            <option style="font-family:'Garamond';" value="Garamond"> Garamond </option>
                                            <option style="font-family:'Courier New';" value="Courier New"> Courier New </option>
                                            <option style="font-family:'Brush Script MT';" value="Brush Script MT"> Brush Script MT </option>
                                            <option style="font-family:'Cursive' ;" value="Cursive"> Cursive </option>
                                            <option style="font-family:'Serif' ;" value="Serif"> Serif </option>
                                            <option style="font-family:'Sans-serif' ;" value="Sans-serif"> Sans-serif </option>
                                        </select>
                                    </div>
                                    <div class="col-md-12">
                                        <label for="validationCustom02" class="form-label">Designation</label>
                                        <input type="text" class="form-control" id="validationCustom02" value="" name="sign2_designation" placeholder="Designation" >
                                        <div class="valid-feedback">Looks good!</div>
                                    </div>
                                    <div class="col-md-12 sign2-img">
                                        <label for="validationCustom04" class="form-label">Signature Image</label>
                                        <input type="file" name="sign2_img" class="custom-file-input" id="files">
                                        <div class="valid-feedback">Looks good!</div>
                                    </div>
                                </div>
                            </div>
                            <div class="signature-3">
                                <hr>
                                <div class="row">
                                    <div class="col-md-3">
                                        <label for="inputState" class="form-label">Name Title</label>
                                        <select id="inputState" name="sign2_nametitle" class="form-select">
                                            <option value="Mr." selected>Mr.</option>
                                            <option value="Mrs.">Mrs.</option>
                                            <option value="Miss.">Miss.</option>
                                        </select>
                                    </div>
                                    <div class="col-md-9">
                                        <label for="validationCustom044" class="form-label">Name</label>
                                        <input type="text" class="form-control" id="validationCustom044" value="" name="sign3" placeholder="Name" >
                                        <div class="valid-feedback">Looks good!</div>
                                    </div>
                                    <div class="col-md-12">
                                        <label for="inputState" class="form-label">Signature Type</label>
                                        <select id="sign3_type" name="sign3_type" class="form-select">
                                            <option value="" selected>Choose...</option>
                                            <option value="Image">Image</option>
                                            <option value="Font">Font</option>
                                        </select>
                                    </div>
                                    <div class="col-md-12 sign3-font">
                                        <label for="inputState" class="form-label">Fonts</label>
                                        <select id="sign3_font" name="sign3_font" class="form-select">
                                            <option value="" selected>Choose...</option>
                                            <option style="font-family:'Arial' ;" value="Arial"> Arial </option>
                                            <option style="font-family:'Verdana';" value="Verdana"> Verdana </option>
                                            <option style="font-family:'Helvetica';" value="Helvetica"> Helvetica </option>
                                            <option style="font-family:'Tahoma';" value="Tahoma"> Tahoma </option>
                                            <option style="font-family:'Trebuchet MS';" value="Trebuchet MS"> Trebuchet MS </option>
                                            <option style="font-family:'Times New Roman';" value="Times New Roman"> Times New Roman </option>
                                            <option style="font-family:'Georgia';" value="Georgia"> Georgia </option>
                                            <option style="font-family:'Garamond';" value="Garamond"> Garamond </option>
                                            <option style="font-family:'Courier New';" value="Courier New"> Courier New </option>
                                            <option style="font-family:'Brush Script MT';" value="Brush Script MT"> Brush Script MT </option>
                                            <option style="font-family:'Cursive' ;" value="Cursive"> Cursive </option>
                                            <option style="font-family:'Serif' ;" value="Serif"> Serif </option>
                                            <option style="font-family:'Sans-serif' ;" value="Sans-serif"> Sans-serif </option>
                                        </select>
                                    </div>
                                    <div class="col-md-12">
                                        <label for="validationCustom02" class="form-label">Designation</label>
                                        <input type="text" class="form-control" id="validationCustom02" value="" name="sign3_designation" placeholder="Designation" >
                                        <div class="valid-feedback">Looks good!</div>
                                    </div>
                                    <div class="col-md-12 sign3-img">
                                        <label for="validationCustom04" class="form-label">Signature Image</label>
                                        <input type="file" name="sign3_img" class="custom-file-input" id="files">
                                        <div class="valid-feedback">Looks good!</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <button class="btn btn-light" type="submit">Submit form</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection()
@section('loadstyles')
    <!-- <link href="{{URL::to('/')}}/assets/plugins/fancy-file-uploader/fancy_fileupload.css" rel="stylesheet" />
    <link href="{{URL::to('/')}}/assets/plugins/Drag-And-Drop/dist/imageuploadify.min.css" rel="stylesheet" />
    <link href="{{URL::to('/')}}/assets/plugins/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet" /> -->
@endsection()
@section('loadscript')
    <script src="{{URL::to('/')}}/assets/plugins/simplebar/js/simplebar.min.js"></script>
    <script src="{{URL::to('/')}}/assets/plugins/metismenu/js/metisMenu.min.js"></script>
    <script src="{{URL::to('/')}}/assets/plugins/perfect-scrollbar/js/perfect-scrollbar.js"></script>
    <!-- <script src="{{URL::to('/')}}/assets/plugins/fancy-file-uploader/jquery.ui.widget.js"></script>
    <script src="{{URL::to('/')}}/assets/plugins/fancy-file-uploader/jquery.fileupload.js"></script>
    <script src="{{URL::to('/')}}/assets/plugins/fancy-file-uploader/jquery.iframe-transport.js"></script>
    <script src="{{URL::to('/')}}/assets/plugins/fancy-file-uploader/jquery.fancy-fileupload.js"></script>
    <script src="{{URL::to('/')}}/assets/plugins/Drag-And-Drop/dist/imageuploadify.min.js"></script> -->
@endsection()
@section('scripts')
$(".signature-3").hide();
$(".signature-2").hide();
$('#flexRadioDefault1').click(function(){
    $(".signature-3").hide();
    $(".signature-2").hide();
});

$('#flexRadioDefault2').click(function(){
    $(".signature-3").hide();
    $(".signature-2").show();
});
$('#flexRadioDefault3').click(function(){
    $(".signature-3").show();
    $(".signature-2").show();
});
$('.sign1-img').hide();
$('.sign1-font').hide();
$('#sign1_type').change(function(){
    if( $(this).val() == 'Image' ) {
        $('.sign1-font').hide();
        $('.sign1-img').show();
    } else if( $(this).val() == 'Font' ) {
        $('.sign1-font').show();
        $('.sign1-img').hide();
    }
});
$('.sign2-img').hide();
 $('.sign2-font').hide();
$('#sign2_type').change(function(){
    if( $(this).val() == 'Image' ) {
        $('.sign2-font').hide();
        $('.sign2-img').show();
    } else if( $(this).val() == 'Font' ) {
        $('.sign2-font').show();
        $('.sign2-img').hide();
    }
});
$('.sign3-img').hide();
$('.sign3-font').hide();
$('#sign3_type').change(function(){
    if( $(this).val() == 'Image' ) {
        $('.sign3-font').hide();
        $('.sign3-img').show();
    } else if( $(this).val() == 'Font' ) {
        $('.sign3-font').show();
        $('.sign3-img').hide();
    }
});
if (window.File && window.FileList && window.FileReader) {
    $("#files").on("change", function(e) {
      var files = e.target.files,
        filesLength = files.length;
      for (var i = 0; i < filesLength; i++) {
        var f = files[i]
        var fileReader = new FileReader();
        fileReader.onload = (function(e) {
          var file = e.target;
          $("<span class=\"pip\">" +
            "<img class=\"imageThumb\" src=\"" + e.target.result + "\" title=\"" + file.name + "\"/>" +
            "<br/><span class=\"remove\">Remove image</span>" +
            "</span>").insertAfter("#files");
          $(".remove").click(function(){
            $(this).parent(".pip").remove();
          });
          
          // Old code here
          /*$("<img></img>", {
            class: "imageThumb",
            src: e.target.result,
            title: file.name + " | Click to remove"
          }).insertAfter("#files").click(function(){$(this).remove();});*/
          
        });
        fileReader.readAsDataURL(f);
      }
      console.log(files);
    });
  } 
var forms = document.querySelectorAll('.needs-validation')
// Loop over them and prevent submission
Array.prototype.slice.call(forms)
.forEach(function (form) {
  form.addEventListener('submit', function (event) {
    if (!form.checkValidity()) {
      event.preventDefault()
      event.stopPropagation()
    }
    form.classList.add('was-validated')
  }, false)
})
@endsection()
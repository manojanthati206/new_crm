@extends('layouts.index')

@section('title')
            Main Category
@endsection()

@section('content')
    <?php $i = 0; ?>
    <!-- Content Header (Page header) -->


    <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
        <div class="breadcrumb-title pe-3">All Main Categories</div>
        <div class="ps-3">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb mb-0 p-0">
                    <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">Main Category</li>
                </ol>
            </nav>
        </div>
        <div class="ms-auto">
            <div class="btn-group">
                <a href="{{ route('terms-and-condition-add') }}">
                    <button type="button" class="btn btn-light">Add New</button>
                </a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-9 mx-auto">
            <div class="card">
                <div class="card-body">
                    <table class="table mb-0">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col" colspan="2">Action</th>
                                <th scope="col">Content</th>
                                <th scope="col">Main Category</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($all_terms as $all_term)
                                <tr>
                                    <td scope="row">{{ ++$i }}</td>
                                    <td>
                                        <a href="{{route('terms-and-condition-edit', $all_term->id)}}">
                                            Edit
                                        </a>
                                    </td>
                                    <td>
                                        <a onclick="return confirm('Are you sure?')" href="{{route('terms-and-condition-delete', $all_term->id)}}">
                                            Delete
                                        </a>
                                    </td>
                                    <td>
                                        {{ $all_term->content }}
                                    </td>
                                    <td>
                                        @if ($all_term->service_id == '1')
                                            Web Development
                                        @endif
                                        @if ($all_term->service_id == '2')
                                            Graphics
                                        @endif
                                        @if ($all_term->service_id == '3')
                                            Video Service
                                        @endif
                                    </td>
                                </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
      <!-- /.content-header -->
    <?php $i = 0;?>

    <!--!! $projects->links() !!-->

@endsection()

@section('scripts')

$("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');

@endsection()


@extends('layouts.index')

@section('title')
            Main Category
@endsection()

@section('content')

    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
        <div class="breadcrumb-title pe-3">Edit Main Category</div>
        <div class="ps-3">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb mb-0 p-0">
                    <li class="breadcrumb-item"><a href="javascript:void(0);"><i class="bx bx-home-alt"></i></a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">Main Category</li>
                </ol>
            </nav>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-9 mx-auto">
            <hr/>
            <div class="card">
                <div class="card-body">
                    <div class="p-4 border rounded">
                        <form method="post" action="{{route('terms-and-condition-update')}}" class="row g-3 needs-validation" enctype="multipart/form-data"  novalidate>
                            @csrf
                            @method('PUT')
                            <input type="hidden" name="id" value="{{$all_term->id}}">
                            <div class="col-md-12">
                                <label for="validationCustom01" class="form-label">Content</label>
                                <textarea class="form-control" id="validationCustom01" value="" name="term_content" placeholder="Content" required>{{$all_term->content}}</textarea>
                                <div class="valid-feedback">Looks good!</div>
                            </div>
                            <div class="col-md-12">
                                <label for="inputState" class="form-label">Service</label>
                                <select id="inputState" name="service_id" class="form-select">
                                    <option selected>Choose...</option>
                                    <option @if ($all_term->service_id == '1')
                                            selected
                                            @endif
                                            value="1">Web Development</option>
                                    <option @if ($all_term->service_id == '2')
                                            selected
                                            @endif
                                            value="2">Graphics</option>
                                    <option @if ($all_term->service_id == '3')
                                            selected
                                            @endif
                                            value="3">Video Services</option>
                                </select>
                            </div>
                            <div class="col-12">
                                <button class="btn btn-light" type="submit">Submit form</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection()
@section('loadstyles')
    <!-- <link href="{{URL::to('/')}}/assets/plugins/fancy-file-uploader/fancy_fileupload.css" rel="stylesheet" />
    <link href="{{URL::to('/')}}/assets/plugins/Drag-And-Drop/dist/imageuploadify.min.css" rel="stylesheet" />
    <link href="{{URL::to('/')}}/assets/plugins/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet" /> -->
@endsection()
@section('loadscript')
    <script src="{{URL::to('/')}}/assets/plugins/simplebar/js/simplebar.min.js"></script>
    <script src="{{URL::to('/')}}/assets/plugins/metismenu/js/metisMenu.min.js"></script>
    <script src="{{URL::to('/')}}/assets/plugins/perfect-scrollbar/js/perfect-scrollbar.js"></script>
    <!-- <script src="{{URL::to('/')}}/assets/plugins/fancy-file-uploader/jquery.ui.widget.js"></script>
    <script src="{{URL::to('/')}}/assets/plugins/fancy-file-uploader/jquery.fileupload.js"></script>
    <script src="{{URL::to('/')}}/assets/plugins/fancy-file-uploader/jquery.iframe-transport.js"></script>
    <script src="{{URL::to('/')}}/assets/plugins/fancy-file-uploader/jquery.fancy-fileupload.js"></script>
    <script src="{{URL::to('/')}}/assets/plugins/Drag-And-Drop/dist/imageuploadify.min.js"></script> -->
@endsection()
@section('scripts')
var forms = document.querySelectorAll('.needs-validation')

// Loop over them and prevent submission
Array.prototype.slice.call(forms)
.forEach(function (form) {
  form.addEventListener('submit', function (event) {
    if (!form.checkValidity()) {
      event.preventDefault()
      event.stopPropagation()
    }

    form.classList.add('was-validated')
  }, false)
})
<!-- $('#fancy-file-upload').FancyFileUpload({
    params: {
        action: 'fileuploader'
    },
    maxfilesize: 1000000
});
$('#image-uploadify').imageuploadify(); -->

@endsection()

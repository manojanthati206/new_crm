@extends('layouts.index')
@section('title')
    Features
@endsection()
@section('content')
    <?php $i = 0; ?>
    <!-- Content Header (Page header) -->
    <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
        <div class="breadcrumb-title pe-3">All Features</div>
        <div class="ps-3">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb mb-0 p-0">
                    <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">Features</li>
                </ol>
            </nav>
        </div>
        <div class="ms-auto">
            <div class="btn-group">
                <a href="{{ route('features-add') }}">
                    <button type="button" class="btn btn-light">Add New</button>
                </a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-9 mx-auto">
            <div class="card">
                <div class="card-body">
                    <table class="table mb-0">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col" colspan="2">Action</th>
                                <th scope="col">Title</th>
                                <th scope="col">Service</th>
                                <th scope="col">Main Category</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($features as $feature)
                                <tr>
                                    <td scope="row">{{ ++$i }}</td>
                                    <td>
                                        <a href="{{route('features-edit', $feature->id)}}">
                                            Edit
                                        </a>
                                    </td>
                                    <td>
                                        <a onclick="return confirm('Are you sure?')" href="{{route('features-delete', $feature->id)}}">
                                            Delete
                                        </a>
                                    </td>
                                    <td>
                                        {{ $feature->feature_title }}
                                    </td>
                                    <td>
                                        @if ($feature->service_id == '1')
                                            Web Development
                                        @endif
                                        @if ($feature->service_id == '2')
                                            Graphics
                                        @endif
                                        @if ($feature->service_id == '3')
                                            Video Service
                                        @endif
                                        @if ($feature->service_id == '4')
                                            Social Media
                                        @endif
                                        @if ($feature->service_id == '5')
                                            Mobile Application
                                        @endif
                                        @if ($feature->service_id == '6')
                                            Digital Marketing
                                        @endif
                                    </td>
                                    <td>
                                        @foreach ($main_cats as $main_cat)
                                            @if ( isset($feature->main_category_id) && isset( $main_cat->id ) )
                                                @if ($feature->main_category_id == $main_cat->id )
                                                    {{ $main_cat->title }}
                                                @endif
                                            @endif
                                        @endforeach
                                    </td>
                                </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
      <!-- /.content-header -->
    <?php $i = 0;?>
    <!--!! $projects->links() !!-->
@endsection()
@section('scripts')
@endsection()
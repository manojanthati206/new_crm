@extends('layouts.index')

@section('title')
    Services
@endsection()

@section('content')
    <?php $i = 0; ?>
    <!-- Content Header (Page header) -->


    <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
        <div class="breadcrumb-title pe-3">All Services</div>
        <div class="ps-3">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb mb-0 p-0">
                    <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">Services</li>
                </ol>
            </nav>
        </div>
        <div class="ms-auto">
            <div class="btn-group">
                <a href="{{ route('mainservices-add') }}">
                    <button type="button" class="btn btn-light">Add New</button>
                </a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12 mx-auto">
            <div class="card">
                <div class="card-body table-responsive">
                    <table class="table mb-0 text-center">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col" >Action</th>
                                <th scope="col">Name</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                            @foreach ($services as $service)
                                <tr>
                                    <td scope="row">{{ ++$i }}</td>
                                    <td>
                                        <a href="{{route('mainservices-edit', $service->id)}}">
                                            Edit
                                        </a> ||
                                        <a onclick="return confirm('Are you sure?')" href="{{route('mainservices-delete', $service->id)}}">
                                            Delete
                                        </a>
                                    </td>
                                    <td>
                                        {{ $service->name  }}
                                    </td>
                                    
                                </tr>
                            @endforeach
                       
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
      <!-- /.content-header -->
    <?php $i = 0;?>

    <!--!! $projects->links() !!-->

@endsection()

@section('scripts')

$("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');

@endsection()

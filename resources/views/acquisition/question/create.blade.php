@extends('layouts.index')

@section('title')
    Question  Create
@endsection()

@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">

        <div class="breadcrumb-title pe-3">Add New Question</div>

        <div class="ps-3">

            <nav aria-label="breadcrumb">

                <ol class="breadcrumb mb-0 p-0">

                    <li class="breadcrumb-item"><a href="javascript:void(0);"><i class="bx bx-home-alt"></i></a>

                    </li>

                    <li class="breadcrumb-item " aria-current="page"><a href="{{ route('questions-index') }}">Question</a></li>

                    <li class="breadcrumb-item active" aria-current="page">Question  Add</li>

                </ol>

            </nav>

        </div>

    </div>

    <div class="row">

        <div class="col-xl-9 mx-auto">

            <hr/>

            <div class="card">

                <div class="card-body">

                    <div class="p-4 border rounded">

                    <form method="post" action="{{ route('questions-store') }}" class="row g-3 needs-validation" enctype="multipart/form-data"  novalidate>

                            @csrf

                            <div class="col-md-12 mt-2">

                                <label for="validationCustom01" class="form-label">Question *</label>

                                <input type="text" class="form-control" id="validationCustom01" value="{{  old('question') }}" name="question" placeholder="Enter Question" required>

                                <div class="valid-feedback">Looks good!</div>

                            </div>

                            <div class="col-md-12 mt-2">

                                <label for="validationCustom01" class="form-label">Service*</label>

                                <select class="single-select form-select"  id="validationCustom01"  name="serviceid">

                                    @foreach($mainservices as $parentservice)

                                        <option value="{{  $parentservice->id }}">{{  $parentservice->name }}</option>

                                    @endforeach

                                </select>

                                <div class="valid-feedback">Looks good!</div>

                            </div>

                            <div class="col-md-12 mt-3">
                                <label for="validationCustom01" class="form-label">Type*</label>
                                <select class=" form-select "  id="validationCustom01"  name="type" >
                                    <option value="1">Text</option>
									<option value="2">File</option>
									<option value="3">Text & File Both</option>
                                    <option value="4">Radio button</option>
                                    <option value="5">Text Area</option>
                                    <option value="6">Page Section</option>
                                    <option value="7">Reference Link</option>
                                </select>
                                <div class="valid-feedback">Looks good!</div> 
                            </div>
                            <div class="col-md-12 mt-3" id="options" style="display:none">
                                <label for="validationCustom01" class="form-label">Options</label>
                                <textarea class="form-control"  id="validationCustom01" cols="40" rows="5" value="" name="options" wrap=physical></textarea>
                                <div class="mt-2">Note: Entry separate by , . Example: Male,Female</div>
                            </div>

                            <div class="col-md-12 mt-2">
                                <label for="validationCustom01" class="form-label">Image </label>
                                <input type="file" class="form-control" id="validationCustom01" value="{{  old('images') }}" name="images[]"  multiple>
                                <div class="valid-feedback">Looks good!</div>
                            </div>

                            <div class="col-12">
                                <button class="btn btn-light" type="submit">Submit </button>
                            </div>

                        </form>                    
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection()

@section('loadstyles')
    <link href="{{ asset('assets/plugins/smart-wizard/css/smart_wizard_all.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/datetimepicker/css/classic.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/plugins/smart-wizard/css/smart_wizard_all.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/datetimepicker/css/classic.date.css') }}" rel="stylesheet" />
	<link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.min.css') }}">
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <!-- <link href="{{URL::to('/')}}/assets/plugins/fancy-file-uploader/fancy_fileupload.css" rel="stylesheet" />
    <link href="{{URL::to('/')}}/assets/plugins/Drag-And-Drop/dist/imageuploadify.min.css" rel="stylesheet" />
    <link href="{{URL::to('/')}}/assets/plugins/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet" /> -->
@endsection()

@section('loadscript') 

<!-- <script src="{{ asset('assets/plugins/select2/js/select2.min.js') }}"></script> -->

    <!-- <script src="{{URL::to('/')}}/assets/plugins/fancy-file-uploader/jquery.ui.widget.js"></script>

    <script src="{{URL::to('/')}}/assets/plugins/fancy-file-uploader/jquery.fileupload.js"></script>

    <script src="{{URL::to('/')}}/assets/plugins/fancy-file-uploader/jquery.iframe-transport.js"></script>

    <script src="{{URL::to('/')}}/assets/plugins/fancy-file-uploader/jquery.fancy-fileupload.js"></script>

    <script src="{{URL::to('/')}}/assets/plugins/Drag-And-Drop/dist/imageuploadify.min.js"></script> -->

@endsection()

@section('scripts')



    var forms = document.querySelectorAll('.needs-validation')



    // Loop over them and prevent submission

    Array.prototype.slice.call(forms)

    .forEach(function (form) {

    form.addEventListener('submit', function (event) {

        if (!form.checkValidity()) {

        event.preventDefault()

        event.stopPropagation()

        }



        form.classList.add('was-validated')

    }, false)

    })
/*
     $('#fancy-file-upload').FancyFileUpload({

        params: {

            action: 'fileuploader'

        },

        maxfilesize: 1000000

    });

    $('#image-uploadify').imageuploadify();
*/


    $('.single-select').select2({

        theme: 'bootstrap4',

        width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style',

        placeholder: $(this).data('placeholder'),

        allowClear: Boolean($(this).data('allow-clear')),

    });

    // option show
        let option = jQuery('.type-select').val();
        if(option == 4){
            jQuery("#options").show();
        }else{
            jQuery("#options").hide();
        }
        
    jQuery('.type-select').change(function(event){
        event.preventDefault();
        
        let option = jQuery('.type-select').val();
        if(option == 4){
            jQuery("#options").show();
        }else{
            jQuery("#options").hide();
        }
    });
@endsection
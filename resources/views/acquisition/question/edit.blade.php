@extends('layouts.index')

@section('title')
            Question Edit
@endsection()


@section('content')

    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
        <div class="breadcrumb-title pe-3">Edit Question</div>
        <div class="ps-3">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb mb-0 p-0">
                    <li class="breadcrumb-item"><a href="javascript:void(0);"><i class="bx bx-home-alt"></i></a>
                    </li>
                    <li class="breadcrumb-item " aria-current="page"><a href="{{ route('questions-index') }}">Question</a> </li>
                    <li class="breadcrumb-item active" aria-current="page">Question Edit</li>
                </ol>
            </nav>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-9 mx-auto">
            <hr/>
            <div class="card">
                <div class="card-body">
                    <div class="p-4 border rounded">
                        <form method="post" action="{{ route('questions-update') }}" class="row g-3 needs-validation" enctype="multipart/form-data"  novalidate>
                            @csrf
                            @method('PUT')
                            <input type="hidden" name="id" value="{{$question->id}}">
                            <div class="col-md-12 mt-2">
                                <label for="validationCustom01" class="form-label">Question *</label>
                                <input type="text" class="form-control" id="validationCustom01" value="{{  $question->question }}" name="question" placeholder="Question " required>
                                <div class="valid-feedback">Looks good!</div>
                            </div>
                            <div class="col-md-12 mt-3">
                                <label for="validationCustom01" class="form-label">Service*</label>
                                <select class="single-select form-select "  id="validationCustom01"  name="serviceid">
                                    @foreach($mainservices as $parentservice)
                                        <option value="{{  $parentservice->id }}" {{ $question->serviceid == $parentservice->id ? 'selected="selected"' : ''}}>{{  $parentservice->name }}</option>
                                    @endforeach
                                </select>
                                <div class="valid-feedback">Looks good!</div>
                            </div>
							<div class="col-md-12 mt-3">
                                <label for="validationCustom01" class="form-label">Type*</label>
                                <select class="single-select form-select type-select"  id="validationCustom01"  name="type" required>
                                    <option value="1" {{  ($question->type == 1)? 'selected=selected': '' }}>Text</option>
									<option value="2" {{  ($question->type == 2)? 'selected=selected': '' }}>File</option>
									<option value="3" {{  ($question->type == 3)? 'selected=selected': '' }}>Text & File Both</option>
                                    <option value="4" {{  ($question->type == 4)? 'selected=selected': '' }}>Radio button</option>
                                    <option value="5" {{  ($question->type == 5)? 'selected=selected': '' }}>Text Area</option>
                                    <option value="6" {{  ($question->type == 6)? 'selected=selected': '' }}>Page section</option>
                                    <option value="7" {{  ($question->type == 7)? 'selected=selected': '' }}>Reference Link</option>
                                </select>
                                <div class="valid-feedback">Looks good!</div> 
                            </div>
                            <div class="col-md-12 mt-3" id="options" style="display:none">
                                <label for="validationCustom01" class="form-label">Options</label>
                                <textarea class="form-control"  id="validationCustom01" cols="40" rows="5"   name="options" wrap=physical>{{$question->options}}</textarea>
                                <div class="mt-2">Note: Entry separate by "|" Example: Male|Female</div>
                            </div>
                            <div class="col-md-12 mt-2">
                                <label for="validationCustom01" class="form-label">Image </label>
                                <input type="file" class="form-control" id="validationCustom01" value="{{  $question->images }}" name="images[]" multiple >
                                <input type="hidden" id="validationCustom01" value="{{  $question->images }}" name="oldimage"  >
								
                                <div class="valid-feedback">Looks good!</div>
                            </div>
                            <div class="col-12">
                                <button class="btn btn-light" type="submit">Submit </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection()
@section('loadstyles')
<link href="{{ asset('assets/plugins/select2/css/select2.min.css') }}" rel="stylesheet" />
	<link href="{{ asset('assets/plugins/select2/css/select2-bootstrap4.css') }}" rel="stylesheet" />
    <!-- <link href="{{URL::to('/')}}/assets/plugins/fancy-file-uploader/fancy_fileupload.css" rel="stylesheet" />
    <link href="{{URL::to('/')}}/assets/plugins/Drag-And-Drop/dist/imageuploadify.min.css" rel="stylesheet" />
    <link href="{{URL::to('/')}}/assets/plugins/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet" /> -->
@endsection()
@section('loadscript')
    <script src="{{URL::to('/')}}/assets/plugins/simplebar/js/simplebar.min.js"></script>
    <script src="{{URL::to('/')}}/assets/plugins/metismenu/js/metisMenu.min.js"></script>
    <script src="{{URL::to('/')}}/assets/plugins/perfect-scrollbar/js/perfect-scrollbar.js"></script>
    <script src="{{ asset('assets/plugins/smart-wizard/js/jquery.smartWizard.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datetimepicker/js/legacy.js') }}"></script>
	<script src="{{ asset('assets/plugins/datetimepicker/js/picker.js') }}"></script>
	<script src="{{ asset('assets/plugins/datetimepicker/js/picker.time.js') }}"></script>
	<script src="{{ asset('assets/plugins/datetimepicker/js/picker.date.js') }}"></script>
	<script src="{{ asset('assets/plugins/bootstrap-material-datetimepicker/js/moment.min.js') }}"></script>
	<script src="{{ asset('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.min.js') }}"></script>
    <!-- <script src="{{URL::to('/')}}/assets/plugins/fancy-file-uploader/jquery.ui.widget.js"></script>
    <script src="{{URL::to('/')}}/assets/plugins/fancy-file-uploader/jquery.fileupload.js"></script>
    <script src="{{URL::to('/')}}/assets/plugins/fancy-file-uploader/jquery.iframe-transport.js"></script>
    <script src="{{URL::to('/')}}/assets/plugins/fancy-file-uploader/jquery.fancy-fileupload.js"></script>
    <script src="{{URL::to('/')}}/assets/plugins/Drag-And-Drop/dist/imageuploadify.min.js"></script> -->
@endsection()
@section('scripts')

    var forms = document.querySelectorAll('.needs-validation')

    // Loop over them and prevent submission
    Array.prototype.slice.call(forms)
    .forEach(function (form) {
    form.addEventListener('submit', function (event) {
        if (!form.checkValidity()) {
        event.preventDefault()
        event.stopPropagation()
        }

        form.classList.add('was-validated')
    }, false)
    })
    /*-- $('#fancy-file-upload').FancyFileUpload({
        params: {
            action: 'fileuploader'
        },
        maxfilesize: 1000000
    });
    $('#image-uploadify').imageuploadify(); */

		$(document).ready(function () {
            

			// Toolbar extra buttons
			var btnFinish = $('<button></button>').text('Finish').addClass('btn btn-info').on('click', function () {
				
			});
			var btnCancel = $('<button></button>').text('Cancel').addClass('btn btn-danger').on('click', function () {
				$('#smartwizard').smartWizard("reset");
			});
			// Step show event
			$("#smartwizard").on("showStep", function (e, anchorObject, stepNumber, stepDirection, stepPosition) {
				$("#prev-btn").removeClass('disabled');
				$("#next-btn").removeClass('disabled');
				if (stepPosition === 'first') {
					$("#prev-btn").addClass('disabled');
				} else if (stepPosition === 'last') {
					$("#next-btn").addClass('disabled');
				} else {
					$("#prev-btn").removeClass('disabled');
					$("#next-btn").removeClass('disabled');
				}
			});
			// Smart Wizard
			$('#smartwizard').smartWizard({
				selected: 0,
				theme: 'dots',
				transition: {
					animation: 'slide-horizontal', // Effect on navigation, none/fade/slide-horizontal/slide-vertical/slide-swing
				},
				toolbarSettings: {
					toolbarPosition: 'bottom', // both bottom
					toolbarExtraButtons: [btnFinish, btnCancel]
				}
			});
			// External Button Events
			$("#reset-btn").on("click", function () {
				// Reset wizard
				$('#smartwizard').smartWizard("reset");
				return true;
			});
			$("#prev-btn").on("click", function () {
				// Navigate previous
				$('#smartwizard').smartWizard("prev");
				return true;
			});
			$("#next-btn").on("click", function () {
				// Navigate next
				$('#smartwizard').smartWizard("next");
				return true;
			});
			// Demo Button Events
			$("#got_to_step").on("change", function () {
				// Go to step
				var step_index = $(this).val() - 1;
				$('#smartwizard').smartWizard("goToStep", step_index);
				return true;
			});
			$("#is_justified").on("click", function () {
				// Change Justify
				var options = {
					justified: $(this).prop("checked")
				};
				$('#smartwizard').smartWizard("setOptions", options);
				return true;
			});
			$("#animation").on("change", function () {
				// Change theme
				var options = {
					transition: {
						animation: $(this).val()
					},
				};
				$('#smartwizard').smartWizard("setOptions", options);
				return true;
			});
			$("#theme_selector").on("change", function () {
				// Change theme
				var options = {
					theme: $(this).val()
				};
				$('#smartwizard').smartWizard("setOptions", options);
				return true;
			});
		});


        $('.datepicker').pickadate({
			selectMonths: true,
	        selectYears: true
		}),
		$('.timepicker').pickatime()

        $(function () {
			$('#date-time').bootstrapMaterialDatePicker({
				format: 'YYYY-MM-DD HH:mm'
			});
			$('#date').bootstrapMaterialDatePicker({
				time: false
			});
		});

        // option show
                   let option = jQuery('.type-select').val();
                    if(option == 4){
                        jQuery("#options").show();
                    }else{
                        jQuery("#options").hide();
                    }
                    
                jQuery('.type-select').change(function(event){
                    event.preventDefault();
                    let option = jQuery('.type-select').val();
                    if(option == 4){
                        jQuery("#options").show();
                    }else{
                        jQuery("#options").hide();
                    }
                });
@endsection
@extends('layouts.index')

@section('title')
    Service
@endsection()

@section('content')
    <?php $i = 0; ?>
    <!-- Content Header (Page header) -->


    <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
        <div class="breadcrumb-title pe-3">All Service</div>
        <div class="ps-3">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb mb-0 p-0">
                    <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">Service</li>
                </ol>
            </nav>
        </div>
        <div class="ms-auto">
            <div class="btn-group">
                <a href="{{ route('services-add') }}">
                    <button type="button" class="btn btn-light">Add New</button>
                </a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-9 mx-auto">
            <div class="card">
                <div class="card-body table-responsive">
                    <table class="table mb-0 text-center">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col" >Action</th>
                                <th scope="col"> Name</th>
                                <th scope="col">Main Service</th>
                                <th scope="col">Image</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                            @foreach ($services as $service)
                                <tr>
                                    <td scope="row">{{ ++$i }}</td>
                                    <td>
                                        <a href="{{route('services-edit', $service->id)}}">
                                            Edit
                                        </a> ||
                                        <a onclick="return confirm('Are you sure?')" href="{{route('services-delete', $service->id)}}">
                                            Delete
                                        </a>
                                    </td>
                                    <td>
                                        {{ $service->name  }}
                                    </td>
                                    <td>
                                        {{ $service->service_name  }} 
                                    </td>
                                    <td>
                                       <img src="{{ url('public')  }}/{{ $service->img}}" alt="" width="100"/>
                                    </td>
                                </tr>
                            @endforeach
                       
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
      <!-- /.content-header -->
    <?php $i = 0;?>

    <!--!! $projects->links() !!-->

@endsection()

@section('scripts')

$("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');

@endsection()

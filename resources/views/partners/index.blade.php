@extends('layouts.index')

@section('title')
             Partners
@endsection()

@section('content')
    <?php $i = 0; ?>
    <!-- Content Header (Page header) -->
    <style type="text/css">
        .imageThumb {
            max-height: 75px;
            border: 2px solid;
            padding: 1px;
            cursor: pointer;
        }
        .pip {
            display: inline-block;
            margin: 10px 10px 0 0;
        }
    </style>

    <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
        <div class="breadcrumb-title pe-3">Partners</div>
        <div class="ps-3">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb mb-0 p-0">
                    <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">Partners</li>
                </ol>
            </nav>
        </div>
        <div class="ms-auto">
            <div class="btn-group">
                <a href="{{ route('stratergic-partnership-add') }}">
                    <button type="button" class="btn btn-light">Add New</button>
                </a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-9 mx-auto">
            <div class="card">
                <div class="card-body">
                    <table class="table mb-0">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Company Name</th>
                                <th scope="col">Representive Name</th>
                                <th scope="col">Mobile / Phone Number</th>
                                <th scope="col">Email</th>
                                <th scope="col">Business Category</th>
                                <th scope="col">General Category of Business</th>
                                <th scope="col">Expertice</th>
                                <th scope="col">Portfolio</th>
                                <th scope="col">Any Document</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($StratergicPartnerships as $Partners)
                                <tr>
                                    <td scope="row">{{ ++$i }}</td>
                                    company_name
                                    representive_name
                                    mobile
                                    email
                                    business_category
                                    general_category_of_business
                                    expertice
                                    portfolio
                                    any_document
                                    <td>
                                        {{ $Partners->company_name }}
                                    </td>
                                    <td>
                                        {{ $Partners->representive_name }}
                                    </td>
                                    <td>
                                        {{ $Partners->mobile }}
                                    </td>
                                    <td>
                                        {{ $Partners->email }}
                                    </td>
                                    <td>
                                        {{ $Partners->business_category }}
                                    </td>
                                    <td>
                                        {{ $Partners->general_category_of_business }}
                                    </td>
                                    <td>
                                        {{ $Partners->expertice }}
                                    </td>
                                    <td>
                                        {{ $Partners->portfolio }}
                                    </td>
                                    <td>
                                        {{ $Partners->any_document }}
                                    </td>
                                    <!-- <td>
                                        <?php
                                        // $imgs = explode(", ",$Partners->Partners_details_screenshot);
                                        // {{ url('public/uploads/'.$imgs[$i]) }}
                                        ?>
                                    </td> -->
                                </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
      <!-- /.content-header -->
    <?php $i = 0;?>


@endsection()

@section('scripts')

  function previewFiles(input){
      var file = $("input[type=file]").get(0).files[0];

      if(file){
          var reader = new FileReader();

          reader.onload = function(){
              $("#previewImg").attr("style", "height: 180px; display:block;");
              $("#previewImg").attr("src", reader.result);
          }

          reader.readAsDataURL(file);
      }
  }
@endsection()

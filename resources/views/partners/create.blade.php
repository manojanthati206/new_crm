<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!--favicon-->
        <link rel="icon" href="{{URL::to('/')}}/assets/images/favicon-32x32.png" type="image/png" />
        <!--plugins-->
        <meta charset="utf-8">
        <title>Partners</title>
        <link rel="stylesheet" href="{{URL::to('/')}}/assets/css/style.css">
        <!-- <script src="https://kit.fontawesome.com/a076d05399.js"></script> -->
    </head>
    <body>
        <div class="container">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <header>Stratergic Partnership</header>
            <div class="progress-bar">
                <div class="step">
                    <p>Name</p>
                    <div class="bullet">
                        <span>1</span>
                    </div>
                    <div class="check fas fa-check"></div>
                </div>
                <div class="step">
                    <p>Contact</p>
                    <div class="bullet">
                        <span>2</span>
                    </div>
                    <div class="check fas fa-check"></div>
                </div>
                <div class="step">
                    <p>Business</p>
                    <div class="bullet">
                        <span>3</span>
                    </div>
                    <div class="check fas fa-check"></div>
                </div>
                <div class="step">
                    <p>Submit</p>
                    <div class="bullet">
                        <span>4</span>
                    </div>
                    <div class="check fas fa-check"></div>
                </div>
            </div>
            <div class="form-outer">
                <form method="post" action="{{ route('stratergic-partnership-store') }}" class="row g-3 needs-validation" enctype="multipart/form-data"  novalidate>
                    @csrf
                    <div class="page slide-page">
                        <div class="title">Basic Info:</div>
                        <div class="field">
                            <div class="label">Company Name</div>
                            <input type="text" class="form-control" id="validationCustom01" value="" name="company_name" placeholder="Company Name" required>
                        </div>
                        <div class="field">
                            <div class="label">Representive Name</div>
                            <input type="text" class="form-control" id="validationCustom06" value="" name="representive_name" placeholder="Representive Name" required>
                        </div>
                        <div class="field">
                            <button class="firstNext next">Next</button>
                        </div>
                    </div>
                    <div class="page">
                        <div class="title">Contact Info:</div>
                        <div class="field">
                            <div class="label">Email Address</div>
                            <input type="email" class="form-control" id="validationCustom03" value="" name="email" placeholder="Email" >
                        </div>
                        <div class="field">
                            <div class="label">Mobile / Phone Number</div>
                            <input type="text" class="form-control" id="validationCustom02" value="" name="mobile" placeholder="Mobile / Phone Number" required>
                        </div>
                        <div class="field btns">
                            <button class="prev-1 prev">Previous</button>
                            <button class="next-1 next">Next</button>
                        </div>
                    </div>
                    <div class="page">
                        <div class="title">Business:</div>
                        <div class="field">
                            <div class="label">Business Category</div>
                            <select name="general_category_of_business">
                                <option value="Management">Management</option>
                                <option value="Finance">Finance</option>
                                <option value="Restaurant">Restaurant</option>
                                <option value="Web Technology">Web Technology</option>
                                <option value="Banking">Banking</option>
                                <option value="Architect ">Architect </option>
                                <option value="Health care">Health care</option>
                                <option value="Education">Education</option>
                                <option value="Transportation">Transportation</option>
                                <option value="Marketing">Marketing</option>
                                <option value="Tourism">Tourism</option>
                                <option value="Fashion & apparel">Fashion & apparel</option>
                                <option value="Textile">Textile</option>
                                <option value="Grooming services">Grooming services</option>
                                <option value="Graphic designing">Graphic designing</option>
                                <option value="Video & animation">Video & animation</option>
                            </select>
                        </div>
                        <div class="field">
                            <div class="label">Numbers of Employee</div>
                            <select name="general_category_of_business">
                                <option value="self employed">Self Employed</option>
                                <option value="< 10">< 10</option>
                                <option value="11-25">11-25</option> 
                                <option value="26-50">26-50</option>
                                <option value="> 51">> 51</option>
                            </select>
                        </div>
                        <div class="field">
                            <div class="label">Expertice</div>
                            <input type="text" class="form-control" id="validationCustom08" value="" name="expertice" placeholder="Software Development / Software Testing / etc..">
                        </div>
                        <div class="field btns">
                            <button class="prev-2 prev">Previous</button>
                            <button class="next-2 next">Next</button>
                        </div>
                    </div>
                    <div class="page">
                        <div class="title">Other:</div>
                        <div class="field">
                            <div class="label">Portfolio</div>
                            <input type="text" class="form-control" id="validationCustom09" value="" name="portfolio" placeholder="Portfolio">
                        </div>
                        <div class="field">
                            <div class="label">Any Document (Brochure / Company Profile)</div>
                            <input type="file" name="any_document[]" class="custom-file-input" id="files" multiple>
                        </div>
                        <div class="field btns">
                            <button class="prev-3 prev">Previous</button>
                            <button class="submit">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <script src="{{URL::to('/')}}/assets/js/script.js"></script>
    </body>
</html>
@extends('layouts.index')

@section('title')
    Reminder
@endsection()

@section('content')
    <?php $i = 0; ?>
    <!-- Content Header (Page header) -->


    <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
        <div class="breadcrumb-title pe-3">All Reminder</div>
        <div class="ps-3">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb mb-0 p-0">
                    <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">Reminder</li>
                </ol>
            </nav>
        </div>
        <div class="ms-auto">
            <div class="btn-group">
                <a href="{{ route('reminder-add') }}">
                    <button type="button" class="btn btn-light">Add New</button>
                </a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-9 mx-auto">
            <div class="card">
                <div class="card-body">
                    <table class="table mb-0">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col" colspan="2">Action</th>
                                <th scope="col">Client Name</th>
                                <th scope="col">Approach Channel</th>
                                <th scope="col">Communication Preference</th>
                                <th scope="col">Contact Info</th>
                                <th scope="col">Last Response Date</th>
                                <th scope="col">Reminder Status</th>
                                <th scope="col">Follow-Ups</th>
                                <th scope="col">Client Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($reminders as $reminder)
                                <tr>
                                    <td scope="row">{{ ++$i }}</td>
                                    <td>
                                        <a href="{{route('reminder-edit', $reminder->id)}}">
                                            Edit
                                        </a>
                                    </td>
                                    <td>
                                        <a onclick="return confirm('Are you sure?')" href="{{route('reminder-delete', $reminder->id)}}">
                                            Delete
                                        </a>
                                    </td>
                                    <td>
                                        {{ $reminder->client_name }}
                                    </td>
                                    <td>
                                        {{ $reminder->approach_channel }}
                                    </td>
                                    <td>
                                        {{ $reminder->communication_preference }}
                                    </td>
                                    <td>
                                        {{ $reminder->contact_info }}
                                    </td>
                                    <td>
                                        {{ $reminder->last_response_date }}
                                    </td>
                                    <td  style="text-transform: capitalize">
                                        {{ $reminder->reminder_status }}
                                    </td>
                                    <td style="text-transform: capitalize">
                                        {{ $reminder->followups }}
                                    </td>
                                    <td style="text-transform: capitalize">
                                        {{ $reminder->client_status }}
                                    </td>
                                </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
      <!-- /.content-header -->
    <?php $i = 0;?>

    <!--!! $projects->links() !!-->

@endsection()

@section('scripts')

$("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');

@endsection()


@extends('layouts.index')

@section('title')
            Reminder
@endsection()

@section('content')

    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
        <div class="breadcrumb-title pe-3">Add New Reminder</div>
        <div class="ps-3">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb mb-0 p-0">
                    <li class="breadcrumb-item"><a href="javascript:void(0);"><i class="bx bx-home-alt"></i></a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">Reminder</li>
                </ol>
            </nav>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-9 mx-auto">
            <hr/>
            <div class="card">
                <div class="card-body">
                    <div class="p-4 border rounded">
                        <form method="post" action="{{ route('reminder-store') }}" class="row g-3 needs-validation" enctype="multipart/form-data"  novalidate>
                            @csrf
                            <div class="col-md-12">
                                <label for="validationCustom01" class="form-label">Client Name</label>
                                <input type="text" class="form-control" id="validationCustom01" value="" name="client_name" placeholder="Client Name" required>
                                <div class="valid-feedback">Looks good!</div>
                            </div>
                            <div class="col-md-12">
                                <label for="validationCustom02" class="form-label">Approach Channel</label>
                                <input type="text" class="form-control" id="validationCustom02" value="" name="approach_channel" placeholder="Approach Channel" required>
                                <div class="valid-feedback">Looks good!</div>
                            </div>

                            <div class="col-md-12">
                                <label for="validationCustom03" class="form-label">Communication Preference</label>
                                <input type="text" class="form-control" id="validationCustom03" value="" name="communication_preference" placeholder="Communication Preference" required>
                                <div class="valid-feedback">Looks good!</div>
                            </div>

                            <div class="col-md-12">
                                <label for="validationCustom33" class="form-label">Email address to send reminder</label>
                                <input type="text" class="form-control" id="validationCustom33" value="" name="reminder_email" placeholder="Enter email address for reminder" required>
                                <div class="valid-feedback">Looks good!</div>
                            </div>

                            <div class="col-md-12">
                                <label for="validationCustom04" class="form-label">Contact Info</label>
                                <input type="text" class="form-control" id="validationCustom04" value="" name="contact_info" placeholder="Contact Info" required>
                                <div class="valid-feedback">Looks good!</div>
                            </div>

                            <div class="col-md-6">
                                <label for="validationCustom05" class="form-label">Last Response Date</label>
                                <input type="text" class="form-control datepicker" id="validationCustom05" name="last_response_date" placeholder="Last Response Date" required>
                                <div class="valid-feedback">Looks good!</div>
                            </div>

                            <div class="col-md-6">
                                <label for="inputState" class="form-label">Client Status</label>
                                <select id="inputState" name="client_status" class="form-select">
                                    <option value="" selected>Choose...</option>
                                    <option value="Active">Active</option>
                                    <option value="Inactive">Inactive</option>
                                    <option value="Pending">Pending</option>
                                </select>
                            </div>
                            <div class="col-12">
                                <button class="btn btn-light" type="submit">Submit form</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection()

@section('scripts')

    $('.datepicker').pickadate({
        selectMonths: true,
        selectYears: true,
    });
    var forms = document.querySelectorAll('.needs-validation');

    // Loop over them and prevent submission
    Array.prototype.slice.call(forms)
    .forEach(function (form) {
    form.addEventListener('submit', function (event) {
    if (!form.checkValidity()) {
    event.preventDefault()
    event.stopPropagation()
    }

    form.classList.add('was-validated')
    }, false)
    })

@endsection()
@section('loadstyles')
    <!-- <link href="{{URL::to('/')}}/assets/plugins/fancy-file-uploader/fancy_fileupload.css" rel="stylesheet" />
    <link href="{{URL::to('/')}}/assets/plugins/Drag-And-Drop/dist/imageuploadify.min.css" rel="stylesheet" />
    <link href="{{URL::to('/')}}/assets/plugins/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet" /> -->
    <link href="{{URL::to('/')}}/assets/plugins/datetimepicker/css/classic.css" rel="stylesheet" />
    <link href="{{URL::to('/')}}/assets/plugins/datetimepicker/css/classic.time.css" rel="stylesheet" />
    <link href="{{URL::to('/')}}/assets/plugins/datetimepicker/css/classic.date.css" rel="stylesheet" />
@endsection()
@section('loadscript')
    <script src="{{URL::to('/')}}/assets/plugins/simplebar/js/simplebar.min.js"></script>
    <script src="{{URL::to('/')}}/assets/plugins/metismenu/js/metisMenu.min.js"></script>
    <script src="{{URL::to('/')}}/assets/plugins/perfect-scrollbar/js/perfect-scrollbar.js"></script>
    <script src="{{URL::to('/')}}/assets/plugins/datetimepicker/js/legacy.js"></script>
    <script src="{{URL::to('/')}}/assets/plugins/datetimepicker/js/picker.js"></script>
    <script src="{{URL::to('/')}}/assets/plugins/datetimepicker/js/picker.time.js"></script>
    <script src="{{URL::to('/')}}/assets/plugins/datetimepicker/js/picker.date.js"></script>
    <!-- <script src="{{URL::to('/')}}/assets/plugins/fancy-file-uploader/jquery.ui.widget.js"></script>
    <script src="{{URL::to('/')}}/assets/plugins/fancy-file-uploader/jquery.fileupload.js"></script>
    <script src="{{URL::to('/')}}/assets/plugins/fancy-file-uploader/jquery.iframe-transport.js"></script>
    <script src="{{URL::to('/')}}/assets/plugins/fancy-file-uploader/jquery.fancy-fileupload.js"></script>
    <script src="{{URL::to('/')}}/assets/plugins/Drag-And-Drop/dist/imageuploadify.min.js"></script> -->
@endsection()

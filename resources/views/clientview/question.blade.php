@extends('clientview.index')

@section('title')
           Question add
@endsection()

@section('content')
    <script type="text/javascript">
        var base_url = "{{URL::to('/')}}";
    </script>
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    {{-- <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
        <div class="breadcrumb-title pe-3">Question</div>
        <div class="ps-3">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb mb-0 p-0">
                    <li class="breadcrumb-item"><a href="javascript:void(0);"><i class="bx bx-home-alt"></i></a>
                    </li>
                    <li class="breadcrumb-item"><a href="{{ url('client/servcies') }}">Serivces</a>
                    <li class="breadcrumb-item active" aria-current="page">Question</li>
                </ol>
            </nav>
        </div>
    </div> --}}
    <div class="row">
        <div class="col-xl-9 mx-auto">
            <hr/>
            <div class="card">
                <div class="card-body">
                    <div class="p-4 border rounded">
                        <div id="smartwizard">
                            <ul class="nav">
                                <?php $Q_total = 0; $Q = 1;
                                    if($questions->count() == 0 ){
                                        dd('Question Not Found..');
                                    }
                                    $total_q = $totalquestion = $questions->count();  // total question
                                    
                                    if($totalquestion == 1 or $totalquestion == 2){
                                        $totalquestion+= 1;
                                    }
                                    $extraQuestion = false;
                                    if($totalquestion % 2 != 0){
                                        $totalquestion += 0;
                                        $extraQuestion = true;
                                    }
                                    if($totalquestion > 13 ){
                                        $tab =  ceil($totalquestion / 6);
                                    }else{
                                        $tab = ceil($totalquestion / 2);
                                    }
                                    
                                    $step =  $totalquestion / $tab;
                                    $step = ceil($step);
                                    $j = 1;
                                ?>
                                @while($j <= $tab)
                                    <li class="nav-item">
                                        <a class="nav-link" href="#step-{{$j}}"><strong>Step {{$j}}</strong> </a>
                                    </li>
                                    <?php $j++ ?>
                                @endwhile
                            </ul>
                            <div class="tab-content">
                                <?php $q_count=0;   ?> 
                                @for($i=1; $i<=$tab ; $i++)
                                    @if ( isset($questions[$q_count]) && !empty($questions[$q_count]) )
                                        <div id="step-{{$i}}" class="tab-pane" role="tabpanel" aria-labelledby="step-{{$i}}">
                                            @for($k=1; $k<=$step; $k++)
                                                @if ($q_count <= $totalquestion-1)
                                                    <div class="col-md-12 mt-2" >
                                                        <form method="post"  id="image-upload{{$q_count}}" class="row g-3 needs-validation" enctype="multipart/form-data"  novalidate>
                                                            <label for="validationCustom01" class="form-label lead">Q) {{ $questions[$q_count]->question }} </label><input type="hidden" id="id_{{$q_count}}" name="id" value="{{ $questions[$q_count]->id }}" />
                                                            <input type="hidden" id="questionid_{{$q_count}}" name="questionid" value="{{ $questions[$q_count]->qid }}"/>
                                                                @csrf
                                                                <?php $ans = '';?>
                                                                <?php $page_title_1 = '';?>
                                                                <?php $page_title_2 = '';?>
                                                                <?php $page_title_3 = '';?>
                                                                <?php $page_title_4 = '';?>
                                                                <?php $page_title_5 = '';?>
                                                                <?php $page_title_6 = '';?>
                                                                <?php $page_title_7 = '';?>
                                                                <?php $page_title_8 = '';?>
                                                                <?php $page_title_9 = '';?>
                                                                <?php $page_title_10 = '';?>
                                                                <?php $page_section_1 = '';?>
                                                                <?php $page_section_2 = '';?>
                                                                <?php $page_section_3 = '';?>
                                                                <?php $page_section_4 = '';?>
                                                                <?php $page_section_5 = '';?>
                                                                <?php $page_section_6 = '';?>
                                                                <?php $page_section_7 = '';?>
                                                                <?php $page_section_8 = '';?>
                                                                <?php $page_section_9 = '';?>
                                                                <?php $page_section_10 = '';?>
                                                                <?php $reference_link_1 = '';?>
                                                                <?php $reference_link_2 = '';?>
                                                                <?php $reference_link_3 = '';?>
                                                                <?php $reference_link_4 = '';?>
                                                                <?php $reference_link_5 = '';?>
                                                                @foreach($answers as $answer)
                                                                    @if( $questions[$q_count]->qid == $answer->questionid )
                                                                         <?php $ans = $answer->answer;?>
                                                                        <?php $page_title_1 = $answer->page_1;?>
                                                                        <?php $page_title_2 = $answer->page_2;?>
                                                                        <?php $page_title_3 = $answer->page_3;?>
                                                                        <?php $page_title_4 = $answer->page_4;?>
                                                                        <?php $page_title_5 = $answer->page_5;?>
                                                                        <?php $page_title_6 = $answer->page_6;?>
                                                                        <?php $page_title_7 = $answer->page_7;?>
                                                                        <?php $page_title_8 = $answer->page_8;?>
                                                                        <?php $page_title_9 = $answer->page_9;?>
                                                                        <?php $page_title_10 = $answer->page_10;?>
                                                                        <?php $page_section_1 = $answer->page_section_1;?>
                                                                        <?php $page_section_2 = $answer->page_section_2;?>
                                                                        <?php $page_section_3 = $answer->page_section_3;?>
                                                                        <?php $page_section_4 = $answer->page_section_4;?>
                                                                        <?php $page_section_5 = $answer->page_section_5;?>
                                                                        <?php $page_section_6 = $answer->page_section_6;?>
                                                                        <?php $page_section_7 = $answer->page_section_7;?>
                                                                        <?php $page_section_8 = $answer->page_section_8;?>
                                                                        <?php $page_section_9 = $answer->page_section_9;?>
                                                                        <?php $page_section_10 = $answer->page_section_10;?>
                                                                        <?php $reference_link_1 = $answer->reference_link_1;?>
                                                                        <?php $reference_link_2 = $answer->reference_link_2;?>
                                                                        <?php $reference_link_3 = $answer->reference_link_3;?>
                                                                        <?php $reference_link_4 = $answer->reference_link_4;?>
                                                                        <?php $reference_link_5 = $answer->reference_link_5;?>
                                                                    @endif
                                                                @endforeach
                                                                @if( $questions[$q_count]->type == 1)
                                                                    <input type="text"  class="form-control question_text_{{$q_count}}" name="question_text_{{$q_count}}" id="validationCustom01" value="{{ old('question',$ans) }}"  placeholder="Write Answer.... " >
                                                                    <div  style="display:none" id="question_msg{{$q_count}}" ></div>
                                                                @elseif ($questions[$q_count]->type == 2)
                                                                    <div class="input-group mb-1">
                                                                        <input type="file"  class="form-control question_img_{{$q_count}}" name="image[]" placeholder="Choose image" id="image{{$q_count}}" value="{{ old('images') }}"   multiple>
                                                                        <div class="input-group-append">
                                                                            <button type="submit" style="display:" class="btn btn-light" id="submit">Upload</button>
                                                                        </div>
                                                                    </div>
                                                                    <?php  if($questions[$q_count]->images != null){ ?>
                                                                        <?php $oldimages = json_decode($questions[$q_count]->images);?>
                                                                        @foreach($oldimages as $oldimage)                                                                
                                                                            <img src="{{ url('/public/'.$oldimage) }}" style="width:150px" alt="">&nbsp;
                                                                        @endforeach
                                                                    <?php } ?> 
                                                                    <div  style="display:none" id="question_msg{{$q_count}}" ></div>
                                                                @elseif ($questions[$q_count]->type == 3)
                                                                    <input type="text"  class="form-control question_text_{{$q_count}}" name="question_text_{{$q_count}}"   value="{{ old('question',$ans) }}"  placeholder="Write Answer...." ><br/>
                                                                    <div class="input-group mb-3">
                                                                        <input type="file"  class="form-control question_img_{{$q_count}}" name="image[]" placeholder="Choose image" id="image{{$q_count}}" value="{{ old('images') }}"   multiple>
                                                                        <div class="input-group-append">
                                                                            <button type="submit" style="display:" class="btn btn-light" id="submit">Upload</button>
                                                                        </div>
                                                                    </div>
                                                                    <?php if($questions[$q_count]->images != null){ ?>
                                                                        <?php $oldimages = json_decode($questions[$q_count]->images);?>
                                                                        @foreach($oldimages as $oldimage)                                                                
                                                                            <img src="{{ url('/public/'.$oldimage) }}" style="width:150px" alt="">&nbsp;
                                                                        @endforeach
                                                                        <?php } ?>  
                                                                    <div  style="display:none" id="question_msg{{$q_count}}" ></div>
                                                                @elseif ($questions[$q_count]->type == 4)
                                                                    <div class="form-check" style="margin: 0 10px;">
                                                                    <?php if(!empty($questions[$q_count]->options)){ ?>
                                                                        <?php $options = explode ("|", $questions[$q_count]->options); $j=1; ?>
                                                                        <select class="form-select type-select question_text_{{$q_count}}"  id="inputState"  name="question_text_{{$q_count}}"   required>
                                                                            <option value="" >Select</option>
                                                                            @foreach($options  AS $opt)
                                                                                <option <?php if($opt==$ans) echo "selected"?> value="{{ $opt }}" >{{ $opt }}</option>
                                                                                <!-- <input type="radio"  class="lead form-check-input question_text_{{$q_count}}" id="question_text_{{$q_count}}{{$j}}" data-textval="{{ old('question',$ans) }}"    ><label class="form-check-label lead" for="question_text_{{$q_count}}{{$j++}}"><?php //echo $opt; ?></label><br/> -->
                                                                            @endforeach
                                                                            </select>
                                                                        </div>
                                                                    <?php } ?>  
                                                                    <div  style="display:none" id="question_msg{{$q_count}}" ></div>
                                                                @elseif( $questions[$q_count]->type == 5)
                                                                    <textarea class="form-control question_text_{{$q_count}}" name="question_text_{{$q_count}}" id="validationCustom01" placeholder="Write Answer.... " >{{ old('question',$ans) }}</textarea>
                                                                    <div  style="display:none" id="question_msg{{$q_count}}" ></div>
                                                                @elseif( $questions[$q_count]->type == 6)
                                                                    <?php
                                                                    $data_val = 1;
                                                                    ?>
                                                                    <div class="qeustion-page-content-1">
                                                                        <input type="text"  class="form-control question_page_text_1_{{$q_count}}" name="question_page_text_1_{{$q_count}}" id="validationCustom01" value="{{ old('question',$page_title_1) }}"  placeholder="Page Title" >
                                                                        <div  style="display:none" id="question_msg_1_{{$q_count}}" ></div>
                                                                    </div>
                                                                    <div class="page-section-1">
                                                                        <textarea class="form-control question_section_text_1_{{$q_count}}" name="question_section_text_1_{{$q_count}}" id="validationCustom01" placeholder="Page Sections" >{{ old('question',$page_section_1) }}</textarea>
                                                                        <div  style="display:none" id="content_msg_1_{{$q_count}}" ></div>
                                                                    </div>
                                                                    <div class="qeustion-page-content-2" style="display:<?php if ( $page_title_2 != '' ) { echo 'block'; $data_val++; } else { echo 'none'; }  ?>;">
                                                                        <input type="text"  class="form-control question_page_text_2_{{$q_count}}" name="question_page_text_2_{{$q_count}}" id="validationCustom01" value="{{ old('question',$page_title_2) }}"  placeholder="Page Title" >
                                                                        <div  style="display:none" id="question_msg_2_{{$q_count}}" ></div>
                                                                    </div>
                                                                    <div class="page-section-2" style="display:<?php if ( $page_section_2 != '' ) { echo 'block';} else { echo 'none'; }  ?>;">
                                                                        <textarea class="form-control question_section_text_2_{{$q_count}}" name="question_section_text_2_{{$q_count}}" id="validationCustom01" placeholder="Page Sections" >{{ old('question',$page_section_2) }}</textarea>
                                                                        <div  style="display:none" id="content_msg_2_{{$q_count}}" ></div>
                                                                    </div>
                                                                    <div class="qeustion-page-content-3" style="display:<?php if ( $page_title_3 != '' ) { echo 'block'; $data_val++; } else { echo 'none'; }  ?>;">
                                                                        <input type="text"  class="form-control question_page_text_3_{{$q_count}}" name="question_page3__text_{{$q_count}}" id="validationCustom01" value="{{ old('question',$page_title_3) }}"  placeholder="Page Title" >
                                                                        <div  style="display:none" id="question_msg_3_{{$q_count}}" ></div>
                                                                    </div>
                                                                    <div class="page-section-3" style="display:<?php if ( $page_section_3 != '' ) { echo 'block'; } else { echo 'none'; }  ?>;">
                                                                        <textarea class="form-control question_section_text_3_{{$q_count}}" name="question_section_text_3_{{$q_count}}" id="validationCustom01" placeholder="Page Sections" >{{ old('question',$page_section_3) }}</textarea>
                                                                        <div  style="display:none" id="content_msg_3_{{$q_count}}" ></div>
                                                                    </div>

                                                                    <div class="qeustion-page-content-4" style="display:<?php if ( $page_title_4 != '' ) { echo 'block'; $data_val++; } else { echo 'none'; }  ?>;">
                                                                        <input type="text"  class="form-control question_page_text_4_{{$q_count}}" name="question_page_text_4_{{$q_count}}" id="validationCustom01" value="{{ old('question',$page_title_4) }}"  placeholder="Page Title" >
                                                                        <div  style="display:none" id="question_msg_4_{{$q_count}}" ></div>
                                                                    </div>
                                                                    <div class="page-section-4" style="display:<?php if ( $page_section_4 != '' ) { echo 'block'; } else { echo 'none'; }  ?>;">
                                                                        <textarea class="form-control question_section_text_4_{{$q_count}}" name="question_section_text_4_{{$q_count}}" id="validationCustom01" placeholder="Page Sections" >{{ old('question',$page_section_4) }}</textarea>
                                                                        <div  style="display:none" id="content_msg_4_{{$q_count}}" ></div>
                                                                    </div>

                                                                    <div class="qeustion-page-content-5" style="display:<?php if ( $page_title_5 != '' ) { echo 'block'; $data_val++; } else { echo 'none'; }  ?>;">
                                                                        <input type="text"  class="form-control question_page_text_5_{{$q_count}}" name="question_page_text_5_{{$q_count}}" id="validationCustom01" value="{{ old('question',$page_title_5) }}"  placeholder="Page Title" >
                                                                        <div  style="display:none" id="question_msg_5_{{$q_count}}" ></div>
                                                                    </div>
                                                                    <div class="page-section-5" style="display:<?php if ( $page_section_5 != '' ) { echo 'block'; } else { echo 'none'; }  ?>;">
                                                                        <textarea class="form-control question_section_text_5_{{$q_count}}" name="question_section_text_5_{{$q_count}}" id="validationCustom01" placeholder="Page Sections" >{{ old('question',$page_section_5) }}</textarea>
                                                                        <div  style="display:none" id="content_msg_5_{{$q_count}}" ></div>
                                                                    </div>

                                                                    <div class="qeustion-page-content-6" style="display:<?php if ( $page_title_6 != '' ) { echo 'block'; $data_val++; } else { echo 'none'; }  ?>;">
                                                                        <input type="text"  class="form-control question_page_text_6_{{$q_count}}" name="question_page_text_6_{{$q_count}}" id="validationCustom01" value="{{ old('question',$page_title_6) }}"  placeholder="Page Title" >
                                                                        <div  style="display:none" id="question_msg_6_{{$q_count}}" ></div>
                                                                    </div>
                                                                    <div class="page-section-6" style="display:<?php if ( $page_section_6 != '' ) { echo 'block'; } else { echo 'none'; }  ?>;">
                                                                        <textarea class="form-control question_section_text_6_{{$q_count}}" name="question_section_text_6_{{$q_count}}" id="validationCustom01" placeholder="Page Sections" >{{ old('question',$page_section_6) }}</textarea>
                                                                        <div  style="display:none" id="content_msg_6_{{$q_count}}" ></div>
                                                                    </div>

                                                                    <div class="qeustion-page-content-7" style="display:<?php if ( $page_title_7 != '' ) { echo 'block'; $data_val++; } else { echo 'none'; }  ?>;">
                                                                        <input type="text"  class="form-control question_page_text_7_{{$q_count}}" name="question_page_text_7_{{$q_count}}" id="validationCustom01" value="{{ old('question',$page_title_7) }}"  placeholder="Page Title" >
                                                                        <div  style="display:none" id="question_msg_7_{{$q_count}}" ></div>
                                                                    </div>
                                                                    <div class="page-section-7" style="display:<?php if ( $page_section_7 != '' ) { echo 'block'; } else { echo 'none'; }  ?>;">
                                                                        <textarea class="form-control question_section_text_7_{{$q_count}}" name="question_section_text_7_{{$q_count}}" id="validationCustom01" placeholder="Page Sections" >{{ old('question',$page_section_7) }}</textarea>
                                                                        <div  style="display:none" id="content_msg_7_{{$q_count}}" ></div>
                                                                    </div>

                                                                    <div class="qeustion-page-content-8" style="display:<?php if ( $page_title_8 != '' ) { echo 'block'; $data_val++; } else { echo 'none'; }  ?>;">
                                                                        <input type="text"  class="form-control question_page_text_8_{{$q_count}}" name="question_page_text_8_{{$q_count}}" id="validationCustom01" value="{{ old('question',$page_title_8) }}"  placeholder="Page Title" >
                                                                        <div  style="display:none" id="question_msg_8_{{$q_count}}" ></div>
                                                                    </div>
                                                                    <div class="page-section-8" style="display:<?php if ( $page_section_8 != '' ) { echo 'block'; } else { echo 'none'; }  ?>;">
                                                                        <textarea class="form-control question_section_text_8_{{$q_count}}" name="question_section_text_8_{{$q_count}}" id="validationCustom01" placeholder="Page Sections" >{{ old('question',$page_section_8) }}</textarea>
                                                                        <div  style="display:none" id="content_msg_8_{{$q_count}}" ></div>
                                                                    </div>

                                                                    <div class="qeustion-page-content-9" style="display:<?php if ( $page_title_9 != '' ) { echo 'block'; $data_val++; } else { echo 'none'; }  ?>;">
                                                                        <input type="text"  class="form-control question_page_text_9_{{$q_count}}" name="question_page_text_9_{{$q_count}}" id="validationCustom01" value="{{ old('question',$page_title_9) }}"  placeholder="Page Title" >
                                                                        <div  style="display:none" id="question_msg_9_{{$q_count}}" ></div>
                                                                    </div>
                                                                    <div class="page-section-9" style="display:<?php if ( $page_section_9 != '' ) { echo 'block'; } else { echo 'none'; }  ?>;">
                                                                        <textarea class="form-control question_section_text_9_{{$q_count}}" name="question_section_text_9_{{$q_count}}" id="validationCustom01" placeholder="Page Sections" >{{ old('question',$page_section_9) }}</textarea>
                                                                        <div  style="display:none" id="content_msg_9_{{$q_count}}" ></div>
                                                                    </div>

                                                                    <div class="qeustion-page-content-10" style="display:<?php if ( $page_title_10 != '' ) { echo 'block'; $data_val++; } else { echo 'none'; }  ?>;">
                                                                        <input type="text"  class="form-control question_page_text_10_{{$q_count}}" name="question_page_text_10_{{$q_count}}" id="validationCustom01" value="{{ old('question',$page_title_10) }}"  placeholder="Page Title" >
                                                                        <div  style="display:none" id="question_msg_10_{{$q_count}}" ></div>
                                                                    </div>
                                                                    <div class="page-section-10" style="display:<?php if ( $page_section_10 != '' ) { echo 'block'; } else { echo 'none'; }  ?>;">
                                                                        <textarea class="form-control question_section_text_10_{{$q_count}}" name="question_section_text_10_{{$q_count}}" id="validationCustom01" placeholder="Page Sections" >{{ old('question',$page_section_10) }}</textarea>
                                                                        <div  style="display:none" id="content_msg_10_{{$q_count}}" ></div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-6 align-items-left">
                                                                            <button class="btn btn-light btn-add-page me-3 mt-2" data-val="{{$data_val}}" style="button">Add Page</button>
                                                                            <button class="btn btn-light btn-remove-page mt-2" data-val="{{$data_val}}" style="button">Remove Page</button>
                                                                        </div>
                                                                    </div>
                                                                @elseif( $questions[$q_count]->type == 7)
                                                                    <?php
                                                                    $data_val = 1;
                                                                    ?>
                                                                    <div class="qeustion-referencelink-1">
                                                                        <input type="text"  class="form-control question_referencelink_1_{{$q_count}}" name="question_referencelink_1_{{$q_count}}" value="{{ old('question',$reference_link_1) }}"  placeholder="Reference Link" >
                                                                        <div  style="display:none" id="question_msg_1_{{$q_count}}" ></div>
                                                                    </div>
                                                                    <div class="qeustion-referencelink-2" style="display:<?php if ( $reference_link_2 != '' ) { echo 'block'; $data_val++; } else { echo 'none'; }  ?>;">
                                                                        <input type="text"  class="form-control question_referencelink_2_{{$q_count}}" name="question_referencelink_2_{{$q_count}}" value="{{ old('question',$reference_link_2) }}"  placeholder="Reference Link" >
                                                                        <div  style="display:none" id="question_msg_2_{{$q_count}}" ></div>
                                                                    </div>
                                                                    <div class="qeustion-referencelink-3" style="display:<?php if ( $reference_link_3 != '' ) { echo 'block'; $data_val++; } else { echo 'none'; }  ?>;">
                                                                        <input type="text"  class="form-control question_referencelink_3_{{$q_count}}" name="question_referencelink_3_{{$q_count}}" value="{{ old('question',$reference_link_3) }}"  placeholder="Reference Link" >
                                                                        <div  style="display:none" id="question_msg_10_{{$q_count}}" ></div>
                                                                    </div>
                                                                    <div class="qeustion-referencelink-4" style="display:<?php if ( $reference_link_4 != '' ) { echo 'block'; $data_val++; } else { echo 'none'; }  ?>;">
                                                                        <input type="text"  class="form-control question_referencelink_4_{{$q_count}}" name="question_referencelink_4_{{$q_count}}" value="{{ old('question',$reference_link_4) }}"  placeholder="Reference Link" >
                                                                        <div  style="display:none" id="question_msg_4_{{$q_count}}" ></div>
                                                                    </div>
                                                                    <div class="qeustion-referencelink-5" style="display:<?php if ( $reference_link_5 != '' ) { echo 'block'; $data_val++; } else { echo 'none'; }  ?>;">
                                                                        <input type="text"  class="form-control question_referencelink_5_{{$q_count}}" name="question_referencelink_5_{{$q_count}}" value="{{ old('question',$reference_link_5) }}"  placeholder="Reference Link" >
                                                                        <div  style="display:none" id="question_msg_5_{{$q_count}}" ></div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-6 align-items-left">
                                                                            <button class="btn btn-light btn-add-link me-3 mt-2" data-val="{{$data_val}}" style="button">Add Reference Link</button>
                                                                            <button class="btn btn-light btn-remove-link mt-2" data-val="{{$data_val}}" style="button">Remove Reference Link</button>
                                                                        </div>
                                                                    </div>
                                                                @endif
                                                        </form><br/>
                                                        <!-- <img id="preview-image-before-upload" src="" alt="preview image" style="max-height: 100px;"><br/> -->
                                                    </div>   
                                                    <div class="col-md-12 " >
                                                        <?php if($questions[$q_count]->expimages != null){
                                                            $images = json_decode($questions[$q_count]->expimages);  
                                                            if ( $images ) {

                                                                ?>
                                                                <label for="validationCustom01" class="form-label lead">Example</label><br/>
                                                                 @foreach($images as $img)
                                                                    <img src="{{ url('/public/'.$img) }}" width="100px"/>&nbsp;
                                                                @endforeach 
                                                                <?php 
                                                            }      
                                                        }?>
                                                    </div> 
                                                    <?php $q_count++;  ?>
                                                <hr/>
                                                @endif
                                            @endfor
                                        </div>                                     
                                    @endif
                                @endfor
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection()
@section('loadstyles')
    <link href="{{ asset('assets/plugins/smart-wizard/css/smart_wizard_all.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/datetimepicker/css/classic.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/plugins/smart-wizard/css/smart_wizard_all.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/datetimepicker/css/classic.date.css') }}" rel="stylesheet" />
	<link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.min.css') }}">
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link href="{{ asset('assets/plugins/input-tags/css/tagsinput.css') }}" rel="stylesheet" />
    <!-- <link href="{{URL::to('/')}}/assets/plugins/fancy-file-uploader/fancy_fileupload.css" rel="stylesheet" />
    <link href="{{URL::to('/')}}/assets/plugins/Drag-And-Drop/dist/imageuploadify.min.css" rel="stylesheet" />
    <link href="{{URL::to('/')}}/assets/plugins/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet" /> -->
    <style>
        .text-success, .text-danger{
            position: sticky;
        }
        .toolbar-bottom button:nth-child(3),.toolbar-bottom button:nth-child(4) {
            display: none !important;
        }
        
        @media (min-width: 320px) and (max-width: 480px) {
            .nav{
                display: none !important;
            }
        }
    </style>
@endsection()
@section('loadscript')
    <script src="{{URL::to('/')}}/assets/plugins/simplebar/js/simplebar.min.js"></script>
    <script src="{{ asset('assets/plugins/input-tags/js/tagsinput.js') }}"></script>
    <script src="{{URL::to('/')}}/assets/plugins/metismenu/js/metisMenu.min.js"></script>
    <script src="{{URL::to('/')}}/assets/plugins/perfect-scrollbar/js/perfect-scrollbar.js"></script>
    <script src="{{ asset('assets/plugins/smart-wizard/js/jquery.smartWizard.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datetimepicker/js/legacy.js') }}"></script>
	<script src="{{ asset('assets/plugins/datetimepicker/js/picker.js') }}"></script>
	<script src="{{ asset('assets/plugins/datetimepicker/js/picker.time.js') }}"></script>
	<script src="{{ asset('assets/plugins/datetimepicker/js/picker.date.js') }}"></script>
	<script src="{{ asset('assets/plugins/bootstrap-material-datetimepicker/js/moment.min.js') }}"></script>
	<script src="{{ asset('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.min.js') }}"></script>
	
    <!-- <script src="{{URL::to('/')}}/assets/plugins/fancy-file-uploader/jquery.ui.widget.js"></script>
    <script src="{{URL::to('/')}}/assets/plugins/fancy-file-uploader/jquery.fileupload.js"></script>
    <script src="{{URL::to('/')}}/assets/plugins/fancy-file-uploader/jquery.iframe-transport.js"></script>
    <script src="{{URL::to('/')}}/assets/plugins/fancy-file-uploader/jquery.fancy-fileupload.js"></script>
    <script src="{{URL::to('/')}}/assets/plugins/Drag-And-Drop/dist/imageuploadify.min.js"></script> -->
@endsection()
@section('scripts')
    $(".btn-add-page").click(function(){
        var d_val = parseInt($(this).attr('data-val'));
        $('.qeustion-page-content-'+(d_val+1)).css('display','block');
        $('.page-section-'+(d_val+1)).css('display','block');
        $(this).attr('data-val',(d_val+1));
        if(d_val == 10){
            $(this).hide();
            $(".btn-remove-page").show();
        }
    });
    $(".btn-remove-page").click(function(){
        var d_val = parseInt($(".btn-add-page").attr('data-val'));
        $('.qeustion-page-content-'+(d_val)).css('display','none');
        $('.page-section-'+(d_val)).css('display','none');
        $(".btn-add-page").attr('data-val',(d_val-1));
        $('.qeustion-page-content-'+(d_val)+'').find('.form-control').val('');
        $('.page-section-'+(d_val)).find('.form-control').val('');
        if(d_val == 1){
            $(this).hide();
            $(".btn-add-page").show();
        }
    });

    $(".btn-add-link").click(function(){
        var d_val = parseInt($(this).attr('data-val'));
        $('.qeustion-referencelink-'+(d_val+1)).css('display','block');
        $(this).attr('data-val',(d_val+1));
        if(d_val == 10){
            $(this).hide();
            $(".btn-remove-link").show();
        }
    });
    $(".btn-remove-link").click(function(){
        var d_val = parseInt($(".btn-add-link").attr('data-val'));
        $('.qeustion-referencelink-'+(d_val)).css('display','none');
        $(".btn-add-link").attr('data-val',(d_val-1));
        $('.qeustion-referencelink-'+(d_val)).find('.form-control').val('');
        if(d_val == 1){
            $(this).hide();
            $(".btn-add-link").show();
        }
    });

    var forms = document.querySelectorAll('.needs-validation')

    // Loop over them and prevent submission
    Array.prototype.slice.call(forms)
    .forEach(function (form) {
    form.addEventListener('submit', function (event) {
        if (!form.checkValidity()) {
        event.preventDefault()
        event.stopPropagation()
        }

        form.classList.add('was-validated')
    }, false)
    })
    /*-- $('#fancy-file-upload').FancyFileUpload({
        params: {
            action: 'fileuploader'
        },
        maxfilesize: 1000000
    });
    $('#image-uploadify').imageuploadify(); */
    $(document).on('click','.btn-form-submit',function(){
        window.location.replace(base_url+"/question-finish/{{$cid}}");
    });
		$(document).ready(function () {
			// Toolbar extra buttons
			var btnFinish = $('<button></button>').text('Finish').addClass('btn btn-info').on('click', function () {
				alert('Finish Clicked');
			});
			var btnCancel = $('<button></button>').text('Cancel').addClass('btn btn-danger').on('click', function () {
				$('#smartwizard').smartWizard("reset");
			});
			// Step show event
			$("#smartwizard").on("showStep", function (e, anchorObject, stepNumber, stepDirection, stepPosition) {
				$("#prev-btn").removeClass('disabled');
				$("#next-btn").removeClass('disabled');
                $(".sw-btn-next").removeClass('invisible');
                $(".sw-btn-prev").removeClass('invisible');
                $(".sw-btn-next").text('Next');
				if (stepPosition === 'first') {
					$("#prev-btn").addClass('disabled');
                    $(".sw-btn-prev").addClass("invisible");
                    
				} else if (stepPosition === 'last') {
					//$("#next-btn").addClass('disabled');
                    $(".sw-btn-next").text('Finish');
                    $(".sw-btn-next").addClass("btn-form-submit");
                    $(".sw-btn-next").attr('type','submit');
                    $(".sw-btn-next").removeClass('disabled');
				} else {
					$("#prev-btn").removeClass('disabled');
					$("#next-btn").removeClass('disabled');
                    $(".sw-btn-next").removeClass("btn-form-submit");
                    $(".sw-btn-next").attr('type','button');
				}
			});
			// Smart Wizard
			$('#smartwizard').smartWizard({
				selected: 0,
				theme: 'dots',
				transition: {
					animation: 'slide-horizontal', // Effect on navigation, none/fade/slide-horizontal/slide-vertical/slide-swing
				},
				toolbarSettings: {
					toolbarPosition: 'bottom', // both bottom
					toolbarExtraButtons: [btnFinish, btnCancel]
				}
			});
			// External Button Events
			$("#reset-btn").on("click", function () {
				// Reset wizard
				$('#smartwizard').smartWizard("reset");
				return true;
			});
			$("#prev-btn").on("click", function () {
				// Navigate previous
				$('#smartwizard').smartWizard("prev");
				return true;
			});
			$("#next-btn").on("click", function () {
				// Navigate next
				$('#smartwizard').smartWizard("next");
				return true;
			});
			// Demo Button Events
			$("#got_to_step").on("change", function () {
				// Go to step
				var step_index = $(this).val() - 1;
				$('#smartwizard').smartWizard("goToStep", step_index);
				return true;
			});
			$("#is_justified").on("click", function () {
				// Change Justify
				var options = {
					justified: $(this).prop("checked")
				};
				$('#smartwizard').smartWizard("setOptions", options);
				return true;
			});
			$("#animation").on("change", function () {
				// Change theme
				var options = {
					transition: {
						animation: $(this).val()
					},
				};
				$('#smartwizard').smartWizard("setOptions", options);
				return true;
			});
			$("#theme_selector").on("change", function () {
				// Change theme
				var options = {
					theme: $(this).val()
				};
				$('#smartwizard').smartWizard("setOptions", options);
				return true;
			});
		});


        $('.datepicker').pickadate({
			selectMonths: true,
	        selectYears: true
		}),
		$('.timepicker').pickatime()

        $(function () {
			$('#date-time').bootstrapMaterialDatePicker({
				format: 'YYYY-MM-DD HH:mm'
			});
			$('#date').bootstrapMaterialDatePicker({
				time: false
			});
		});

        /* cleint ajax*/
        jQuery(document).ready(function(event){
           
            <?php //$q=1;?>
            @for($q =0 ; $q <= $totalquestion; $q++)
                jQuery(".question_text_{{$q}}").change(function(event){
                    event.preventDefault();
                    var id = jQuery("#id_{{$q}}").val();  
                    var question_text = jQuery(".question_text_{{$q}}").val(); 
                    var question_img = jQuery(".question_img_{{$q}}").files;                      
                    var qid = jQuery("#questionid_{{$q}}").val();
                    jQuery.ajax({
                        url: "{{ url('client/questionsans/') }}",
                        data: {
                            "_token": "{{ csrf_token() }}",
                            "id" : id,
                            "questionid":qid,
                            "question_text": question_text,
                            "qestoin_img":question_img,
                        },
                        type:"post",
                        success:function(result){
                            $('#question_msg{{$q}}').show();
                            $('#question_msg{{$q}}').removeClass( "text-danger" ).addClass('text-success');
                            $('#question_msg{{$q}}').text(result.success);
                        }
                    });
                });
                /* image upload form */
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
             /* preview
                $('#image{{$q}}').change(function () {
                    let reader = new FileReader();
                    
                    reader.onload = (e) => {
                        $('#preview-image-before-upload').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(this.files[0]);
                });
                */
                $('#image-upload{{$q}}').submit(function (e) {
                    e.preventDefault();
                    var qid = jQuery("#id_{{$q}}").val();
                    var formData = new FormData(this); 
                    $.ajax({
                        type: 'POST',
                        url: "{{  url('client/new_questionsans/') }}",
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: (data) => {
                            $('#question_msg{{$q}}').show();
                            $('#question_msg{{$q}}').removeClass( "text-danger" ).addClass('text-success');
                            $('#question_msg{{$q}}').text(data.success);
                        },
                        error: function (data) {
                            console.log(data);
                        }
                    });
                });

                @for($t=1 ; $t <= 10; $t++)
                    jQuery(".question_page_text_{{$t}}_{{$q}}").change(function(event){
                        event.preventDefault();
                        var id = jQuery("#id_{{$t}}").val();  
                        var question_page_text_{{$t}} = jQuery(".question_page_text_{{$t}}_{{$q}}").val();                
                        var qid = jQuery("#questionid_{{$q}}").val();
                        jQuery.ajax({
                            url: "{{ url('client/pagesection_'.$t.'/') }}",
                            data: {
                                "_token": "{{ csrf_token() }}",
                                "id" : id,
                                "questionid":qid,
                                "pagesection_{{$t}}": question_page_text_{{$t}},
                            },
                            type:"post",
                            success:function(result){
                                $('#question_msg_{{$t}}_{{$q}}').show();
                                $('#question_msg_{{$t}}_{{$q}}').removeClass( "text-danger" ).addClass('text-success');
                                $('#question_msg_{{$t}}_{{$q}}').text(result.success);
                            }
                        });
                    });
                    jQuery(".question_section_text_{{$t}}_{{$q}}").change(function(event){
                        event.preventDefault();
                        var id = jQuery("#id_{{$t}}").val();  
                        var question_section_text_{{$t}} = jQuery(".question_section_text_{{$t}}_{{$q}}").val();                
                        var qid = jQuery("#questionid_{{$q}}").val();
                        jQuery.ajax({
                            url: "{{ url('client/pagesectioncontent_'.$t.'/') }}",
                            data: {
                                "_token": "{{ csrf_token() }}",
                                "id" : id,
                                "questionid":qid,
                                "pagesectioncontent_{{$t}}": question_section_text_{{$t}},
                            },
                            type:"post",
                            success:function(result){
                                $('#content_msg_{{$t}}_{{$q}}').show();
                                $('#content_msg_{{$t}}_{{$q}}').removeClass( "text-danger" ).addClass('text-success');
                                $('#content_msg_{{$t}}_{{$q}}').text(result.success);
                            }
                        });
                    });

                    jQuery(".question_referencelink_{{$t}}_{{$q}}").change(function(event){
                        event.preventDefault();
                        var id = jQuery("#id_{{$t}}").val();  
                        var referencelink{{$t}} = jQuery(".question_referencelink_{{$t}}_{{$q}}").val();                
                        var qid = jQuery("#questionid_{{$q}}").val();
                        jQuery.ajax({
                            url: "{{ url('client/referencelink_'.$t.'/') }}",
                            data: {
                                "_token": "{{ csrf_token() }}",
                                "id" : id,
                                "questionid":qid,
                                "referencelink_{{$t}}": referencelink{{$t}},
                            },
                            type:"post",
                            success:function(result){
                                $('#question_msg_{{$t}}_{{$q}}').show();
                                $('#question_msg_{{$t}}_{{$q}}').removeClass( "text-danger" ).addClass('text-success');
                                $('#question_msg_{{$t}}_{{$q}}').text(result.success);
                            }
                        });
                    });

                    /* image upload form */
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                @endfor
                <?php //$q++; ?>
            @endfor
    });
@endsection

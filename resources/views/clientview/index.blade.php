<!doctype html>
<html lang="en">
   <head>
      <!-- Required meta tags -->
      <meta charset="utf-8">
      <meta name="csrf-token" content="{{ csrf_token() }}" />
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!--favicon-->
      <link rel="icon" href="{{URL::to('/')}}/assets/images/favicon-32x32.png" type="image/png" />
      <!--plugins-->
      <link href="{{URL::to('/')}}/assets/plugins/simplebar/css/simplebar.css" rel="stylesheet" />
      <link href="{{URL::to('/')}}/assets/plugins/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet" />
      <link href="{{URL::to('/')}}/assets/plugins/metismenu/css/metisMenu.min.css" rel="stylesheet" />
      <link href="{{URL::to('/')}}/assets/plugins/datatable/css/dataTables.bootstrap5.min.css" rel="stylesheet" />
      @yield('loadstyles')
      <!-- loader-->
      <link href="{{URL::to('/')}}/assets/css/pace.min.css" rel="stylesheet" />
      <script src="{{URL::to('/')}}/assets/js/pace.min.js"></script>
      <!-- Bootstrap CSS -->
      <link href="{{URL::to('/')}}/assets/css/bootstrap.min.css" rel="stylesheet">
      <link href="{{URL::to('/')}}/assets/css/app.css" rel="stylesheet">
      <link href="{{URL::to('/')}}/assets/css/icons.css" rel="stylesheet">
      <!-- Theme Style CSS -->
      <!-- <link rel="stylesheet" href="{{URL::to('/')}}/assets/css/dark-theme.css" />
         <link rel="stylesheet" href="{{URL::to('/')}}/assets/css/semi-dark.css" />
         
         <link rel="stylesheet" href="{{URL::to('/')}}/assets/css/header-colors.css" /> -->
      <style>
         .page-footer {
            left: 0;
         }
         .page-wrapper {
            margin-left: 0;
         }
      </style> 
      <title>@yield('title')</title>
   </head>
   <body class="bg-theme bg-theme2">
      <!--wrapper-->
      <div class="wrapper">
         <!--sidebar wrapper -->
         {{--<div class="sidebar-wrapper" data-simplebar="true">
            <div class="sidebar-header">
               <div>
                  <img src="{{URL::to('/')}}/assets/images/logo-icon.png" class="logo-icon" alt="logo icon">
               </div>
               <div>
                  <h4 class="logo-text">neshallWeb</h4>
               </div>
               <div class="toggle-icon ms-auto">
                  <i class='bx bx-arrow-to-left'></i>
               </div>
            </div>
            <!--navigation-->
             <ul class="metismenu" id="menu">
               <li>
                  <a href="{{ url('client/edit') }}/{{ session('clientid') }}" class="">
                     <div class="parent-icon"><i class='bx bx-home-circle'></i>
                     </div>
                     <div class="menu-title">Client Details </div>
                  </a>
               </li>
               <li>
                  <a href="{{ url('client/service') }}/{{ session('clientid') }}" class="">
                     <div class="parent-icon"><i class='bx bx-home-circle'></i>
                     </div>
                     <div class="menu-title">Acquisition Form</div>
                  </a>
               </li>
            </ul>
            <!--end navigation-->
         </div> --}}
         <!--end sidebar wrapper -->
         <!--start page wrapper -->
         <div class="page-wrapper">
            <div class="page-content">
               @yield('content')
            </div>
         </div>
         <!--end page wrapper -->
         <!--start overlay-->
         <div class="overlay toggle-icon"></div>
         <!--end overlay-->
         <!--Start Back To Top Button--> <a href="javaScript:;" class="back-to-top"><i class='bx bxs-up-arrow-alt'></i></a>
         <!--End Back To Top Button-->
         <footer class="page-footer" >
            <p class="mb-0">Copyright © {{ date('Y')}}. All right reserved.</p>
         </footer>
      </div>
      <!--end wrapper-->
      <!--start switcher-->
      <!--end switcher-->
      <!-- Bootstrap JS -->
      <script src="{{URL::to('/')}}/assets/js/bootstrap.bundle.min.js"></script>
      <!--plugins-->
      <script src="{{URL::to('/')}}/assets/js/jquery.min.js"></script>
      @yield('loadscript')
      <script>
         $(document).ready(function() {
         
             @yield('scripts')
         
           } );
         
      </script>
      <!--app JS-->
      <script src="{{URL::to('/')}}/assets/js/app.js"></script>
      <script></script>
   </body>
</html>
@extends('clientview.index')

@section('title')
    Client add
@endsection()

@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <div class="card">
        <div class="card-body">
            <div class="row">
                <h1>Your Details</h1>
                <hr/>
                @foreach ($answers as $answer)
                    <?php $q_type = '';?>
                    <div class="col-md-12">
                        <h6 for="validationCustom01" class="form-label">
                            @foreach ($questions as $question)
                                @if($question->id == $answer->questionid)
                                    {{$question->question}}
                                    <?php $q_type = $question->type;?>
                                @endif
                            @endforeach
                        </h6>
                        <?php
                        if( $q_type == 6 ) {
                            if( $answer->page_1 != '' ) {
                                ?>
                                 <label>Page Title : {{$answer->page_1}}</label><br>
                                <?php
                            }
                            if( $answer->page_section_1 != '' ) {
                                ?>
                                 <label>Section : {{$answer->page_section_1}}</label><br>
                                <?php
                            }
                            if( $answer->page_2 != '' ) {
                                ?>
                                 <label>Page Title : {{$answer->page_2}}</label><br>
                                <?php
                            }
                            if( $answer->page_section_2 != '' ) {
                                ?>
                                 <label>Section : {{$answer->page_section_2}}</label><br>
                                <?php
                            }
                            if( $answer->page_3 != '' ) {
                                ?>
                                 <label>Page Title : {{$answer->page_3}}</label><br>
                                <?php
                            }
                            if( $answer->page_section_3 != '' ) {
                                ?>
                                 <label>Section : {{$answer->page_section_3}}</label><br>
                                <?php
                            }
                            if( $answer->page_4 != '' ) {
                                ?>
                                 <label>Page Title : {{$answer->page_4}}</label><br>
                                <?php
                            }
                            if( $answer->page_section_4 != '' ) {
                                ?>
                                 <label>Section : {{$answer->page_section_4}}</label><br>
                                <?php
                            }
                            if( $answer->page_5 != '' ) {
                                ?>
                                 <label>Page Title : {{$answer->page_5}}</label><br>
                                <?php
                            }
                            if( $answer->page_section_5 != '' ) {
                                ?>
                                 <label>Section : {{$answer->page_section_5}}</label><br>
                                <?php
                            }
                            if( $answer->page_6 != '' ) {
                                ?>
                                 <label>Page Title : {{$answer->page_6}}</label><br>
                                <?php
                            }
                            if( $answer->page_section_6 != '' ) {
                                ?>
                                 <label>Section : {{$answer->page_section_6}}</label><br>
                                <?php
                            }
                            if( $answer->page_7 != '' ) {
                                ?>
                                 <label>Page Title : {{$answer->page_7}}</label><br>
                                <?php
                            }
                            if( $answer->page_section_7 != '' ) {
                                ?>
                                 <label>Section : {{$answer->page_section_7}}</label><br>
                                <?php
                            }
                            if( $answer->page_8 != '' ) {
                                ?>
                                 <label>Page Title : {{$answer->page_8}}</label><br>
                                <?php
                            }
                            if( $answer->page_section_8 != '' ) {
                                ?>
                                 <label>Section : {{$answer->page_section_8}}</label><br>
                                <?php
                            }
                            if( $answer->page_9 != '' ) {
                                ?>
                                 <label>Page Title : {{$answer->page_9}}</label><br>
                                <?php
                            }
                            if( $answer->page_section_9 != '' ) {
                                ?>
                                 <label>Section : {{$answer->page_section_9}}</label><br>
                                <?php
                            }
                            if( $answer->page_10 != '' ) {
                                ?>
                                 <label>Page Title : {{$answer->page_10}}</label><br>
                                <?php
                            }
                            if( $answer->page_section_10 != '' ) {
                                ?>
                                 <label>Section : {{$answer->page_section_10}}</label><br>
                                <?php
                            }
                        }
                        if( $q_type == 7 ) {
                            if( $answer->reference_link_1 != '' ) {
                                ?>
                                 <label>{{$answer->reference_link_1}}</label><br>
                                <?php
                            }
                            if( $answer->reference_link_2 != '' ) {
                                ?>
                                 <label>{{$answer->reference_link_2}}</label><br>
                                <?php
                            }
                            if( $answer->reference_link_3 != '' ) {
                                ?>
                                 <label>{{$answer->reference_link_3}}</label><br>
                                <?php
                            }
                            if( $answer->reference_link_4 != '' ) {
                                ?>
                                 <label>{{$answer->reference_link_4}}</label><br>
                                <?php
                            }
                            if( $answer->reference_link_5 != '' ) {
                                ?>
                                 <label>{{$answer->reference_link_5}}</label><br>
                                <?php
                            }
                        }
                        ?>
                        <label>{{$answer->answer}}</label>
                    </div>
                @endforeach
                <div class="col-xl-9 mx-auto">
                        <button class="btn btn-light btn-edit">Edit</button>
                    <a href="{{URL::to('/')}}/submit-question" onclick="return confirm('Are you checked your data?')">
                        <button class="btn btn-light">Submit</button>
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection()

@section('loadstyles')
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link href="{{ asset('assets/plugins/smart-wizard/css/smart_wizard_all.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/datetimepicker/css/classic.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/plugins/smart-wizard/css/smart_wizard_all.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/datetimepicker/css/classic.date.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.min.css') }}">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <!-- <link href="{{URL::to('/')}}/assets/plugins/fancy-file-uploader/fancy_fileupload.css" rel="stylesheet" />
    <link href="{{URL::to('/')}}/assets/plugins/Drag-And-Drop/dist/imageuploadify.min.css" rel="stylesheet" />
    <link href="{{URL::to('/')}}/assets/plugins/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet" /> -->
@endsection()
@section('loadscript')
    <script src="{{URL::to('/')}}/assets/plugins/simplebar/js/simplebar.min.js"></script>
    <script src="{{URL::to('/')}}/assets/plugins/metismenu/js/metisMenu.min.js"></script>
    <script src="{{URL::to('/')}}/assets/plugins/perfect-scrollbar/js/perfect-scrollbar.js"></script>
    <script src="{{ asset('assets/plugins/smart-wizard/js/jquery.smartWizard.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datetimepicker/js/legacy.js') }}"></script>
    <script src="{{ asset('assets/plugins/datetimepicker/js/picker.js') }}"></script>
    <script src="{{ asset('assets/plugins/datetimepicker/js/picker.time.js') }}"></script>
    <script src="{{ asset('assets/plugins/datetimepicker/js/picker.date.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-material-datetimepicker/js/moment.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.min.js') }}"></script>
    <!-- <script src="{{URL::to('/')}}/assets/plugins/fancy-file-uploader/jquery.ui.widget.js"></script>
    <script src="{{URL::to('/')}}/assets/plugins/fancy-file-uploader/jquery.fileupload.js"></script>
    <script src="{{URL::to('/')}}/assets/plugins/fancy-file-uploader/jquery.iframe-transport.js"></script>
    <script src="{{URL::to('/')}}/assets/plugins/fancy-file-uploader/jquery.fancy-fileupload.js"></script>
    <script src="{{URL::to('/')}}/assets/plugins/Drag-And-Drop/dist/imageuploadify.min.js"></script> -->
    <script type="text/javascript">
        function gotoPrev(){
            window.location = document.referrer;
        }
    </script>
@endsection()
@section('scripts')
    jQuery('.btn-edit').click(function(){
        var referrer =  document.referrer;
        window.history.back();
    });
@endsection
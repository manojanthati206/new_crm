@extends('clientview.index')

@section('title')
    Client add
@endsection()

@section('content')
@if ($errors->any())
    <div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
{{-- <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
   <div class="breadcrumb-title pe-3">Add New Client</div>
   <div class="ps-3">
      <nav aria-label="breadcrumb">
         <ol class="breadcrumb mb-0 p-0">
            <li class="breadcrumb-item"><a href="javascript:void(0);"><i class="bx bx-home-alt"></i></a>
            </li>
            <li class="breadcrumb-item active" aria-current="page">Client Add</li>
         </ol>
      </nav>
   </div>
</div> --}}
<div class="row">
   <div class="col-xl-9 mx-auto">
      <hr/>
      <div class="card">
         <div class="card-body">
            <div class="p-3 p-md-4 border rounded">
               <div id="smartwizard" class="sw-theme-dark">
                  <ul class="nav">
                     <li class="nav-item">
                        <a class="nav-link" data-val="step-1" href="#step-1">	<strong>Step 1</strong> </a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link" data-val="step-2" href="#step-2">	<strong>Step 2</strong> </a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link" data-val="step-3" href="#step-3">	<strong>Step 3</strong> </a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link" data-val="step-4" href="#step-4">	<strong>Step 4</strong> </a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link" data-val="step-5" href="#step-5"> <strong>Step 5</strong> </a>
                     </li>
                  </ul>
                  <form method="post" action="{{ route('clients-update') }}" class="row g-3 needs-validation" enctype="multipart/form-data"  novalidate>
                     @csrf
                     @method('PUT')
                     <input type="hidden" name="id" id="cid"  value="{{$client->id}}">
                     <div class="tab-content">
                        <div id="step-1" class="tab-pane" role="tabpanel" aria-labelledby="step-1">
                           <h3>Full Name : <span id="c_fname">{{ $client->first_name }}</span> <span id="c_lname">{{ $client->last_name }}</span></h3>
                           <div class="col-md-12 mt-2" >
                              <label for="validationCustom01" class="form-label">First Name *</label> 
                              <input type="text"  class="form-control first_name" id="validationCustom01" value="{{ $client->first_name }}" name="first_name" placeholder="First Name" required>
                              <div  style="visibility: hidden;" id="first_name" >Error</div>
                           </div>
                           <div class="col-md-12 mt-2">
                              <label for="validationCustom01" class="form-label">Last Name *</label>
                              <input type="text" class="form-control last_name" id="validationCustom01" value="{{ $client->last_name   }}" name="last_name" placeholder="Last Name" required>
                              <div  style="visibility: hidden;" id="last_name" >Error</div>
                           </div>
                           <br/>
                           <style type="text/css">
                              [type="date"] {
                                 border-radius: 5px;
                                 display: block;
                                 padding: 0.375rem 0.75rem;
                                 font-size: 1rem;
                                 font-weight: 400;
                                 line-height: 1.5;
                                 color: #ffffff;
                                 background-color: rgb(173 173 173 / 15%);
                                 background-clip: padding-box;
                                 border: 1px solid #90909073;
                                 -webkit-appearance: none;
                                 -moz-appearance: none;
                                 appearance: none;
                                 border-radius: 0.25rem;
                                 transition: border-color .15s ease-in-out, box-shadow .15s ease-in-out;
                              }
                           </style>
                           <label for="validationCustom01" class="form-label"><b>Personal Information</b></label>
                           <div class="col-md-12 mt-2">
                              <label class="form-label">Date Of Birth</label>
                              <?php 
                              if(empty($client->date_of_birth)) {
                                 $client->date_of_birth = '01-01-2000';
                              }
                              ?>
                              <input type="date" class="dob" id="birthday" name="date_of_birth" value="<?php echo date('Y-m-d',strtotime($client->date_of_birth) ) ?>"  placeholder="Date of Birth..." >
                              <!-- <input class=" form-control result datepicker  dob" type="text"  name="date_of_birth" value="<?php echo date('d F, Y',strtotime($client->date_of_birth) ) ?>" required> -->
                              <div  style="visibility: hidden;" id="dob" >Error</div>
                           </div>
                           <br/>
                        </div>
                        <div id="step-2" class="tab-pane" role="tabpanel" aria-labelledby="step-3">
                           <h3>Contact Information *</h3>
                           <div class="col-md-12 mt-2">
                              <label for="validationCustom01" class="form-label">Mobile No. *</label>
                              <input type="text" class="form-control mobile" id="validationCustom01" value="{{  $client->mobile }}" name="mobile" placeholder="Mobile Number" required>
                              <div  style="visibility: hidden;" id="mobile" >Error</div>
                           </div>
                           <div class="col-md-12 mt-2">
                              <label for="validationCustom01" class="form-label">Landline </label>
                              <input type="text" class="form-control landline" id="validationCustom01" value="{{  $client->landline }}" name="landline" placeholder="Landline" >
                              <div  style="visibility: hidden;" id="landline" >Error</div>
                           </div>
                           <div class="col-md-12 mt-2">
                              <label for="validationCustom01" class="form-label">Email ID *</label>
                              <input type="email" class="form-control emailid" id="validationCustom01" value="{{  $client->email_id }}" name="email_id" placeholder="Email ID" required>
                              <div  style="visibility: hidden;" id="emailid" >Error</div>
                           </div>
                           <div class="col-md-12 mt-2">
                              <label for="inputState" class="form-label">Prefered Communication Platform</label>
                              <select id="inputState" class="form-select prefered_communication_platform" name="prefered_communication_platform">
                                 <option value="">Choose...</option>
                                 <option selected value="Skype">Skype</option>
                                 <option value="WhatsApp">WhatsApp</option>
                                 <option value="Telegram">Telegram</option>
                              </select>
                              <div  style="visibility: hidden;" id="prefered_communication_platform" >Error</div>
                           </div>
                           <div class="col-md-12 mt-2">
                              <label for="validationCustom01" class="form-label">Skype ID | WhatsApp/Telegram Number  </label>
                              <input type="text" class="form-control skypeid" id="validationCustom01" value="{{  $client->skype_id }}" name="skype_id" placeholder="Skype ID | WhatsApp/Telegram Number" >
                              <div  style="visibility: hidden;" id="skypeid" >Error</div>
                           </div>
                        </div>
                        <div id="step-3" class="tab-pane" role="tabpanel" aria-labelledby="step-2">
                           <h3>Residential/PermanentAddress *</h3>
                           <div class="col-md-12 mt-2">
                              <label for="validationCustom01" class="form-label">Address1 *</label>
                              <input type="text" class="form-control address1" id="validationCustom01" value="{{  $client->address1 }}" name="address1" placeholder="House No./Flat No./Society" required>
                              <div  style="visibility: hidden;" id="address1" >Error</div>
                           </div>
                           <div class="col-md-12 mt-2">
                              <label for="validationCustom01" class="form-label">Address2 *</label>
                              <input type="text" class="form-control address2" id="validationCustom01" value="{{  $client->address2 }}" name="address2" placeholder="Street Name, etc" required>
                              <div  style="visibility: hidden;" id="address2" >Error</div>
                           </div>
                           <div class="col-md-12 mt-2">
                              <label for="validationCustom01" class="form-label">Address3 </label>
                              <input type="text" class="form-control address3" id="validationCustom01" value="{{  $client->address3 }}" name="address3" placeholder="Near By" >
                              <div  style="visibility: hidden;" id="address3" >Error</div>
                           </div>
                           <div class="col-md-12 mt-2">
                              <label for="validationCustom01" class="form-label">Landmark </label>
                              <input type="text" class="form-control landmark" id="validationCustom01" value="{{  $client->landmark }}" name="landmark" placeholder="Landmark" >
                              <div  style="visibility: hidden;" id="landmark" >Error</div>
                           </div>
                           <div class="row">
                              <div class="col-md-6 mt-2">
                                 <label for="validationCustom01" class="form-label">Area/City *</label> 
                                 <input type="text" class="form-control city" id="validationCustom01" value="{{  $client->area_city }}" name="area_city" placeholder="Area / City" required>
                                 <div  style="visibility: hidden;" id="city" >Error</div>
                              </div>
                              <div class="col-md-6 mt-2">
                                 <label for="validationCustom01" class="form-label">PinCode *</label>
                                 <input type="tel" class="form-control pincode" id="validationCustom01" value="{{  $client->pincode }}" name="pincode" placeholder="Pin Code" required>
                                 <div  style="visibility: hidden;" id="pincode" >Error</div>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-md-6 mt-2">
                                 <label for="validationCustom01" class="form-label">State *</label>
                                 <input type="text" class="form-control state" id="validationCustom01" value="{{  $client->state }}" name="state" placeholder="State" required>
                                 <div  style="visibility: hidden;" id="state" >Error</div>
                              </div>
                              <div class="col-md-6 mt-2">
                                 <label for="validationCustom01" class="form-label">Country *</label>
                                 <input type="tel" class="form-control country" id="validationCustom01" value="{{  $client->country }}" name="country" placeholder="Country" required>
                                 <div  style="visibility: hidden;" id="country" >Error</div>
                              </div>
                           </div>
                        </div>
                        <div id="step-4" class="tab-pane" role="tabpanel" aria-labelledby="step-3">
                           <h3>Business Information</h3>
                           <div class="col-md-12 mt-2">
                              <label for="validationCustom01" class="form-label">Registered Business Name</label>
                              <input type="text" class="form-control business_name" id="validationCustom01" value="{{  $client->business_name }}" name="business_name" placeholder="Business Name">
                              <div  style="visibility: hidden;" id="business_name" >Error</div>
                           </div>
                           <div class="col-md-12 mt-2">
                              <label for="validationCustom01" class="form-label">Company Number</label>
                              <input type="text" class="form-control company_number" id="validationCustom01" value="{{  $client->company_number }}" name="company_number" placeholder="Company Number">
                              <div  style="visibility: hidden;" id="company_number" >Error</div>
                           </div>
                           <div class="col-md-12 mt-2">
                              <label for="validationCustom01" class="form-label">Driving Licence</label>
                              <input type="text" class="form-control driving_licence" id="validationCustom01" value="{{  $client->driving_licence }}" name="driving_licence" placeholder="Driving Licence">
                              <div  style="visibility: hidden;" id="driving_licence" >Error</div>
                           </div>

                           <div class="col-md-12 mt-2">
                              <div class="form-check form-switch">
                                 <input class="form-check-input" type="checkbox" id="flexSwitchCheckDefault" name="business_same_as_resedential" value="no">
                                 <label class="form-check-label" for="flexSwitchCheckDefault">Business same as resedential</label>
                              </div>
                              <div  style="visibility: hidden;" id="business_same_as_resedential" >Error</div>
                           </div>
                           <div class="col-md-12 mt-2 business-address" >
                              <label for="validationCustom01" class="form-label">Business Address1</label>
                              <input type="text" class="form-control business_address1" id="validationCustom01" value="{{  $client->business_address1 }}" name="business_address1" placeholder="House No./Flat No./Society">
                              <div  style="visibility: hidden;" id="business_address1" >Error</div>
                           </div>
                           <div class="col-md-12 mt-2 business-address" >
                              <label for="validationCustom01" class="form-label">Business Address2</label>
                              <input type="text" class="form-control business_address2" id="validationCustom01" value="{{  $client->business_address2 }}" name="business_address2" placeholder="Street Name, etc">
                              <div  style="visibility: hidden;" id="business_address2" >Error</div>
                           </div>
                           <!--   -->
                        </div>
                        <div id="step-5" class="tab-pane" role="tabpanel" aria-labelledby="step-4">
                           <h3>Signatory Information</h3>
                           <div class="col-md-12 mt-2">
                              <label for="validationCustom01" class="form-label">Signature Name</label>
                              <strong class="c_fname">{{ $client->first_name }}</strong> <strong class="c_lname">{{ $client->last_name }}</strong>
                           </div>
                           <div class="col-md-12 mt-2">
                              <label for="validationCustom01" class="form-label">Signature Email</label>
                              <strong class="email_id">{{  $client->email_id }}</strong>
                           </div>
                           <div class="col-md-12 mt-2">
                              <label for="validationCustom01" class="form-label"><span class="c_fname">{{ $client->first_name }}</span> <span class="c_lname">{{ $client->last_name }}</span> Designation </label>
                              <input type="text" class="form-control sign1_designation" id="validationCustom01" value="{{  $client->sign1_designation }}" name="sign1_designation" placeholder="Signature Designation" >
                              <div  style="visibility: hidden;" id="sign1_designation" >Error</div>
                           </div>
                           <div class="col-md-12 mt-2">
                              <button class="btn btn-light" type="button" id="btn-add-signatory" data-id="1">Add signatory member</button>
                           </div>
                           <div class="col-md-12 mt-2 sign2">
                              <label for="validationCustom01" class="form-label">Signature Name 2</label>
                              <input type="text" class="form-control sign2_name" id="validationCustom01" value="{{  $client->sign2_name }}" name="sign2_name" placeholder="Signature Name" >
                              <div  style="visibility: hidden;" id="sign2_name" >Error</div>
                           </div>
                           <div class="col-md-12 mt-2 sign2">
                              <label for="validationCustom01" class="form-label">Signature Email 2 </label>
                              <input type="text" class="form-control sign2_email" id="validationCustom01" value="{{  $client->sign2_email }}" name="sign2_email" placeholder="Signature Email" >
                              <div  style="visibility: hidden;" id="sign2_email" >Error</div>
                           </div>
                           <div class="col-md-12 mt-2 sign2">
                              <label for="validationCustom01" class="form-label">Signature Designation 2 </label>
                              <input type="text" class="form-control sign2_designation" id="validationCustom01" value="{{  $client->sign2_designation }}" name="sign2_designation" placeholder="Signature Designation" >
                              <div  style="visibility: hidden;" id="sign2_designation" >Error</div>
                           </div>
                           <div class="col-md-12 mt-2 sign2">
                              <button class="btn btn-light" type="button" id="btn-remove-signatory2" data-id="1">Remove</button>
                           </div>
                           <hr class="sign2">
                           <div class="col-md-12 mt-2 sign3">
                              <label for="validationCustom01" class="form-label">Signature Name 3</label>
                              <input type="text" class="form-control sign3_name" id="validationCustom01" value="{{  $client->sign3_name }}" name="sign3_name" placeholder="Signature Name" >
                              <div  style="visibility: hidden;" id="sign3_name" >Error</div>
                           </div>
                           <div class="col-md-12 mt-2 sign3">
                              <label for="validationCustom01" class="form-label">Signature Email 3</label>
                              <input type="text" class="form-control sign3_email" id="validationCustom01" value="{{  $client->sign3_email }}" name="sign3_email" placeholder="Signature Email" >
                              <div  style="visibility: hidden;" id="sign3_email" >Error</div>
                           </div>
                           <div class="col-md-12 mt-2 sign3">
                              <label for="validationCustom01" class="form-label">Signature Designation 3 </label>
                              <input type="text" class="form-control sign3_designation" id="validationCustom01" value="{{  $client->sign3_designation }}" name="sign3_designation" placeholder="Signature Designation" >
                              <div  style="visibility: hidden;" id="sign3_designation" >Error</div>
                           </div>
                           <div class="col-md-12 mt-2 sign3">
                              <button class="btn btn-light" type="button" id="btn-remove-signatory3" data-id="1">Remove</button>
                           </div>
                           <hr class="sign3">
                           <?php
                           if ( isset($client_type) && $client_type == 'indian'){
                              ?> 
                              <h3>Tax Information</h3>
                              <div class="col-md-12 mt-2">
                                 <label for="validationCustom01" class="form-label">Pancard</label>
                                 <input type="text" class="form-control pancard" id="validationCustom01" value="{{  $client->pancard }}" name="pancard" placeholder="Pancard" >
                                 <div  style="visibility: hidden;" id="pancard" >Error</div>
                              </div>
                              <div class="col-md-12 mt-2">
                                 <label for="validationCustom01" class="form-label">GSTIN </label>
                                 <input type="text" class="form-control gstin" id="validationCustom01" value="{{  $client->gstin }}" name="gstin" placeholder="GSTIN" >
                                 <div  style="visibility: hidden;" id="gstin" >Error</div>
                              </div>
                              <?php   
                           }
                           ?>
                           <input type="hidden" name="client_type" value="{{  $client_type }}">
                           <input type="hidden" name="client_form" value="true">
                           {{-- <div class="col-md-12  text-center">
                                <a href="{{ url('client/service') }}/{{ session('clientid') }}" class='btn btn-light '>Go Qusetion</a>
                           </div>  --}}
                          
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection()

@section('loadstyles')
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link href="{{ asset('assets/plugins/smart-wizard/css/smart_wizard_all.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/datetimepicker/css/classic.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/plugins/smart-wizard/css/smart_wizard_all.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/datetimepicker/css/classic.date.css') }}" rel="stylesheet" />
	<link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.min.css') }}">
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <!-- <link href="{{URL::to('/')}}/assets/plugins/fancy-file-uploader/fancy_fileupload.css" rel="stylesheet" />
    <link href="{{URL::to('/')}}/assets/plugins/Drag-And-Drop/dist/imageuploadify.min.css" rel="stylesheet" />
    <link href="{{URL::to('/')}}/assets/plugins/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet" /> -->
    <style>
        /*.toolbar-bottom button:nth-child(3),*/.toolbar-bottom button:nth-child(4) {
            display: none !important;
        }
        .text-success, .text-danger{
            position: sticky;
        }
        .picker--focused .picker__day--selected {
            color: #393737 !important;
        }
        /*@media (min-width: 320px) and (max-width: 480px) {
            .nav{
                display: none !important;
            }
        }*/
    </style>
@endsection()
@section('loadscript')
    <script src="{{URL::to('/')}}/assets/plugins/simplebar/js/simplebar.min.js"></script>
    <script src="{{URL::to('/')}}/assets/plugins/metismenu/js/metisMenu.min.js"></script>
    <script src="{{URL::to('/')}}/assets/plugins/perfect-scrollbar/js/perfect-scrollbar.js"></script>
    <script src="{{ asset('assets/plugins/smart-wizard/js/jquery.smartWizard.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datetimepicker/js/legacy.js') }}"></script>
	<script src="{{ asset('assets/plugins/datetimepicker/js/picker.js') }}"></script>
	<script src="{{ asset('assets/plugins/datetimepicker/js/picker.time.js') }}"></script>
	<script src="{{ asset('assets/plugins/datetimepicker/js/picker.date.js') }}"></script>
	<script src="{{ asset('assets/plugins/bootstrap-material-datetimepicker/js/moment.min.js') }}"></script>
	<script src="{{ asset('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.min.js') }}"></script>
    <!-- <script src="{{URL::to('/')}}/assets/plugins/fancy-file-uploader/jquery.ui.widget.js"></script>
    <script src="{{URL::to('/')}}/assets/plugins/fancy-file-uploader/jquery.fileupload.js"></script>
    <script src="{{URL::to('/')}}/assets/plugins/fancy-file-uploader/jquery.iframe-transport.js"></script>
    <script src="{{URL::to('/')}}/assets/plugins/fancy-file-uploader/jquery.fancy-fileupload.js"></script>
    <script src="{{URL::to('/')}}/assets/plugins/Drag-And-Drop/dist/imageuploadify.min.js"></script> -->
@endsection()
@section('scripts')
   $('.sign2').css('display','none');
   $('.sign3').css('display','none');
   jQuery('.nav-link').focus(function(){
      var steps = $(this).data('val');
      if(steps == 'step-4'){

      }
   });
   $('#btn-remove-signatory2').click(function(){
         $('.sign2').css('display','none');
         $('#btn-add-signatory').data('id','1');
         var tab_height = $('.tab-content').height();
         $('.tab-content').css('height',tab_height-440);
   });
   $('#btn-remove-signatory3').click(function(){
         $('.sign3').css('display','none');
         $('#btn-add-signatory').data('id','2');
         var tab_height = $('.tab-content').height();
         $('.tab-content').css('height',tab_height-380);
   });
   $('#btn-add-signatory').click(function(){
      var vl = $(this).data('id');
      var tab_height = $('.tab-content').height();
      if( vl == '1' ) {
         $(this).data('id','2');
         //$(this).attr('data-id','2');
         $('.sign2').css('display','block');
         $('.tab-content').css('height',tab_height+440);
      } else if( vl == '2' ) {
         //$(this).attr('data-id','3');
         $(this).data('id','3');
         $('.sign3').css('display','block');
         $('.tab-content').css('height',tab_height+380);
      }
   });
   $('#flexSwitchCheckDefault').change(function(){
      var this_val;
      if( $(this).prop('checked') == true ) {
         this_val = true;
         $('.business-address').css('display','none');
         $(this).val('yes');
      } else {
         this_val = false;
         $('.business-address').css('display','block');
         $(this).val('no');
      }
   });
    var forms = document.querySelectorAll('.needs-validation')

    // Loop over them and prevent submission
    Array.prototype.slice.call(forms)
    .forEach(function (form) {
    form.addEventListener('submit', function (event) {
        if (!form.checkValidity()) {
        event.preventDefault()
        event.stopPropagation()
        }

        form.classList.add('was-validated')
    }, false)
    })
    /*-- $('#fancy-file-upload').FancyFileUpload({
        params: {
            action: 'fileuploader'
        },
        maxfilesize: 1000000
    });
    $('#image-uploadify').imageuploadify(); */

			// Toolbar extra buttons
		    var btnFinish = $('<button></button>').text('Finish').addClass('btn btn-info').on('click', function () {
			});
			var btnCancel = $('<button></button>').text('Cancel').addClass('btn btn-danger').on('click', function () {
				$('#smartwizard').smartWizard("reset");
			})
			// Step show event
			$("#smartwizard").on("showStep", function (e, anchorObject, stepNumber, stepDirection, stepPosition) {
				$("#prev-btn").removeClass('disabled');
				$("#next-btn").removeClass('disabled');
            $(".sw-btn-next").removeClass('invisible');
             $(".sw-btn-prev").removeClass('invisible');
             
             if (stepPosition === 'first') {
                 $("#prev-btn").addClass('disabled');
                 $(".sw-btn-prev").addClass("invisible");
                 $(".btn-info").addClass('disabled');
             } else if (stepPosition === 'last') {
                // $(".sw-btn-next").addClass("invisible");
                /* $( ".sw-btn-next" ).after( "<a href='' class='btn'>Go Qusetion</a>" );*/
                 $("#next-btn").addClass('invisible');
                 $(".btn-info").removeClass('disabled');
             } else {
                 $("#prev-btn").removeClass('disabled');
                 $("#next-btn").removeClass('disabled');
             }
			});
			// Smart Wizard
			$('#smartwizard').smartWizard({
				selected: 0,
				theme: 'dots',
				transition: {
					animation: 'slide-horizontal', // Effect on navigation, none/fade/slide-horizontal/slide-vertical/slide-swing
				},
				toolbarSettings: {
					toolbarPosition: 'bottom', // both bottom
					toolbarExtraButtons: [btnFinish, btnCancel]
				}
			});
			// External Button Events
			$("#reset-btn").on("click", function () {
				// Reset wizard
				$('#smartwizard').smartWizard("reset");
				return true;
			});
			$("#prev-btn").on("click", function () {
				// Navigate previous
				$('#smartwizard').smartWizard("prev");
				return true;
			});
			$("#next-btn").on("click", function () {
				// Navigate next
				$('#smartwizard').smartWizard("next");
				return true;
			});
			// Demo Button Events
			$("#got_to_step").on("change", function () {
				// Go to step
				var step_index = $(this).val() - 1;
				$('#smartwizard').smartWizard("goToStep", step_index);
				return true;
			});
			$("#is_justified").on("click", function () {
				// Change Justify
				var options = {
					justified: $(this).prop("checked")
				};
				$('#smartwizard').smartWizard("setOptions", options);
				return true;
			});
			$("#animation").on("change", function () {
				// Change theme
				var options = {
					transition: {
						animation: $(this).val()
					},
				};
				$('#smartwizard').smartWizard("setOptions", options);
				return true;
			});
			$("#theme_selector").on("change", function () {
				// Change theme
				var options = {
					theme: $(this).val()
				};
				$('#smartwizard').smartWizard("setOptions", options);
				return true;
			});


        $('.datepicker').pickadate({
			selectMonths: true,
	        selectYears: true
		}),
		$('.timepicker').pickatime()

        $(function () {
			$('#date-time').bootstrapMaterialDatePicker({
				format: 'YYYY-MM-DD HH:mm'
			});
			$('#date').bootstrapMaterialDatePicker({
				time: false
			});
		});

        /* cleint ajax*/
        jQuery(document).ready(function(event){
            // name display on after heading
            jQuery(".first_name").keyup(function(){
                let fname = jQuery(".first_name").val();        
                $('#c_fname').text(fname);
                $('.c_fname').text(fname);
            });
            jQuery(".last_name").keyup(function(){
                let lname = jQuery(".last_name").val();        
                $('#c_lname').text(lname);
                $('.c_lname').text(lname);
            }); 
            jQuery(".emailid").keyup(function(){
                let email_id = jQuery(".emailid").val();        
                $('.email_id').text(email_id);
            }); 

            jQuery(".first_name").change(function(event){
                event.preventDefault();
                var first_name = jQuery(".first_name").val();  
                var cid = jQuery("#cid").val();      
                jQuery.ajax({
                    url: "{{ url('client/first_name/') }}/"+cid,
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "first_name": first_name
                    },
                    type:"post",
                    success:function(result){
                        $('#first_name').css('visibility', 'visible');
                        $('#first_name').removeClass( "text-danger" ).addClass('text-success');
                        $('#first_name').text(result.success);
                    },
                    error:function (response) {
                        $('#first_name').css('visibility', 'visible');
                        $('#first_name').removeClass( "text-success" ).addClass('text-danger');
                        $('#first_name').text(response.responseJSON.errors.first_name);
                    }
                });
            });

            jQuery(".last_name").change(function(event){
                event.preventDefault();
                var last_name = jQuery(".last_name").val();        
                jQuery.ajax({
                    url: "{{ url('client/last_name/'.$client->id) }}",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "last_name": last_name
                    },
                    type:"post",
                    success:function(result){
                        $('#last_name').css('visibility', 'visible');
                        $('#last_name').removeClass( "text-danger" ).addClass('text-success');
                        $('#last_name').text(result.success);
                    },
                    error:function (response) {
                        $('#last_name').css('visibility', 'visible');
                        $('#last_name').removeClass( "text-success" ).addClass('text-danger');
                        $('#last_name').text(response.responseJSON.errors.last_name);
                    }
                });
            });
        
        jQuery(".dob").change(function(event){
            event.preventDefault();
            var dob = jQuery(".dob").val();        
            jQuery.ajax({
                url: "{{ url('client/dob/'.$client->id) }}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "dob": dob
                },
                type:"post",
                success:function(result){
                    $('#dob').css('visibility', 'visible');
                    $('#dob').removeClass( "text-danger" ).addClass('text-success');
                    $('#dob').text(result.success);
                },
                error:function (response) {
                    $('#dob').css('visibility', 'visible');
                    $('#dob').removeClass( "text-success" ).addClass('text-danger');
                    $('#dob').text(response.responseJSON.errors.date_of_birth);
                }
            });
        });

        jQuery(".address1").change(function(event){
            event.preventDefault();
            var address1 = jQuery(".address1").val();        
            jQuery.ajax({
                url: "{{ url('client/address1/'.$client->id) }}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "address1": address1
                },
                type:"post",
                success:function(result){
                    $('#address1').css('visibility', 'visible');
                    $('#address1').removeClass( "text-danger" ).addClass('text-success');
                    $('#address1').text(result.success);
                },
                error:function (response) {
                    $('#address1').css('visibility', 'visible');
                    $('#address1').removeClass( "text-success" ).addClass('text-danger');
                    $('#address1').text(response.responseJSON.errors.address1);
                }
            });
        });
        jQuery(".address2").change(function(event){
            event.preventDefault();
            var address2 = jQuery(".address2").val();        
            jQuery.ajax({
                url: "{{ url('client/address2/'.$client->id) }}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "address2": address2
                },
                type:"post",
                success:function(result){
                    $('#address2').css('visibility', 'visible');
                    $('#address2').removeClass( "text-danger" ).addClass('text-success');
                    $('#address2').text(result.success);
                },
                error:function (response) {
                    $('#address2').css('visibility', 'visible');
                    $('#address2').removeClass( "text-success" ).addClass('text-danger');
                    $('#address2').text(response.responseJSON.errors.address2);
                }
            });
        });
        jQuery(".address3").change(function(event){
            event.preventDefault();
            var address3 = jQuery(".address3").val();        
            jQuery.ajax({
                url: "{{ url('client/address3/'.$client->id) }}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "address3": address3
                },
                type:"post",
                success:function(result){
                    $('#address3').css('visibility', 'visible');
                    $('#address3').removeClass( "text-danger" ).addClass('text-success');
                    $('#address3').text(result.success);
                },
                error:function (response) {
                    $('#address3').css('visibility', 'visible');
                    $('#address3').removeClass( "text-success" ).addClass('text-danger');
                    $('#address3').text(response.responseJSON.errors.address3);
                }
            });
        });

        jQuery(".landmark").change(function(event){
            event.preventDefault();
            var landmark = jQuery(".landmark").val();        
            jQuery.ajax({
                url: "{{ url('client/landmark/'.$client->id) }}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "landmark": landmark
                },
                type:"post",
                success:function(result){
                    $('#landmark').css('visibility', 'visible');
                    $('#landmark').removeClass( "text-danger" ).addClass('text-success');
                    $('#landmark').text(result.success);
                },
                error:function (response) {
                    $('#landmark').css('visibility', 'visible');
                    $('#landmark').removeClass( "text-success" ).addClass('text-danger');
                    $('#landmark').text(response.responseJSON.errors.landmark);
                }
            });
        });

        jQuery(".city").change(function(event){
            event.preventDefault();
            var city = jQuery(".city").val();        
            jQuery.ajax({
                url: "{{ url('client/city/'.$client->id) }}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "city": city
                },
                type:"post",
                success:function(result){
                    $('#city').css('visibility', 'visible');
                    $('#city').removeClass( "text-danger" ).addClass('text-success');
                    $('#city').text(result.success);
                },
                error:function (response) {
                    $('#city').css('visibility', 'visible');
                    $('#city').removeClass( "text-success" ).addClass('text-danger');
                    $('#city').text(response.responseJSON.errors.area_city);
                }
            });
        });
        
        jQuery(".pincode").change(function(event){
            event.preventDefault();
            var pincode = jQuery(".pincode").val();        
            jQuery.ajax({
                url: "{{ url('client/pincode/'.$client->id) }}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "pincode": pincode
                },
                type:"post",
                success:function(result){
                    $('#pincode').css('visibility', 'visible');
                    $('#pincode').removeClass( "text-danger" ).addClass('text-success');
                    $('#pincode').text(result.success);
                },
                error:function (response) {
                    $('#pincode').css('visibility', 'visible');
                    $('#pincode').removeClass( "text-success" ).addClass('text-danger');
                    $('#pincode').text(response.responseJSON.errors.pincode);
                }
            });
        });

        jQuery(".state").change(function(event){
            event.preventDefault();
            var state = jQuery(".state").val();        
            jQuery.ajax({
                url: "{{ url('client/state/'.$client->id) }}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "state": state
                },
                type:"post",
                success:function(result){
                    $('#state').css('visibility', 'visible');
                    $('#state').removeClass( "text-danger" ).addClass('text-success');
                    $('#state').text(result.success);
                },
                error:function (response) {
                    $('#state').css('visibility', 'visible');
                    $('#state').removeClass( "text-success" ).addClass('text-danger');
                    $('#state').text(response.responseJSON.errors.state);
                }
            });
        });
        
        jQuery(".country").change(function(event){
            event.preventDefault();
            var country = jQuery(".country").val();        
            jQuery.ajax({
                url: "{{ url('client/country/'.$client->id) }}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "country": country
                },
                type:"post",
                success:function(result){
                    $('#country').css('visibility', 'visible');
                    $('#country').removeClass( "text-danger" ).addClass('text-success');
                    $('#country').text(result.success);
                },
                error:function (response) {
                    $('#country').css('visibility', 'visible');
                    $('#country').removeClass( "text-success" ).addClass('text-danger');
                    $('#country').text(response.responseJSON.errors.country);
                }
            });
        });
        
        jQuery(".mobile").change(function(event){
            event.preventDefault();
            var mobile = jQuery(".mobile").val();        
            jQuery.ajax({
                url: "{{ url('client/mobile/'.$client->id) }}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "mobile": mobile
                },
                type:"post",
                success:function(result){
                    $('#mobile').css('visibility', 'visible');
                    $('#mobile').removeClass( "text-danger" ).addClass('text-success');
                    $('#mobile').text(result.success);
                },
                error:function (response) {
                    $('#mobile').css('visibility', 'visible');
                    $('#mobile').removeClass( "text-success" ).addClass('text-danger');
                    $('#mobile').text(response.responseJSON.errors.mobile);
                }
            });
        });
        
        jQuery(".landline").change(function(event){
            event.preventDefault();
            var landline = jQuery(".landline").val();        
            jQuery.ajax({
                url: "{{ url('client/landline/'.$client->id) }}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "landline": landline
                },
                type:"post",
                success:function(result){
                    $('#landline').css('visibility', 'visible');
                    $('#landline').removeClass( "text-danger" ).addClass('text-success');
                    $('#landline').text(result.success);
                },
                error:function (response) {
                    $('#landline').css('visibility', 'visible');
                    $('#landline').removeClass( "text-success" ).addClass('text-danger');
                    $('#landline').text(response.responseJSON.errors.landline);
                }
            });
        });
        
        jQuery(".emailid").change(function(event){
            event.preventDefault();
            var emailid = jQuery(".emailid").val();        
            jQuery.ajax({
                url: "{{ url('client/emailid/'.$client->id) }}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "emailid": emailid
                },
                type:"post",
                success:function(result){
                    $('#emailid').css('visibility', 'visible');
                    $('#emailid').removeClass( "text-danger" ).addClass('text-success');
                    $('#emailid').text(result.success);
                },
                error:function (response) {
                    $('#emailid').css('visibility', 'visible');
                    $('#emailid').removeClass( "text-success" ).addClass('text-danger');
                    $('#emailid').text(response.responseJSON.errors.emailid);
                }
            });
        });
        
        jQuery(".skypeid").change(function(event){
            event.preventDefault();
            var skypeid = jQuery(".skypeid").val();        
            jQuery.ajax({
                url: "{{ url('client/skypeid/'.$client->id) }}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "skypeid": skypeid
                },
                type:"post",
                success:function(result){
                    $('#skypeid').css('visibility', 'visible');
                    $('#skypeid').removeClass( "text-danger" ).addClass('text-success');
                    $('#skypeid').text(result.success);
                },
                error:function (response) {
                    $('#skypeid').css('visibility', 'visible');
                    $('#skypeid').removeClass( "text-success" ).addClass('text-danger');
                    $('#skypeid').text(response.responseJSON.errors.skypeid);
                }
            });
        });
        
        jQuery(".pancard").change(function(event){
            event.preventDefault();
            var pancard = jQuery(".pancard").val();        
            jQuery.ajax({
                url: "{{ url('client/pancard/'.$client->id) }}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "pancard": pancard
                },
                type:"post",
                success:function(result){
                    $('#pancard').css('visibility', 'visible');
                    $('#pancard').removeClass( "text-danger" ).addClass('text-success');
                    $('#pancard').text(result.success);
                },
                error:function (response) {
                    $('#pancard').css('visibility', 'visible');
                    $('#pancard').removeClass( "text-success" ).addClass('text-danger');
                    $('#pancard').text(response.responseJSON.errors.pancard);
                }
            });
        });
        
        jQuery(".gstin").change(function(event){
            event.preventDefault();
            var gstin = jQuery(".gstin").val();        
            jQuery.ajax({
                url: "{{ url('client/gstin/'.$client->id) }}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "gstin": gstin
                },
                type:"post",
                success:function(result){
                    $('#gstin').css('visibility', 'visible');
                    $('#gstin').removeClass( "text-danger" ).addClass('text-success');
                    $('#gstin').text(result.success);
                },
                error:function (response) {
                    $('#gstin').css('visibility', 'visible');
                    $('#gstin').removeClass( "text-success" ).addClass('text-danger');
                    $('#gstin').text(response.responseJSON.errors.gstin);
                }
            });
        });

        jQuery(".prefered_communication_platform").change(function(event){
            event.preventDefault();
            var prefered_communication_platform = jQuery(".prefered_communication_platform").val();        
            jQuery.ajax({
                url: "{{ url('client/prefered_communication_platform/'.$client->id) }}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "prefered_communication_platform": prefered_communication_platform
                },
                type:"post",
                success:function(result){
                    $('#prefered_communication_platform').css('visibility', 'visible');
                    $('#prefered_communication_platform').removeClass( "text-danger" ).addClass('text-success');
                    $('#prefered_communication_platform').text(result.success);
                },
                error:function (response) {
                    $('#prefered_communication_platform').css('visibility', 'visible');
                    $('#prefered_communication_platform').removeClass( "text-success" ).addClass('text-danger');
                    $('#prefered_communication_platform').text(response.responseJSON.errors.prefered_communication_platform);
                }
            });
        });

        jQuery(".business_name").change(function(event){
            event.preventDefault();
            var business_name = jQuery(".business_name").val();        
            jQuery.ajax({
                url: "{{ url('client/business_name/'.$client->id) }}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "business_name": business_name
                },
                type:"post",
                success:function(result){
                    $('#business_name').css('visibility', 'visible');
                    $('#business_name').removeClass( "text-danger" ).addClass('text-success');
                    $('#business_name').text(result.success);
                },
                error:function (response) {
                    $('#business_name').css('visibility', 'visible');
                    $('#business_name').removeClass( "text-success" ).addClass('text-danger');
                    $('#business_name').text(response.responseJSON.errors.business_name);
                }
            });
        });

        jQuery(".business_same_as_resedential").change(function(event){
            event.preventDefault();
            var business_same_as_resedential = jQuery(".business_same_as_resedential").val();        
            jQuery.ajax({
                url: "{{ url('client/business_same_as_resedential/'.$client->id) }}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "business_same_as_resedential": business_same_as_resedential
                },
                type:"post",
                success:function(result){
                    $('#business_same_as_resedential').css('visibility', 'visible');
                    $('#business_same_as_resedential').removeClass( "text-danger" ).addClass('text-success');
                    $('#business_same_as_resedential').text(result.success);
                },
                error:function (response) {
                    $('#business_same_as_resedential').css('visibility', 'visible');
                    $('#business_same_as_resedential').removeClass( "text-success" ).addClass('text-danger');
                    $('#business_same_as_resedential').text(response.responseJSON.errors.business_same_as_resedential);
                }
            });
        });

        jQuery(".business_address1").change(function(event){
            event.preventDefault();
            var business_address1 = jQuery(".business_address1").val();        
            jQuery.ajax({
                url: "{{ url('client/business_address1/'.$client->id) }}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "business_address1": business_address1
                },
                type:"post",
                success:function(result){
                    $('#business_address1').css('visibility', 'visible');
                    $('#business_address1').removeClass( "text-danger" ).addClass('text-success');
                    $('#business_address1').text(result.success);
                },
                error:function (response) {
                    $('#business_address1').css('visibility', 'visible');
                    $('#business_address1').removeClass( "text-success" ).addClass('text-danger');
                    $('#business_address1').text(response.responseJSON.errors.business_address1);
                }
            });
        });
        
        jQuery(".business_address2").change(function(event){
            event.preventDefault();
            var business_address2 = jQuery(".business_address2").val();        
            jQuery.ajax({
                url: "{{ url('client/business_address2/'.$client->id) }}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "business_address2": business_address2
                },
                type:"post",
                success:function(result){
                    $('#business_address2').css('visibility', 'visible');
                    $('#business_address2').removeClass( "text-danger" ).addClass('text-success');
                    $('#business_address2').text(result.success);
                },
                error:function (response) {
                    $('#business_address2').css('visibility', 'visible');
                    $('#business_address2').removeClass( "text-success" ).addClass('text-danger');
                    $('#business_address2').text(response.responseJSON.errors.business_address2);
                }
            });
        });
        
        jQuery(".business_address3").change(function(event){
            event.preventDefault();
            var business_address3 = jQuery(".business_address3").val();        
            jQuery.ajax({
                url: "{{ url('client/business_address3/'.$client->id) }}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "business_address3": business_address3
                },
                type:"post",
                success:function(result){
                    $('#business_address3').css('visibility', 'visible');
                    $('#business_address3').removeClass( "text-danger" ).addClass('text-success');
                    $('#business_address3').text(result.success);
                },
                error:function (response) {
                    $('#business_address3').css('visibility', 'visible');
                    $('#business_address3').removeClass( "text-success" ).addClass('text-danger');
                    $('#business_address3').text(response.responseJSON.errors.business_address3);
                }
            });
        });
        
        jQuery(".company_number").change(function(event){
            event.preventDefault();
            var company_number = jQuery(".company_number").val();        
            jQuery.ajax({
                url: "{{ url('client/company_number/'.$client->id) }}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "company_number": company_number
                },
                type:"post",
                success:function(result){
                    $('#company_number').css('visibility', 'visible');
                    $('#company_number').removeClass( "text-danger" ).addClass('text-success');
                    $('#company_number').text(result.success);
                },
                error:function (response) {
                    $('#company_number').css('visibility', 'visible');
                    $('#company_number').removeClass( "text-success" ).addClass('text-danger');
                    $('#company_number').text(response.responseJSON.errors.company_number);
                }
            });
        });
        
        jQuery(".driving_licence").change(function(event){
            event.preventDefault();
            var driving_licence = jQuery(".driving_licence").val();        
            jQuery.ajax({
                url: "{{ url('client/driving_licence/'.$client->id) }}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "driving_licence": driving_licence
                },
                type:"post",
                success:function(result){
                    $('#driving_licence').css('visibility', 'visible');
                    $('#driving_licence').removeClass( "text-danger" ).addClass('text-success');
                    $('#driving_licence').text(result.success);
                },
                error:function (response) {
                    $('#driving_licence').css('visibility', 'visible');
                    $('#driving_licence').removeClass( "text-success" ).addClass('text-danger');
                    $('#driving_licence').text(response.responseJSON.errors.driving_licence);
                }
            });
        });
        
        jQuery(".sign2_name").change(function(event){
            event.preventDefault();
            var sign2_name = jQuery(".sign2_name").val();        
            jQuery.ajax({
                url: "{{ url('client/sign2_name/'.$client->id) }}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "sign2_name": sign2_name
                },
                type:"post",
                success:function(result){
                    $('#sign2_name').css('visibility', 'visible');
                    $('#sign2_name').removeClass( "text-danger" ).addClass('text-success');
                    $('#sign2_name').text(result.success);
                },
                error:function (response) {
                    $('#sign2_name').css('visibility', 'visible');
                    $('#sign2_name').removeClass( "text-success" ).addClass('text-danger');
                    $('#sign2_name').text(response.responseJSON.errors.sign2_name);
                }
            });
        });
        
        jQuery(".sign2_email").change(function(event){
            event.preventDefault();
            var sign2_email = jQuery(".sign2_email").val();        
            jQuery.ajax({
                url: "{{ url('client/sign2_email/'.$client->id) }}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "sign2_email": sign2_email
                },
                type:"post",
                success:function(result){
                    $('#sign2_email').css('visibility', 'visible');
                    $('#sign2_email').removeClass( "text-danger" ).addClass('text-success');
                    $('#sign2_email').text(result.success);
                },
                error:function (response) {
                    $('#sign2_email').css('visibility', 'visible');
                    $('#sign2_email').removeClass( "text-success" ).addClass('text-danger');
                    $('#sign2_email').text(response.responseJSON.errors.sign2_email);
                }
            });
        });
        
        jQuery(".sign2_designation").change(function(event){
            event.preventDefault();
            var sign2_designation = jQuery(".sign2_designation").val();        
            jQuery.ajax({
                url: "{{ url('client/sign2_designation/'.$client->id) }}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "sign2_designation": sign2_designation
                },
                type:"post",
                success:function(result){
                    $('#sign2_designation').css('visibility', 'visible');
                    $('#sign2_designation').removeClass( "text-danger" ).addClass('text-success');
                    $('#sign2_designation').text(result.success);
                },
                error:function (response) {
                    $('#sign2_designation').css('visibility', 'visible');
                    $('#sign2_designation').removeClass( "text-success" ).addClass('text-danger');
                    $('#sign2_designation').text(response.responseJSON.errors.sign2_designation);
                }
            });
        });
        jQuery(".sign1_designation").change(function(event){
            event.preventDefault();
            var sign1_designation = jQuery(".sign1_designation").val();        
            jQuery.ajax({
                url: "{{ url('client/sign1_designation/'.$client->id) }}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "sign1_designation": sign1_designation
                },
                type:"post",
                success:function(result){
                    $('#sign1_designation').css('visibility', 'visible');
                    $('#sign1_designation').removeClass( "text-danger" ).addClass('text-success');
                    $('#sign1_designation').text(result.success);
                },
                error:function (response) {
                    $('#sign1_designation').css('visibility', 'visible');
                    $('#sign1_designation').removeClass( "text-success" ).addClass('text-danger');
                    $('#sign1_designation').text(response.responseJSON.errors.sign1_designation);
                }
            });
        });
        
        jQuery(".sign3_name").change(function(event){
            event.preventDefault();
            var sign3_name = jQuery(".sign3_name").val();        
            jQuery.ajax({
                url: "{{ url('client/sign3_name/'.$client->id) }}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "sign3_name": sign3_name
                },
                type:"post",
                success:function(result){
                    $('#sign3_name').css('visibility', 'visible');
                    $('#sign3_name').removeClass( "text-danger" ).addClass('text-success');
                    $('#sign3_name').text(result.success);
                },
                error:function (response) {
                    $('#sign3_name').css('visibility', 'visible');
                    $('#sign3_name').removeClass( "text-success" ).addClass('text-danger');
                    $('#sign3_name').text(response.responseJSON.errors.sign3_name);
                }
            });
        });
        
        jQuery(".sign3_email").change(function(event){
            event.preventDefault();
            var sign3_email = jQuery(".sign3_email").val();        
            jQuery.ajax({
                url: "{{ url('client/sign3_email/'.$client->id) }}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "sign3_email": sign3_email
                },
                type:"post",
                success:function(result){
                    $('#sign3_email').css('visibility', 'visible');
                    $('#sign3_email').removeClass( "text-danger" ).addClass('text-success');
                    $('#sign3_email').text(result.success);
                },
                error:function (response) {
                    $('#sign3_email').css('visibility', 'visible');
                    $('#sign3_email').removeClass( "text-success" ).addClass('text-danger');
                    $('#sign3_email').text(response.responseJSON.errors.sign3_email);
                }
            });
        });
        
        jQuery(".sign3_designation").change(function(event){
            event.preventDefault();
            var sign3_designation = jQuery(".sign3_designation").val();        
            jQuery.ajax({
                url: "{{ url('client/sign3_designation/'.$client->id) }}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "sign3_designation": sign3_designation
                },
                type:"post",
                success:function(result){
                    $('#sign3_designation').css('visibility', 'visible');
                    $('#sign3_designation').removeClass( "text-danger" ).addClass('text-success');
                    $('#sign3_designation').text(result.success);
                },
                error:function (response) {
                    $('#sign3_designation').css('visibility', 'visible');
                    $('#sign3_designation').removeClass( "text-success" ).addClass('text-danger');
                    $('#sign3_designation').text(response.responseJSON.errors.sign3_designation);
                }
            });
        });
    });
@endsection
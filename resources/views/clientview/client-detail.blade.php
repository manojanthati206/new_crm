@extends('clientview.index')

@section('title')
    Client add
@endsection()

@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
        <strong>Whoops!</strong>  There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <div class="card">
        <div class="page-content">
            <div class="row">
                <div class="col-xl-6 mx-auto">
                    
                    <h1>Your Details</h1>
                    <hr/>
                <!-- <center> -->
                    <strong class="">Full Name : </strong> {{ $client->first_name }} {{ $client->last_name }}<br>
                    <strong class="">Mobile No.: </strong>  {{ $client->mobile  }} <br>
                    <strong class="">Email Id : </strong>   {{ $client->email_id   }} <br>
                    <strong class="">Date of Birth: </strong>  {{ date('d M, Y',strtotime($client->date_of_birth))  }} <br>
                    @if( isset( $client->skype_id ) )
                        <strong class="">Skype ID: </strong>  {{ $client->skype_id  }} <br>
                    @endif
                    @if( isset( $client->gstin ) )
                    <strong class="">GSTIN : </strong> {{ $client->gstin  }} 
                    @endif
                    @if( isset( $client->sign1_designation ) )
                    <strong class="">Signature Designation 1: </strong> {{ $client->sign1_designation  }}<br>
                    @endif
                    @if( isset( $client->sign2_name ) )
                    <strong class="">Signature Name 2: </strong> {{ $client->sign2_name  }}<br>
                    @endif
                    @if( isset( $client->sign2_email ) )
                    <strong class="">Signature Email 2: </strong> {{ $client->sign2_email  }}<br>
                    @endif
                    @if( isset( $client->sign2_designation ) )
                    <strong class="">Signature Designation 2: </strong> {{ $client->sign2_designation  }}<br>
                    @endif
                    @if( isset( $client->landline ) )
                    <strong class="">Landline No.: </strong> {{ $client->landline  }} <br>
                    @endif
                    @if( isset( $client->business_name ) )
                    <strong class="">Business Name.: </strong> {{ $client->business_name  }} <br>
                    @endif
                    @if( isset( $client->company_number ) )
                    <strong class="">Company Number: </strong> {{ $client->company_number  }}<br>
                    @endif
                    @if( isset( $client->driving_licence ) )
                    <strong class="">Driving Licence: </strong> {{ $client->driving_licence  }}<br>
                    @endif
                    @if( isset( $client->prefered_communication_platform ) )
                    <strong class="">Prefered Communication Platform: </strong> {{ $client->prefered_communication_platform  }}<br>
                    @endif
                    @if( isset( $client->sign3_name ) )

                    <strong class="">Signature Name 3: </strong> {{ $client->sign3_name  }}<br>
                    @endif
                    @if( isset( $client->sign3_email ) )
                    <strong class="">Signature Email 3: </strong> {{ $client->sign3_email  }}<br>
                    @endif
                    @if( isset( $client->sign3_designation ) )
                    <strong class="">Signature Designation 3: </strong> {{ $client->sign3_designation  }}<br>
                    @endif
                    <strong class="">Address: </strong> {{ $client->address1  }} {{ $client->address2  }} {{ $client->address3  }}<br>
                    <?php
                    if( $client->business_same_as_resedential=='no' ) {
                        ?>
                        <strong class="">Business Address: </strong> {{ $client->business_address1  }} {{ $client->business_address2  }} {{ $client->business_address3  }}<br>
                        <?php
                    }
                    ?>
                    @if( isset( $client->landmark ) )
                    <strong class="">Landmark: </strong> {{ $client->landmark  }}  <br>
                    @endif
                    @if( isset( $client->area_city ) )
                    <strong class="">City: </strong> {{ $client->area_city  }} {{ $client->pincode  }} <br>
                    @endif
                    @if( isset( $client->state ) )
                    <strong class="">State: </strong> {{ $client->state  }} {{ $client->country  }} <br>
                    @endif
                    @if( isset( $client->pancard ) )
                    <strong class="">Pancard: </strong> {{ $client->pancard  }} <br>
                    @endif
                <!-- </center>  -->
                <center class="mx-auto">
                    <a href="{{URL::to('/')}}/client/edit/{{ $client->id  }}/{{ $client_type }}">
                        <button class="btn btn-light">Edit</button>
                    </a>
                    <a href="{{URL::to('/')}}/submit-form/{{ $client->id  }}" onclick="return confirm('Are you checked all the data you have filled?')">
                        <button class="btn btn-light">Submit</button>
                    </a>
                </center>
                </div>
            </div>
        </div>
    </div>
@endsection()

@section('loadstyles')
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link href="{{ asset('assets/plugins/smart-wizard/css/smart_wizard_all.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/datetimepicker/css/classic.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/plugins/smart-wizard/css/smart_wizard_all.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/datetimepicker/css/classic.date.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.min.css') }}">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <!-- <link href="{{URL::to('/')}}/assets/plugins/fancy-file-uploader/fancy_fileupload.css" rel="stylesheet" />
    <link href="{{URL::to('/')}}/assets/plugins/Drag-And-Drop/dist/imageuploadify.min.css" rel="stylesheet" />
    <link href="{{URL::to('/')}}/assets/plugins/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet" /> -->
@endsection()
@section('loadscript')
    <script src="{{URL::to('/')}}/assets/plugins/simplebar/js/simplebar.min.js"></script>
    <script src="{{URL::to('/')}}/assets/plugins/metismenu/js/metisMenu.min.js"></script>
    <script src="{{URL::to('/')}}/assets/plugins/perfect-scrollbar/js/perfect-scrollbar.js"></script>
    <script src="{{ asset('assets/plugins/smart-wizard/js/jquery.smartWizard.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datetimepicker/js/legacy.js') }}"></script>
    <script src="{{ asset('assets/plugins/datetimepicker/js/picker.js') }}"></script>
    <script src="{{ asset('assets/plugins/datetimepicker/js/picker.time.js') }}"></script>
    <script src="{{ asset('assets/plugins/datetimepicker/js/picker.date.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-material-datetimepicker/js/moment.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.min.js') }}"></script>
    <!-- <script src="{{URL::to('/')}}/assets/plugins/fancy-file-uploader/jquery.ui.widget.js"></script>
    <script src="{{URL::to('/')}}/assets/plugins/fancy-file-uploader/jquery.fileupload.js"></script>
    <script src="{{URL::to('/')}}/assets/plugins/fancy-file-uploader/jquery.iframe-transport.js"></script>
    <script src="{{URL::to('/')}}/assets/plugins/fancy-file-uploader/jquery.fancy-fileupload.js"></script>
    <script src="{{URL::to('/')}}/assets/plugins/Drag-And-Drop/dist/imageuploadify.min.js"></script> -->
@endsection()
@section('scripts')
@endsection
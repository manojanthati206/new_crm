<!DOCTYPE html>
<html>
<head>
	<title>Service Agreement PDF</title>
</head>
<body>
	<p>
		Great news,
	</p>
	<p>
		Thank you for completing service agreement. We have officially established a client relationship. It would be our pleasure helping you for your any digital needs. 
	</p>
		Soon, you’ll be receiving first payment link from neshallWeb. neshallWeb has partnered with RazorPay to facilitate you with optimum payment options. You will have option to choose credit card, debit card, online banking, PayPal and other available form of payment for your convenience. 
	</p>
	<p>
		Meantime, if you have any comments or questions, feel free to contact us by replying to this email. We’d be happy to help you!
	</p>
	<p>
		Thanks and Regards,<br>
		The neshallWeb team
	</p>
	<p>
		P.s. If you have received this email via error, you can reach out to us by replying to this email.
	</p>
</body>
</html>

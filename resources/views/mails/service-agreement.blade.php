<!DOCTYPE html>
<html>
<head>
	<title>Service Agreement</title>
</head>
<body>
	<p>
		Hello,
	</p>
	<p>
		Thanks again for choosing us and we are looking forward to our promising journey and helping you along the way. 
	</p>
	<p>
		This email is to formalize the service contract and getting necessary inputs from representative or signatories of your organization.
	</p>
	<p>
		We have prepared an estimate and a service document to proceed further in the direction to begin working on your project. These are just few formalities left to do so.
	</p>
	</p>
		Read the documents thoroughly and provide your input as required. We tried to make document formalities work smoother, we hope it proves convenient.
	</p>
	<p>
		To get started, please click on the following <a style="color: blue;text-decoration: underline;" href="{{ route('service-agreement-signature',['estimate_ids'=>$service_data['estimate_ids'],'client_email'=>$service_data['client_email'],'no_of_client'=>$service_data['no_of_client']]) }}"> Service Agreement</a> and provide necessary inputs.
	</p>
	<p>
		Meantime, if you have any comments or questions, feel free to contact us by replying to this email. We’d be happy to help you!
	</p>
	<p>
		Regards,
		The neshallWeb team
	</p>
	<p>
		P.s. If you have received this email via error, you can reach out to us by replying to this email.
	</p>

</body>
</html>

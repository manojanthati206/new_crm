<!DOCTYPE html>
<html>
<head>
	<title>Client</title>
</head>
<body>
	<p>
		Hello,
	</p>
	<p>
		Thanks for choosing neshallWeb.
	</p>
	<p>
		The next step would be to have some information to generate service agreement and formalizing the service contract. The agenda of this form is to include signatories and business's legal information.
	</p>
	<p>
		To get started, please click on the following <a style="color: blue;text-decoration: underline;" href="{{$new_urlid}}"> Client Detail Form</a> and complete the form.
	</p>
	<p>
		Meantime, if you have any comments or questions, feel free to contact us by replying to this email. We’d be happy to help you!
	</p>
	<p>
		Regards,<br>
		The neshallWeb team
	</p>
	<p>
		P.s. If you have received this email by error then you can reach out to us via same email for any inquiry. 
	</p>
</body>
</html>

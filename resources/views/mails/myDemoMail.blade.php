<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Introduction Email</title>
</head>

<body style="margin: 0; padding: 0; box-sizing: border-box;">

    <div class="my-tamplate" style="background-color: #fff; width: 750px; margin: auto;">

        <section class="main-content" style="">
            <div class="container" style="width: 100%; margin: 0 auto;">
                <div class="row" style="position:relative;">
                    <div class="logo" style="padding: 10px 0;display: table; width: 100%;">
                        <div style="width:50%;display: table-cell;"><img src="https://newcrm.neshallweb.com/assets/images/email-template-img/logo1.png" alt="" style="width:180px;"></div>
                        <div style="width:50%; text-align: right; display: table-cell;vertical-align: middle;"><img src="https://newcrm.neshallweb.com/assets/images/email-template-img/social-icon.png" alt="" style=""></div>
                    </div>
                    <div class="banner-content" style="position: relative;">
                        <div class="content">
                            <img style="width:100%;display: block;" src="https://newcrm.neshallweb.com/assets/images/email-template-img/intro-img1.jpg">
                        </div>
                    </div>
                </div>


            </div>
        </section>
        <section style="padding: 40px 30px;">
            <div class="container" style="width: 100%; margin: 0 auto;">
                <div class="row" style="text-align:center; margin-bottom:40px;">
                    <h3 style="font-size: 22px; line-height: 32px; color: #000000; width: 95%; margin:0 auto; margin-bottom: 20px;">We are thrilled that you have expressed interest in neshallAds and our services! At neshallAds</h3>
                    <p style="font-size: 16px; line-height: 28px; color: #626262; width: 95%; margin:0 auto; margin-bottom: 20px;">we specialize in creating promotional videos using CGI and other software tools, which allows us to create high-quality videos without the need for a traditional film shoot. </p>
                    <a href="#" style="padding: 14px 26px; font-size:16px; font-weight: 600; background-color:#0277b7; color: #ffffff; text-decoration:none; border-radius: 50px; display:inline-block; margin-top:0px;">Get Started</a>
                </div>
                <div class="row" style="display:flex;">
                    <div class="img" style="width: 50%; padding-right: 15px;">
                        <img src="https://newcrm.neshallweb.com/assets/images/email-template-img/intro-img2.jpg" alt="" style="width:100%;">
                    </div>
                    <div class="img" style="width: 50%; padding-left: 15px;">
                        <p style="font-size:16px; line-height: 26px; color: #626262; margin-top:0;">Our team of experienced professionals is dedicated to helping businesses like yours showcase their products and services in the most effective and engaging way possible. We have worked with a wide range of clients across various industries and have a proven track record of delivering results. </p>
                    </div>
                </div>
                <div class="row" style="display:flex; margin-top: 10px;">
                    <p style="font-size:16px; line-height: 26px; color: #626262; margin:0;">Thank you for considering neshallAds for your promotional video needs. We are excited to learn more about your business and how we can help you reach your goals. Please don't hesitate to reach out to us with any questions or to discuss your project in more detail.  </p>
                </div>
            </div>
        </section>
        <section style="padding: 0px 30px; margin-bottom: 30px;">
            <div class="container" style="width: 100%; margin: 0 auto;">
                <div class="row" style="text-align:center;background-color: #0277b7; padding: 40px 30px 40px 30px;">
                    <h4 style="font-size:22px; color: #ffffff; margin:0; font-weight: 500;">Company signature with website and social media links. </h4>
                </div>
            </div>
        </section>
        <footer>
            <div class="footer-top" style="background-color: #f5f5f5; padding: 40px 30px 30px 30px;">
                <div style="display:flex;">
                    <div style="text-align:center; color: #323232; width:33.3333%;">
                        <img src="https://newcrm.neshallweb.com/assets/images/email-template-img/call-icon-dark.png">
                        <p style="margin-bottom: 10px;">+91 635 442 5147</p>
                        <p style="margin:0;  font-size: 16px;">+91 635 442 5147</p>
                    </div>
                    <div style="text-align:center; color: #323232; width:33.3333%;">
                        <img src="https://newcrm.neshallweb.com/assets/images/email-template-img/mail-icon-dark.png">
                        <p style=" margin-bottom: 0px; font-size: 16px;">info@neshallweb.com</p>
                    </div>
                    <div style="text-align:center; color: #323232; width:33.3333%;">
                        <img src="https://newcrm.neshallweb.com/assets/images/email-template-img/location-icon-dark.png">
                        <p style="margin-bottom:0;  font-size: 16px; line-height: 26px;">#125, Kuber Siddhi Building, Atak Pardi, Valsad, Gujarat-396007, INDIA</p>
                    </div>
                </div>
            </div>
            <div class="footer-bottom" style="background-color: #000000; padding: 15px 30px 15px 30px;">
                <div style="text-align:center;">
                    <div style="font-size:16px; color:#ffffff;">Copyright @ 2022 neshallWeb</div>
                </div>
            </div>
        </footer>
    </div>
</body>

</html>

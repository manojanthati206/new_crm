
@extends('layouts.blank')

@section('title')
            Dashboard
@endsection()

@section('content')

    @if ($message = Session::get('success'))
        <div style="width: 85%;" class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    @if ($message = Session::get('fail'))
        <div class="alert alert-fail">
            <p>{{ $message }}</p>
        </div>
    @endif
    <div id="dashboard" style="height: 600px;">
        <?php
            if( isset($submit_ac_form)  && ! empty($submit_ac_form)) {
                ?>
                {!!$submit_ac_form!!}
                <div style="width: 85%;" class="alert alert-success">
                    <p>Thank you for submiting the acquition form.</p>
                </div>
                    <button class="btn btn-light" onclick="funclose()">Close</button>
                <?php
            }
        ?>
    </div>
@endsection()
@section('loadstyles')
    <!-- <link href="{{URL::to('/')}}/assets/plugins/fancy-file-uploader/fancy_fileupload.css" rel="stylesheet" />
    <link href="{{URL::to('/')}}/assets/plugins/Drag-And-Drop/dist/imageuploadify.min.css" rel="stylesheet" />
    <link href="{{URL::to('/')}}/assets/plugins/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet" /> -->
@endsection()
@section('loadscript')
    <script src="{{URL::to('/')}}/assets/plugins/simplebar/js/simplebar.min.js"></script>
    <script src="{{URL::to('/')}}/assets/plugins/metismenu/js/metisMenu.min.js"></script>
    <script src="{{URL::to('/')}}/assets/plugins/perfect-scrollbar/js/perfect-scrollbar.js"></script>
    <!-- <script src="{{URL::to('/')}}/assets/plugins/fancy-file-uploader/jquery.ui.widget.js"></script>
    <script src="{{URL::to('/')}}/assets/plugins/fancy-file-uploader/jquery.fileupload.js"></script>
    <script src="{{URL::to('/')}}/assets/plugins/fancy-file-uploader/jquery.iframe-transport.js"></script>
    <script src="{{URL::to('/')}}/assets/plugins/fancy-file-uploader/jquery.fancy-fileupload.js"></script>
    <script src="{{URL::to('/')}}/assets/plugins/Drag-And-Drop/dist/imageuploadify.min.js"></script> -->
    <script type="text/javascript">
        setTimeout(function() {
            window.close();
        }, 5000);
        function funclose(){
            var win = window.open('', '_self'); win.close()
        }
    </script>
@endsection()
@section('scripts')
    let height = $('.simplebar-content-wrapper').height()-100;
    $('#dashboard').css('height',height+'px');
@endsection()

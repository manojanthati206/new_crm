@extends('layouts.index')



@section('title')

    Clients

@endsection()



@section('content')

<script>

        function copyToClipboard(text) {

        var sampleTextarea = document.createElement("textarea");

        document.body.appendChild(sampleTextarea);

        sampleTextarea.value = text; //save main text in it

        sampleTextarea.select(); //select textarea contenrs

        document.execCommand("copy");

        document.body.removeChild(sampleTextarea);

    }

</script>

    <?php $i = 0; ?>

    <!-- Content Header (Page header) -->





    <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">

        <div class="breadcrumb-title pe-3">All Services</div>

        <div class="ps-3">

            <nav aria-label="breadcrumb">

                <ol class="breadcrumb mb-0 p-0">

                    <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>

                    </li>

                    <li class="breadcrumb-item active" aria-current="page">Servicea</li>

                </ol>

            </nav>

        </div>

        <div class="ms-auto">

            <div class="btn-group">

                <?php if(empty($urlid)){ ?>

                    <!-- <a href="{{ route('clients-url') }}"> -->

                        <button type="button" class="btn btn-light" data-bs-toggle="modal" data-bs-target="#exampleVerticallycenteredModal">Generate URl</button>

                    <!-- </a> -->

                <?php } ?>
                <!-- Button trigger modal -->
                <div class="modal fade" id="exampleVerticallycenteredModal" tabindex="-1" style="display: none;" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content">
                            <form action="{{ route('clients-url') }}" method="get">
                                <div class="modal-header">
                                    <h5 class="modal-title">Send Mail to client</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    <label>Client Email</label>
                                    <input type="text" name="email" class="form-control" placeholder="Client Email" required>
                                    <label>Client Country</label>
                                    <select class="single-select form-select type-select" id="validationCustom01" name="client_type" required="">
                                        <option value="other_countries" selected="selected">Other Countries</option>
                                        <option value="indian">India</option>
                                    </select>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary">Send Link</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <input type="hidden" value="<?php if(!empty($urlid)){ echo $urlid; } ?>" id="caurl">

                <?php if(!empty($urlid)){ 

                    echo'<span onclick="clickaddurl()" class="btn btn-light" id="copybtn">Copy URL</span>';}

                ?>

                     <script>

                        

                        function clickaddurl(){

                            var copyText = document.getElementById("caurl");

                            copyToClipboard(copyText.value);

                            jQuery("#copybtn").text('Copied!');

                            const myTimeout = setTimeout(redirecturl, 2000);

                            function redirecturl(){

                                window.location.replace('{{ route('clients-index')}}');

                            }

                        }

                    </script>

                <a href="{{ route('clients-add') }}">

                    <button type="button" class="btn btn-light">Add New</button>

                </a>

            </div>

        </div>

    </div>

    <div class="row">

        <div class="col-xl-12 mx-auto">

            <div class="card">

                <div class="card-body table-responsive">

                    <table class="table mb-0 text-center">

                        <thead>

                            <tr>

                                <th scope="col">#</th>

                                <th scope="col">Action</th>

                                <th scope="col">Create Service Link</th>

                                <th scope="col">Full Name</th>

                                <th scope="col">Email ID</th>

                                <th scope="col">Mobile No.</th>

                                <th scope="col">Birth date</th>

                            </tr>

                        </thead>

                        <tbody>

                            

                            @foreach ($clients as $client)

                                <tr>

                                    <td scope="row">{{ ++$i }}</td>

                                    <td>

                                        <a href="{{route('clients-edit', $client->id)}}">

                                            Edit

                                        </a> ||

                                        <a onclick="return confirm('Are you sure?')" href="{{route('clients-delete', $client->id)}}">

                                            Delete

                                        </a> ||

                                        <a href="{{route('clients-view', $client->id)}}">

                                            View

                                        </a>

                                    </td>

                                    <td>

                                        <a href="{{url('client/service', $client->id)}}">

                                            Create

                                        </a> 

                                    </td>

                                    <td>

                                        {{ $client->first_name  }} {{ $client->last_name  }}

                                    </td>

                                    <td>

                                        {{ $client->email_id  }} 

                                    </td>

                                    <td>

                                        {{ $client->mobile  }} 

                                    </td>

                                    <td>

                                        {{   date('d M Y', strtotime($client->date_of_birth)) }} 

                                    </td>

                                    

                                </tr>

                            @endforeach

                       

                        </tbody>

                    </table>

                </div>

            </div>

        </div>

    </div>

      <!-- /.content-header -->

    <?php $i = 0;?>



    <!--!! $projects->links() !!-->



@endsection()



@section('scripts')



    $("#example1").DataTable({

      "responsive": true, "lengthChange": false, "autoWidth": false,

      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]

    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');



    

@endsection()


@extends('layouts.index')

@section('title')
    Service Send
@endsection()

@section('content')

    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
        <div class="breadcrumb-title pe-3">Services Send</div>
        <div class="ps-3">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb mb-0 p-0">
                    <li class="breadcrumb-item"><a href="{{url('/')}}"><i class="bx bx-home-alt"></i></a></li>
                    <li class="breadcrumb-item"><a href="{{url('clients/index')}}">Clients</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Services Send</li>
                </ol>
            </nav>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-9 mx-auto">      
            <hr/>
            <div class="card">
                <div class="card-body">
                    <div class="p-4 border rounded">
                    <div id="smartwizard1">
                        
                        <form method="post"  class="row g-3 needs-validation" enctype="multipart/form-data"  novalidate>
                           
                            <input type="hidden" name="id" id="qid"  value="{{ session('qansid') }}">
                            <div class="tab-content">
                                <div   role="tabpanel" aria-labelledby="step-1">
                                    <h3>Select Category</h3>
                                        <div class="col-md-12 mt-2" >
                                            <label for="validationCustom01" class="form-label">Category *</label>
                                            <select class="single-select form-select mainservices"  id="validationCustom01"  name="mianservices">
                                                <option value="" >Select Category</option>
                                                @foreach($mainservices as $parentservice)
                                                    <option value="{{  $parentservice->id }}" {{-- $question->serviceid == $parentservice->id ? 'selected="selected"' : '' --}}>{{  $parentservice->name }}</option>
                                                @endforeach
                                            </select>
                                            <div class="valid-feedback">Looks good!</div>
                                        </div><br/>
                                    <h3>Select Service</h3>
                                        <div class="col-md-12 mt-2" >
                                            <label for="validationCustom01" class="form-label">Category *</label>
                                            <select class="single-select form-select services"  id="validationCustom01"  name="services">
                                                <option value="" >Select Service</option>
                                            </select>
                                            <div class="valid-feedback">Looks good!</div>
                                        </div>
                                        <br/>
                                        <div class="text-center">
                                            <span id="msg"  ></span>
                                        </div>    
                                </div>
                                
                            </div>
                        </form>
                    </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection()
@section('loadstyles')
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link href="{{ asset('assets/plugins/smart-wizard/css/smart_wizard_all.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/datetimepicker/css/classic.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/plugins/smart-wizard/css/smart_wizard_all.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/datetimepicker/css/classic.date.css') }}" rel="stylesheet" />
	<link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.min.css') }}">
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <style>
        .toolbar-bottom button:nth-child(3),.toolbar-bottom button:nth-child(4) {
            display: none !important;
        }
    </style>
@endsection()
@section('loadscript')
    <script src="{{URL::to('/')}}/assets/plugins/simplebar/js/simplebar.min.js"></script>
    <script src="{{URL::to('/')}}/assets/plugins/metismenu/js/metisMenu.min.js"></script>
    <script src="{{URL::to('/')}}/assets/plugins/perfect-scrollbar/js/perfect-scrollbar.js"></script>
    <script src="{{ asset('assets/plugins/smart-wizard/js/jquery.smartWizard.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datetimepicker/js/legacy.js') }}"></script>
	<script src="{{ asset('assets/plugins/datetimepicker/js/picker.js') }}"></script>
	<script src="{{ asset('assets/plugins/datetimepicker/js/picker.time.js') }}"></script>
	<script src="{{ asset('assets/plugins/datetimepicker/js/picker.date.js') }}"></script>
	<script src="{{ asset('assets/plugins/bootstrap-material-datetimepicker/js/moment.min.js') }}"></script>
	<script src="{{ asset('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.min.js') }}"></script>
@endsection()

@section('scripts')
     
        /* equations ajax*/
        jQuery(document).ready(function(event){
            jQuery(".mainservices").change(function(event){
                event.preventDefault();
                var mainservices = jQuery(".mainservices").val(); 
                var qid = jQuery('#qid').val();
              
                jQuery.ajax({
                    url: "{{ url('client/servcies') }}",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "mainservices": mainservices,
                    },
                    type:"post",
                    success:function(result){
                       jQuery(".services").html(result);
                    }
                });
            });

            jQuery(".services").change(function(event){
                event.preventDefault();
                var services = jQuery(".services").val();  
                jQuery.ajax({
                    url: "{{ url('client/servicesstore/') }}",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "services": services
                    },
                    type:"post",
                    success:function(result){
                        jQuery("#msg").html(result);
                    },
                    error:function(result){
                        jQuery("#msg").html(result);
                    }
                });
            });
    });
@endsection
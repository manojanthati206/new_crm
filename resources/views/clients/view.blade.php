@extends('layouts.index')

@section('title')
    Clients
@endsection()

@section('content')
    <?php $i = 0; ?>
    <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
        <div class="breadcrumb-title pe-3">Add New Client</div>
            <div class="ps-3">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-0 p-0">
                        <li class="breadcrumb-item"><a href="{{ url('/')}}"><i class="bx bx-home-alt"></i></a>
                        </li>
                        <li class="breadcrumb-item " aria-current="page"><a href="{{ route('clients-index') }}">Clients</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Client View</li>
                    </ol>
                </nav>
            </div>
        </div>
    <div class="card">
		<div class="card-body">
			<h1>Client Details. Hi {{ $client->first_name }}</h1>
			<hr/>
            <div class="row h5">
                <div class="col-md-6">
                    <strong class="">Full Name : </strong>{{ $client->first_name }} {{ $client->last_name }}<br>
                    <strong class="">Mobile No.: </strong>{{ $client->mobile  }} <br>
                    <strong class="">Email Id : </strong>{{ $client->email_id   }} <br>
                    <strong class="">Date of Birth: </strong>{{ date('d M, Y',strtotime($client->date_of_birth))  }} <br>
                    <strong class="">GSTIN : </strong>{{ $client->gstin  }} 
                    @if( isset( $client->gstin ) )
                    <strong class="">GSTIN : </strong> {{ $client->gstin  }} 
                    @endif
                    @if( isset( $client->sign1_designation ) )
                    <strong class="">Signature Designation 1: </strong> {{ $client->sign1_designation  }}<br>
                    @endif
                    @if( isset( $client->sign2_name ) )
                    <strong class="">Signature Name 2: </strong> {{ $client->sign2_name  }}<br>
                    @endif
                    @if( isset( $client->sign2_email ) )
                    <strong class="">Signature Email 2: </strong> {{ $client->sign2_email  }}<br>
                    @endif
                    @if( isset( $client->sign2_designation ) )
                    <strong class="">Signature Designation 2: </strong> {{ $client->sign2_designation  }}<br>
                    @endif
                    @if( isset( $client->landline ) )
                    <strong class="">Landline No.: </strong> {{ $client->landline  }} <br>
                    @endif
                    @if( isset( $client->sign3_name ) )

                    <strong class="">Signature Name 3: </strong> {{ $client->sign3_name  }}<br>
                    @endif
                    @if( isset( $client->sign3_email ) )
                    <strong class="">Signature Email 3: </strong> {{ $client->sign3_email  }}<br>
                    @endif
                    @if( isset( $client->sign3_designation ) )
                    <strong class="">Signature Designation 3: </strong> {{ $client->sign3_designation  }}<br>
                    @endif
                    <strong class="">Address: </strong> {{ $client->address1  }} {{ $client->address2  }} {{ $client->address3  }}<br>
                    <?php
                    if( $client->business_same_as_resedential=='no' ) {
                        ?>
                        <strong class="">Business Address: </strong> {{ $client->business_address1  }} {{ $client->business_address2  }} {{ $client->business_address3  }}<br>
                        <?php
                    }
                    ?>
                </div>
                <div class="col-md-6">
                    <strong class="">Landline No.: </strong>{{ $client->landline  }} <br>
                    <strong class="">Address: </strong>{{ $client->address1  }} {{ $client->address2  }} {{ $client->address3  }}<br>
                    <strong class="">Landmark: </strong>{{ $client->landmark  }}  <br>
                    <strong class="">City: </strong>{{ $client->area_city  }} {{ $client->pincode  }} <br>
                    <strong class="">State: </strong>{{ $client->state  }} {{ $client->country  }} <br>
                    <strong class="">Pancard: </strong>{{ $client->pancard  }} <br>
                    @if( isset( $client->business_name ) )
                    <strong class="">Business Name.: </strong> {{ $client->business_name  }} <br>
                    @endif
                    @if( isset( $client->company_number ) )
                    <strong class="">Company Number: </strong> {{ $client->company_number  }}<br>
                    @endif
                    @if( isset( $client->driving_licence ) )
                    <strong class="">Driving Licence: </strong> {{ $client->driving_licence  }}<br>
                    @endif
                    @if( isset( $client->prefered_communication_platform ) )
                    <strong class="">Prefered Communication Platform: </strong> {{ $client->prefered_communication_platform  }}<br>
                    @endif
                    @if( isset( $client->skype_id ) )
                        <strong class="">ID: </strong>  {{ $client->skype_id  }} <br>
                    @endif
                    @if( isset( $client->landmark ) )
                    <strong class="">Landmark: </strong> {{ $client->landmark  }}  <br>
                    @endif
                    @if( isset( $client->area_city ) )
                    <strong class="">City: </strong> {{ $client->area_city  }} {{ $client->pincode  }} <br>
                    @endif
                    @if( isset( $client->state ) )
                    <strong class="">State: </strong> {{ $client->state  }} {{ $client->country  }} <br>
                    @endif
                    @if( isset( $client->pancard ) )
                    <strong class="">Pancard: </strong> {{ $client->pancard  }} <br>
                    @endif
                </div>
            </div>
                <hr/>
            <div class="row">
                <div class="col-md-12">
                    <h3>Acquistion Form</h3>
                    <ul>
                        <?php $a=$b=$c=$d=1; ?>
                        @foreach($acquistion as $val)
                            @if($val->mainserviceid == 1)
                                <?php echo ($a == 1)? '<h4>Graphics Design</h4>' : ''; $a++; ?>
                                <li class="lead">Qusetion : {{$val->question}}  <br/> 
                                Answer : {{$val->answer}} <br/>
                                <?php $images = json_decode($val->images); ?>
                                @if(!empty($images))
                                    Images :  
                                    @foreach($images as $img)
                                        <img src="{{ url('public/') }}/{{$img}}" alt="" class="my-2"width="100px;"/>
                                    @endforeach
                                @endif
                                </li>
                            @elseif($val->mainserviceid == 2)
                            <?php echo ($b == 1)? '<hr><h4>Video Editing</h4>' : ''; $b++; ?>
                                <li class="lead">Qusetion : {{$val->question}} <br/> 
                                Answer : {{$val->answer}} <br/>
                                <?php $images = json_decode($val->images); ?>
                                @if(!empty($images))
                                    Images :  
                                    @foreach($images as $img)
                                        <img src="{{ url('public/') }}/{{$img}}" alt="" class="my-2"width="100px;"/>
                                    @endforeach
                                @endif
                                </li>
                            @elseif($val->mainserviceid == 3)
                            <?php echo ($c == 1)? '<hr><h4>Web Developer</h4>' : ''; $c++; ?>
                                <li class="lead">Qusetion : {{$val->question}}  <br/> 
                                Answer : {{$val->answer}} <br/>
                                <?php
                                if( $val->type == '6' ) {
                                    if( $val->page_1 != '' ) {
                                        ?>
                                        Page Title : <?= $val->page_1; ?><br>
                                        <?php    
                                    }
                                    if( $val->page_section_1 != '' ) {
                                        ?>
                                        Sections : <?= $val->page_section_1; ?><br>
                                        <?php    
                                    }
                                    if( $val->page_2 != '' ) {
                                        ?>
                                        Page Title : <?= $val->page_2; ?><br>
                                        <?php    
                                    }
                                    if( $val->page_section_2 != '' ) {
                                        ?>
                                        Sections : <?= $val->page_section_2; ?><br>
                                        <?php    
                                    }
                                    if( $val->page_3 != '' ) {
                                        ?>
                                        Page Title : <?= $val->page_3; ?><br>
                                        <?php    
                                    }
                                    if( $val->page_section_3 != '' ) {
                                        ?>
                                        Sections : <?= $val->page_section_3; ?><br>
                                        <?php    
                                    }
                                    if( $val->page_4 != '' ) {
                                        ?>
                                        Page Title : <?= $val->page_4; ?><br>
                                        <?php    
                                    }
                                    if( $val->page_section_4 != '' ) {
                                        ?>
                                        Sections : <?= $val->page_section_4; ?><br>
                                        <?php    
                                    }
                                    if( $val->page_5 != '' ) {
                                        ?>
                                        Page Title : <?= $val->page_5; ?><br>
                                        <?php    
                                    }
                                    if( $val->page_section_5 != '' ) {
                                        ?>
                                        Sections : <?= $val->page_section_5; ?><br>
                                        <?php    
                                    }
                                    if( $val->page_6 != '' ) {
                                        ?>
                                        Page Title : <?= $val->page_6; ?><br>
                                        <?php    
                                    }
                                    if( $val->page_section_6 != '' ) {
                                        ?>
                                        Sections : <?= $val->page_section_6; ?><br>
                                        <?php    
                                    }
                                    if( $val->page_7 != '' ) {
                                        ?>
                                        Page Title : <?= $val->page_7; ?><br>
                                        <?php    
                                    }
                                    if( $val->page_section_7 != '' ) {
                                        ?>
                                        Sections : <?= $val->page_section_7; ?><br>
                                        <?php    
                                    }
                                    if( $val->page_8 != '' ) {
                                        ?>
                                        Page Title : <?= $val->page_8; ?><br>
                                        <?php    
                                    }
                                    if( $val->page_section_8 != '' ) {
                                        ?>
                                        Sections : <?= $val->page_section_8; ?><br>
                                        <?php    
                                    }
                                    if( $val->page_9 != '' ) {
                                        ?>
                                        Page Title : <?= $val->page_9; ?><br>
                                        <?php    
                                    }
                                    if( $val->page_section_9 != '' ) {
                                        ?>
                                        Sections : <?= $val->page_section_9; ?><br>
                                        <?php    
                                    }
                                    if( $val->page_10 != '' ) {
                                        ?>
                                        Page Title : <?= $val->page_10; ?><br>
                                        <?php    
                                    }
                                    if( $val->page_section_10 != '' ) {
                                        ?>
                                        Sections : <?= $val->page_section_10; ?><br>
                                        <?php    
                                    }
                                    
                                }
                                if( $val->type == '7' ) {
                                    if( $val->reference_link_1 != '' ) {
                                        ?>
                                        <?= $val->reference_link_1; ?><br>
                                        <?php    
                                    }
                                    if( $val->reference_link_2 != '' ) {
                                        ?>
                                        <?= $val->reference_link_2; ?><br>
                                        <?php    
                                    }
                                    if( $val->reference_link_3 != '' ) {
                                        ?>
                                        <?= $val->reference_link_3; ?><br>
                                        <?php    
                                    }
                                    if( $val->reference_link_4 != '' ) {
                                        ?>
                                        <?= $val->reference_link_4; ?><br>
                                        <?php    
                                    }
                                    if( $val->reference_link_5 != '' ) {
                                        ?>
                                        <?= $val->reference_link_5; ?><br>
                                        <?php    
                                    }
                                }
                                ?>
                                <?php $images = json_decode($val->images); ?>
                                @if(!empty($images))
                                    Images :  
                                    @foreach($images as $img)
                                        <img src="{{ url('public/') }}/{{$img}}" alt="" class="my-2"width="100px;"/>
                                    @endforeach
                                @endif
                                </li>
                            @elseif($val->mainserviceid == 4)
                            <?php echo ($d == 1)? '<hr><h4>Digital Marketing</h4>' : ''; $d++; ?>
                                <li class="lead">Qusetion : {{$val->question}}  <br/> 
                                Answer : {{$val->answer}} <br/>
                                <?php $images = json_decode($val->images); ?>
                                @if(!empty($images))
                                    Images :  
                                    @foreach($images as $img)
                                        <img src="{{ url('public/') }}/{{$img}}" alt="" class="my-2"width="100px;"/>
                                    @endforeach
                                @endif
                                </li>
                            @endif        
                        @endforeach
                    </ul>
                    <br/>
                    <hr/>
                    <br/>
                </div>
            </div>
		</div>
	</div>



    @endsection()

@section('scripts')

$("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');

@endsection()
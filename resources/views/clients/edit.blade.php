@extends('layouts.index')
@section('title')
            Client Edit
@endsection()
@section('content')
    @if ($errors->any())
<div class="alert alert-danger">
   <strong>Whoops!</strong> There were some problems with your input.<br><br>
   <ul>
      @foreach ($errors->all() as $error)
      <li>{{ $error }}</li>
      @endforeach
   </ul>
</div>
@endif
<div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
   <div class="breadcrumb-title pe-3">Edit Client</div>
   <div class="ps-3">
      <nav aria-label="breadcrumb">
         <ol class="breadcrumb mb-0 p-0">
            <li class="breadcrumb-item"><a href="javascript:void(0);"><i class="bx bx-home-alt"></i></a>
            </li>
            <li class="breadcrumb-item active" aria-current="page"><a href="{{ route('clients-index') }}">Client</a> </li>
            <li class="breadcrumb-item active" aria-current="page">Client Edit</li>
         </ol>
      </nav>
   </div>
</div>
<div class="row">
   <div class="col-xl-9 mx-auto">
      <hr/>
      <div class="card">
         <div class="card-body">
            <div class="p-4 border rounded">
               <div id="smartwizard">
                  <ul class="nav">
                     <li class="nav-item">
                        <a class="nav-link" href="#step-1"> <strong>Step 1</strong> </a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link" href="#step-2"> <strong>Step 2</strong> </a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link" href="#step-3"> <strong>Step 3</strong> </a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link" href="#step-4"> <strong>Step 4</strong> </a>
                     </li>
                  </ul>
                  <form method="post" action="{{ route('clients-update') }}" class="row g-3 needs-validation" id="update-form" enctype="multipart/form-data"  novalidate>
                     @csrf
                     @method('PUT')
                     <input type="hidden" name="id" value="{{$client->id}}">
                     <div class="tab-content">
                        <div id="step-1" class="tab-pane" role="tabpanel" aria-labelledby="step-1">
                           <h3>Client/Vendor’sName/Full Name</h3>
                           <div class="col-md-12 mt-2">
                              <label for="validationCustom01" class="form-label">First Name *</label>
                              <input type="text" class="form-control" id="validationCustom01" value="{{ $client->first_name }}" name="first_name" placeholder="First Name" required>
                              <div class="valid-feedback">Looks good!</div>
                           </div>
                           <div class="col-md-12 mt-2">
                              <label for="validationCustom01" class="form-label">Last Name *</label>
                              <input type="text" class="form-control" id="validationCustom01" value="{{ $client->last_name   }}" name="last_name" placeholder="Last Name" required>
                              <div class="valid-feedback">Looks good!</div>
                           </div>
                           <br/>
                           <label for="validationCustom01" class="form-label"><b>Personal Information</b></label>
                           <div class="col-md-12 mt-2">
                              <label class="form-label">Date Of Birth</label>
                              <input class=" form-control datepicker result" type="text"  name="date_of_birth" value="<?php echo date('d F, Y',strtotime($client->date_of_birth) )  ?>"placeholder="Date of Birth..." required>
                              <div class="valid-feedback">Looks good!</div>
                           </div>
                        </div>
                        <div id="step-2" class="tab-pane" role="tabpanel" aria-labelledby="step-2">
                           <h3>Residential/PermanentAddress *</h3>
                           <div class="col-md-12 mt-2">
                              <label for="validationCustom01" class="form-label">Address *</label>
                              <input type="text" class="form-control" id="validationCustom01" value="{{  $client->address1 }}" name="address1" placeholder="House No./Flat No./Society" required>
                              <div class="valid-feedback">Looks good!</div>
                           </div>
                           <div class="col-md-12 mt-2">
                              <label for="validationCustom01" class="form-label">Street Name *</label>
                              <input type="text" class="form-control" id="validationCustom01" value="{{  $client->address2 }}" name="address2" placeholder="Street Name, etc" required>
                              <div class="valid-feedback">Looks good!</div>
                           </div>
                           <div class="col-md-12 mt-2">
                              <label for="validationCustom01" class="form-label">NearBuy </label>
                              <input type="text" class="form-control" id="validationCustom01" value="{{  $client->address3 }}" name="address3" placeholder="NearBuy" >
                              <div class="valid-feedback">Looks good!</div>
                           </div>
                           <div class="col-md-12 mt-2">
                              <label for="validationCustom01" class="form-label">Landmark </label>
                              <input type="text" class="form-control" id="validationCustom01" value="{{  $client->landmark }}" name="landmark" placeholder="Landmark" >
                              <div class="valid-feedback">Looks good!</div>
                           </div>
                           <div class="row">
                              <div class="col-md-6 mt-2">
                                 <label for="validationCustom01" class="form-label">Area/City</label> 
                                 <input type="text" class="form-control" id="validationCustom01" value="{{  $client->area_city }}" name="area_city" placeholder="Area / City" required>
                                 <div class="valid-feedback">Looks good!</div>
                              </div>
                              <div class="col-md-6 mt-2">
                                 <label for="validationCustom01" class="form-label">PinCode</label>
                                 <input type="tel" class="form-control" id="validationCustom01" value="{{  $client->pincode }}" name="pincode" placeholder="Pin Code" required>
                                 <div class="valid-feedback">Looks good!</div>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-md-6 mt-2">
                                 <label for="validationCustom01" class="form-label">State</label>
                                 <input type="text" class="form-control" id="validationCustom01" value="{{  $client->state }}" name="state" placeholder="State" required>
                                 <div class="valid-feedback">Looks good!</div>
                              </div>
                              <div class="col-md-6 mt-2">
                                 <label for="validationCustom01" class="form-label">Country</label>
                                 <input type="tel" class="form-control" id="validationCustom01" value="{{  $client->country }}" name="country" placeholder="Country" required>
                                 <div class="valid-feedback">Looks good!</div>
                              </div>
                           </div>
                        </div>
                        <div id="step-3" class="tab-pane" role="tabpanel" aria-labelledby="step-3">
                           <h3>Contact Information *</h3>
                           <div class="col-md-12 mt-2">
                              <label for="validationCustom01" class="form-label">Mobile No. *</label>
                              <input type="text" class="form-control" id="validationCustom01" value="{{  $client->mobile }}" name="mobile" placeholder="Mobile Number" required>
                              <div class="valid-feedback">Looks good!</div>
                           </div>
                           <div class="col-md-12 mt-2">
                              <label for="validationCustom01" class="form-label">Landline </label>
                              <input type="text" class="form-control" id="validationCustom01" value="{{  $client->landline }}" name="landline" placeholder="Landline" >
                              <div class="valid-feedback">Looks good!</div>
                           </div>
                           <div class="col-md-12 mt-2">
                              <label for="validationCustom01" class="form-label">Email ID *</label>
                              <input type="email" class="form-control" id="validationCustom01" value="{{  $client->email_id }}" name="email_id" placeholder="Email ID" required>
                              <div class="valid-feedback">Looks good!</div>
                           </div>
                           <div class="col-md-12 mt-2">
                              <label for="validationCustom01" class="form-label">Skype ID </label>
                              <input type="text" class="form-control" id="validationCustom01" value="{{  $client->skype_id }}" name="skype_id" placeholder="Skype ID" >
                              <div class="valid-feedback">Looks good!</div>
                           </div>
                        </div>
                        <div id="step-4" class="tab-pane" role="tabpanel" aria-labelledby="step-4">
                           <h3>Tax Information</h3>
                           <div class="col-md-12 mt-2">
                              <label for="validationCustom01" class="form-label">Pancard</label>
                              <input type="text" class="form-control" id="validationCustom01" value="{{  $client->pancard }}" name="pancard" placeholder="Pancard" >
                              <div class="valid-feedback">Looks good!</div>
                           </div>
                           <div class="col-md-12 mt-2">
                              <label for="validationCustom01" class="form-label">GSTIN </label>
                              <input type="text" class="form-control" id="validationCustom01" value="{{  $client->gstin }}" name="gstin" placeholder="GSTIN" >
                              <div class="valid-feedback">Looks good!</div>
                           </div>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection()
@section('loadstyles')
    <link href="{{ asset('assets/plugins/smart-wizard/css/smart_wizard_all.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/datetimepicker/css/classic.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/plugins/smart-wizard/css/smart_wizard_all.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/datetimepicker/css/classic.date.css') }}" rel="stylesheet" />
   <link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.min.css') }}">
   <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <!-- <link href="{{URL::to('/')}}/assets/plugins/fancy-file-uploader/fancy_fileupload.css" rel="stylesheet" />
    <link href="{{URL::to('/')}}/assets/plugins/Drag-And-Drop/dist/imageuploadify.min.css" rel="stylesheet" />
    <link href="{{URL::to('/')}}/assets/plugins/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet" /> -->
    <style>
        .picker--focused .picker__day--selected {
            color: #393737 !important;
        }
        @media (min-width: 320px) and (max-width: 480px) {
                .nav{
                    display: none !important;
                }
        }
    </style>
@endsection()
@section('loadscript')
    <script src="{{URL::to('/')}}/assets/plugins/simplebar/js/simplebar.min.js"></script>
    <script src="{{URL::to('/')}}/assets/plugins/metismenu/js/metisMenu.min.js"></script>
    <script src="{{URL::to('/')}}/assets/plugins/perfect-scrollbar/js/perfect-scrollbar.js"></script>
    <script src="{{ asset('assets/plugins/smart-wizard/js/jquery.smartWizard.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datetimepicker/js/legacy.js') }}"></script>
   <script src="{{ asset('assets/plugins/datetimepicker/js/picker.js') }}"></script>
   <script src="{{ asset('assets/plugins/datetimepicker/js/picker.time.js') }}"></script>
   <script src="{{ asset('assets/plugins/datetimepicker/js/picker.date.js') }}"></script>
   <script src="{{ asset('assets/plugins/bootstrap-material-datetimepicker/js/moment.min.js') }}"></script>
   <script src="{{ asset('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.min.js') }}"></script>
    <!-- <script src="{{URL::to('/')}}/assets/plugins/fancy-file-uploader/jquery.ui.widget.js"></script>
    <script src="{{URL::to('/')}}/assets/plugins/fancy-file-uploader/jquery.fileupload.js"></script>
    <script src="{{URL::to('/')}}/assets/plugins/fancy-file-uploader/jquery.iframe-transport.js"></script>
    <script src="{{URL::to('/')}}/assets/plugins/fancy-file-uploader/jquery.fancy-fileupload.js"></script>
    <script src="{{URL::to('/')}}/assets/plugins/Drag-And-Drop/dist/imageuploadify.min.js"></script> -->
@endsection()
@section('scripts')
    var forms = document.querySelectorAll('.needs-validation')
    // Loop over them and prevent submission
    Array.prototype.slice.call(forms)
    .forEach(function (form) {
    form.addEventListener('submit', function (event) {
        if (!form.checkValidity()) {
        event.preventDefault()
        event.stopPropagation()
        }
        form.classList.add('was-validated')
    }, false)
    })
    /*-- $('#fancy-file-upload').FancyFileUpload({
        params: {
            action: 'fileuploader'
        },
        maxfilesize: 1000000
    });
    $('#image-uploadify').imageuploadify(); */
      $(document).ready(function () {
         // Toolbar extra buttons
         var btnFinish = $('<button></button>').text('Finish').addClass('btn btn-info').on('click', function () {
            $('#update-form').submit();
         });
         var btnCancel = $('<button></button>').text('Cancel').addClass('btn btn-danger').on('click', function () {
            $('#smartwizard').smartWizard("reset");
         });
         // Step show event
         $("#smartwizard").on("showStep", function (e, anchorObject, stepNumber, stepDirection, stepPosition) {
            $("#prev-btn").removeClass('disabled');
            $("#next-btn").removeClass('disabled');
            if (stepPosition === 'first') {
               $("#prev-btn").addClass('disabled');
            } else if (stepPosition === 'last') {
               $("#next-btn").addClass('disabled');
            } else {
               $("#prev-btn").removeClass('disabled');
               $("#next-btn").removeClass('disabled');
            }
         });
         // Smart Wizard
         $('#smartwizard').smartWizard({
            selected: 0,
            theme: 'dots',
            transition: {
               animation: 'slide-horizontal', // Effect on navigation, none/fade/slide-horizontal/slide-vertical/slide-swing
            },
            toolbarSettings: {
               toolbarPosition: 'bottom', // both bottom
               toolbarExtraButtons: [btnFinish, btnCancel]
            }
         });
         // External Button Events
         $("#reset-btn").on("click", function () {
            // Reset wizard
            $('#smartwizard').smartWizard("reset");
            return true;
         });
         $("#prev-btn").on("click", function () {
            // Navigate previous
            $('#smartwizard').smartWizard("prev");
            return true;
         });
         $("#next-btn").on("click", function () {
            // Navigate next
            $('#smartwizard').smartWizard("next");
            return true;
         });
         // Demo Button Events
         $("#got_to_step").on("change", function () {
            // Go to step
            var step_index = $(this).val() - 1;
            $('#smartwizard').smartWizard("goToStep", step_index);
            return true;
         });
         $("#is_justified").on("click", function () {
            // Change Justify
            var options = {
               justified: $(this).prop("checked")
            };
            $('#smartwizard').smartWizard("setOptions", options);
            return true;
         });
         $("#animation").on("change", function () {
            // Change theme
            var options = {
               transition: {
                  animation: $(this).val()
               },
            };
            $('#smartwizard').smartWizard("setOptions", options);
            return true;
         });
         $("#theme_selector").on("change", function () {
            // Change theme
            var options = {
               theme: $(this).val()
            };
            $('#smartwizard').smartWizard("setOptions", options);
            return true;
         });
      });
        $('.datepicker').pickadate({
         selectMonths: true,
           selectYears: true
      }),
      $('.timepicker').pickatime()
        $(function () {
         $('#date-time').bootstrapMaterialDatePicker({
            format: 'YYYY-MM-DD HH:mm'
         });
         $('#date').bootstrapMaterialDatePicker({
            time: false
         });
      });
@endsection
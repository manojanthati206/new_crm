<!DOCTYPE html>
<html>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>neshallWeb</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>
<body>
   <h1>
        Hello {{ session('clientname') }},
    </h1>
    <p>
        Thanks for signing up for our company.
    </p>
    <p>
        This is just a short message to confirm that your project is now active and that you’ll be receiving great insights from us shortly.
    </p>
    <p>
        To get started, please click on the following <a style="color: blue;text-decoration: underline;" href="{{ route('questiona.Q_Mail',['cid'=>session('clientid'), 'sid'=> session('currentservicesid'), 'msid'=>session('mainservice')]) }}" class="btn btn-link">Acquisition Link</a> and complete the form.
    </p>
    <p>
        If you have any comments or questions in the meantime, feel free to contact us by replying to this email. We’d be happy to help you!
    </p>
    <p>
        Regards,<br>
        The neshallWeb team
    </p>
    <p>
        P.s. If you have been sent this email in error, please feel free to contact us.
    </p>
</body>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</body>
</html>
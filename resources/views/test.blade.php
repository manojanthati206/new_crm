
@extends('layouts.blank')

@section('title')
            Dashboard
@endsection()

@section('content')

    @if ($message = Session::get('success'))
        <div style="width: 85%;" class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    @if ($message = Session::get('fail'))
        <div class="alert alert-fail">
            <p>{{ $message }}</p>
        </div>
    @endif
    <div class="row">
        <div class="col-xl-9 mx-auto">
            <hr/>
            <div class="card">
                <div class="card-body">
                    <div class="p-4 border rounded">
                        <form method="post" action="{{ route('upwork-client-replies-store') }}" class="row g-3 needs-validation" enctype="multipart/form-data"  novalidate>
                            @csrf
                            <div class="col-md-12">
                                <label for="validationCustom01" class="form-label">Client Question</label>
                                <input type="text" class="form-control" id="validationCustom01" value="" name="client_question" placeholder="Question" required>
                                <div class="valid-feedback">Looks good!</div>
                            </div>
                            <div id="add-page">
                                <input type="text" class="form-control" id="validationCustom01" value="" name="page" placeholder="Page">
                            </div>
                            <div id="add-section">
                                <input type="text" class="form-control" id="validationCustom01" value="" name="section" placeholder="section">
                            </div>
                            <div class="col-12">
                                <button class="btn btn-light btn-add-page" type="button">Add Page</button>
                                <button class="btn btn-light btn-add-section" type="button">Add Section</button>
                                <button class="btn btn-light" type="submit">Submit form</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection()
@section('loadstyles')
    <!-- <link href="{{URL::to('/')}}/assets/plugins/fancy-file-uploader/fancy_fileupload.css" rel="stylesheet" />
    <link href="{{URL::to('/')}}/assets/plugins/Drag-And-Drop/dist/imageuploadify.min.css" rel="stylesheet" />
    <link href="{{URL::to('/')}}/assets/plugins/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet" /> -->
@endsection()
@section('loadscript')
    <script src="{{URL::to('/')}}/assets/plugins/simplebar/js/simplebar.min.js"></script>
    <script src="{{URL::to('/')}}/assets/plugins/metismenu/js/metisMenu.min.js"></script>
    <script src="{{URL::to('/')}}/assets/plugins/perfect-scrollbar/js/perfect-scrollbar.js"></script>
    <!-- <script src="{{URL::to('/')}}/assets/plugins/fancy-file-uploader/jquery.ui.widget.js"></script>
    <script src="{{URL::to('/')}}/assets/plugins/fancy-file-uploader/jquery.fileupload.js"></script>
    <script src="{{URL::to('/')}}/assets/plugins/fancy-file-uploader/jquery.iframe-transport.js"></script>
    <script src="{{URL::to('/')}}/assets/plugins/fancy-file-uploader/jquery.fancy-fileupload.js"></script>
    <script src="{{URL::to('/')}}/assets/plugins/Drag-And-Drop/dist/imageuploadify.min.js"></script> -->
    <script type="text/javascript">
        $(document).ready(function(){
            $('.btn-add-page').click(function(){
                $("#add-page").append('<input type="text" class="form-control" id="validationCustom01" value="" name="page" placeholder="Page">');
            });
            $('.btn-add-section').click(function(){
                $("#add-section").append('<input type="text" class="form-control" id="validationCustom01" value="" name="page" placeholder="Page">');
            });
        });
    </script>
@endsection()
@section('scripts')

@endsection()
